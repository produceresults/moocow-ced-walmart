<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

$installer = $this;
$installer->startSetup();
$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('walmart/walmarttaxcodes')} (
  `id` int(11) NOT NULL  auto_increment,
  `tax_code` varchar(70) NOT NULL UNIQUE,
  `cat_desc` text,
  `sub_cat_desc` text,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS {$this->getTable('walmart/walmartorder')} (
  `id` int(10) NOT NULL  auto_increment,
  `merchant_order_id` text COMMENT 'Merchant Order Id',
  `deliver_by` datetime DEFAULT NULL COMMENT 'Deliver By',
  `order_place_date` datetime DEFAULT NULL COMMENT 'Order Place Date',
  `magento_order_id` text COMMENT 'Magento Order Id',
  `status` text COMMENT 'status',
  `order_data` text COMMENT 'Order Data',
  `shipment_data` text COMMENT 'Shipping Data',
  `purchase_order_id` text COMMENT 'Reference Order Id',
  `customer_cancelled` smallint(6) DEFAULT NULL COMMENT 'Customer Cancelled',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='walmart_order_detail';


CREATE TABLE IF NOT EXISTS `{$this->getTable('walmart/catlist')}` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`csv_cat_id` text NOT NULL,
`csv_parent_id` text NOT NULL,
`name` text NOT NULL,
`path` varchar(255) NOT NULL UNIQUE,
`level` text NOT NULL,
`walmart_required_attributes` text NOT NULL,
`walmart_attributes` text  NOT NULL,
`magento_cat_id` text NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS {$this->getTable('walmart/walmartattribute')} (
`id` int(10) NOT NULL  auto_increment,
  `walmart_attribute_name` varchar(255) NULL UNIQUE,
  `magento_attribute_code` text NULL,
  `walmart_attribute_doc` text NULL,
  `is_mapped` text NULL,
  `walmart_attribute_enum` text NULL,
  `walmart_attribute_level` text NULL,
  `walmart_attribute_type` text NULL,
  `walmart_attribute_depends_on` text NULL,
  PRIMARY KEY (`id`)
)CHARACTER SET utf8 COLLATE utf8_general_ci;


CREATE TABLE IF NOT EXISTS `{$this->getTable('walmart/walmartfeed')}` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`feed_id` text NOT NULL,
`feed_status` text  NULL,
`feed_source` text  NULL,
`feed_type` text NULL,
`level` text  NULL,
`items_received` text NULL,
`items_succeeded` text  NULL,
`items_failed` text NULL,
`items_processing` text NULL,
`feed_date` text NULL,
`feed_file` text NULL,
`feed_errors` text NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `{$this->getTable('walmart/orderimport')}` (
  `id` int(10) NOT NULL  auto_increment,
  `purchase_order_id` text NOT NULL COMMENT 'Purchase Order Id',
  `reference_number` varchar(255) NOT NULL DEFAULT '0' COMMENT 'Reference_Number',
  `reason` text NOT NULL COMMENT 'Reason',
  `order_data` text NOT NULL COMMENT 'Order Data',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='walmart_order_import_error';

CREATE TABLE IF NOT EXISTS `{$this->getTable('walmart/walmartrefund')}` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `magento_order_id` varchar(255) NOT NULL COMMENT 'magento_order_id',
  `quantity_returned` varchar(255) NOT NULL COMMENT 'quantity_returned',
  `refund_quantity` varchar(255) NOT NULL COMMENT 'refund_quantity',
  `refund_reason` varchar(255) NOT NULL COMMENT 'refund_reason',
  `refund_feedback` varchar(255) NOT NULL COMMENT 'refund_feedback',
  `refund_amount` varchar(255) NOT NULL COMMENT 'refund_amount',
  `refund_tax` varchar(255) NOT NULL COMMENT 'refund_tax',
  `refund_shippingcost` varchar(255) NOT NULL COMMENT 'refund_shippingcost',
  `refund_shippingtax` varchar(255) NOT NULL COMMENT 'refund_shippingtax',
  `refund_purchaseOrderId` varchar(255) NOT NULL COMMENT 'refund_orderid',
  `refund_customerOrderId` varchar(255) NOT NULL COMMENT 'refund_customerOrderId',
  `refund_id` varchar(255) NOT NULL COMMENT 'refund_id',
  `refund_status` varchar(255) NOT NULL COMMENT 'refund_status',
  `saved_data` text NOT NULL COMMENT 'saved_data',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='walmart_refund_table';
");

// insertion category data
$catlistHelper = Mage::helper('walmart/catlist');
$categoryData = $catlistHelper->getCategoryArray();
$installer->getConnection()
    ->insertArray($this->getTable('walmart/catlist'),['csv_cat_id', 'csv_parent_id', 'name', 'path', 'level', 'walmart_required_attributes','walmart_attributes'], $categoryData);

//insertion attribute map data
$attributes=$catlistHelper->getAttributeMapData();
$installer->getConnection()->insertArray($installer->getTable('walmart/walmartattribute'),
    [ 'walmart_attribute_name', 'magento_attribute_code', 'walmart_attribute_doc',
        'is_mapped', 'walmart_attribute_enum', 'walmart_attribute_level',
        'walmart_attribute_type', 'walmart_attribute_depends_on',
    ],
    $attributes
);

//insertion taxcodes data
$taxcodes=$catlistHelper->getTaxcodes();
$installer->getConnection()->insertArray($installer->getTable('walmart/walmarttaxcodes'),
    [ 'id','tax_code', 'cat_desc', 'sub_cat_desc',
    ],
    $taxcodes
);


$eavSetup = new Mage_Eav_Model_Entity_Setup('core_setup');
$sNewSetName = 'Walmart';
$iCatalogProductEntityTypeId = (int) $eavSetup->getEntityTypeId('catalog_product');

$oAttributeset = Mage::getModel('eav/entity_attribute_set')
    ->setEntityTypeId($iCatalogProductEntityTypeId)
    ->setAttributeSetName($sNewSetName);

if ($oAttributeset->validate()) {
    $eavSetup->addAttributeGroup('catalog_product', 'Default', 'Walmart', 2000);
}
// Add existing attribute to group
$attributeSetId = $eavSetup->getDefaultAttributeSetId($iCatalogProductEntityTypeId);
$attributeGroupId = $eavSetup->getAttributeGroupId($iCatalogProductEntityTypeId, $attributeSetId, $sNewSetName);

$attributeId = $eavSetup->getAttributeId($iCatalogProductEntityTypeId, 'color');
$eavSetup->addAttributeToGroup($iCatalogProductEntityTypeId, $attributeSetId, $attributeGroupId, $attributeId, null);




$eavSetup->addAttribute('catalog_product', 'walmart_product_validation', [
        'group'            => 'Walmart',
        'note'             => 'Walmart Product Validation',
        'input'            => 'text',
        'type'             => 'text',
        'label'            => 'Walmart Product Validation',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_shelf_description', [
        'group'            => 'Walmart',
        'note'             => '1 to 1000 characters, 
                Abbreviated list of key item features in no more than three bullet points.
                 This is viewable in search, category, and shelf pages. Format bullet points with HTML.',
        'frontend_class'   => 'validate-length maximum-length-1000',
        'input'            => 'text',
        'type'             => 'varchar',
        'label'            => 'Walmart Self Description',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 1,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_productid_type', [
        'group'            => 'Walmart',
        'note'             => 'Type of unique identifier used in the "Product ID" field.
                 Example: UPC; GTIN; ISBN; ISSN; EAN',
        'input'            => 'select',
        'type'             => 'varchar',
        'label'            => 'Walmart Product Id Type',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 2,
        'user_defined'     => 1,
        'source'           => 'Ced_Walmart_Model_Source_Pricetype',
        'searchable'       => 1,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_productid', [
        'group'            => 'Walmart',
        'note'             => ' 1 to 14 characters, Alphanumeric ID that uniquely identifies the product.',
        'frontend_class'   => 'validate-length maximum-length-14',
        'input'            => 'text',
        'type'             => 'varchar',
        'label'            => 'Walmart Product Id',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 3,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_brand', [
        'group'            => 'Walmart',
        'note'             => '1 to 4000 characters',
        'frontend_class'   => 'validate-length maximum-length-4000',
        'input'            => 'text',
        'type'             => 'text',
        'label'            => 'Walmart Brand',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 4,
        'user_defined'     => 1,
        'searchable'       => 1,
        'filterable'       => 0,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_product_taxcode', [
        'group'            => 'Walmart',
        'note'             => '1 -  10 characters, Code used to identify tax properties of the product',
        'frontend_class'   => 'validate-length maximum-length-10',
        'input'            => 'text',
        'type'             => 'varchar',
        'label'            => 'Walmart Product Tax Code',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'searchable'       => 1,
        'filterable'       => 0,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_product_status', [
        'group'            => 'Walmart',
        'note'             => 'Walmart Product Status',
        'input'            => 'select',
        'type'             => 'varchar',
        'label'            => 'Walmart Product Status',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'source'           => 'Ced_Walmart_Model_Source_Productstatus',
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);
$eavSetup->addAttribute('catalog_product', 'walmart_status', [
        'group'            => 'Walmart',
        'note'             => 'Walmart Product status',
        'input'            => 'text',
        'type'             => 'text',
        'label'            => 'Walmart Product status',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_clothing_size', [
        'group'            => 'Walmart',
        'note'             => 'Cloth Size',
        'input'            => 'text',
        'type'             => 'text',
        'label'            => 'Cloth Size',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_cellphone_serprovider', [
        'group'            => 'Walmart',
        'note'             => 'Cell Phone Service Provider',
        'input'            => 'text',
        'type'             => 'text',
        'label'            => 'Cell Phone Service Provider',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_esrb_rating', [
        'group'            => 'Walmart',
        'note'             => 'ESRB Rating',
        'input'            => 'select',
        'type'             => 'varchar',
        'label'            => 'ESRB Rating',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'source'           => 'Ced_Walmart_Model_Source_EsrbRating',
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_alcohol_by_volume', [
        'group'            => 'Walmart',
        'note'             => 'Alcohol By Volume',
        'input'            => 'text',
        'type'             => 'text',
        'label'            => 'Alcohol By Volume',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_food_form', [
        'group'            => 'Walmart',
        'note'             => 'Food Form',
        'input'            => 'text',
        'type'             => 'text',
        'label'            => 'Food Form',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_jewelry_style', [
        'group'            => 'Walmart',
        'note'             => 'Jewelry Style',
        'input'            => 'select',
        'type'             => 'varchar',
        'label'            => 'Jewelry Style',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'source'           => 'Ced_Walmart_Model_Source_JewelryStyle',
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_mpaa_rating', [
        'group'            => 'Walmart',
        'note'             => 'MPAA Rating',
        'input'            => 'select',
        'type'             => 'varchar',
        'label'            => 'MPAA Rating',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 5,
        'user_defined'     => 1,
        'source'           => 'Ced_Walmart_Model_Source_MpaaRating',
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'walmart_product_validation', [
        'group'            => 'Walmart',
        'note'             => 'Walmart Product Validation',
        'input'            => 'text',
        'type'             => 'text',
        'label'            => 'Walmart Product Validation',
        'backend'          => '',
        'visible'          => 1,
        'required'         => 0,
        'sort_order'       => 7,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           =>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);

$eavSetup->addAttribute('catalog_product', 'colorvalue', [
        'group'            => 'General',
        'note'             => 'ColorValue',
        'input'            => 'text',
        'type'             => 'text',
        'label'            => 'ColorValue',
        'backend'          => '',
        'visible'          => 0,
        'required'         => 0,
        'sort_order'       => 7,
        'user_defined'     => 1,
        'comparable'       => 0,
        'visible_on_front' => 0,
        'global'           =>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ]
);


$installer->endSetup();
