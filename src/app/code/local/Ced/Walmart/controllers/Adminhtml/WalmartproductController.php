<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ced_Walmart_Adminhtml_WalmartproductController extends Mage_Adminhtml_Controller_Action{

    /**
     * @todo change for walmart
     */
    public function clearallAction(){

        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = "DELETE FROM ". $resource->getTableName('walmart/fileinfo') ." WHERE status = 'Processed with errors' OR status = 'Processed successfully'";

        $writeConnection->query($query);

        $query = 'TRUNCATE TABLE '.$resource->getTableName('walmart/errorfile').'';
        $writeConnection->query($query);

        Mage::getSingleton('adminhtml/session')->addSuccess('Rejected batch File Log cleared.');

        $this->_redirect('*/*/rejected');
    }

    /**
     * @todo change for walmart
     */
    public function resubmitAction(){

        $jfile_id = $this->getRequest()->getPost('walmartinfofile_id');
        $error_fileid = $this->getRequest()->getPost('id');

        $loadfile = Mage::getModel('walmart/fileinfo')->load($jfile_id);
        $products = $loadfile->magento_batch_info;

        $model_error = Mage::getModel('walmart/errorfile')->load($error_fileid);
        if($model_error->getId()){
            $model_error->setStatus('Resubmit Requested');
            $model_error->save();
            Mage::getSingleton('adminhtml/session')->addSuccess('Inventory File submission successfully done.');
        }
        $this->_redirect("*/*/massimport",array("product"=>$products));
    }

    public function newAction(){
        Mage::getModel('walmart/observer')->updateProduct();
        $this->_redirect('*/*/rejected');
    }

    public function massimportAction(){
        $productids = $this->getRequest()->getPost('product');
        $id[] = $this->getRequest()->getParam('id');
        $productids = empty($productids)?$id:$productids;

        $dataHelper = Mage::helper('walmart');

        if($dataHelper->createProductOnWalmart($productids))
        {
            Mage::getSingleton('adminhtml/session')->addSuccess(count($productids) . 'Products Uploaded Successfully');
            $this->_redirect('adminhtml/adminhtml_walmartrequest/uploadproduct');

        } else {

            Mage::getSingleton('adminhtml/session')->addError('Products Upload Failed , Please validate and try again.');
            $this->_redirect('adminhtml/adminhtml_walmartrequest/uploadproduct');
        }
    }

    /*
    * Action created for showing Product Tax Codes
    */
    public function taxcodesAction(){
        $this->loadLayout();
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('walmart/adminhtml_taxcodes_grid')->toHtml()
        );
    }

    /**
     * @todo change for walmart
     */
    public function massDeleteAction(){
        $success_count=0;
        if(sizeof($this->getRequest()->getParam('error_ids'))>0){
            $errorIds = $this->getRequest()->getParam('error_ids');
            foreach($errorIds as $errorid){
                $error = Mage::getModel('walmart/errorfile')->load($errorid);
                $error->delete();
                $success_count++;
            }
        }
        if($success_count>0){
            Mage::getSingleton('adminhtml/session')->addSuccess("$success_count record(s) successfully deleted.");
        }else{
            Mage::getSingleton('adminhtml/session')->addNotice("No record(s) deleted.");
        }
        $this->_redirect('*/*/rejected');
    }

    public function massValidateAction(){
        $success_count=0;
        if(sizeof($this->getRequest()->getParam('error_ids'))>0){
            $errorIds = $this->getRequest()->getParam('error_ids');
            foreach($errorIds as $errorid){
                $error = Mage::getModel('walmart/errorfile')->load($errorid);
                $error->delete();
                $success_count++;
            }
        }
        if($success_count>0){
            Mage::getSingleton('adminhtml/session')->addSuccess("$success_count record(s) successfully deleted.");
        }else{
            Mage::getSingleton('adminhtml/session')->addNotice("No record(s) deleted.");
        }
        $this->_redirect('*/*/rejected');
    }


}
