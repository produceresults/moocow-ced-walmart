<?php
/**
  * CedCommerce
  *
  * NOTICE OF LICENSE
  *
  * This source file is subject to the Academic Free License (AFL 3.0)
  * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
  * It is also available through the world-wide-web at this URL:
  * http://opensource.org/licenses/afl-3.0.php
  *
  * @category    Ced
  * @package     Ced_Walmart
  * @author      CedCommerce Core Team <connect@cedcommerce.com>
  * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
  * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
  */
  
class Ced_Walmart_Adminhtml_WalmartfetchattrvalController extends Mage_Adminhtml_Controller_Action{

public function fetchAction()
	{
			 $name = $this->getRequest()->getPost('mag_att_code');
    $attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
    $attributeId = $attributeInfo->getAttributeId();
    $attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    $attributeOptions = $attribute ->getSource()->getAllOptions(false); 
    //print_r($attributeOptions);die();


			$mwalmartattr_id = $this->getRequest()->getPost('walmart_id');
			//test code start	

			 $units_or_options = array();
            $csv = new Varien_File_Csv();
            $file = Mage::getBaseDir("var") . DS . "walmartcsv" . DS . "Walmart_Taxonomy_attribute.csv";

            if (!file_exists($file)) { ?>

            	<label><strong>Note:</strong></label>
				<p>Walmart Extension Csv missing please check "Walmart_Taxonomy_attribute.csv" exist at "var/walmartcsv" location.</p>
               
               <?php return;
            }

            $taxonomy = $csv->getData($file);
            unset($taxonomy[0]);

            $save_attr_id = false;
            $save_attr_unitType = false;

            foreach ($taxonomy as $txt) {
                if (number_format($txt[0], 0, '', '') == $mwalmartattr_id) {

                    $save_attr_id = number_format($txt[0], 0, '', '');
                    $save_attr_unitType = $txt[6];
                    break;
                }
            }
           
            if ($save_attr_id == false) { ?>
                <label><strong>Note:</strong></label>
				<p>Walmart Atrribute id: <?php echo $mwalmartattr_id ?> which you trying to map is not available  in walmart.com. </p>
               <?php return;
            }

			//test code end 
			$csv = new Varien_File_Csv();
            $file = Mage::getBaseDir("var") . DS . "walmartcsv" . DS . "Walmart_Taxonomy_attribute_value.csv";


            if (!file_exists($file)) {?>
            		<label><strong>Note:</strong></label>
				<p>Walmart Extension Csv missing please check "Walmart_Taxonomy_attribute_value.csv" exist at "var/walmartcsv" location.</p>
					<?php  return;
            }
            $taxonomy = $csv->getData($file);


            unset($taxonomy[0]);
            try {
                if ($save_attr_unitType == 2) {
                    foreach ($taxonomy as $txt) {
                        $numberfomat_id = number_format($txt[0], 0, '', '');

                        if ($mwalmartattr_id == $numberfomat_id) {

                            $units_or_options[] = $txt[2];
                        }
                    }
                } else if ($save_attr_unitType == 0) {

                    foreach ($taxonomy as $txt) {
                        if ($mwalmartattr_id == number_format($txt[1], 0, '', '')) {
                            $units_or_options[] = $txt[2];
                        }
                    }

                }
            } catch (Exception $e) { ?>

            <label><strong>Note:</strong></label>
				<p> <?php $e->getMessage() ?> </p>
             <?php  return;
            }
            //test s

	   $options_array = $units_or_options;?>
	  <div class="hor-scroll">
		
			<?php if($save_attr_unitType == 2){ ?>
			
				<label><strong>Note:</strong></label>
				<p>Walmart Atrribute id: <?php echo $mwalmartattr_id ?> which you trying to map is a <b>UNIT</b> type attribute in walmart.com. You need to Add or Create new options based on these values in your Drop down options </p>
				<p>Options: <b><?php echo $row['unit'] ?></b></p>
				<label>Example: <strong>"Your value"{space}"UNIT"</strong></label>
				<p>We have taken <b>10</b> as Value for example.</p> 
					<select>
						<?php foreach ($options_array as $data) { ?>
							<option value="<?php echo '10 '.$data; ?>"><?php echo '10 '.$data; ?></option>
						<?php } ?> 
					</select> 
				
		  <?php 
			  } 
		else if($save_attr_unitType == 0) {  ?>
			<?php if(count($options_array)>0){ ?>
				<label><strong>Note:</strong></label>
				<p>Walmart Atrribute id: <?php echo $mwalmartattr_id ?> which you trying to map is a <b>Dropdown</b> type attribute  & the options of this attribute is fixed on walmart.com You need to Add or Create new options under Manage Label / Options tab based on these values </p>
				<p>Options: <b><?php foreach ($options_array as $data) { ?>
							<?php echo $data; ?>,
						<?php } ?> </b></p>
				<label>Magento Attribute Value
				<select id ="mag_attr_vals">
						<option value="">Please select Options</option>
						<?php foreach ($attributeOptions as $data) { 
							if($data['label']!='Admin' && $data['label']!='Main Website')
							{?>
									<option value="<?php echo $data['label']; ?>"><?php echo $data['label']; ?></option>
							<?php }
							 } ?> 
					</select> Mapped with this Walmart Attribute Value
					<select id ="walmart_attr_vals">
						<option value="">Please select Options</option>
						<?php foreach ($options_array as $data) { ?>
							<option value="<?php echo $data; ?>"><?php echo $data; ?></option>
						<?php } ?> 
					</select> 
					
					<button style="" onclick ="saveMapping()" id ="save_map" class="scalable " type="button" title="Save Mapping"><span><span><span>Save Mapping</span></span></span></button>
				
			<?php }}

			else if($save_attr_unitType == 1) { ?>
			
				<label><strong>Note:</strong></label>
				<p>Walmart Atrribute id: <?php echo $mwalmartattr_id ?> which you trying to map is a <b>Free Text </b> type attribute  & the options of this attribute you can use anything which you want.</p>
			<?php } 

			else { ?>
			<label><strong>Note:</strong></label>
				<p>Walmart Atrribute id: <?php echo $mwalmartattr_id ?> which you trying to map is not available  in walmart.com. </p>
				<?php }
			
			?>
			
		
	  </div>
	  <?php 
            //test e
	}
	public function mapAction()
	{
		$arr = array();
		$updated_data = '';
		$updated_data1 = '';
		$mwalmartattr_id = trim($this->getRequest()->getPost('walmart_id'));
		
		$walmartattrval = trim($this->getRequest()->getPost('walmartattrval'));
		$magattrval = trim($this->getRequest()->getPost('magattrval'));
		$walmartattribute = Mage::getModel('walmart/walmartattribute');
        $collection = $walmartattribute->getCollection()->addFieldToFilter('walmart_attr_id', $mwalmartattr_id)->getData();

        $updated_data = json_decode($collection[0]['walmart_attr_val'],true);
        $arr = $updated_data;
        if($walmartattrval!='' && $magattrval!='' && $magattrval!='-- Please Select --')
        {
        	$arr[$magattrval]=$walmartattrval;
        	
   		} 
   		
       
        $walmartattribute = Mage::getModel('walmart/walmartattribute')->load($collection[0][id]);
        $walmartattribute->setData('walmart_attr_val',json_encode($arr));
       	$walmartattribute->save();
       	?>
       	<table border="2px">
	  <th>Magento Attribute Value</th>
	  <th>Walmart Attribute Value</th>
	 <th>Action</th>
	  <?php 
	  $walmartattribute = Mage::getModel('walmart/walmartattribute');
	$collection = $walmartattribute->getCollection()->addFieldToFilter('walmart_attr_id', $mwalmartattr_id)->getData();

        $updated_data = json_decode($collection[0]['walmart_attr_val'],true);

	  foreach($updated_data as $keyy=>$vall)
	  { ?>
	   <tr>
	  <td><?php echo $keyy;?> </td>
	  <td><?php echo $vall;?> </td>
	  <td><button onclick ="deleteMapping('<?php echo $keyy ?>')" class="scalable" id ="delete_attr_val" type="button" title="Delete"><span><span><span>Delete</span></span></span></button></td>
	  </tr>
	  <?php	}?>
	  
	  </table>
	<?php }
	public function deleteAction()
	{
		$arr = array();
		$mwalmartattr_id = $this->getRequest()->getPost('walmart_id');
		$del_val = $this->getRequest()->getPost('walmart_val');

		$walmartattribute = Mage::getModel('walmart/walmartattribute');
        $collection = $walmartattribute->getCollection()->addFieldToFilter('walmart_attr_id', $mwalmartattr_id)->getData();

        $updated_data = json_decode($collection[0]['walmart_attr_val'],true);
        foreach ($updated_data as $key11 => $value11) {
        	if($key11 == $del_val)
        	{
        		unset($updated_data[$key11]);
        	}
        }
        $arr = $updated_data;
       $walmartattribute = Mage::getModel('walmart/walmartattribute')->load($collection[0][id]);
        $walmartattribute->setData('walmart_attr_val',json_encode($arr));
       	$walmartattribute->save();
       	?>
       	<table border="2px">
	  <th>Magento Attribute Value</th>
	  <th>Walmart Attribute Value</th>
	 	<th>Action</th>
	  <?php 
	  $walmartattribute = Mage::getModel('walmart/walmartattribute');
	$collection = $walmartattribute->getCollection()->addFieldToFilter('walmart_attr_id', $mwalmartattr_id)->getData();

        $updated_data = json_decode($collection[0]['walmart_attr_val'],true);

	  foreach($updated_data as $keyy=>$vall)
	  { ?>
	   <tr>
	  <td><?php echo $keyy;?> </td>
	  <td><?php echo $vall;?> </td>
	  <td><button onclick ="deleteMapping('<?php echo $keyy ?>')" class="scalable" id ="delete_attr_val" type="button" title="Delete"><span><span><span>Delete</span></span></span></button></td>
	  </tr>
	  <?php	}?>
	  
	  </table>
	  <?php
	}
	public function noticeAction()
	{
		$globalnotice ='read'; 
		Mage::getSingleton('core/session')->setGlobalnotice($globalnotice);
	}
}
?>	

