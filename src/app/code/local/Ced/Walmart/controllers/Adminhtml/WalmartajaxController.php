<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ced_Walmart_Adminhtml_WalmartajaxController extends Mage_Adminhtml_Controller_Action
{
    public function massValidateAction()
    {
        $validatedProductIds = '';
        if(sizeof($this->getRequest()->getParam('product'))>0){
            $productIds = $this->getRequest()->getParam('product');
            $productIds['callToGetItem'] = '0';
            $datahelper = Mage::helper('walmart');
            $validatedProductIds = $datahelper->validateProducts($productIds);
        }
        $countProducts = count($validatedProductIds);
        if ($countProducts > 0) {
            Mage::getSingleton('adminhtml/session')->addSuccess($countProducts." record(s) successfully validated.");
        } else {
            Mage::getSingleton('adminhtml/session')->addError("No Products(s) validated. Please Check the Validate Tab for more details");
        }
        $this->_redirect('*/adminhtml_walmartrequest/uploadproduct');
    }

    public function masspriceupdateAction()
    {
        $dataHelper = Mage::Helper('walmart/data');
        $productids = $this->getRequest()->getParam('product');

        if (count($productids) == 0) {
            Mage::getSingleton('adminhtml/session')->addError('No Product selected for Price Update.');
            $this->_redirect('*/adminhtml_walmartrequest/uploadproduct');
        }
        $countSuccess = 0;
        foreach($productids as $id) {
            $productArray[]['id'] = $id;
            $countSuccess++;
        }

        if($countSuccess>0 && $dataHelper->updatePriceOnWalmart($productArray))
        {
            Mage::getSingleton('adminhtml/session')->addSuccess($countSuccess.' item(s) Price Synced Successfully');
        } else {
            Mage::getSingleton('adminhtml/session')->addError('Item(s) Price Sync Failed');
        }
        $this->_redirect('*/adminhtml_walmartrequest/uploadproduct');
    }

    public function massinventoryupdateAction()
    {
        $dataHelper = Mage::Helper('walmart/data');
        $productids = $this->getRequest()->getParam('product');

        if (count($productids) == 0) {
            Mage::getSingleton('adminhtml/session')->addError('No Product selected for Inventory Update.');
            $this->_redirect('*/adminhtml_walmartrequest/uploadproduct');
        }
        $countSuccess = 0;
        foreach($productids as $id) {
            $productArray[]['id'] = $id;
            $countSuccess++;
        }

        if($countSuccess>0 && $dataHelper->updateInventoryOnWalmart($productArray))
        {
            Mage::getSingleton('adminhtml/session')->addSuccess($countSuccess.' item(s) Inventory Synced Successfully');
        } else {
            Mage::getSingleton('adminhtml/session')->addError('Item(s) Inventory Sync Failed');
        }
        $this->_redirect('*/adminhtml_walmartrequest/uploadproduct');

    }

    public function massretireAction()
    {
        $productids = $this->getRequest()->getParam('product');
        $counter = 0;
        foreach ($productids as  $value) {
            $sku = Mage::getModel('catalog/product')->load($value)->getSku();
            $dataHelper = Mage::helper('walmart/data');
            $checkSku = $dataHelper->getItem($sku);

            if ($checkSku) {
                $requestSent =  $dataHelper->deleteRequest('v2/items/'.$sku);
                $response = json_decode($requestSent, true);
                $response = empty($response) ? false : $response;
                if (isset($response['message'])) {
                    $counter++;

                } elseif (isset($response['error'][0]['description'])) {
                    Mage::log('Delete Error for Sku: '.$sku.' with error message'.
                        $response['error'][0]['description'], null, 'WalmartProductDel.log');

                    $this->_redirect('walmart/products/index');
                } else {
                    Mage::log('ProductDelete MassActon: ' . $requestSent,null,'WalmartProductDel.log');

                }
            }
        }
        if ($counter > 0) {
            Mage::getSingleton('adminhtml/session')
                ->addSuccess('Retire Request for'.$counter.'has been sent to Walmart');
        } else {
            Mage::getSingleton('adminhtml/session')->addError('Product retire failed ');
        }
        $this->_redirect('*/adminhtml_walmartrequest/uploadproduct');
    }

    public function errordetailsAction(){
        if($this->getRequest()->getParam('id')){
            $product_id=$this->getRequest()->getParam('id');
            $this->getResponse()->setBody(
                $this->getLayout()
                    ->createBlock('core/template')->setTemplate("ced/walmart/walmarterror.phtml")
                    ->setData('id',$product_id)
                    ->toHtml()
            );

        }
    }

    public function massimportAction()
    {
        $data = $this->getRequest()->getParam('product');
        if ($data) {
            Mage::Helper('walmart/walmart')->createuploadDir();

            $productids = (array_chunk($data, $this->_bulk_upload_batch));
            Mage::getSingleton('adminhtml/session')->setProductChunks($productids);
            $this->loadLayout();
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError('No Product Selected.');
            $this->_redirect('*/adminhtml_walmartrequest/uploadproduct');
        }
    }

    public function syncproductstatusAction()
    {
        for($i=0 ;$i<10000;$i++)
        {
            $url ='v2/items/?limit=20&offset='.$i;

            $walmartProduct = json_decode(Mage::helper('walmart')->getRequest($url),true);
            if(isset($walmartProduct['error']))
            {
                break;
            }
            else
            {
                foreach ($walmartProduct['MPItemView'] as $product)
                {
                    $temp = $productModel = Mage::getModel('catalog/product');
                    if($id = $productModel->getIdBySku($product['sku']))
                    {
                        $temp->load($id)->setWalmartProductStatus($product['publishedStatus'])->save();
                    }
                }
            }
        }
        $this->_redirect('*/adminhtml_walmartrequest/uploadproduct');
    }
}


