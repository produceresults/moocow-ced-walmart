<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Cedcommerce.com license and  can be accessed
 * through the world-wide-web at this URL:
 * http://www.cedcommerce.com/license-agreement.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to the file if you wish to upgrade this extension to newer
 * version in the future.
 */
class Ced_Walmart_Adminhtml_WalmartorderController extends Mage_Adminhtml_Controller_Action
{
	public function clearallAction(){
	
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		$query = "TRUNCATE TABLE ". $resource->getTableName('walmart/orderimport') ."";
		$writeConnection->query($query);
		
		Mage::getSingleton('adminhtml/session')->addSuccess('Failed Walmart.com Order Log cleared.');
		
		$this->_redirect('*/*/failedorders');
	} 

    public function returnAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('walmart/return');
        $this->renderLayout();
    }

    public function newAction()
    {
        Mage::getModel('walmart/observer')->walmartreturn();
        $this->_redirect('adminhtml/adminhtml_walmartorder/return');
    }

    public function fetchAction()
    {     
        $data = Mage::Helper('walmart/order')->fetchLatestWalmartOrders();
        //Mage::helper('walmart/order')->fetchLatestWalmartOrders();
        $this->_redirect('adminhtml/adminhtml_walmartorder/walmartorder');
    }

    public function directedcancelAction()
    {
        Mage::getModel('walmart/observer')->directCancel();
        $this->_redirect('adminhtml/adminhtml_walmartorder/walmartorder');
    }

    public function saveAction()
    {
        $helper = Mage::helper('walmart');
        $details_saved_after = "";
        $order_id = "";
        $id = "";
        $status = "";
        $returnid = "";
        $agreeto_return = false;
        if ($this->getRequest()->getParam('id') && $this->getRequest()->getParam('returnid')) {
            $returnid = $this->getRequest()->getParam('returnid');
            $id = $this->getRequest()->getParam('id');
            $id = trim($id);
            $data_ship = array();
            $items = array();
            $details_saved_after = $this->getRequest()->getParams();
            $details_saved_after = $helper->prepareDataAfterSubmitReturn($details_saved_after, $id);
            $order_id = $this->getRequest()->getParam('merchant_order_id');
            $order_id = trim($order_id);
            $magento_order_id = 0;
            $magento_order_id = $helper->getMagentoIncrementOrderId($order_id);

            if($magento_order_id!=NULL){

                    if (is_numeric($magento_order_id) &&  $magento_order_id == 0) {
                         Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                         $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                        return; 
                    }

                    if(is_numeric($magento_order_id)==false  &&  $magento_order_id == '') {
                         Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                        $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                        return; 
                    }
            }else{
                 Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                return;

            }   
            /*
            if ($magento_order_id == 0) {
                Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                return;
            } */

            $order = "";

            $order = Mage::getModel('sales/order')->loadByIncrementId($magento_order_id);
            if (!$order->getId()) {
                Mage::getSingleton('adminhtml/session')->addError("Order not Exists for this return.");
                $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                return;

            }
            $refund_flag = false;
            $refund_flag = $helper->checkOrderInRefund($order_id);
            if ($refund_flag) {
                Mage::getSingleton('adminhtml/session')->addError("Order Refund is already exists.Can't generate return.");
                $this->_redirect('adminhtml/adminhtml_walmartorder/return');
                return;
            }
            $data_ship['merchant_order_id'] = $order_id;
            $status = $this->getRequest()->getParam('agreeto_return');
            if ($status == 0) {
                $agreeto_return = false;
            } else {
                $agreeto_return = true;
            }
            $data_ship['agree_to_return_charge'] = $agreeto_return;
            $sku_detail = $this->getRequest()->getParam('sku_details');
            if (count($sku_detail) > 0) {
                $i = 0;
                foreach ($sku_detail as $key => $detail) {
                    if ($detail['want_to_return'] == '0') {
                        continue;
                    }
                    if ($detail['changes_made'] == '1') {
                        continue;
                    }
                    if ($detail['return_quantity'] == "") {
                        Mage::getSingleton('adminhtml/session')->addError("Please enter Qty Returned.");
                        $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                        return;
                    }
                    if ($detail['refund_quantity'] == "") {
                        Mage::getSingleton('adminhtml/session')->addError("Please enter Qty Refunded.");
                        $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                        return;
                    }
                    $detail['return_quantity'] = (int)trim($detail['return_quantity']);
                    $detail['refund_quantity'] = (int)trim($detail['refund_quantity']);
                    if (is_numeric($detail['refund_quantity']) && $detail['refund_quantity'] >= 0 && $detail['refund_quantity'] <= $detail['return_quantity']) {

                    } else {
                        Mage::getSingleton('adminhtml/session')->addError("Please enter correct value to Qty Refunded.");
                        $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                        return;
                    }

                    $check = array();
                    $check = $helper->getRefundedQtyInfo($order, $detail['merchant_sku']);
                    if ($check['error'] == '1') {
                        $error_msg = "";
                        $error_msg = "Error for Order Item with sku : " . $detail['merchant_sku'] . " ";
                        $error_msg = $error_msg . $check['error_msg'];
                        Mage::getSingleton('adminhtml/session')->addError($error_msg);
                        $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                        return;
                    }
                    $qty_already_refunded = 0;
                    $available_to_refund_qty = 0;
                    $qty_ordered = 0;
                    $qty_already_refunded = $check['qty_already_refunded'];
                    $available_to_refund_qty = $check['available_to_refund_qty'];
                    $qty_ordered = $check['qty_ordered'];
                    if ($detail['refund_quantity'] > $available_to_refund_qty) {
                        Mage::getSingleton('adminhtml/session')->addError("Error to generate return for sku : " . $detail['merchant_sku'] . " -> Qty Refunded is greater than Qty Available for Refund.");
                        $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                        return;
                    }
                    if ($detail['refund_quantity'] < 0) {
                        continue;
                    }
                    $arr = array();
                    $arr['order_item_id'] = $detail['order_item_id'];
                    $arr['total_quantity_returned'] = (int)$detail['return_quantity'];
                    $arr['order_return_refund_qty'] = (int)$detail['refund_quantity'];
                    $arr['return_refund_feedback'] = "";
                    if ($detail['return_refundfeedback'] != "") {
                        $arr['return_refund_feedback'] = $detail['return_refundfeedback'];
                    }
                    $return_principal = "";
                    $return_shipping_tax = "";
                    $return_shipping_cost = "";
                    $return_tax = "";
                    $return_principal = (float)trim($detail['return_principal']);
                    $return_shipping_tax = (float)trim($detail['return_shipping_tax']);
                    $return_shipping_cost = (float)trim($detail['return_shipping_cost']);
                    $return_tax = (float)trim($detail['return_tax']);
                    if ($return_principal === "" || $return_principal < 0 || $return_shipping_tax === "" || $return_shipping_tax < 0 || $return_tax === "" || $return_tax < 0 || $return_shipping_cost === "" || $return_shipping_cost < 0) {
                        Mage::getSingleton('adminhtml/session')->addError("Please enter correct values in Amount,Shipping cost,Shipping tax or Tax.");
                        $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                        return;
                    }
                    $arr['refund_amount'] = array('principal' => $return_principal, 'tax' => $return_tax, 'shipping_tax' => $return_shipping_tax, 'shipping_cost' => $return_shipping_tax);
                    $data_ship['items'][] = $arr;

                    $i++;
                }
                if ($i == 0) {
                    Mage::getSingleton('adminhtml/session')->addError("No item's 'Want to Send' is selected Yes to send its data to Walmart.com.");
                    $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                    return;
                }

                $data = Mage::helper('walmart')->CPutRequest('/returns/' . $returnid . '/complete', json_encode($data_ship));
                $responsedata = json_decode($data);

                if ($responsedata->errors && count($responsedata->errors) > 0) {
                    $error_data = $responsedata->errors;
                    $str = "";
                    foreach ($error_data as $val) {
                        if ($str == "") {
                            $str = $val;
                        } else {
                            $str = $str . "<br/>" . $val;
                        }
                    }
                    Mage::getSingleton('adminhtml/session')->addError($str);
                    $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                    return;
                }
                if (empty($responsedata) || $responsedata == "") {
                    $model = "";
                    $details_saved_after = $helper->saveChangesMadeValue($details_saved_after);
                    $details_saved_after = serialize($details_saved_after);
                    $model = Mage::getModel('walmart/walmartreturn')->load($id)->setData('details_saved_after', $details_saved_after)->save();
                    //$checkstatus=false;
                    //$checkstatus=Mage::helper('walmart')->getStatusOfReturn($id);
                    $model = "";
                    $model = Mage::getModel('walmart/walmartreturn')->load($id)->setData('status', 'inprogress')->save();
                    $res = "/returns/state/" . $returnid;
                    $returndetails = "";
                    $returndetails = Mage::helper('walmart')->CGetRequest(rawurlencode($res));
                    
                    $return = "";
                    if ($returndetails) {
                        $return = json_decode($returndetails);
                        $serialized_details = "";
                        $serialized_details = serialize($return);
                        $return_status = '';
                        $return_status = $return->return_status;
                        if ($return->return_status == "completed by merchant") {
                            $model = "";
                            $model = Mage::getModel('walmart/walmartreturn')->load($id)->setData('status', 'completed')->save();
                            $flag = false;
                            $flag = Mage::helper('walmart')->generateCreditMemoForReturn($id);
                        }
                    }
                    Mage::getSingleton('adminhtml/session')->addSuccess('Your return has been posted to walmart successfully.');
                    if ($agreeto_return) {
                        //$flag=false;
                        //$flag=Mage::helper('walmart')->generateCreditMemo($id);
                    }
                    $this->_redirect('adminhtml/adminhtml_walmartorder/return');
                    return;
                } else {
                    Mage::getSingleton('adminhtml/session')->addSuccess('Return data not submitted.Please try again');
                    $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                    return;
                }

            } else {
                Mage::getSingleton('adminhtml/session')->addError("Return Data Missing.Please try again.");
                $this->_redirect('adminhtml/adminhtml_walmartorder/edit', array('id' => $id));
                return;
            }


        } else {
            Mage::getSingleton('adminhtml/session')->addError("Return Id not found.");
            $this->_redirect('adminhtml/adminhtml_walmartorder/return');
            return;
        }
        /*$this->loadLayout();
        $this->_setActiveMenu('walmart/return');
        $this->renderLayout();*/
    }

    public function editAction()
    {

        $helper = Mage::helper('walmart');
        $id = $this->getRequest()->getParam('id');

        $returnModel = Mage::getModel('walmart/walmartreturn')->load($id);
        $refund_flag = false;

        if ($returnModel->getId() || $id == 0) { 
            $return_data = array();
            $resulting_data = array();

            if ($returnModel->getData('details_saved_after') != "") {
                $details_saved_after = "";
                $details_saved_after = $returnModel->getData('details_saved_after');
                $return_data = "";
                $return_data = unserialize($details_saved_after);
                $magento_order_id = 0;
                
                $magento_order_id = $helper->getMagentoIncrementOrderId($return_data['merchant_order_id']);
                
                if($magento_order_id!=NULL){

                    if (is_numeric($magento_order_id) &&  $magento_order_id == 0) {
                        Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                        $this->_redirect('*/*/return');
                        return;  
                    }

                    if(is_numeric($magento_order_id)==false  &&  $magento_order_id == '') {
                        Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                        $this->_redirect('*/*/return');
                        return;  
                    }
                }else{
                    Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                    $this->_redirect('*/*/return');
                    return;      

                }      

                //$magento_order_id = $helper->getMagentoIncrementOrderId($return_data['merchant_order_id']);
                //if ($magento_order_id == 0) {
                    //Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                    //$this->_redirect('*/*/return');
                    //return;
                //}

                $order = "";
                $order = Mage::getModel('sales/order')->loadByIncrementId($magento_order_id);
                if (!$order->getId()) {
                    Mage::getSingleton('adminhtml/session')->addError("Order not Exists for this return.");
                    $this->_redirect('*/*/return');
                    return;

                }
                $resulting_data = $return_data;
                $resulting_data['status'] = $returnModel->getData('status');
                $view_case_for_return = false;
                $view_case_for_return = $helper->checkViewCaseForReturn($return_data);
                if (!$view_case_for_return) {
                    $skus = "";
                    $skus = $return_data['sku_details'];
                    foreach ($skus as $key => $detail) {
                        $orderItem = "";
                        $qty_refunded = 0;
                        $orderItem = $order->getItemsCollection()->getItemByColumnValue('sku', $detail['merchant_sku']);
                        $qty_refunded = (int)$orderItem->getData('qty_refunded');
                        if ($qty_refunded > 0) {
                            Mage::getSingleton('adminhtml/session')->addError("Order Item with sku : " . $detail['merchant_sku'] . " is refunded without using Return Functionality.");
                            $this->_redirect('*/*/return');
                            return;
                        }
                    }
                }


            } 

            elseif ($returnModel->getData('return_details') != "") {

                $return_ser_data = "";
                $return_ser_data = $returnModel->getData('return_details');

                $return_data = "";
                $return_data = unserialize($return_ser_data);
                
                $resulting_data['status'] = $returnModel->getData('status');
                $resulting_data['id'] = $returnModel->getData('id');
                $resulting_data['returnid'] = $returnModel->getData('returnid');
                $resulting_data['merchant_order_id'] = $return_data->merchant_order_id;

                $magento_order_id = 0;
                $magento_order_id = $helper->getMagentoIncrementOrderId($return_data->merchant_order_id);

                
               if($magento_order_id!=NULL){
                    
                    if (is_numeric($magento_order_id) &&  $magento_order_id == 0) {
                        Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                        $this->_redirect('*/*/return');
                        return;  
                    }

                    if(is_numeric($magento_order_id)==false  &&  $magento_order_id == '') {
                        Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                        $this->_redirect('*/*/return');
                        return;  
                    }
                }else{
                    Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                    $this->_redirect('*/*/return');
                    return;      

                }      


                //if ($magento_order_id == 0) {
                   // Mage::getSingleton('adminhtml/session')->addError('Incomplete information of Order in Return.');
                   // $this->_redirect('*/*/return');
                   // return;
                //}

                $order = "";
                $order = Mage::getModel('sales/order')->loadByIncrementId($magento_order_id);
                if (!$order->getId()) {
                    Mage::getSingleton('adminhtml/session')->addError("Order not Exists for this return.");
                    $this->_redirect('*/*/return');
                    return;

                }
                $refund_flag = $helper->checkOrderInRefund($return_data->merchant_order_id);
                if ($refund_flag) {
                    Mage::getSingleton('adminhtml/session')->addError("Refund of this order already exists.Return can't be generated.");
                    $this->_redirect('*/*/return');
                    return;
                }
                $resulting_data['merchant_return_authorization_id'] = $return_data->merchant_return_authorization_id;
                $resulting_data['merchant_return_charge'] = $return_data->merchant_return_charge;
                $resulting_data['reference_order_id'] = $return_data->reference_order_id;
                $resulting_data['reference_return_authorization_id'] = $return_data->reference_return_authorization_id;
                $resulting_data['refund_without_return'] = $return_data->refund_without_return;
                $resulting_data['return_date'] = $return_data->return_date;
                $resulting_data['return_status'] = $return_data->return_status;
                $resulting_data['shipping_carrier'] = $return_data->shipping_carrier;
                $resulting_data['tracking_number'] = $return_data->tracking_number;
                $i = 0;
                $error_msg = "";
                foreach ($return_data->return_merchant_SKUs as $sku_detail) {
                    $check = array();
                    $check = $helper->getRefundedQtyInfo($order, $sku_detail->merchant_sku);
                   
                    if ($check['error'] == '1') {
                        $error_msg = $error_msg . "<br/>Error for Order Item with sku : " . $sku_detail->merchant_sku . " ";
                        $error_msg = $error_msg . $check['error_msg'];
                        //continue;
                        Mage::getSingleton('adminhtml/session')->addError($error_msg);
                        $this->_redirect('*/*/return');
                        return;
                    }
                    //$resulting_data['sku_details']["sku$i"]['created']=0;
                    $resulting_data['sku_details']["sku$i"]['changes_made'] = 0;
                    $resulting_data['sku_details']["sku$i"]['qty_already_refunded'] = $check['qty_already_refunded'];
                    $resulting_data['sku_details']["sku$i"]['available_to_refund_qty'] = $check['available_to_refund_qty'];
                    $resulting_data['sku_details']["sku$i"]['qty_ordered'] = $check['qty_ordered'];
                    $resulting_data['sku_details']["sku$i"]['order_item_id'] = $sku_detail->order_item_id;
                    $resulting_data['sku_details']["sku$i"]['return_quantity'] = $sku_detail->return_quantity;
                    $resulting_data['sku_details']["sku$i"]['merchant_sku'] = $sku_detail->merchant_sku;
                    $resulting_data['sku_details']["sku$i"]['reason'] = $sku_detail->reason;
                    $resulting_data['sku_details']["sku$i"]['return_principal'] = $sku_detail->requested_refund_amount->principal;
                    $resulting_data['sku_details']["sku$i"]['return_tax'] = $sku_detail->requested_refund_amount->tax;
                    $resulting_data['sku_details']["sku$i"]['return_shipping_cost'] = $sku_detail->requested_refund_amount->shipping_cost;
                    $resulting_data['sku_details']["sku$i"]['return_shipping_tax'] = $sku_detail->requested_refund_amount->shipping_tax;
                    $i++;
                }
                if ($i == 0) {
                    Mage::getSingleton('adminhtml/session')->addError("No items found in return order.");
                    $this->_redirect('*/*/return');
                    return;
                }
            }
            Mage::register('return_data', $resulting_data);

            $this->loadLayout();

            $this->_setActiveMenu('walmart/set_time');
            $this->_addBreadcrumb('Return Manager', 'Return Manager');
            $this->_addBreadcrumb('Return Description', 'Return Description');

            $this->getLayout()->getBlock('head')
                ->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()
                ->createBlock('walmart/adminhtml_return_edit'))
                ->_addLeft($this->getLayout()
                        ->createBlock('walmart/adminhtml_return_edit_tabs')
                );
            $this->renderLayout();
        } else {
            Mage::register('return_data', '');

            Mage::getSingleton('adminhtml/session')->addError('Incomplete information for Return');

            $this->_redirect('*/*/walmartorder');
        }
    }

    public function detailAction()
    {
        $this->loadLayout();
        $this->renderLayout();

    }

    public function acknowledgeAction()
    {

        $orderid = $this->getRequest()->getParam('order_id');
        $Incrementid = $this->getRequest()->getParam('increment_id');


        $resultdata = Mage::getModel('walmart/walmartorder')->getCollection()
            ->addFieldToFilter('magento_order_id', $Incrementid)
            ->addFieldToSelect('order_data')
            ->addFieldToSelect('id')
            ->addFieldToSelect('merchant_order_id')
            ->getData();


        if (empty($resultdata) || count($resultdata) == 0) {
            Mage::getSingleton('adminhtml/session')->addError('Order can not be acknowledged no information found.');

            $this->_redirect("adminhtml/sales_order/view", array('order_id' => $orderid));
            return;
        }

        $serialize_data = unserialize($resultdata[0]['order_data']);

        // check datat exist in the table or not
        if (empty($serialize_data) || count($serialize_data) == 0) {
            // data not exist so insert the data into "walmart_orderdetail" table
            // call API walmart
            $result = Mage::helper('walmart')->CGetRequest('/orders/withoutShipmentDetail/' . $resultdata[0]['merchant_order_id']);
            $Ord_result = json_decode($result);

            if (empty($result) || count($result) == 0) {
                Mage::getSingleton('adminhtml/session')->addError('unable to get order information from Walmart.com for acknowledge.');
                $this->_redirect("adminhtml/sales_order/view", array('order_id' => $orderid));
                return;
            } else {
                //serialize
                $jobj = Mage::getModel('walmart/walmartorder')->load($resultdata[0]['id']);
                $jobj->setOrderData(serialize($Ord_result));
                $jobj->save();

                $serialize_data = $Ord_result;
            }
        }

        if (empty($serialize_data)) {
            Mage::getSingleton('adminhtml/session')->addError('unable to get order information from Walmart.com for acknowledge.');
            $this->_redirect("adminhtml/sales_order/view", array('order_id' => $orderid));
            return;
        }

        $fullfill_array = array();
        foreach ($serialize_data->order_items as $k => $valdata) {
            $fullfill_array[] = array('order_item_acknowledgement_status' => 'fulfillable',
                'order_item_id' => $valdata->order_item_id);
        }


        $order_id = $resultdata[0]['merchant_order_id'];
        $data_var = array();
        $data_var['acknowledgement_status'] = "accepted";


        $data_var['order_items'] = $fullfill_array;


        $data = Mage::helper('walmart')->CPutRequest('/orders/' . $order_id . '/acknowledge', json_encode($data_var));

        $response = json_decode($data);

        if (!empty($response) && $response != null && count($response->errors) > 0 && $response->errors[0] != "") {
            Mage::getSingleton('adminhtml/session')->addError('Order not Acknowledged error: ' . $response->errors[0] . ' on Walmart.com');

            $this->_redirect("adminhtml/sales_order/view", array('order_id' => $orderid));
            return;
        } else {
            $modeldata = Mage::getModel('walmart/walmartorder')->getCollection()
                ->addFieldToFilter('magento_order_id', $Incrementid)->getData();

            if (count($modeldata) > 0) {
                $id = $modeldata[0]['id'];
                $model = Mage::getModel('walmart/walmartorder')->load($id);//->addData($update);
                $model->setStatus('acknowledged');
                $model->save();
                /*
                                $order = Mage::getModel('sales/order')->load($orderid);
                                if($order->canInvoice()) {
                                    $invoiceId = Mage::getModel('sales/order_invoice_api')->create($order->getIncrementId(), array());
                                    $invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($invoiceId);
                                    $invoice->save();
                                }*/

                Mage::getSingleton('adminhtml/session')->addSuccess('Your Walmart Order ' . $Incrementid . ' has been acknowledged successfully');
            } else {
                Mage::getSingleton('adminhtml/session')->addError('Your Walmart Order ' . $Incrementid . ' has been acknowledged but on updated on Walmart.com.');
            }

        }

        $this->_redirect("adminhtml/sales_order/view", array('order_id' => $orderid));

        return;

    }

    public function rejectreasonAction()
    {

        $orderid = $this->getRequest()->getParam('order_id');
        $Incrementid = $this->getRequest()->getParam('increment_id');

        $resultdata = Mage::getModel('walmart/walmartorder')->getCollection()
            ->addFieldToFilter('magento_order_id', $Incrementid)
            ->getData();


        if (count($resultdata) > 0) {
            $JorderData = unserialize($resultdata[0]['order_data']);

            if (empty($JorderData) || count($JorderData) == 0) {
                // now call api to update order data
                $result = Mage::helper('walmart')->CGetRequest('/orders/withoutShipmentDetail/' . $resultdata[0]['merchant_order_id']);
                $Ord_result = json_decode($result);

                if (empty($result) || count($result) == 0) {
                    Mage::getSingleton('adminhtml/session')->addError('unable to get order information from Walmart.com for rejection.');
                    $this->_redirect("adminhtml/sales_order/view", array('order_id' => $orderid));
                } else {
                    //serialize
                    $jobj = Mage::getModel('walmart/walmartorder')->load($resultdata[0]['id']);
                    $jobj->setOrderData(serialize($Ord_result));
                    $jobj->save();

                    //data saved now set into registry
                    Mage::register('current_walmartorder', $Ord_result);
                }
            } else {
                // We have order data form api now save it into registry
                Mage::register('current_walmartorder', $JorderData);
            }

        } else {
            Mage::getSingleton('adminhtml/session')->addSuccess('No information found for this walmart order.');

            $this->_redirect("adminhtml/sales_order/view", array('order_id' => $orderid));
        }

        $this->loadLayout();
        $this->renderLayout();

    }

    public function rejectAction()
    {

        $rej_Order_reasonArr = array(
            'rejected_item_error' => 'rejected - item level error',
            'rejected_shiploc_error' => 'rejected - ship from location not available',
            'rejected_shipmeth_error' => 'rejected - shipping method not supported',
            'rejected_addr_error' => 'rejected - unfulfillable address',
            'accepted' => 'accepted',
        );


        $rej_item_reasonArr = array(
            'nonfulfillable_skuerr' => 'nonfulfillable - invalid merchant SKU',
            'nonfulfillable_inven_err' => 'nonfulfillable - no inventory',
            'fulfillable' => 'fulfillable'
        );


        $Incrementid = $this->getRequest()->getParam('increment_id');
        $merchant_order_id = $this->getRequest()->getParam('merchant_order_id');
        $order_id = $this->getRequest()->getParam('order_id');
        $items_order_idsArr = $this->getRequest()->getParam('order_item_id'); // order items ids array

        $order_Ack_status = $this->getRequest()->getParam('acknowledgement_status');
        $order_item_level_status = $this->getRequest()->getParam('order_item_acknowledgement_status');

        $reject_items_arr = array();

        if (isset($Incrementid) && $Incrementid != null) { // proceed

            // if order ack status is fulfill accepted
            if ($order_Ack_status == 'accepted') {
                Mage::getSingleton('adminhtml/session')->addError('You have to acknowledge order if order Acknowledgement Status is "accepted" othervise select other reasons from acknowledgement status.');

                $this->_redirect('adminhtml/adminhtml_walmartorder/rejectreason/',
                    array('increment_id' => $Incrementid, 'order_id' => $order_id));
            }

            // if item level error exist
            if ($order_Ack_status == 'rejected_item_error') {
                //check option for
                $item_issue = true;
                $count_total = count($order_item_level_status);
                $fullfillsel_count = 0;

                foreach ($order_item_level_status as $k => $valdata) {
                    if ($valdata == 'fulfillable') {
                        $fullfillsel_count++;
                    }

                    $reject_items_arr[] = array(
                        'order_item_acknowledgement_status' => $rej_item_reasonArr[$order_item_level_status[$k]],
                        'order_item_id' => $items_order_idsArr[$k]);

                }

                if ($fullfillsel_count == $count_total) {

                    Mage::getSingleton('adminhtml/session')->addError('You can not select all orders item level "fullfillable" if you have selected "Acknowledgement status" rejected - item level error.');

                    $this->_redirect('adminhtml/adminhtml_walmartorder/rejectreason',
                        array('increment_id' => $Incrementid, 'order_id' => $order_id));
                    return;
                }

            } else { // in normal case
                foreach ($order_item_level_status as $k => $valdata) {

                    $reject_items_arr[] = array(
                        'order_item_acknowledgement_status' => $rej_item_reasonArr[$order_item_level_status[$k]],
                        'order_item_id' => $items_order_idsArr[$k]);
                }
            }
            // All thinsg fine now go for acknowledgement

            $data_var = array();
            $data_var['acknowledgement_status'] = $rej_Order_reasonArr[$order_Ack_status];
            $data_var['order_items'] = $reject_items_arr;


            // Call Walmart.com APi to reject error

            $modeldata = Mage::getModel('walmart/walmartorder')->getCollection()->addFieldToFilter('magento_order_id', $Incrementid)->getData();

            // var_dump($data_var);echo '<br/><br/>';

            $data = Mage::helper('walmart')->CPutRequest('/orders/' . $merchant_order_id . '/acknowledge', json_encode($data_var));


            $response = json_decode($data);

            //var_dump($response);die;

            if (!empty($response) && $response != null && count($response->errors) > 0 && $response->errors[0] != "") {
                // api call failed
                Mage::getSingleton('adminhtml/session')->addError('Order not Rejected error: ' . $response->errors[0] . ' on Walmart.com');

                $this->_redirect("adminhtml/sales_order/view", array('order_id' => $order_id));
                return;

            } else {
                // api called successfull
                $modeldata = Mage::getModel('walmart/walmartorder')->getCollection()->addFieldToFilter('magento_order_id', $Incrementid)->getData();

                if (count($modeldata) > 0) {
                    try {
                        $id = $modeldata[0]['id'];
                        $model = Mage::getModel('walmart/walmartorder')->load($id); //->addData($update);
                        $model->setStatus('rejected');
                        $model->save();
                        // now cancel the Order order state
                        $order = Mage::getModel('sales/order')->loadByIncrementID($Incrementid);
                        if ($order->canCancel()) {
                            try {
                                $order->cancel();
                                $order->getStatusHistoryCollection(true);
                                $order->save();
                                // now convert to walmart rejected order state
                                /*$order->setState("walmart_rejected", true);
                                $order->save();*/
                                Mage::getSingleton('adminhtml/session')->addSuccess('Your Walmart Order ' . $Incrementid . ' has been rejected');
                            } catch (Exception $e) {
                                Mage::getSingleton('adminhtml/session')->addError('Your Walmart Order rejected on Walmart.com but not updated in magento because of this error: ' . $e->getMessage());

                            }
                        }
                    } catch (Exception $e) {
                        Mage::getSingleton('adminhtml/session')->addError('Your Walmart Order rejected on Walmart.com but not updated in magento because of this error: ' . $e->getMessage());
                    }
                }

            }


        } else {
            Mage::getSingleton('adminhtml/session')->addError('Sorry!! No information found for this order.');
        }
        $this->_redirect("adminhtml/sales_order/view", array('order_id' => $order_id));
    }


    

    public function generateInvoice($order, $itemQty)
    {
        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice($itemQty);
        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
        $invoice->register();
        $invoice->getOrder()->setCustomerNoteNotify(false);
        $invoice->getOrder()->setIsInProcess(true);

        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($invoice)
            ->addObject($invoice->getOrder());

        $transactionSave->save();
    }

    public function generateShipment($order, $itemQty)
    {
        $shipment = $order->prepareShipment($itemQty);
        if ($shipment) {
            $shipment->register();
            $shipment->getOrder()->setIsInProcess(true);
            try {
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($shipment)
                    ->addObject($shipment->getOrder())
                    ->save();
            } catch (Mage_Core_Exception $e) {
                Mage::log("Errror While Creating Shipment..." . $e->getMessage());
            }
        }
    }

    public function generateCreditMemo($order ,$itemQtytoCancel){
        $qtys = array('qtys' => $itemQtytoCancel);
        $service = Mage::getModel('sales/service_order', $order);
        $service->prepareCreditmemo($qtys)->register()->save();
    }

    public function markOrderComplete($order)
    {
        $order->setData('state', "complete");
        $order->setStatus("complete");
        $history = $order->addStatusHistoryComment(' Order was set to Complete by walmart.com ', false);
        $history->setIsCustomerNotified(false);
        $order->save();
        $id = $order->getIncrementId();
        $model_data = Mage::getModel('walmart/autoship')->getCollection()
            ->addFieldToFilter('order_id', $id)
            ->getData();
            if($model_data)
            {
           foreach ($model_data as $key => $value) {
            $model = Mage::getModel('walmart/autoship')->load($value['id'])->setData('walmart_shipment_status', 'shipped')->save();
            }  
           }
            
    }

    public function saveWalmartShipData($walmartmodel, $data_ship, $cancel_walmart_order, $order_is_complete)
    {
        $ship_dbdata = $walmartmodel->getShipmentData();
        if(isset($ship_dbdata)){
            $temp_arr = unserialize($ship_dbdata);
            $temp_arr["shipments"][]=$data_ship["shipments"][0];
        }else{$temp_arr = $data_ship;}

        if ($cancel_walmart_order) {
            $walmartmodel->setStatus('cancelled');
            $walmartmodel->setShipmentData(serialize($temp_arr));
            $walmartmodel->save();
        } elseif($order_is_complete) {
            $walmartmodel->setStatus('complete');
            $walmartmodel->setShipmentData(serialize($temp_arr));
            $walmartmodel->save();
        } else{
            $walmartmodel->setStatus('inprogress');
            $walmartmodel->setShipmentData(serialize($temp_arr));
            $walmartmodel->save();
        }
    }

    public function cancelOrderinMagento($orderModel){
        if ($orderModel->canCancel()) {
            $orderModel->cancel();
            $orderModel->setStatus('canceled');
            $orderModel->save();
        }
    }

    public function shippingexceptionAction()
    {

        $fullfillmentnodeid = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_fullfillmentnode');
        $sku = $this->getRequest()->getPost('sku');
        $chargeamount = $this->getRequest()->getPost('chargeamount');
        $exceptiontype = $this->getRequest()->getPost('exceptiontype');
        $shippinglevel = $this->getRequest()->getPost('shippinglevel');
        $shippingmethod = $this->getRequest()->getPost('shippingmethod');
        $overridetype = $this->getRequest()->getPost('override');

        if ($shippinglevel) {
            $shipping = array();
            $shipping['fulfillment_nodes'][] = array('fulfillment_node_id' => "$fullfillmentnodeid", 'shipping_exceptions' => array(array('service_level' => $shippinglevel, 'override_type' => $overridetype, 'shipping_charge_amount' => (int)$chargeamount, 'shipping_exception_type' => $exceptiontype)));
        } else {
            $shipping = array();
            $shipping['fulfillment_nodes'][] = array('fulfillment_node_id' => "$fullfillmentnodeid", 'shipping_exceptions' => array(array('shipping_method' => $shippingmethod, 'override_type' => $overridetype, 'shipping_charge_amount' => (int)$chargeamount, 'shipping_exception_type' => $exceptiontype)));

        }
        $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/shippingexception', json_encode($shipping));

    }

    public function failedordersAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('walmart/failedorders');
        $this->renderLayout();
    }

    public function gridAction()

    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('walmart/adminhtml_failedorders_grid')->toHtml()
        );

    }
    public function grid2Action()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('walmart/adminhtml_walmartorder_grid')->toHtml()
        );

    }

    public function walmartOrderAction()
    {

        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'walmartorders.csv';
        $grid = $this->getLayout()->createBlock('walmart/adminhtml_walmartorder_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /*
     * @acknowledge Orders
     */
    /*public function massAcknowledgeOrderAction()
    {
        $successcount = 0;
        $error = array();
        if (sizeof($this->getRequest()->getParam('order_ids')) > 0) {
            $OrderIds = $this->getRequest()->getParam('order_ids');
            foreach ($OrderIds as $orderid) {
                $resultdata = Mage::getModel('walmart/walmartorder')->load($orderid);
                if (sizeof($resultdata) > 0 && $resultdata->getStatus() == "ready") {
                    $serialize_data = unserialize($resultdata->getOrderData());
                    /* check data not exist into the table or not */
                   /* if (empty($serialize_data) || count($serialize_data) == 0) {
                        /* data not exist so insert the data into "walmart_orderdetail" table
                           call API walmart
                        */
                      /*  $result = Mage::helper('walmart')->CGetRequest('/orders/withoutShipmentDetail/' . $resultdata->getMerchantOrderId());
                        $Ord_result = json_decode($result);

                        if (empty($result) || count($result) == 0) {
                            $error[] = 'Unable to get order information from Walmart.com for acknowledge';
                            continue;
                        } else {
                            $resultdata->setOrderData(serialize($Ord_result));
                            $resultdata->save();
                            $serialize_data = $Ord_result;
                        }
                    }
                    if (empty($serialize_data)) {
                        $error[] = 'unable to get order information from Walmart.com for acknowledge.';
                        continue;
                    }
                    $fullfill_array = array();
                    foreach ($serialize_data->order_items as $k => $valdata) {
                        $fullfill_array[] = array('order_item_acknowledgement_status' => 'fulfillable',
                            'order_item_id' => $valdata->order_item_id);
                    }

                    $order_id = $resultdata->getMerchantOrderId();
                    $data_var = array();
                    $data_var['acknowledgement_status'] = "accepted";

                    $data_var['order_items'] = $fullfill_array;

                    $data = Mage::helper('walmart')->CPutRequest('/orders/' . $order_id . '/acknowledge', json_encode($data_var));
                    $response = json_decode($data);

                    if (count($response->errors) > 0 && $response->errors[0] != "") {
                        $error[] = 'Order not Acknowledged error: ' . $response->errors[0] . ' on Walmart.com';
                        continue;

                    } else {
                        $resultdata->setStatus('acknowledged');
                        $resultdata->save();
                        ++$successcount;
                    }

                } else {
                    $error[] = 'Order can not be acknowledged.';
                }
            }
        } else {
            $error[] = 'Order can not be acknowledged no information found.';
        }

        if ($successcount) {
            Mage::getSingleton('adminhtml/session')->addSuccess($successcount . ' Walmart Order has been Acknowledged Successfully!');
        }
        if (sizeof($error) > 0) {
            foreach ($error as $message) {
                Mage::getSingleton('adminhtml/session')->addError($message);
            }
        }

        
    }*/

    /*
     * @Delete Failed Walmart Orders Log
     */
    public function deletewalmartorderlogAction()
    {
        $successcount = 0;
        if (sizeof($this->getRequest()->getParam('order_ids')) > 0) {

            $logwalmartorders = $this->getRequest()->getParam('order_ids');
            foreach ($logwalmartorders as $orderid) {
                $OrderErrorLog = Mage::getModel('walmart/orderimport')->load($orderid);
                try {
                    if (sizeof($OrderErrorLog) > 0) {
                        $OrderErrorLog->delete();
                        ++$successcount;
                    }
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
        }
        if ($successcount > 0) {
            Mage::getSingleton('adminhtml/session')->addSuccess($successcount . ' Walmart Order Log Deleted Successfully!');
        }
        $this->_redirect('adminhtml/adminhtml_walmartorder/failedorders');
    }

    public function massdeleteorderAction()
    {
        $successcount = 0;
        if (sizeof($this->getRequest()->getParam('order_ids')) > 0) {

            $walmart_orders_ids = $this->getRequest()->getParam('order_ids');
            foreach ($walmart_orders_ids as $orderid) {
                $walmart_orders_data = Mage::getModel('walmart/walmartorder')->load($orderid);
                try {
                    if (sizeof($walmart_orders_data) > 0) {
                        $walmart_orders_data->delete();
                        ++$successcount;
                    }
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
        }
        if ($successcount > 0) {
            Mage::getSingleton('adminhtml/session')->addSuccess($successcount . ' Walmart Order Deleted Successfully!');
        }
        $this->_redirect('adminhtml/adminhtml_walmartorder/walmartorder');
    }

    public function exportReturnCsvAction()
    {
        $fileName = 'walmartreturnorders.csv';
        $grid = $this->getLayout()->createBlock('walmart/adminhtml_return_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
    public function autoshipAction(){
        $this->loadLayout();
        $this->renderLayout();
    }
    public function deleteautoshiplogAction()
    {
        $successcount = 0;
        if (sizeof($this->getRequest()->getParam('order_ids')) > 0) {

            $logwalmartorders = $this->getRequest()->getParam('order_ids');
            foreach ($logwalmartorders as $orderid) {
                $OrderErrorLog = Mage::getModel('walmart/autoship')->load($orderid);
                try {
                    if (sizeof($OrderErrorLog) > 0) {
                        $OrderErrorLog->delete();
                        ++$successcount;
                    }
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
        }
        if ($successcount > 0) {
            Mage::getSingleton('adminhtml/session')->addSuccess($successcount . ' Walmart Order shipment Log Deleted Successfully!');
        }
        $this->_redirect('adminhtml/adminhtml_walmartorder/autoship');
    }
    public function shipAction()
    {
        $timezone = date_default_timezone_get();
        if ($timezone == 'UTC') {
            $StandardOffsetUTC ='';
        } else {
            $timezone = Mage::getStoreConfig('general/locale/timezone');
            $transitions = array_slice($timezone->getTransitions(), -3, null, true);
            foreach (array_reverse($transitions, true) as $transition) {
                if ($transition['isdst'] == 1) {
                    continue;
                }
                $StandardOffsetUTC = sprintf('UTC %+03d:%02u', $transition['offset'] / 3600,
                    abs($transition['offset']) % 3600 / 60);
            }
        }
        /*$offsetEnd = $this->getStandardOffsetUTC();*/
        if (empty($StandardOffsetUTC) || trim($StandardOffsetUTC) == '') {
            $offset = '.0000000-00:00';
        } else {
            $offset = '.0000000' . trim($StandardOffsetUTC);
        }
        // collect ship data

        $shipToDatetime = strtotime($this->getRequest()->getPost('ship_todate'));
       /* $exptime = strtotime($this->getRequest()->getPost('exp_deliver'));*/
        $carrtime = strtotime($this->getRequest()->getPost('carre_pickdate'));
        // get time values
        $shipToDate = date("Y-m-d", $shipToDatetime) . 'T' . date("H:i:s", $shipToDatetime) . $offset;
        //$expDelivery = date("Y-m-d", $exptime) . 'T' . date("H:i:s", $exptime) . $offset;
        $carrierPickdate = date("Y-m-d", $carrtime) . 'T' . date("H:i:s", $carrtime) . $offset;

       /* $walmart_order_row = $this->getRequest()->getPost('order_table_row');*/
        $postData = $this->getRequest()->getPost();
        $trackingUrl = $postData['tracking_url'];
        $tracking = $postData['tracking'];
        $id = $postData['key1'];
        $orderid = $postData['order'];
        $carrier = $postData['carrier'];
        $methodCode = $postData['methodCode'];
        $orderId = $postData['orderid'];
        $itemsData = $postData['items'];
        $data = json_decode($itemsData);
        if (count($itemsData) == 0) {
            $this->getResponse()->setBody("You have no item in your Order.");
            return;
        }

        $orderToComplete = NULL;
        $orderCancel = NULL;
        $mixed = NULL;
        $cancelArray = [];
        $shipmentArray = [];
        $dataShip = [];

        foreach ($data as $itemsData) {
            $lineNumber = $itemsData[4];
            $merchantSku = $itemsData[0];
            $quantityOrdered = $itemsData[1];
            $quantityToShip = $itemsData[3];
            $quantityToCancel = $itemsData[2];
            $dayReturn = $itemsData[5];
            $k = 0;
            $time = time() + ($k + 1);
            $shpId = implode("-", str_split($time, 3));
            $rma = $itemsData[4];
            //flag for 3 cases complete , cancel and mixed.
            if ($quantityOrdered == $quantityToShip && ($quantityToCancel == 0 || empty($quantityToCancel))) {
                $orderToComplete[$merchantSku] = 'complete';
                // case 1 complete_order
                $shipmentArray [] = [
                    'lineNumber' => $lineNumber,
                    'shipment_item_id' => $shpId,
                    'merchant_sku' => $merchantSku,
                    'response_shipment_sku_quantity' => intval($quantityToShip),
                    'RMA_number' => $rma,
                    'days_to_return' => intval($dayReturn),
                ];
                $uniqueRandomNumber = $id.mt_rand(10, 10000);
                $dataShip = [];
                $zip = "10001";/*trim($this->scopeConfigManager->getValue('walmartconfiguration/return_location/zip_code'));*/

                $dataShip['shipments'][] = [
                    'purchaseOrderId' => $orderId,
                    'alt_shipment_id' => $uniqueRandomNumber,
                    'shipment_tracking_number' => $tracking,
                    'response_shipment_date' => $shipToDate,
                    'ship_from_zip_code' => $zip,
                    'carrier_pick_up_date' => $carrierPickdate,
                    'carrier' => $carrier,
                    'shipment_tracking_url' => $trackingUrl,
                    'methodCode' => $methodCode,
                    'shipment_items' => $shipmentArray
                ];
                continue;

            } elseif ($quantityOrdered == $quantityToCancel ) {
                // case3 cancel order
                $orderCancel[$merchantSku] = 'cancel';
                $cancelArray [] = [
                    'lineNumber' => $lineNumber,
                    'shipment_item_id' => "$shpId",
                    'merchant_sku' => $merchantSku,
                    'response_shipment_cancel_qty' => ( int ) $quantityToCancel
                ];
                $uniqueRandomNumber = $id.mt_rand(10, 10000);
                $dataShip = [];
                $zip = '10001';//trim($this->scopeConfigManager->getValue('walmartconfiguration/return_location/zip_code'));
                $dataShip['shipments'][] = [
                    'purchaseOrderId' => $orderId,
                    'alt_shipment_id' => $uniqueRandomNumber,
                    'shipment_tracking_number' => $tracking,
                    'response_shipment_date' => $shipToDate,
                    'ship_from_zip_code' => $zip,
                    'carrier_pick_up_date' => $carrierPickdate,
                    'carrier' => $carrier,
                    'shipment_tracking_url' => $trackingUrl,
                    'methodCode' => $methodCode,
                    'shipment_items' => $shipmentArray,
                    'cancel_items' => $cancelArray
                ];
                continue;
            } elseif ($quantityOrdered == ($quantityToShip + $quantityToCancel) && $quantityToShip > 0) {
                // case 2 mixed/complete case shipment() (this case is for multiple shipment)
                $mixed[$merchantSku] = 'mixed';
                $lineNumbers = explode(',', $lineNumber);
                $count = count($lineNumbers);
                $lineNumber = array_chunk($lineNumbers, intval($quantityToShip));
                $shipmentArray [] = [
                    'lineNumber' => implode(',', $lineNumber[0]),
                    'shipment_item_id' => $shpId,
                    'merchant_sku' => $merchantSku,
                    'response_shipment_sku_quantity' => intval($quantityToShip),
                    'response_shipment_cancel_qty' => intval($quantityToCancel),
                    'RMA_number' => $rma,
                    'days_to_return' => intval($dayReturn),
                ];
                $cancelArray [] = [
                    'lineNumber' => implode(',', $lineNumber[1]),
                    'shipment_item_id' => $shpId,
                    'merchant_sku' => $merchantSku,
                    'response_shipment_sku_quantity' => intval($quantityToShip),
                    'response_shipment_cancel_qty' => intval($quantityToCancel),
                    'RMA_number' => $rma,
                    'days_to_return' => intval($dayReturn),
                ];
                $uniqueRandomNumber = $id.mt_rand(10, 10000);
                $dataShip = [];
                /*$zip = trim($this->scopeConfigManager->getValue('walmartconfiguration/return_location/zip_code'));*/
                $zip = '10001';
                $dataShip['shipments'][] = [
                    'purchaseOrderId' => $orderId,
                    'alt_shipment_id' => $uniqueRandomNumber,
                    'shipment_tracking_number' => $tracking,
                    'response_shipment_date' => $shipToDate,
                    'ship_from_zip_code' => $zip,
                    'carrier_pick_up_date' => $carrierPickdate,
                    'carrier' => $carrier,
                    'shipment_tracking_url' => $trackingUrl,
                    'methodCode' => $methodCode,
                    'shipment_items' => $shipmentArray,
                    'cancel_items' => $cancelArray
                ];
                continue;
            }
        }
        if ($dataShip) {
            $msg = Mage::helper('walmart/order')
                ->putShipOrder($dataShip, $postData, $orderToComplete, $orderCancel, $mixed);
        } else {
            $msg = "You have no information to Ship on Walmart.com";
        }
        return $this->getResponse()->setBody( $msg );
    }
}
