<?php
/**
  * CedCommerce
  *
  * NOTICE OF LICENSE
  *
  * This source file is subject to the Academic Free License (AFL 3.0)
  * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
  * It is also available through the world-wide-web at this URL:
  * http://opensource.org/licenses/afl-3.0.php
  *
  * @category    Ced
  * @package     Ced_Walmart
  * @author      CedCommerce Core Team <connect@cedcommerce.com>
  * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
  * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
  */
  
class Ced_Walmart_Adminhtml_WalmartrefundsettlementController extends Mage_Adminhtml_Controller_Action{
	
	
	public function  updaterefundAction(){
		Mage::getModel('walmart/observer')->updaterefund();
		$this->_redirect('*/*/refund');
	}

	public function exportRefundCsvAction()
    {
        $fileName   = 'walmartrefundorders.csv';
        $grid       = $this->getLayout()->createBlock('walmart/adminhtml_refund_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

	public function saveAction(){
           
    		$refund_mer_orderid=$this->getRequest()->getPost('refund_orderid');
            $skudetails=$this->getRequest()->getParam('sku_details');
            $refundquantity = 0;
            foreach ($skudetails as $rqty) {
                if ($rqty['refund_quantity'] == 0 && $refundquantity == $rqty['refund_quantity']) { 
                	echo $rqty['refund_quantity']; 
                	$flag = true;
                } else { 
                	$flag = false;
                }  
                $refundquantity = $rqty['refund_quantity'];  
            }
          
            if ($flag) {
            	Mage::getSingleton('adminhtml/session')->addError(__('Please Enter Refund Quantity'));
  				$this->_redirect('*/*/refund');
            } else {  
             
	            $dataHelper = Mage::Helper('walmart'); 
		        $orderModel = Mage::getModel('walmart/walmartorder')
		    					 ->load($refund_mer_orderid, 'purchase_order_id');
	            $refundmodel = Mage::getModel('walmart/walmartrefund');

		        if (count($orderModel) != 0) {
	                $orderModel = Mage::getModel('walmart/walmartorder')
		    					->load($refund_mer_orderid, 'merchant_order_id');
		        }
		        $magentoOrderId = $orderModel->getMagentoOrderId();

		        $order = Mage::getModel('sales/order')->loadByIncrementId($magentoOrderId);
		       /* if (!$order->getId()) {
		            Mage::getSingleton('adminhtml/session')->addError(__('Order not Exists for this refund.'));
		            return $this->_redirect('refund');
		        }*/
		        if (count($skudetails) == 0) {
		            Mage::getSingleton('adminhtml/session')->addError(__('Please select any Item of Order to refund.'));
		            return $this->_redirect('*/*/refund');
		        }
		        if (count($skudetails) != 0) {
		            foreach ($skudetails as $key=>$sku) {
		                $this->validateDetails($sku);
		                $orderData['purchaseOrderId'] = $refund_mer_orderid;
		                $refundQuantity = $sku['refund_quantity'];

		                if ($refundQuantity >0) {
			                $lineNumbers = explode(',', $sku['lineNumber']);
			                $lineNumbers = array_chunk($lineNumbers, $refundQuantity);
			                if (isset($lineNumbers[1])) {
			                    $refundLineNumbers = $lineNumbers[1];
			                } else {
			                    $refundLineNumbers = $lineNumbers[0];
			                }

			                $orderData['amount'] = '-'.(float)trim($sku['return_actual_principal']);
			                $orderData['taxAmount'] = '-'.(float)trim($sku['return_tax']);
			                $orderData['taxName'] = 'Item Price Tax';
			                $orderData['shipping'] = '-'.(float)trim($sku['return_shipping_cost']);
			                $orderData['shippingTax'] = '-'.$sku['return_shipping_tax'];
			                $orderData['refundReason'] = $sku['return_refundreason'];
			                $orderData['refunReasonShipping'] = $sku['return_refundreason'];
			                $orderData['refundComments'] = $sku['return_refundfeedback'];
			                $skutemp = $sku['merchant_sku'];
                            $orderData['lineNumber'] = $refundLineNumbers;
                            $response = $dataHelper->refundOrder($refund_mer_orderid, $orderData);
                            if (!isset($response['ns4:errors'])) {
                                $refundmodel->setData('magento_order_id', $magentoOrderId)
                                            ->setData('quantity_returned', $sku['return_quantity'])
                                            ->setData('refund_quantity', $sku['refund_quantity'])
                                            ->setData('refund_reason', $sku['return_refundreason'])
                                            ->setData('refund_feedback', $sku['return_refundfeedback'])
                                            ->setData('refund_amount', $sku['return_principal'])
                                            ->setData('refund_tax', $sku['return_tax'])
                                            ->setData('refund_shippingcost', $sku['return_shipping_cost'])
                                            ->setData('refund_shippingtax', $sku['return_shipping_tax'])
                                            ->setStatus('Complete')
                                            ->setData('refund_purchaseOrderId', $refund_mer_orderid)
                                            ->setSavedData(serialize($response));
                                $refundmodel->save();
                                //$collection = Mage::getModel('sales/order')->getCollection()
                                      //              ->addFieldToFilter('increment_id', $magentoOrderId);
                                //$orderId = $collection->getEntityId();
                               /* $itemmodel=Mage::getModel('sales/order_item')->load($orderId,'order_id');
                                foreach ($itemmodel as $data) {
                                    if ($data->getSku() == $skutemp) {
                                        $id=$data->getItemId();
                                        break;
                                    }
                                }*/
                               /* $itemmodel=Mage::getModel('sales/order_item')->load($id);
                                $itemmodel->setData('qty_refunded', $sku['refund_quantity']);
                                $itemmodel->save();         */     
                            }   
			            }
		            } 
                    if (!isset($response['ns4:errors'])) {
    		            $orderHelper->generateCreditMemoForRefund($skudetails,$refund_mer_orderid);
                        return $this->_redirect('*/*/refund');
                    } else {
		                Mage::getSingleton('adminhtml/session')->addError(__('Error Generating Refund'));
		                return $this->_redirect('*/*/refund');
		            }
		        }
		    }
	        Mage::getSingleton('adminhtml/session')->addError(__('No Data Provided'));
	        $this->_redirect('*/*/refund');
	}

	/**
     * Validate details
     * @param string $detail
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function validateDetails($detail)
    {
        if ($detail['return_quantity']=="" || $detail['return_quantity']<0) {
            Mage::getSingleton('adminhtml/session')->addError("Please enter Qty Returned for sku : 
				".$detail['merchant_sku']); 
            return $this->_redirect('*/*/refund');
        }
        if ($detail['refund_quantity']=="") {
            Mage::getSingleton('adminhtml/session')->addError("Please enter Qty Refunded for sku : 
				".$detail['merchant_sku']);
            return $this->_redirect('*/*/refund');
        }
        if ($detail['return_principal']<0) {
            Mage::getSingleton('adminhtml/session')->addError("Please enter  correct Refund Amount 
				for sku : ".$detail['merchant_sku']);
            return $this->_redirect('*/*/refund');
        }
        if ($detail['return_shipping_cost']<0) {
            Mage::getSingleton('adminhtml/session')->addError("Please enter  correct Refund Shipping Cost for sku : ".$detail['merchant_sku']);
            return $this->_redirect('*/*/refund');
        }
        if ($detail['return_shipping_tax']<0) {
            Mage::getSingleton('adminhtml/session')->addError("Please enter  correct Refund Shipping Tax for sku : ".$detail['merchant_sku']);
            return $this->_redirect('*/*/refund');
        }
        if ($detail['return_tax']<0) {
            Mage::getSingleton('adminhtml/session')->addError("Please enter  correct Refund Tax for 
				sku : ".$detail['merchant_sku']);
            return $this->_redirect('*/*/refund');
        }
        if ($detail['return_refundreason']=="") {
            Mage::getSingleton('adminhtml/session')->addError("Please enter Refund reason for sku :
				".$detail['merchant_sku']);
            return $this->_redirect('*/*/refund');
        }
        if ($detail['return_refundfeedback']=="") {
            Mage::getSingleton('adminhtml/session')->addError("Please enter Refund reason for sku :
			 ".$detail['merchant_sku']);
            return $this->_redirect('*/*/refund');
        }
    }
	
	public function refundAction(){
		
		$this->loadLayout();
		//$this->_setActiveMenu('walmart/return');
		$this->renderLayout();
	}
	public function editAction()
      {  
      	$id = $this->getRequest()->getParam('id');
		
      	$refundModel = Mage::getModel('walmart/walmartrefund')->load($id);
		//print_r($refundModel->getData());die();
           if ($refundModel->getId() || $id == 0)
           {
	            
	            Mage::register('refund_data', $refundModel);
	             $this->loadLayout();
	             $this->_setActiveMenu('walmart/set_time');
	             $this->_addBreadcrumb('Refund Manager', 'Refund Manager');
	             $this->_addBreadcrumb('Refund Description', 'Refund Description');
	             $this->getLayout()->getBlock('head')
	                  ->setCanLoadExtJs(true);
					  
	             $this->_addContent($this->getLayout()
	                  ->createBlock('walmart/adminhtml_refund_edit'))
	                  ->_addLeft($this->getLayout()
	                  ->createBlock('walmart/adminhtml_refund_edit_tabs')
	              );
				  
	             $this->renderLayout();
           }
           else
           {
                 Mage::getSingleton('adminhtml/session')->addError('Refund not created');
                 $this->_redirect('*/*/');
            }
       }
       public function newAction()
       {
          $this->_forward('edit');
       }
       public function getchildhtmlAction(){ 
       		if ($this->getRequest()->getParam('purchase_order_id')) { 
            $walmarthelper = Mage::Helper('walmart/walmart');
            $msg['success']="";
            $msg['error']="";
            $magentoOrderId="";
            $orderData='';
            $shipmentData='';
            $merchantOrderId=$this->getRequest()->getParam('purchase_order_id');
            $merchantOrderId=trim($merchantOrderId);
            if ($merchantOrderId) {
                $return = true;//$this->checkReturnAlreadyGenerated($merchantOrderId);
                if ($return === false) {
                    return $return;
                }
            }
            if (!isset($merchantOrderId)) {
                $msg['error']="Please enter merchant order id :".$merchantOrderId;
                return $this->getResponse()->setBody( json_encode($msg) );
            }
            $collection="";
            try{
                $collection=Mage::getModel('walmart/walmartorder')->getCollection();
                $collection->addFieldToFilter( 'purchase_order_id', $merchantOrderId );
             
                if ($collection->getSize()>0) { 
                    foreach ($collection as $coll) {
                        $magentoOrderId=$coll->getData('magento_order_id');
                        $orderData=$coll->getData('shipment_data');
                        //$shipmentData=$coll->getData('shipment_data');
                        break;
                    }
                } else {
                    $collection = Mage::getModel('walmart/walmartorder')->getCollection();
                    $collection->addFieldToFilter( 'merchant_order_id', $merchantOrderId );
                    if ($collection->getSize()>0) {
                        foreach ($collection as $coll) {
                            $magentoOrderId=$coll->getData('magento_order_id');
                            $orderData=$coll->getData('shipment_data');
                           // $shipmentData=$coll->getData('shipment_data');
                            break;
                        }
                    }
                }
                // var_dump($orderData);die;
                $updatedRefundqtyData = $walmarthelper->getUpdatedRefundQty($merchantOrderId);

                $refundcollection = Mage::getModel('walmart/walmartrefund')->getCollection()
                    ->addFieldToFilter('refund_purchaseOrderId', $merchantOrderId );
                $refund_qty= [];
                if ($refundcollection->getSize()>0) {
                    foreach ($refundcollection as $coll) {
                        $refund_data = unserialize($coll->getData('saved_data'));
                    }
                }

                if ($magentoOrderId == "" || $orderData == '') {
                    $msg['error']="Order not found.Please enter correct Order Id.";
                    return $this->getResponse()->setBody( json_encode($msg) );
                }

                $order_decoded_data="";
                $itemsData= [] ;
                $shipmentData = unserialize($orderData);
                
                $shipmentData=json_decode(str_replace('ns3:', '', $shipmentData['shippedData']),true)['order'];
                
                if (!empty($shipmentData['orderLines']['orderLine'])) {
                	if (isset($shipmentData['orderLines']['orderLine'][0])) {
                	 	foreach ($shipmentData['orderLines']['orderLine'] as $value) {
                        	$itemsData[]=$value;
                    	}
                	} else {
                		$itemsData[]=$shipmentData['orderLines']['orderLine'];
                	}
                    
                } else {

                    $msg['error']="Items Not found in Selected Order.Please enter correct Order Id.";
                    return $this->getResponse()->setBody( json_encode($msg) );

                }

                if (count($itemsData)<=0) {
                    $msg['error']="Items Data not found for selected Order.Please enter correct Order Id.";
                    return $this->getResponse()->setBody( json_encode($msg) );
                }
                $order ="";
                $order = Mage::getModel('sales/order')->loadByIncrementId($magentoOrderId);
                /*if (!$order->getId()) {
                    $msg['error']="Order data not found.Please enter correct Order Id.";
                    return $this->getResponse()->setBody( json_encode($msg) );
                }
                $order->setStatus('complete');
		         if ($order->getStatus()!='complete' ) {
		                    $msg['error']="Can't generate refunds for incompleted orders.This order is incomplete.";
		                    return $this->getResponse()->setBody( json_encode($msg) );
                }*/
                $return_flag=false;
                $error_msg='';
                $j=0;

                foreach ($itemsData as $item) {
                    $merchant_sku="";
                    $merchant_sku=$item['item']['sku'];

                    $check = [];

                    $check=$walmarthelper->getRefundedQtyInfo($order,$merchant_sku);

                    if ($check['error']=='1') {
                        $error_msg=$error_msg."Error for Order Item with sku : ".$merchant_sku."-> ";
                        $error_msg=$error_msg.$check['error_msg'];
                        continue;
                    }
                    $j++;
                }
                if ($j==0) {
                    $msg['error']=$error_msg;
                    return $this->getResponse()->setBody( json_encode($msg) );
                }

               // $resultPage = $this->resultPageFactory->create();
             /*   $html=$resultPage->getLayout()
                    ->createBlock('Ced\Walmart\Block\Adminhtml\Refund')->setTemplate("refund/refundhtml.phtml")
                    ->setData('items_data', $itemsData)
                    ->setData('helper', $helper)
                    ->setData('order', $order)
                    ->setData('merchant_order_id', $merchantOrderId)
                    ->setData('refundtotalqty', $updatedRefundqtyData)
                    ->setData('objectManager', $this->_objectManager)
                    ->toHtml();*/

                    $html=$this->getLayout()
										->createBlock('core/template')->setTemplate("ced/walmart/refundhtml.phtml")
										 ->setData('items_data', $itemsData)
                    ->setData('helper', $walmarthelper)
                    ->setData('order', $order)
                    ->setData('merchant_order_id', $merchantOrderId)
                    ->setData('refundtotalqty', $updatedRefundqtyData)
                    ->toHtml();


                $msg['success']=$html;
                $this->getResponse()->setBody(
                    json_encode($msg)
                );
                return false;
            }catch(\Exception $e) { 
                $msg['error']=$e->getMessage();
                return $this->getResponse()->setBody( json_encode($msg) );

            }


        } else { 
            $msg['error']="Merchant Order Id not found.Please enter again.";
            return $this->getResponse()->setBody( json_encode($msg) );

        }
       			
       }
	
}
