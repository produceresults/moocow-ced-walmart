<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ced_Walmart_Adminhtml_WalmartfeedController extends Mage_Adminhtml_Controller_Action
{
    public function walmartfeedAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function massdeletefeedAction()
    {
        $successcount = 0;
        if (sizeof($this->getRequest()->getParam('feed_ids')) > 0) {

            $walmart_feed_ids = $this->getRequest()->getParam('feed_ids');
            foreach ($walmart_feed_ids as $feedid) {
                $walmart_feeds_data = Mage::getModel('walmart/walmartfeed')->load($feedid);
                try {
                    if (sizeof($walmart_feeds_data) > 0) {
                        $walmart_feeds_data->delete();
                        ++$successcount;
                    }
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            }
        }
        if ($successcount > 0) {
            Mage::getSingleton('adminhtml/session')->addSuccess($successcount . ' Walmart Order Deleted Successfully!');
        }
        $this->_redirect('adminhtml/adminhtml_walmartfeed/walmartfeed');
    }

    public function feedsyncAction()
    {
        try {
            $feeds =   Mage::Helper("walmart/data")->getFeeds();
            if (isset($feeds['results'] )){
                foreach ( $feeds['results'] as $feed ) {
                    $feedModel = Mage::getModel('walmart/walmartfeed');
                    $checkFeed = $feedModel->load($feed['feedId'], 'feed_id')->getData();
                    if (!empty($checkFeed)) {
                        $feedModel->load($feed['feedId'], 'feed_id');
                    } else {
                        $feedModel->setData('feed_id', $feed['feedId']);
                    }
                    $feedModel->setData('feed_status', $feed['feedStatus']);
                    $feedModel->setData('items_received', $feed['itemsReceived']);
                    $feedModel->setData('items_succeeded', $feed['itemsSucceeded']);
                    $feedModel->setData('items_failed', $feed['itemsFailed']);
                    $feedModel->setData('items_processing', $feed['itemsProcessing']);
                    $feedModel->setData('feed_date', date( 'Y-m-d H:i:s', substr($feed['feedDate'], 0, 10)));
                    if (isset($feed['feedType'])) {
                        $feedModel->setData('feed_type', $feed['feedType']);
                    }
                    if (isset($feed['feedSource'])) {
                        $feedModel->setData('feed_source', $feed['feedSource']);
                    }
                    if (isset($feed['itemsFailed']) && $feed['itemsFailed'] > 0) {
                        $errors = Mage::Helper("walmart/data")->getFeeds($feed['feedId'], true); //print_r($errors);die;
                        if (isset($errors['elements']['itemDetails'])) {
                            $feedModel->setData('feed_errors', json_encode($errors['elements']['itemDetails']));
                        }
                    }
                    $feedModel->save();
                }

            }
        }
        catch (\Exception $e) {
            Mage::log("Walmart Feeds Sync Failed : fetchFeeds : " . $e->getMessage(), null, "walmart.log");
        }
        $this->_redirect('adminhtml/adminhtml_walmartfeed/walmartfeed');

    }

}		


