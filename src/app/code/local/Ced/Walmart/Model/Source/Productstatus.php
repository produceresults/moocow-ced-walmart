<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ced_Walmart_Model_Source_Productstatus extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{

    public function getAllOptions()
    {
        return [
            [
                'value' => 'UNPUBLISHED',
                'label' => __('UNPUBLISHED')
            ],
            [
                'value' => 'STAGE',
                'label' => __('STAGE')
            ],
            [
                'value' => 'transmit',
                'label' => __('Transmit')
            ],
            [
                'value' => 'data_fix',
                'label' => __('Data Fix')
            ],
            [
                'value' => 'ingestion_in_progress',
                'label' => __('Ingestion in Progress')
            ],
            [
                'value' => 'publish_in_progress',
                'label' => __('Publish in Progress')
            ],
            [
                'value' => 'PUBLISHED',
                'label' => __('PUBLISHED')
            ],
            [
                'value' => 'system_error',
                'label' => __('System Error')
            ],
            [
                'value' => 'data_fix_ticket',
                'label' => __('Data Fix - Ticket')
            ],
            [
                'value' => 'system_error_ticket',
                'label' => __('System Error - Ticket')
            ]
        ];
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option['value']] = $option['label'];
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value)
    {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }


}
