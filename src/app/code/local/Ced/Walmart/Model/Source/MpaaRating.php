<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class Ced_Walmart_Model_Source_MpaaRating extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
	/**
     * @return array
     */
    public function getAllOptions()
    {
        //"PG,PG-13,G,R,NC-17,Unrated,Not Rated",
        return [
            [
                'value' => ' ',
                'label' => __('Please Select a Value')
            ],
            [
                'value' => 'PG',
                'label' => __('PG')
            ],
            [
                'value' => 'PG-13',
                'label' => __('PG-13')
            ],
            [
                'value' => 'G',
                'label' => __('G')
            ],
            [
                'value' => 'R',
                'label' => __('R')
            ],
            [
                'value' => 'NC-17',
                'label' => __('NC-17')
            ],
            [
                'value' => 'Unrated',
                'label' => __('Unrated')
            ],
            [
                'value' => 'Not Rated',
                'label' => __('Not Rated')
            ]
        ];
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $options = [];
        foreach ($this->getAllOptions() as $option) {
            $options[$option['value']] = (string)$option['label'];
        }
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {

        return $this->getOptions();
    }

    /**
     * Get walmart product status labels array with empty value
     *
     * @return array
     */
    public function getAllOption()
    {
        $options = $this->getOptionArray();
        array_unshift($options, ['value' => '', 'label' => '']);
        return $options;
    }

    /**
     * Get walmart product status labels array for option element
     *
     * @return array
     */
    public function getOptions()
    {
        $res = [];
        foreach ($this->getOptionArray() as $index => $value) {
            $res[] = ['value' => $index, 'label' => $value];
        }
        return $res;
    }

    /**
     * Get walmart product status
     *
     * @param string $optionId
     * @return null|string
     */
    public function getOptionText($optionId)
    {
        $options = $this->getOptionArray();
        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
	
}