<?php

class Ced_Walmart_Model_Product_HealthAndBeauty extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert FoodAndBeverage Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl', 'swatchImages/swatchImage/swatchVariantAttribute',
                'collection', 'variantAttributeNames/variantAttributeName', 'flexibleSpendingAccountEligible',
                'variantGroupId', 'isPrimaryVariant', 'fabricContent/fabricContentValue', 'isAdultProduct',
                'fabricCareInstructions/fabricCareInstruction', 'brand', 'manufacturer', 'modelNumber',
                'manufacturerPartNumber', 'gender', 'color/colorValue', 'ageGroup/ageGroupValue', 'isReusable',
                'isDisposable', 'material/materialValue', 'isPowered', 'numberOfPieces', 'character/characterValue',
                'powerType', 'isPersonalizable', 'bodyParts/bodyPart', 'isPortable', 'cleaningCareAndMaintenance',
                'isSet', 'isTravelSize', 'recommendedUses/recommendedUse', 'shape',
                'compatibleBrands/compatibleBrand'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'HealthAndBeautyElectronics' : {
                    $data['HealthAndBeautyElectronics'] =
                        $this->setHealthAndBeautyElectronics($product, $attributes);
                    break;
                    }
                case 'Optical' : {
                    $data['Optical'] =
                        $this->setOptical($product, $attributes);
                    break;
                    }
                case 'MedicalAids' : {
                    $data['MedicalAids'] =
                        $this->setMedicalAids($product, $attributes);
                    break;
                    }
                case 'PersonalCare' : {
                    $data['PersonalCare'] =
                        $this->setMedicalAids($product, $attributes);
                    break;
                    }
                case 'MedicineAndSupplements' : {
                    $data['MedicineAndSupplements'] =
                        $this->setMedicineAndSupplements($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert HealthAndBeautyElectronics Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setHealthAndBeautyElectronics($product = [], $attributes = [])
    {
        $walmartAttr = [
            'batteriesRequired', 'batterySize', 'connections/connection',
            'isCordless','hasAutomaticShutoff', 'screenSize/unit', 'screenSize/measure','displayTechnology'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Optical Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setOptical($product = [], $attributes = [])
    {
        $walmartAttr = [
            'frameMaterial/frameMaterialValue', 'shape', 'eyewearFrameStyle',
            'lensMaterial','eyewearFrameSize','uvRating', 'isPolarized','lensTint','isScratchResistant',
            'hasAdaptiveLenses', 'lensType/lensTypeValue'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert MedicalAids Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setMedicalAids($product = [], $attributes = [])
    {
        $walmartAttr = [
            'isInflatable', 'isWheeled', 'isFoldable', 'isIndustrial',
            'diameter/unit', 'diameter/measure', 'isAssemblyRequired','assemblyInstructions','maximumWeight/unit',
            'maximumWeight/unit','isLatexFree','isAntiAging', 'isHypoallergenic','isOilFree','isParabenFree',
            'isNoncomodegenic','scent', 'isUnscented','isVegan', 'isWaterproof','isWaterproof',
            'healthConcerns/healthConcern'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert PersonalCare Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setPersonalCare($product = [], $attributes = [])
    {
        $walmartAttr = [
            'ingredientClaim/ingredientClaimValue', 'isLatexFree',
            'absorbency', 'resultTime/unit', 'resultTime/measure', 'skinCareConcern','skinType','hairType',
            'skinTone','spfValue','isAntiAging', 'isHypoallergenic','isOilFree','isParabenFree',
            'isNoncomodegenic','scent','isUnscented','isVegan', 'isWaterproof','isTinted','isSelfTanning',
            'isDrugFactsLabelRequired','drugFactsLabel', 'activeIngredients/activeIngredient/activeIngredientName',
            'activeIngredients/activeIngredient/activeIngredientPercentage', 'inactiveIngredients/inactiveIngredient',
            'form','organicCertifications/organicCertification','instructions',
            'stopUseIndications/stopUseIndication'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert MedicineAndSupplements Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setMedicineAndSupplements($product = [], $attributes = [])
    {
        $walmartAttr = [
            'isDrugFactsLabelRequired', 'drugFactsLabel',
            'isSupplementFactsLabelRequired', 'supplementFactsLabel', 'servingSize', 'servingsPerContainer',
            'activeIngredients/activeIngredient/activeIngredientName',
            'activeIngredients/activeIngredient/activeIngredientPercentage',
            'inactiveIngredients/inactiveIngredient', 'healthConcerns/healthConcern','form',
            'organicCertifications/organicCertification','instructions','dosage',
            'stopUseIndications/stopUseIndication'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}