<?php

class Ced_Walmart_Model_Product_Media extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Media Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'title,originalLanguages,variantGroupId,variantAttributeNames/variantAttributeName',
                'isPrimaryVariant','isAdultProduct','awardsWon/awardsWonValue',
                'character/characterValue','targetAudience/targetAudienceValue','isDownloadableContentAvailable',
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'TVShows' : {
                    $data['TVShows'] = $this->setTVShows($product, $attributes);
                    break;
                    }
                case 'Music' : {
                    $data['Music'] = $this->setMusic($product, $attributes);
                    break;
                    }
                case 'BooksAndMagazines' : {
                    $data['BooksAndMagazines'] = $this->setBooksAndMagazines($product, $attributes);
                    break;
                    }
                case 'Movies' : {
                    $data['Movies'] = $this->setMovies($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert TVShows Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setTVShows($product = [], $attributes = [])
    {
        $walmartAttr = [

            'digitalVideoFormats','tvRating','tvShowGenre','tvShowSubgenre','tvNetwork','tvShowSeason',
            'numberOfEpisodes','episode','director','actors/actor','screenwriter','studioProductionCompany',
            'videoStreamingQuality','audioTrackCodec','duration/unit','duration/measure','dvdReleaseDate',
            'isDubbed','dubbedLanguages/dubbedLanguage','hasSubtitles','subtitledLanguages/subtitledLanguage',
            'seriesTitle','numberInSeries','aspectRatio'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Music Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setMusic($product = [], $attributes = [])
    {
        $walmartAttr = [
            'musicGenre','musicSubGenre','performer/performerValue','songwriter','musicMediaFormat',
            'musicProducer','recordLabel', 'numberOfDiscs','numberOfTracks','releaseDate','musicReleaseType',
            'hasParentalAdvisoryLabel','trackListings/trackListing','seriesTitle','numberInSeries'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert BooksAndMagazines Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setBooksAndMagazines($product = [], $attributes = [])
    {
        $walmartAttr = [
            'condition','gender','color/colorValue','material/materialValue','pattern/patternValue','edition',
            'subject','bookFormat','genre','subgenre','author/authorValue','editor','illustrator',
            'publisher','translator','translatedFrom','fictionNonfiction','isUnabridged','originalPublicationDate',
            'publicationDate','readingLevel','numberOfPages','issue','seriesTitle','numberInSeries','title',
            'variantGroupId','isPrimaryVariant','isAdultProduct','isDownloadableContentAvailable'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Movies Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setMovies($product = [], $attributes = [])
    {
        $walmartAttr = [
            'mpaaRating','movieGenre','movieSubgenre','theatricalReleaseDate','digitalVideoFormats','director',
            'actors/actor','screenwriter','studioProductionCompany','videoStreamingQuality','audioTrackCodec',
            'duration/unit','duration/measure','dvdReleaseDate','isDubbed','hasSubtitles','seriesTitle',
            'numberInSeries','aspectRatio','title','variantGroupId','isPrimaryVariant','isAdultProduct',
            'isDownloadableContentAvailable'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}