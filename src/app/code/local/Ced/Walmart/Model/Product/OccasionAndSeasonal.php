<?php

class Ced_Walmart_Model_Product_OccasionAndSeasonal extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert OccasionAndSeasonal Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);
        print_r($product);die;
        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl', 'swatchImages/swatchImage/swatchVariantAttribute',
                'variantAttributeNames/variantAttributeName', 'variantGroupId', 'isPrimaryVariant',
                'holidayLightingStyle/holidayLightingStyleValue', 'fabricCareInstructions/fabricCareInstruction',
                'brand', 'manufacturer', 'modelNumber', 'manufacturerPartNumber', 'color/colorValue',
                'pattern/patternValue', 'material/materialValue', 'numberOfPieces', 'powerType',
                'occasion/occasionValue', 'recommendedUses/recommendedUse', 'isAssemblyRequired',
                'assemblyInstructions', 'watts/unit', 'watts/measure', 'lightBulbType'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'DecorationsAndFavors' : {
                    $data['DecorationsAndFavors'] =
                        $this->setDecorationsAndFavors($product, $attributes);
                    break;
                    }
                case 'Funeral' : {
                    $data['Funeral'] = $this->setFuneral($product, $attributes);
                    break;
                    }
                case 'CeremonialClothingAndAccessories' : {
                    $data['CeremonialClothingAndAccessories'] =
                        $this->setCeremonialClothingAndAccessories($product, $attributes);
                    break;
                    }
                case 'Costumes' : {
                    $data['Costumes'] =
                        $this->setCostumes($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert DecorationsAndFavors Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setDecorationsAndFavors($product = [], $attributes = [])
    {
        $walmartAttr = [
            'fabricContent/fabricContentValue', 'isAdultProduct', 'finish', 'isRecyclable', 'theme/themeValue',
            'gender', 'ageGroup/ageGroupValue', 'numberOfPieces', 'character/characterValue', 'isPowered',
            'powerType', 'isPersonalizable', 'isMadeFromRecycledMaterial',
            'recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
            'recycledMaterialContent/recycledMaterialContentValue/percentageOfRecycledMaterial', 'isInflatable',
            'isAnimated', 'targetAudience/targetAudienceValue', 'makesNoise', 'shape'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }


    /**
     * Insert Funeral Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setFuneral($product = [], $attributes = [])
    {
        $walmartAttr = [
            'finish', 'fillMaterial/fillMaterialValue'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }


    /**
     * Insert CeremonialClothingAndAccessories Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setCeremonialClothingAndAccessories($product = [], $attributes = [])
    {
        $walmartAttr = [

            'fabricContent/fabricContentValue', 'clothingSize', 'gender', 'clothingSizeType',
            'pattern/patternValue', 'apparelCategory'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Costumes Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setCostumes($product = [], $attributes = [])
    {
        $walmartAttr = [
            'fabricContent/fabricContentValue', 'theme/themeValue', 'clothingSize', 'gender', 'ageGroup',
            'clothingSizeType', 'animalType', 'character/characterValue', 'targetAudience/targetAudienceValue'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}