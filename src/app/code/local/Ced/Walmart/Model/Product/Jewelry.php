<?php

class Ced_Walmart_Model_Product_Jewelry extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Jewelry Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'size','jewelryStyle','metal','plating','swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute','karats','gemstone',
                'variantAttributeNames/variantAttributeName','birthstone','variantGroupId','gemstoneShape',
                'isPrimaryVariant','carats/unit','carats/value','diamondClarity','gemstoneCut','chainLength/unit',
                'chainLength/measure','brand', 'manufacturer', 'modelNumber',
                'manufacturerPartNumber','gender','color/colorValue','ageGroup/ageGroupValue',
                'material/materialValue', 'pattern/patternValue',
                'character/characterValue', 'occasion/occasionValue',
                'isPersonalizable', 'bodyParts', 'isPersonalizable',
                'bodyParts/bodyPart','recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/percentageOfRecycledMaterial',

            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'Rings' : {
                    $data['Rings'] = $this->setRings($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert Jewelry/Rings Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setRings($product = [], $attributes = [])
    {
        $walmartAttr = [
            'ringStyle/ringStyleValue'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }
}