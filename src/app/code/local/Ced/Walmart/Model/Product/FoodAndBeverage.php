<?php

class Ced_Walmart_Model_Product_FoodAndBeverage extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert FoodAndBeverage Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'isNutritionFactsLabelRequired', 'nutritionFactsLabel',
                'foodForm', 'isImitation', 'foodAllergenStatements/foodAllergenStatement', 'usdaInspected',
                'vintage', 'timeAged/unit', 'timeAged/measure', 'variantAttributeNames/variantAttributeName',
                'isGmoFree','variantGroupId', 'isPrimaryVariant', 'isBpaFree', 'isPotentiallyHazardousFood',
                'isReadyToEat','caffeineDesignation', 'brand', 'manufacturer', 'spiceLevel', 'flavor', 'beefCut',
                'poultryCut', 'color/colorValue', 'isMadeInHomeKitchen', 'nutrientContentClaims/nutrientContentClaim',
                'safeHandlingInstructions', 'character/characterValue', 'occasion/occasionValue', 'isPersonalizable',
                'fatCaloriesPerGram', 'recommendedUses/recommendedUse', 'carbohydrateCaloriesPerGram',
                'totalProtein/unit', 'totalProtein/measure', 'totalProteinPercentageDailyValue/unit',
                'totalProteinPercentageDailyValue/measure', 'proteinCaloriesPerGram', 'isFairTrade', 'isIndustrial',
                'ingredients', 'releaseDate', 'servingSize', 'servingsPerContainer',
                'organicCertifications/organicCertification', 'instructions', 'calories', 'caloriesFromFat/unit',
                'caloriesFromFat/measure', 'totalFat/unit', 'totalFat/measure','totalFatPercentageDailyValue/unit',
                'totalFatPercentageDailyValue/measure','totalCarbohydrate/unit','totalCarbohydrate/measure',
                'totalCarbohydratePercentageDailyValue/unit','totalCarbohydratePercentageDailyValue/measure',
                'nutrients/nutrient'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'AlcoholicBeverages' : {
                    $data['AlcoholicBeverages'] = $this->setAlcoholicBeverages($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert AlcoholicBeverages Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setAlcoholicBeverages($product = [], $attributes = [])
    {
        $walmartAttr = [
            'alcoholContentByVolume', 'alcoholProof', 'alcoholClassAndType', 'neutralSpiritsColoringAndFlavoring',
            'whiskeyPercentage', 'isEstateBottled', 'wineAppellation', 'wineVarietal', 'containsSulfites', 'isNonGrape'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}