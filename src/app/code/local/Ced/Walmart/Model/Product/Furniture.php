<?php

class Ced_Walmart_Model_Product_Furniture extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Furniture Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl', 'swatchImages/swatchImage/swatchVariantAttribute',
                'collection', 'variantAttributeNames/variantAttributeName', 'variantGroupId', 'isPrimaryVariant',
                'fabricContent/fabricContentValue', 'finish', 'homeDecorStyle',
                'fabricCareInstructions/fabricCareInstruction', 'brand', 'manufacturer', 'theme/themeValue',
                'modelNumber', 'manufacturerPartNumber', 'gender', 'color/colorValue', 'ageGroup',
                'recommendedRooms/recommendedRoom', 'mountTypeValue', 'isAntique', 'pattern/patternValue',
                'material/materialValue', 'isPowered', 'numberOfPieces', 'character/characterValue', 'powerType',
                'isMadeFromRecycledMaterial', 'recommendedUses/recommendedUse',
                'recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/percentageOfRecycledMaterial',
                'recommendedLocations/recommendedLocation', 'seatingCapacity', 'isInflatable', 'isWheeled',
                'numberOfDrawers', 'numberOfShelves', 'isFoldable', 'isIndustrial', 'isAssemblyRequired',
                'assemblyInstructions', 'fillMaterial/fillMaterialValue', 'shape'
            ];

            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'Seating' : {
                    $data['Seating'] = $this->setSeating($product, $attributes);
                    break;
                    }
                case 'TVFurniture' : {
                    $data['TVFurniture'] = $this->setTVFurniture($product, $attributes);
                    break;
                    }
                case 'Beds' : {
                    $data['Beds'] = $this->setBeds($product, $attributes);
                    break;
                    }
                case 'Mattresses' : {
                    $data['Mattresses'] = $this->setMattresses($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert Seating Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setSeating($product = [], $attributes = [])
    {
        $walmartAttr = [
            'seatBackHeight/unit', 'seatBackHeight/measure', 'seatMaterial', 'seatHeight/unit',
            'seatHeight/measure', 'frameMaterial'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert TVFurniture Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setTVFurniture($product = [], $attributes = [])
    {
        $walmartAttr = [
            'maximumScreenSize/unit', 'maximumScreenSize/measure'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Beds Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setBeds($product = [], $attributes = [])
    {
        $walmartAttr = [
            'bedStyle', 'bedSize'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Mattresses Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setMattresses($product = [], $attributes = [])
    {
        $walmartAttr = [
            'mattressFirmness', 'mattressThickness', 'pumpIncluded', 'bedSize',
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}