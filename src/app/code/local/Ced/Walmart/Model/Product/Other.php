<?php

class Ced_Walmart_Model_Product_Other extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Other Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $variantAttr = explode(',', $type['variantattr']);
            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $variantAttr;
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl','swatchImages/swatchImage/swatchVariantAttribute',
                'systemOfMeasurement', 'variantAttributeNames/variantAttributeName','variantGroupId','isPrimaryVariant',
                'fabricContent/fabricContentValue','finish', 'fabricCareInstructions/fabricCareInstruction','brand',
                'manufacturer','modelNumber','manufacturerPartNumber','gender','color/colorValue',
                'recommendedRooms/recommendedRoom','connections/connection','material/materialValue',
                'pattern/patternValue','isPowered','character/characterValue','powerType', 'isPortable',
                'recommendedLocations/recommendedLocation','isRetractable','isFoldable','isCollectible','isIndustrial',
                'isAssemblyRequired','assemblyInstructions','recommendedSurfaces/recommendedSurface','volts/unit',
                'volts/measure','shape','displayTechnology'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'Storage' : {
                    $data['Storage'] = $this->setStorage($product, $attributes);
                    break;
                }
                case 'CleaningAndChemical' : {
                    $data['CleaningAndChemical'] = $this->setCleaningAndChemical($product, $attributes);
                    break;
                }
            }
        }
        return $data;
    }


    /**
     * Insert Other/Storage Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setStorage($product = [], $attributes = [])
    {
        $walmartAttr = [
            'collection', 'shelfDepth/unit', 'shelfDepth/measure', 'shelfStyle','recommendedUses/recommendedUse',
            'drawerPosition', 'drawerDimensions','numberOfDrawers','numberOfShelves','maximumWeight/unit',
            'maximumWeight/measure','capacity'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Other/CleaningAndChemical Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setCleaningAndChemical($product = [], $attributes = [])
    {
        $walmartAttr = [
            'isRecyclable', 'isBiodegradable', 'isEnergyStarCertified', 'isCombustible','isMadeFromRecycledMaterial',
            'recycledMaterialContent/recycledMaterialContentValue', 'isFlammable','ingredients','handleLength/unit',
            'handleLength/measure','fluidOunces/unit','fluidOunces/measure','scent',
            'activeIngredients/activeIngredient/activeIngredientName',
            'activeIngredients/activeIngredient/activeIngredientPercentage', 'inactiveIngredients/inactiveIngredient',
            'form','instructions'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}