<?php

class Ced_Walmart_Model_Product_Animal extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert FoodAndBeverage Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();
        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'animalBreed', 'swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute', 'animalLifestage', 'minimumWeight/unit',
                'variantAttributeNames/variantAttributeName', 'variantGroupId',
                'isPrimaryVariant', 'fabricContent/fabricContentValue',
                'fabricCareInstructions/fabricCareInstruction', 'brand', 'manufacturer', 'modelNumber',
                'manufacturerPartNumber', 'color/colorValue', 'animalType', 'material/materialValue',
                'pattern/patternValue', 'isPortable', 'isFoldable', 'maximumWeight/unit', 'maximumWeight/measure',
                'petSize'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'AnimalHealthAndGrooming' : {
                    $data['AnimalHealthAndGrooming'] =
                        $this->setAnimalHealthAndGrooming($product, $attributes);
                    break;
                    }

                case 'AnimalAccessories' : {
                    $data['AnimalAccessories'] =
                        $this->setAnimalAccessories($product, $attributes);
                    break;
                    }

                case 'AnimalFood' : {
                    $data['AnimalFood'] =
                        $this->setAnimalFood($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert AnimalHealthAndGrooming Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setAnimalHealthAndGrooming($product = [], $attributes = [])
    {
        $walmartAttr = [
            'condition', 'isDisposable', 'powerType', 'animalHealthConcern', 'hairLength/hairLengthValue',
            'scent', 'isDrugFactsLabelRequired', 'drugFactsLabel',
            'activeIngredients/activeIngredient/activeIngredientName',
            'activeIngredients/activeIngredient/activeIngredientPercentage',
            'inactiveIngredients/inactiveIngredient', 'form', 'instructions', 'dosage',
            'stopUseIndications/stopUseIndication', 'petSize'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert AnimalAccessories Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setAnimalAccessories($product = [], $attributes = [])
    {
        $walmartAttr = [
            'condition', 'minimumTemperature/unit', 'minimumTemperature/measure', 'clothingSize',
            'batteriesRequired', 'batterySize', 'character/characterValue', 'isRetractable', 'isReflective',
            'makesNoise', 'maximumTemperature/unit', 'maximumTemperature/measure', 'capacity', 'shape'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert AnimalFood Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setAnimalFood($product = [], $attributes = [])
    {

        $walmartAttr = [
            'flavor', 'petFoodForm', 'nutrientContentClaims/nutrientContentClaim', 'isGrainFree',
            'feedingInstructions', 'animalHealthConcern', 'ingredients'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}