<?php

class Ced_Walmart_Model_Product_Office extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Office Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'inkColor/inkColorValue','numberOfSheets','isRefillable','swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute','systemOfMeasurement',
                'variantAttributeNames/variantAttributeName','variantGroupId','isAntiglare',
                'isPrimaryVariant','fabricContent/fabricContentValue','finish','isRecyclable','isMagnetic','brand',
                'envelopeSize','condition','holeSize/unit','holeSize/unit','manufacturer','theme/themeValue', 'paperSize',
                'year','modelNumber','manufacturerPartNumber','calendarFormat/unit','calendarTerm/value',
                'color/colorValue','batteriesRequired','ageGroup/ageGroupValue','dexterity','batterySize',
                'material/materialValue', 'pattern/patternValue','isPowered',
                'character/characterValue','powerType', 'occasion/occasionValue','isMadeFromRecycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/percentageOfRecycledMaterial',
                'recommendedUses/recommendedUse','isRetractable','isIndustrial','isTearResistant','capacity',
                'brightness/unit','brightness/measure','shape','compatibleDevices/compatibleDevice'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}