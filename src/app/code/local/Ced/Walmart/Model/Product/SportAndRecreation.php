<?php

class Ced_Walmart_Model_Product_SportAndRecreation extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert FoodAndBeverage Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
            'type' => 'simple',
            'variantid' => null,
            'variantattr' => null,
            'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute',
                'variantAttributeNames/variantAttributeName', 'variantGroupId','isPrimaryVariant',
                'fabricContent/fabricContentValue/materialName','isWeatherResistant','finish',
                'fabricCareInstructions/fabricCareInstruction',
                'brand', 'condition','manufacturer', 'modelNumber','manufacturerPartNumber','clothingSize',
                'gender', 'color/colorValue',
                'ageGroup/ageGroupValue', 'batteriesRequired', 'dexterity', 'batterySize',
                'fishingLinePoundTest', 'fishingLocation', 'animalType', 'material/materialValue',
                'pattern/patternValue','isPowered','numberOfPieces','character/characterValue','fitnessGoal',
                'powerType','maximumIncline/unit','maximumIncline/measure','isPortable',
                'cleaningCareAndMaintenance','bladeType','recommendedUses/recommendedUse','tentType',
                'recommendedLocations/recommendedLocation','seatingCapacity','tireDiameter/unit',
                'tireDiameter/measure','season/seasonValue','isWheeled','isMemorabilia','isFoldable',
                'isCollectible','isAssemblyRequired','maximumRecommendedAge/unit',
                'maximumRecommendedAge/measure','assemblyInstructions','minimumRecommendedAge/unit',
                'minimumRecommendedAge/measure','ballCoreMaterial/ballCoreMaterialValue',
                'footballSize','sport','basketballSize','maximumWeight/unit','maximumWeight/measure',
                'soccerBallSize','batDrop','isTearResistant','isSpaceSaving','capacity','velocity/unit',
                'velocity/measure','isWaterproof','hasAutomaticShutoff','shape',
                'wirelessTechnologies/wirelessTechnologie','horsepower/unit','horsepower/measure'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'Cycling' : {
                    $data['Cycling'] = $this->setCycling($product, $attributes);
                    break;
                    }
                case 'Optics' : {
                        $data['Optics'] = $this->setCycling($product, $attributes);
                        break;
                        }
                case 'Weapons' : {
                        $data['Weapons'] = $this->setCycling($product, $attributes);
                        break;
                        }
                }
        }
        return $data;
    }

    /**
     * Insert Cycling Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setCycling($product = [], $attributes = [])
    {
        $walmartAttr = [
            'bicycleFrameSize/unit','bicycleFrameSize/measure','bicycleWheelDiameter/unit',
            'bicycleWheelDiameter/measure','bicycleTireSize',
            'numberOfSpeeds','lightBulbType'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Optics Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setOptics($product = [], $attributes = [])
    {
        $walmartAttr = [
            'powerType','magnification','fieldOfView','isFogResistant','lensDiameter/unit','lensDiameter/measure',
            'isMulticoated','opticalZoom','digitalZoom','focusType/focusTypeValue',
            'lockType','sensorResolution/unit','sensorResolution/measure','hasDovetailBarSystem','hasLcdScreen',
            'hasMemoryCardSlot','hasNightVision','isParfocal',
            'attachmentStyle','displayResolution/unit','displayResolution/measure','focalRatio','lensCoating',
            'operatingTemperature/unit','operatingTemperature/measure','isLockable','screenSize/unit',
            'screenSize\measure','displayTechnology'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

     /**
     * Insert Weapons Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setWeapons($product = [], $attributes = [])
    {
        $walmartAttr = [
            'shotgunGauge','velocity/unit','velocity/measure','firearmAction','caliber/unit',
            'caliber/measure','ammunitionType','firearmChamberLength',
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}