<?php

class Ced_Walmart_Model_Product_ArtAndCraft extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert ArtAndCraft Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'metal', 'isRefillable', 'plating', 'swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute', 'ageRange',
                'variantAttributeNames/variantAttributeName', 'variantGroupId', 'isPrimaryVariant',
                'fabricContent/fabricContentValue', 'finish', 'isRecyclable', 'culturalStyle', 'chainLength/unit',
                'chainLength/unit', 'fabricCareInstructions/fabricCareInstruction', 'brand', 'condition',
                'manufacturer', 'theme/themeValue', 'modelNumber', 'manufacturerPartNumber', 'gender',
                'color/colorValue', 'ageGroup', 'isBulk', 'isReusable', 'isDisposable', 'isAntique',
                'pattern/patternValue', 'material/materialValue', 'isAntitarnish', 'numberOfPieces',
                'character/characterValue', 'isPowered', 'powerType', 'occasion/occasionValue',
                'isPersonalizable', 'artPaintType', 'isPortable', 'isMadeFromRecycledMaterial',
                'recommendedUses/recommendedUse',
                'recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/percentageOfRecycledMaterial',
                'isRetractable', 'gotsCertification', 'isFoldable', 'isCollectible', 'isHandmade', 'diameter/unit',
                'diameter/measure', 'skillLevel', 'isSelfAdhesive', 'recommendedSurfaces/recommendedSurface',
                'capacity', 'subject', 'scent', 'form', 'organicCertifications/organicCertification', 'shape'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }
}