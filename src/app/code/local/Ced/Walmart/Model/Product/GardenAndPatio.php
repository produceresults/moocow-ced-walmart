<?php

class Ced_Walmart_Model_Product_GardenAndPatio extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert GardenAndPatio Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl', 'swatchImages/swatchImage/swatchVariantAttribute',
                'variantAttributeNames/variantAttributeName', 'variantGroupId', 'isPrimaryVariant',
                'fabricContent/fabricContentValue','isWeatherResistant','finish','homeDecorStyle','plantCategory',
                'fabricCareInstructions/fabricCareInstruction','brand', 'condition','manufacturer', 'theme/themeValue',
                'minimumTemperature/unit','minimumTemperature/measure','modelNumber', 'manufacturerPartNumber',
                'color/colorValue','isBulk','ageGroup/ageGroupValue','batteriesRequired','batterySize',
                'isEnergyStarCertified','isAntique','material/materialValue','pattern/patternValue','numberOfPieces',
                'character/characterValue','isPowered','powerType','occasion/occasionValue','coverageArea/unit',
                'coverageArea/measure','cleaningCareAndMaintenance','isMadeFromRecycledMaterial',
                'recommendedUses/recommendedUse',
                'recycledMaterialContent/recycledMaterialContentValue/recycledMaterial','flowRate/unit',
                'flowRate/measure','recommendedLocations/recommendedLocation','hasRadiantHeat','season/seasonValue',
                'isWheeled','isFoldable','isIndustrial','maximumWeight/unit','maximumWeight/measure',
                'isTearResistant','installationType','capacity','fuelType','volts/unit','volts/measure',
                'watts/unit','watts/measure','btu','isWaterproof','hasAutomaticShutoff',
                'frameMaterial/frameMaterialValue','shape','displayTechnology','lightBulbType'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'GrillsAndOutdoorCooking' : {
                    $data['GrillsAndOutdoorCooking'] =
                        $this->setGrillsAndOutdoorCooking($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert GrillsAndOutdoorCooking Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setGrillsAndOutdoorCooking($product = [], $attributes = [])
    {
        $walmartAttr = [
            'flavor', 'numberOfBurners','hasSideShelf', 'hasCharcoalBasket', 'totalCookingArea/unit',
            'totalCookingArea/measure','sideBurnerSize/unit', 'sideBurnerSize/measure', 'hasTankTray',
            'lifespan'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}