<?php

class Ced_Walmart_Model_Product_Home extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Home Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute', 'collection',
                'variantAttributeNames/variantAttributeName', 'variantGroupId', 'isPrimaryVariant',
                'fabricContent/fabricContentValue', 'finish', 'homeDecorStyle',
                'fabricCareInstructions/fabricCareInstruction', 'brand', 'manufacturer', 'theme/themeValue',
                'modelNumber', 'manufacturerPartNumber', 'gender', 'color/colorValue', 'ageGroup/ageGroupValue',
                'recommendedRooms/recommendedRoom', 'batteriesRequired', 'batterySize', 'isReusable',
                'isDisposable', 'isAntique', 'material/materialValue', 'pattern/patternValue', 'isPowered',
                'numberOfPieces', 'character/characterValue', 'powerType', 'occasion/occasionValue',
                'isPersonalizable', 'isPortable', 'cleaningCareAndMaintenance', 'isCordless',
                'isMadeFromRecycledMaterial', 'isSet', 'recommendedUses/recommendedUse',
                'recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/percentageOfRecycledMaterial',
                'isRetractable', 'isWheeled', 'isFoldable', 'isIndustrial', 'isAssemblyRequired',
                'assemblyInstructions', 'shape'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'LargeAppliances' : {
                    $data['LargeAppliances'] = $this->setLargeAppliances($product, $attributes);
                    break;
                    }
                case 'Bedding' : {
                    $data['Bedding'] = $this->setBedding($product, $attributes);
                    break;
                    }
                case 'HomeDecor' : {
                    $data['HomeDecor'] = $this->setHomeDecor($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert LargeAppliances Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setLargeAppliances($product = [], $attributes = [])
    {
        $walmartAttr = [
            'isEnergyGuideLabelRequired', 'energyGuideLabel',
            'isEnergyStarCertified', 'isRemoteControlIncluded', 'hasCfl', 'isLightingFactsLabelRequired',
            'lightingFactsLabel', 'capacity', 'volumeCapacity/unit', 'volumeCapacity/measure', 'fuelType',
            'loadPosition', 'volts/unit', 'volts/measure', 'watts/unit', 'watts/measure', 'btu',
            'maximumRoomSize/unit', 'maximumRoomSize/measure', 'runTime/unit', 'runTime/measure',
            'cordLength/unit', 'cordLength/measure', 'isSmart', 'hasAutomaticShutoff'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Bedding Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setBedding($product = [], $attributes = [])
    {
        $walmartAttr = [
            'fabricContent/fabricContentValue',
            'fabricCareInstructions/fabricCareInstruction', 'bedSize', 'threadCount'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert HomeDecor Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setHomeDecor($product = [], $attributes = [])
    {
        $walmartAttr = [
            'rugSize', 'clockNumberType', 'curtainPanelStyle',
            'fillMaterial/fillMaterialValue', 'scent', 'threadCount', 'displayTechnology'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}