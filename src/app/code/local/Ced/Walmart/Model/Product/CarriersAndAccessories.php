<?php

class Ced_Walmart_Model_Product_CarriersAndAccessories extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert CarriersAndAccessories Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl', 'swatchImages/swatchImage/swatchVariantAttribute',
                'variantAttributeNames/variantAttributeName', 'variantGroupId', 'isPrimaryVariant',
                'fabricContent/fabricContentValue', 'isWeatherResistant',
                'fabricCareInstructions/fabricCareInstruction', 'brand', 'dimensions', 'condition', 'isLined',
                'manufacturer', 'numberOfWheels', 'modelNumber', 'handleMaterial/handleMaterialValue',
                'manufacturerPartNumber', 'gender', 'color/colorValue', 'handleType', 'ageGroup/ageGroupValue',
                'designer', 'leatherGrade', 'material/materialValue', 'pattern/patternValue',
                'character/characterValue', 'monogramLetter', 'numberOfPieces', 'zipperMaterial', 'isPersonalizable',
                'lockingMechanism', 'hardOrSoftCase', 'isMadeFromRecycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/percentageOfRecycledMaterial',
                'isWheeled', 'isFairTrade', 'capacity', 'isWaterproof'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'CasesAndBags' : {
                    $data['CasesAndBags'] = $this->setCasesAndBags($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert CasesAndBags Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setCasesAndBags($product = [], $attributes = [])
    {
        $walmartAttr = [
           'finish', 'isReusable', 'occasion/occasionValue', 'recommendedUses/recommendedUse','bagStyle','isFoldable',
            'fastenerType','numberOfCompartments', 'hasRemovableStrap','isTsaApproved','sport/sportValue',
            'maximumWeight/unit','maximumWeight/value', 'shape','screenSize/unit','screenSize/value',
            'compatibleBrands/compatibleBrand', 'compatibleDevices/compatibleDevice'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}