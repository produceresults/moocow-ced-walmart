<?php

class Ced_Walmart_Model_Product_Toy extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Toy Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
            'type' => 'simple',
            'variantid' => null,
            'variantattr' => null,
            'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'animalBreed','swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute','ageRange','minimumWeight/unit',
                'minimumWeight/measure','variantAttributeNames/variantAttributeName','variantGroupId','isPrimaryVariant',
                'fabricContent/fabricContentValue/materialName','isAdultProduct','finish','isRecyclable',
                'fabricCareInstructions/fabricCareInstruction','brand',
                'manufacturer','theme/themeValue','modelNumber','manufacturerPartNumber','gender','color/colorValue',
                'ageGroup/ageGroupValue','awardsWon/awardsWonValue','isEnergyStarCertified','animalType',
                'material/materialValue','isPowered',
                'numberOfPieces','characterValue','powerType','isRemoteControlIncluded','occasion/occasionValue',
                'isPersonalizable','isMadeFromRecycledMaterial',
                'isTravelSize','recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
                'seatingCapacity','isInflatable','isAssemblyRequired','maximumRecommendedAge/unit',
                'maximumRecommendedAge/measure','assemblyInstructions','minimumRecommendedAge/unit',
                'minimumRecommendedAge/measure','sport/sportValue','skillLevel','maximumWeight/unit',
                'maximumWeight/measure','targetAudience/targetAudienceValue','maximumSpeed/unit',
                'maximumSpeed/measure','educationalFocus/educationalFocus',
                'capacity','skinTone','volts/unit','volts/measure','vehicleType','shape','screenSize/unit',
                'screenSize/measure','displayTechnology'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['cat_id']) {
                case 'Puzzles' : {
                    $data['Puzzles'] = $this->setPuzzles($product, $attributes);
                    break;
                    }
                case 'Games' : {
                    $data['Games'] = $this->setGames($product, $attributes);
                    break;
                    }
                case 'Toy' : {
                    $data['Toy'] = $this->setToy($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert Puzzles Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setPuzzles($product = [], $attributes = [])
    {
        $walmartAttr = [
            'numberOfPieces'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Games Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setGames($product = [], $attributes = [])
    {
        $walmartAttr = [
            'batteriesRequired','batterySize','makesNoise','numberOfPlayers/minimumNumberOfPlayers',
            'numberOfPlayers/minimumNumberOfPlayers','numberOfPlayers/maximumNumberOfPlayers'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Toy Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setToy($product = [], $attributes = [])
    {
        $walmartAttr = [
            'batteriesRequired','batterySize','makesNoise','fillMaterial/fillMaterialValue'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}