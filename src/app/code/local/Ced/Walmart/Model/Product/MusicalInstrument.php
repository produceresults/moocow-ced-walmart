<?php

class Ced_Walmart_Model_Product_MusicalInstrument extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert MusicalInstrument Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute','variantGroupId',
                'variantAttributeNames/variantAttributeName','isPrimaryVariant','brand','condition','manufacturer',
                'modelNumber','manufacturerPartNumber','color/colorValue','material/materialValue',
                'numberOfPieces','isPersonalizable','isPortable','recommendedUses/recommendedUse',
                'recommendedLocations/recommendedLocation'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'SoundAndRecording' : {
                    $data['SoundAndRecording'] = $this->setSoundAndRecording($product, $attributes);
                    break;
                    }
                case 'InstrumentAccessories' : {
                    $data['InstrumentAccessories'] = $this->setInstrumentAccessories($product, $attributes);
                    break;
                    }
                case 'MusicalInstruments' : {
                    $data['MusicalInstruments'] = $this->setMusicalInstruments($product, $attributes);
                    break;
                    }
                case 'MusicCasesAndBags' : {
                    $data['MusicCasesAndBags'] = $this->setMusicCasesAndBags($product, $attributes );
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert SoundAndRecording Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setSoundAndRecording($product = [], $attributes = [])
    {
        $walmartAttr = [
            'hasSignalBooster','hasWirelessMicrophone','batteriesRequired','batterySize','isPowered','powerType',
            'isRemoteControlIncluded','audioPowerOutput','equalizerControl',
            'inputsAndOutputs/inputsAndOutput/inputOutputType',
            'inputsAndOutputs/inputsAndOutput/inputOutputQuantity','hasIntegratedSpeakers','hasBluetooth',
            'batteryLife/unit','batteryLife/measure','wirelessTechnologies/wirelessTechnology'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }


    /**
     * Insert InstrumentAccessories Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setInstrumentAccessories($product = [], $attributes = [])
    {
        $walmartAttr = [
            'hasSignalBooster','hasWirelessMicrophone','batteriesRequired',
            'pattern/patternValue',
            'batterySize','isRemoteControlIncluded','instrument/instrumentValue',
            'inputsAndOutputs/inputsAndOutput/inputOutputType',
            'inputsAndOutputs/inputsAndOutput/inputOutputQuantity',
            'displayTechnology','hasBluetooth','batteryLife/unit','batteryLife/measure',
            'wirelessTechnologies/wirelessTechnology'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert MusicalInstruments Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setMusicalInstruments($product = [], $attributes = [])
    {
        $walmartAttr = [
            'finish','hasSignalBooster','hasWirelessMicrophone','ageGroup/ageGroupValue','batteriesRequired',
            'batterySize','powerType','isPortable','recommendedUses/recommendedUse',
            'recommendedLocations/recommendedLocation',
            'audioPowerOutput','isCollectible','musicalInstrumentFamily','isAcoustic','isElectric','isFretted',
            'instrument/instrumentValue','shape','inputsAndOutputs/inputsAndOutput/inputOutputType',
            'inputsAndOutputs/inputsAndOutput/inputOutputQuantity',
            'hasIntegratedSpeakers','displayTechnology','hasBluetooth','batteryLife/unit','batteryLife/measure'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }


    /**
     * Insert MusicCasesAndBags Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setMusicCasesAndBags($product = [], $attributes = [])
    {
        $walmartAttr = [

            'fabricContent/fabricContentValue','fabricCareInstructions/fabricCareInstruction','hardOrSoftCase',
            'isWheeled','instrument/instrumentValue','shape'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}