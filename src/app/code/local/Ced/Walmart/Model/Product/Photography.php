<?php

class Ced_Walmart_Model_Product_Photography extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Photography Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute','accessoriesIncluded/accessoriesIncludedValue',
                'variantAttributeNames/variantAttributeName','variantGroupId','isPrimaryVariant','isWeatherResistant',
                'hasSignalBooster','hasWirelessMicrophone','brand','manufacturer','modelNumber',
                'manufacturerPartNumber','gender','color/colorValue','batteriesRequired','batterySize',
                'memoryCardType/memoryCardTypeValue','connections/connection','material/materialValue','numberOfPieces',
                'isPortable','cleaningCareAndMaintenance','recommendedLocations/recommendedLocation',
                'isAssemblyRequired','assemblyInstructions','isWaterproof',
                'hasTouchscreen','recordableMediaFormats/recordableMediaFormat','compatibleBrands/compatibleBrand',
                'compatibleDevices/compatibleDevice','wirelessTechnologies/wirelessTechnology'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'PhotoAccessories' : {
                    $data['PhotoAccessories'] = $this->setPhotoAccessories($product, $attributes);
                    break;
                }
                case 'CamerasAndLenses' : {
                    $data['CamerasAndLenses'] = $this->setCamerasAndLenses($product, $attributes);
                    break;
                }
            }
        }
        return $data;
    }


    /**
     * Insert Photography/PhotoAccessories Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setPhotoAccessories($product = [], $attributes = [])
    {
        $walmartAttr = [
            'fabricContent/fabricContentValue', 'condition', 'pattern/patternValue', 'isRemoteControlIncluded',
            'isMadeFromRecycledMaterial', 'occasion/occasionValue', 'hardOrSoftCase','isCordless','lightOutput/unit',
            'lightOutput/measure','maximumWeight/unit','maximumWeight/measure','capacity', 'volts/unit','volts/measure',
            'watts/unit','watts/measure','shape', 'inputsAndOutputs/inputsAndOutput/inputOutputType',
            'inputsAndOutputs/inputsAndOutput/inputOutputQuantity','displayTechnology','hasBluetooth','lightBulbType',
            'wirelessTechnologies/wirelessTechnologie'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Photography/CamerasAndLenses Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setCamerasAndLenses($product = [], $attributes = [])
    {
        $walmartAttr = [
            'ageGroup/ageGroupValue', 'powerType', 'diameter/unit', 'diameter/measure','numberOfMegapixels/unit',
            'numberOfMegapixels/measure','focalLength/measure','focalLength/unit', 'hasShoulderStrap','hasHandle',
            'magnification','fieldOfView','isFogResistant','lensDiameter/unit','lensDiameter/measure','isMulticoated',
            'shootingPrograms','shootingMode','opticalZoom','selfTimerDelay/unit','selfTimerDelay/measure',
            'hasSelfTimer','hasRemovableFlash','digitalZoom','focusType/focusTypeValue','hasRedEyeReduction',
            'minimumShutterSpeed/unit','minimumShutterSpeed/unit','lockType','maximumShutterSpeed/unit',
            'maximumShutterSpeed/measure','sensorResolution/unit','sensorResolution/measure','maximumShootingSpeed',
            'minimumAperture','hasDovetailBarSystem','hasLcdScreen','maximumAperture','hasMemoryCardSlot',
            'microphoneIncluded','hasNightVision','lensFilterType','isParfocal','flashType','filmCameraType',
            'attachmentStyle','exposureModes/exposureMode','cameraLensType','displayResolution/unit',
            'displayResolution/measure','focalRatio','lensCoating','operatingTemperature/unit',
            'operatingTemperature/measure','isLockable','lensType/lensTypeValue','screenSize/unit','screenSize/measure',
            'displayTechnology','hasFlash','standbyTime/unit','standbyTime/measure',
            'activeIngredients/activeIngredient/activeIngredientPercentage', 'inactiveIngredients/inactiveIngredient',
            'form','instructions'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}