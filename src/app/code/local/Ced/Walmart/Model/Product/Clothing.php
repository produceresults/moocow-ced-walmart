<?php

class Ced_Walmart_Model_Product_Clothing extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Clothing Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl',
                'swatchImages/swatchImage/swatchVariantAttribute', 'variantGroupId',
                'variantAttributeNames/variantAttributeName', 'isPrimaryVariant',
                'fabricContent/fabricContentValue/materialName', 'fabricCareInstructions/fabricCareInstruction',
                'brand', 'manufacturer', 'modelNumber', 'clothingSize', 'gender', 'color/colorValue',
                'ageGroup/ageGroupValue', 'clothingSizeType', 'isMaternity', 'pattern/patternValue',
                'character/characterValue', 'occasion/occasionValue', 'apparelCategory', 'isPersonalizable',
                'isMadeFromRecycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/percentageOfRecycledMaterial',
                'gotsCertification', 'season/seasonValue', 'sport/sportValue'
            ];

            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'ShirtsAndTops' : {
                    $data['ShirtsAndTops'] = $this->setShirtsAndTops($product, $attributes);
                    break;
                    }
                case 'WomensSwimsuits' : {
                    $data['WomensSwimsuits'] = $this->setWomensSwimsuits($product, $attributes);
                    break;
                    }
                case 'Bras' : {
                    $data['Bras'] = $this->setBras($product, $attributes);
                    break;
                    }

                case 'Skirts' : {
                    $data['Skirts'] = $this->setSkirts($product, $attributes);
                    break;
                    }

                case 'PantsAndShorts' : {
                    $data['PantsAndShorts'] = $this->setPantsAndShorts($product, $attributes);
                    break;
                    }

                case 'ClothingAccessories' : {
                    $data['ClothingAccessories'] = $this->setClothingAccessories($product, $attributes);
                    break;
                    }

                case 'Dresses' : {
                    $data['Dresses'] = $this->setDresses($product, $attributes);
                    break;
                    }

                case 'DressShirts' : {
                    $data['DressShirts'] = $this->setDressShirts($product, $attributes);
                    break;
                    }

                case 'Socks' : {
                    $data['Socks'] = $this->setSocks($product, $attributes);
                    break;
                    }

                case 'Panties' : {
                    $data['Panties'] = $this->setPanties($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert Clothing/ShirtsAndTops Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setShirtsAndTops($product = [], $attributes = [])
    {
        $walmartAttr = [
            'shirtSize', 'shirtNeckStyle', 'sleeveStyle'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert WomensSwimsuits Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setWomensSwimsuits($product = [], $attributes = [])
    {
        $walmartAttr = [
            'braSize', 'swimsuitStyle'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Clothing/Bras Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setBras($product = [], $attributes = [])
    {
        $walmartAttr = [
            'braSize', 'braStyle'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Clothing/Skirts Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setSkirts($product = [], $attributes = [])
    {
        $walmartAttr = [
            'waistSize/unit','waistSize/measure', 'skirtAndDressCut'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Clothing/PantsAndShorts Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setPantsAndShorts($product = [], $attributes = [])
    {
        $walmartAttr = [
            'pantSize/inseam','pantSize/waistSize/unit','pantSize/waistSize/measure','pantRise', 'pantStyle', 'pantPanelStyle'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Clothing/ClothingAccessories Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setClothingAccessories($product = [], $attributes = [])
    {
        $walmartAttr = [
            'material/materialValue'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Clothing/Dresses Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setDresses($product = [], $attributes = [])
    {
        $walmartAttr = [
            'skirtAndDressCut','dressStyle','sleeveStyle'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Clothing/DressShirts Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setDressShirts($product = [], $attributes = [])
    {
        $walmartAttr = [
            'dressShirtSize/neckSize', 'dressShirtSize/sleeveLength','collarType','sleeveStyle'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Clothing/Socks Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setSocks($product = [], $attributes = [])
    {
        $walmartAttr = [
            'sockSize', 'sockStyle'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Clothing/Panties Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setPanties($product = [], $attributes = [])
    {
        $walmartAttr = [
            'pantySize', 'pantyStyle'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}