<?php

class Ced_Walmart_Model_Product_Baby extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Baby Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
        'type' => 'simple',
        'variantid' => null,
        'variantattr' => null,
        'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];

        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'swatchImages/swatchImage/swatchImageUrl', 'swatchImages/swatchImage/swatchVariantAttribute',
                'ageRange', 'minimumWeight/measure', 'variantAttributeNames/variantAttributeName', 'variantGroupId',
                'isPrimaryVariant', 'fabricContent/fabricContentValue',
                'fabricCareInstructions/fabricCareInstruction', 'brand', 'condition', 'manufacturer', 'modelNumber',
                'manufacturerPartNumber', 'gender', 'color/colorValue', 'ageGroup', 'isReusable', 'isDisposable',
                'pattern/patternValue', 'material/materialValue', 'numberOfPieces', 'character/characterValue',
                'occasion/occasionValue', 'isPersonalizable', 'isPortable', 'isMadeFromRecycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/recycledMaterial',
                'recycledMaterialContent/recycledMaterialContentValue/percentageOfRecycledMaterial',
                'recommendedUses/recommendedUse', 'numberOfChannels', 'isFairTrade', 'maximumRecommendedAge/unit',
                'maximumRecommendedAge/measure', 'minimumRecommendedAge/unit', 'minimumRecommendedAge/measure',
                'sport/sportValue', 'maximumWeight/unit', 'maximumWeight/measure',
                'diaposableBabyDiaperType/diaposableBabyDiaperTypeValue', 'capacity', 'scent',
                'organicCertifications/organicCertification', 'screenSize/unit',  'screenSize/measure',
                'displayTechnology'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
            switch ($category['csv_cat_id']) {
                case 'ChildCarSeats' : {
                    $data['ChildCarSeats'] = $this->setChildCarSeats($product, $attributes);
                    break;
                    }
                case 'BabyClothing' : {
                    $data['BabyClothing'] = $this->setBabyClothing($product, $attributes);
                    break;
                    }
                case 'BabyFootwear' : {
                    $data['BabyFootwear'] = $this->setBabyFootwear($product, $attributes);
                    break;
                    }
                case 'Strollers' : {
                    $data['Strollers'] = $this->setStrollers($product, $attributes);
                    break;
                    }
                case 'BabyFurniture' : {
                    $data['BabyFurniture'] = $this->setBabyFurniture($product, $attributes);
                    break;
                    }
                case 'BabyToys' : {
                    $data['BabyToys'] = $this->setBabyToys($product, $attributes);
                    break;
                    }
                case 'BabyFood' : {
                    $data['BabyFood'] = $this->setBabyFood($product, $attributes);
                    break;
                    }
            }
        }
        return $data;
    }

    /**
     * Insert ChildCarSeats Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setChildCarSeats($product = [], $attributes = [])
    {
        $walmartAttr = [
            'childCarSeatType', 'facingDirection', 'forwardFacingMinimumWeight/unit',
            'forwardFacingMinimumWeight/measure', 'forwardFacingMaximumWeight/unit',
            'forwardFacingMaximumWeight/measure', 'rearFacingMinimumWeight/unit',
            'rearFacingMinimumWeight/measure', 'rearFacingMaximumWeight/unit',
            'rearFacingMaximumWeight/measure', 'hasLatchSystem'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert BabyClothing Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setBabyClothing($product = [], $attributes = [])
    {
        $walmartAttr = [
            'color/colorValue', 'apparelCategory', 'season/seasonValue', 'babyClothingSize'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert BabyFootwear Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setBabyFootwear($product = [], $attributes = [])
    {
        $walmartAttr = [
            'shoeCategory', 'shoeSize', 'shoeWidth', 'shoeStyle', 'shoeClosure'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert Strollers Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setStrollers($product = [], $attributes = [])
    {
        $walmartAttr = [
            'seatingCapacity', 'strollerType'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert BabyFurniture Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setBabyFurniture($product = [], $attributes = [])
    {
        $walmartAttr = [
            'collection', 'finish', 'homeDecorStyle', 'isWheeled', 'isFoldable', 'isAssemblyRequired',
            'assemblyInstructions', 'mattressFirmness', 'fillMaterial/fillMaterialValue', 'bedSize', 'shape'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert BabyToys Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setBabyToys($product = [], $attributes = [])
    {
        $walmartAttr = [
            'animalBreed', 'ageRange', 'theme/themeValue', 'batteriesRequired', 'batterySize',
            'awardsWon/awardsWonValue', 'animalType', 'isPowered', 'powerType', 'isRemoteControlIncluded',
            'makesNoise', 'fillMaterial/fillMaterialValue', 'educationalFocus/educationalFocus'
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

    /**
     * Insert BabyFood Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @return string|[]
     */
    public function setBabyFood($product = [], $attributes = [])
    {
        $walmartAttr = [
            'flavor', 'nutrientContentClaims/nutrientContentClaim', 'servingSize', 'servingsPerContainer',
            'isBabyFormulaLabelRequired', 'babyFormulaLabel', 'isChildrenUnder2LabelRequired',
            'childrenUnder2Label', 'isChildrenUnder4LabelRequired', 'childrenUnder4Label',
            'fluidOuncesSupplying100Calories', 'calories', 'caloriesFromFat/unit', 'caloriesFromFat/measure',
            'totalFat/unit', 'totalFat/measure', 'totalFatPercentageDailyValue/unit',
            'totalFatPercentageDailyValue/measure', 'totalCarbohydrate/unit', 'totalCarbohydrate/measure',
            'totalCarbohydratePercentageDailyValue/value', 'totalCarbohydratePercentageDailyValue/measure',
            'nutrients/nutrient/nutrientName', 'nutrients/nutrient/nutrientAmount',
            'nutrients/nutrient/nutrientPercentageDailyValue',
        ];
        $data = [];

        if (!empty($product) && !empty($attributes)) {
            foreach ($walmartAttr as $attr) {
                if (!empty($product[$attributes[$attr]])) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }
        }
        return $data;
    }

}