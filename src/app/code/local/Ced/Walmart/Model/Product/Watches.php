<?php

class Ced_Walmart_Model_Product_Watches extends Ced_Walmart_Model_Product_Base
{
    /**
     * Insert Watches Category Data
     * @param string|[] $product
     * @param string|[] $attributes
     * @param string|[] $category
     * @param string|[] $type
     * @return string|[]
     */
    public function setData(
        $product,
        $attributes = [],
        $category = [],
        $type = [
            'type' => 'simple',
            'variantid' => null,
            'variantattr' => null,
            'isprimary' => '0'
        ]
    ) {
        $this->productObject = $product;
        $product = $product->toArray();

        $product['blank'] = '';
        $attributes['variantGroupId'] = 'blank';
        $attributes['variantAttributeNames/variantAttributeName'] = 'blank';
        $attributes['isPrimaryVariant'] = 'blank';
        $this->attributes = $attributes;
        $product = $this->extractSelectValues($product);

        if (isset($type['type'],$type['variantid'], $type['variantattr']) && !empty($type['variantid'])) {
            $attributes['variantGroupId'] = 'variantGroupId';
            $attributes['variantAttributeNames/variantAttributeName'] = 'variantAttributeNames/variantAttributeName';
            $attributes['isPrimaryVariant'] = 'isPrimaryVariant';

            $product['variantGroupId'] = $type['variantid'];
            $product['variantAttributeNames/variantAttributeName'] = $type['variantattr'];
            $product['isPrimaryVariant'] = $type['isprimary'];
        }

        $data = [];

        if (!empty($product) && !empty($attributes) && !empty($category)) {
            $walmartAttr = [
                'watchBandMaterial/watchBandMaterialValue', 'metal', 'watchCaseShape', 'plating',
                'swatchImages/swatchImage/swatchImageUrl', 'swatchImages/swatchImage/swatchVariantAttribute',
                'watchStyle/watchStyleValue', 'gemstone/gemstoneValue', 'variantAttributeNames/variantAttributeName',
                'variantGroupId', 'gemstoneShape', 'isPrimaryVariant', 'carats/unit', 'carats/value',
                'fabricContent/fabricContentValue', 'isWeatherResistant', 'finish', 'brand', 'manufacturer',
                'theme/themeValue', 'modelNumber', 'manufacturerPartNumber', 'gender', 'color/colorValue',
                'ageGroup/ageGroupValue', 'batteriesRequired', 'batterySize', 'material/materialValue',
                'pattern/patternValue', 'character/characterValue', 'powerType', 'occasion/occasionValue',
                'isPersonalizable', 'isWaterproof', 'displayTechnology'
            ];
            foreach ($walmartAttr as $attr) {
                if (isset($product[$attributes[$attr]]) && !empty($product[$attributes[$attr]]) ) {
                    $data = array_merge_recursive($data, $this->generateArray($attr, $product[$attributes[$attr]]));
                }
            }

        return $data;
    }

    return $data;
    }

}