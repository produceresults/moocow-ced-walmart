<?php

class Ced_Walmart_Model_Product_Base 
{
    /**
     * Current Product Object
     * @var $productOject
     */
    public $productOject;

    /**
     * All Walmart-Magento Mapped Attributes
     * @var $attributes
     */
    public $attributes;
    

    /**
     * Extract Magento Select Type Attribute Value
     * @param string|[] $product
     * @return string|[]
     */
    public function extractSelectValues($product = [])
    {
        foreach ($this->attributes as $value) {
            try {
                if ($value != 'blank') {
                  /*  echo get_class($this->productObject->getResource()->getAttribute($value));
                    die;*/
                    $attr = $this->productObject->getResource()->getAttribute($value);                    
                    if($attr instanceof Mage_Catalog_Model_Resource_Eav_Attribute)
                    if ($attr->getSourceModel($attr)) {
                        $product[$value] =
                            $attr->getSource()->getOptionText($this->productObject->getData($value));
                        if ($product[$value] == 'No') {
                            $product[$value] = 'false';
                        } elseif ($product[$value] == 'Yes') {
                            $product[$value] = 'true';
                        }
                    }
                }

            }
            catch (\Exception $e) {
                print_r($e->getMessage());
            }

        }

        return $product;
    }

    /**
     * Generate Array
     * @param null $name
     * @param string|[] $value
     * @return array|bool
     */
    public function generateArray($name = null, $value = null)
    {
        try {
            if ($name != null) {
                $attributeArray = explode("/", $name);
                if (count($attributeArray) == 1) {
                    $returnArray = [
                        $attributeArray[0] => (string)$value,
                    ];
                    return $returnArray;
                }
                if (count($attributeArray) == 2) {
                    if (is_array($value)) {
                        $returnArray[$attributeArray[0]]['_attribute'] = [];
                        foreach ($value as $key => $val) {
                            $returnArray[$attributeArray[0]]['_value'][$key][$attributeArray[1]] = (string)$val;
                        }

                    } else {
                        $returnArray = [$attributeArray[0] => [
                            $attributeArray[1] => (string)$value,
                        ]];

                    }
                    return $returnArray;

                }
                if (count($attributeArray) == 3) {
                    $returnArray = [$attributeArray[0] => [
                        $attributeArray[1] => [
                            $attributeArray[2] => (string)$value,
                        ],
                    ]];
                    return $returnArray;
                }
            }
            return false;
        }
        catch (\Exception $e) {
            Mage::log('Attributes Model : ' . $e->getMessage(),null,'walmart.log');
        }

    }
}