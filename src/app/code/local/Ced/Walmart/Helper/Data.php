<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ced_Walmart_Helper_Data extends Mage_Core_Helper_Abstract{

    const GET_ORDERS_SUB_URL = 'v3/orders';
    const GET_ORDERS_REFUND_SUB_URL = 'v2/orders';
    const GET_ORDERS_RELEASED_SUB_URL = 'v3/orders/released';
    const GET_ITEMS_SUB_URL = 'v2/items';
    const GET_FEEDS_SUB_URL = 'v2/feeds';
    const GET_FEED_SUB_URL = 'v2/feeds/feeditems/';
    const GET_FEEDS_ITEMS_SUB_URL = 'v2/feeds?feedType=item';
    const GET_FEEDS_INVENTORY_SUB_URL = 'v2/feeds?feedType=inventory';
    const GET_FEEDS_PRICE_SUB_URL = 'v2/feeds?feedType=price';
    const GET_INVENTORY_SUB_URL = 'v2/inventory';
    const GET_REPORTS_SUB_URL = 'v2/getReport';
    const UPDATE_PRICE_SUB_URL = 'v2/prices';
    const WALMART_API_URL = 'https://marketplace.walmartapis.com/';

    /**
     * File Manager
     * @var $fileIo
     */
    public $fileIo;

    /**
     * Xml Parser
     * @var $xml
     */
    public $xml;

    /**
     * Date/Time
     * @var $dateTime
     */
    public $dateTime;

    /**
     * Api Base Url
     * @var string $apiUrl
     */
    public $apiUrl;

    /**
     * Api Consumer Id
     * @var string $apiConsumerId
     */
    public $apiConsumerId;

    /**
     * Api Consumer Channel Id
     * @var string $apiConsumerChannelId
     */
    public $apiConsumerChannelId;

    /**
     * Api Private Key
     * @var string $apiPrivateKey
     */
    public $apiPrivateKey;

    /**
     * Api Signature Class Object
     * @var Mage_Core_Helper_Abstract
     */
    public $apiSignature;

    /**
     * Walmart Helper
     * @var \\Ced\Walmart\Helper\Walmart
     */
    public $walmartHelper;

    /**
     * Debug Mode Check
     * @var boolean $debugMode
     */
    public $debugMode;

    public function __construct()
    {
        $this->fileIo = new Varien_Io_File();
        $this->apiSignature = Mage::Helper('walmart/signature');
        /*$this->apiUrl= Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_apiurl');*/
        $this->apiConsumerId = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_customerId');
        $this->apiPrivateKey = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_privatekey');
        $this->apiConsumerChannelId = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_channelId');
        $date = date_create();
        $this->apiSignature->timestamp = date_timestamp_get($date)*1000;
        $this->debugMode = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_debug_mode');
        // $this->apiSignature->timestamp = $this->apiSignature->getMilliseconds();
    }

    /**
     * Post Request on https://marketplace.walmartapis.com/     * @param $url
     * @param array $params
     * @return bool|mixed|string
     */
    public function postRequest($url, $params = [])
    {
        try {
            $signature = $this->apiSignature->getSignature($url, 'POST',$this->apiSignature->timestamp);
            $body = false;
            $url =  self::WALMART_API_URL.$url;
            $phpFlag = false;
            if (isset($params['file'])) {
                if (version_compare(phpversion(), '5.5.0', '>=') === true) {
                    $body['file'] = new \CURLFile($params['file'], 'application/xml');
                } else {
                    $phpFlag = true;
                    $file = trim($params['file']);
                    $body['file'] = "@$file;type=application/xml";
                }
            } elseif (isset($params['data'])) {
                $body = $params['data'];
            }
            $headers = [];
            $headers[] = "WM_SVC.NAME: Walmart Marketplace";
            $headers[] = "WM_QOS.CORRELATION_ID: " . (string)mt_rand(1000000000000000,9999999999999999);
            $headers[] = "WM_SEC.TIMESTAMP: " . $this->apiSignature->timestamp;
            $headers[] = "WM_SEC.AUTH_SIGNATURE: " . trim($signature);
            $headers[] = "WM_CONSUMER.ID: " .  $this->apiConsumerId;

            if (isset($params['file']) && !empty($params['file'])) {
                $headers[] = "Content-Type: multipart/form-data;";
            } elseif (isset($params['data']) && !empty($params['data'])) {
                $headers[] = "Content-Type: application/xml";
            } else {
                $headers[] = "Content-Type: application/json";
            }
            $headers[] = "Accept: application/xml";
            if (isset($params['headers']) && !empty($params['headers'])) {
                $headers[] = $params['headers'];
            }
            $headers[] = "HOST: marketplace.walmartapis.com";
            $ch = curl_init();
            if ($phpFlag) {
                curl_setopt ($ch, CURLOPT_SAFE_UPLOAD, false);
            }
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $serverOutput = curl_exec($ch);
            $curlError = curl_error($ch);
            curl_close($ch);
            $this->logger(
                $url,
                $params,
                $headers,
                $serverOutput,
                $curlError,
                $desc = "walmart->Helper->Data.php : getRequest()"
            );
            return $serverOutput;

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get Request on https://marketplace.walmartapis.com/
     * @param string $url
     * @param string|[] $params
     * @return string
     */
    public function getRequest($url, $params = [])
    {
        try {
            $signature = $this->apiSignature->getSignature($url, 'GET', $this->apiSignature->timestamp);
            $url = self::WALMART_API_URL . $url;
            $timestamp = $this->apiSignature->timestamp;
            $consumerId = $this->apiConsumerId;
            if (isset($params['timestamp'], $params['signature'], $params['consumer_id'])) {
                $timestamp = $params['timestamp'];
                $signature = $params['signature'];
                $consumerId = $params['consumer_id'];
                $url = $params['url'];
            }
            $headers = [];
            $headers[] = "WM_SVC.NAME: Walmart Marketplace";
            $headers[] = "WM_QOS.CORRELATION_ID: " . (string)mt_rand(1000000000000000, 9999999999999999);
            $headers[] = "WM_SEC.TIMESTAMP: " . $timestamp;
            $headers[] = "WM_SEC.AUTH_SIGNATURE: " . trim($signature);
            $headers[] = "WM_CONSUMER.ID: " . $consumerId;
            $headers[] = "Content-Type: application/json";
            $headers[] = "Accept: application/xml";
            if (isset($params['headers']) && !empty($params['headers'])) {
                $headers[] = $params['headers'];
            }
            $headers[] = "HOST: marketplace.walmartapis.com";
            /*echo $url;
            die;*/
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            if (isset($params['validate']) && $params['validate']) {
                curl_setopt($ch, CURLOPT_HEADER, 1);
            } else {
                curl_setopt($ch, CURLOPT_HEADER, 0);
            }
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $serverOutput = curl_exec($ch);
            $curlError = curl_error($ch);
            curl_close($ch);
            $this->logger(
                $url,
                $params,
                $headers,
                $serverOutput,
                $curlError,
                $desc = "walmart->Helper->Data.php : getRequest()"
            );
            return $serverOutput;

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Delete Request on https://marketplace.walmartapis.com/
     * @param string $url
     * @param string|[] $params
     * @return string
     */
    public function deleteRequest($url, $params = [])
    {
        try {
            $signature = $this->apiSignature->getSignature($url, 'DELETE',$this->apiSignature->timestamp);
            $url = self::WALMART_API_URL.$url;

            $headers = [];
            $headers[] = "WM_SVC.NAME: Walmart Marketplace";
            $headers[] = "WM_QOS.CORRELATION_ID: " .(string)mt_rand(1000000000000000,9999999999999999);
            $headers[] = "WM_SEC.TIMESTAMP: " . $this->apiSignature->timestamp;
            $headers[] = "WM_SEC.AUTH_SIGNATURE: " . $signature;
            $headers[] = "WM_CONSUMER.ID: " .  $this->apiConsumerId;
            $headers[] = "Content-Type: application/json";
            $headers[] = "Accept: application/xml";
            if (isset($params['headers']) && !empty($params['headers'])) {
                $headers[] = $params['headers'];
            }
            $headers[] = "HOST: marketplace.walmartapis.com";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $serverOutput = curl_exec($ch);
            $curlError = curl_error($ch);
            curl_close($ch);
            $this->logger(
                $url,
                $params,
                $headers,
                $serverOutput,
                $curlError,
                $desc = "walmart->Helper->Data.php : getRequest()"
            );
            return $serverOutput;

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Create Product for Walmart
     * @param array $ids
     * @return bool|mixed|string
     */
    public function createProductOnWalmart($ids)
    {
        $ids = $this->validateProducts($ids);
        if(count($ids) > 0 )
        {
            $currency = Mage::app()->getStore()->getBaseCurrencyCode();
            $productToUpload = [
                'MPItemFeed' => [
                    '_attribute' => [
                        'xmlns' => 'http://walmart.com/',
                        'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
                        'xsi:schemaLocation' => 'http://walmart.com/ MPItem.xsd',
                    ],
                    '_value' => [
                        0 => [
                            'MPItemFeedHeader' => [
                                'version' => '2.1',
                                'requestId' =>(string)$this->apiSignature->timestamp,
                                'requestBatchId' => (string)$this->apiSignature->timestamp,
                            ],
                        ]

                    ],
                ]

            ];

            $key = 1;
            foreach ($ids as $id) {

                if (isset($id['parentid'])) {
                    $category = $this->getWalmartCategory($id['parentid']);
                } else {
                    $category = $this->getWalmartCategory($id['id']);
                }
                $product = Mage::getModel('catalog/product')
                    ->load($id['id']);
                $price = $this->getWalmartPrice($product);
                $productArray = $product->toArray();
                if (isset($id['parentid'])) {
                    $attributes = $this->getWalmartAttributes($id['parentid'], [
                        'required' => false, 'mapped' => true, 'validation' => false
                    ]);
                } else {
                    $attributes = $this->getWalmartAttributes($id['id'], [
                        'required' => false, 'mapped' => true, 'validation' => false
                    ]);
                }
              if($category['csv_parent_id']) {
                    $indexOfcat = 'csv_parent_id';
                } else {
                    $indexOfcat = 'csv_cat_id';
                }

                if(isset($productArray[$attributes['productIdentifiers/productIdentifier/productId']]) &&  $productArray[$attributes['productIdentifiers/productIdentifier/productId']])
                {
                    $ProductIdentifier =[

                        'productIdentifier' => [
                            'productIdType' =>
                                'UPC',
                            'productId' =>
                                trim($productArray[$attributes['productIdentifiers/productIdentifier/productId']]),
                        ]

                    ];
                }else{

                    $ProductIdentifier =[

                        'productIdentifier' => [
                            'productIdType' =>
                                'EAN',
                            'productId' =>
                                trim($productArray['ean']),
                        ]

                    ];


                }
                $uploadType = 'MPItemUpdate';
                if ($id['status'] == false) {
                    $uploadType = 'MPItem';
                }
                $productToUpload['MPItemFeed']['_value'][$key][$uploadType] = [
                    'sku' => $productArray['sku'],
                    'Product' => [
                        'productName' => htmlspecialchars($productArray[$attributes['productName']]),
                        'longDescription' =>
                            '<![CDATA[' . substr($productArray[$attributes['longDescription']], 0, 3700) . ']]>',
                        'shelfDescription' => '<![CDATA[' . $productArray[$attributes['shelfDescription']] . ']]>',
                        'shortDescription' => '<![CDATA[' . $productArray[$attributes['shortDescription']] . ']]>',
                        'mainImage' => [
                            //'mainImageUrl' => $product[$attributes['mainImage/mainImageUrl']],
                            'mainImageUrl' =>
                                Mage::getStoreConfig('web/unsecure/base_url').'media/catalog/product'.$productArray[$attributes['mainImage/mainImageUrl']],
                            'altText' => substr(htmlspecialchars($productArray[$attributes['productName']]), 0, 145),
                        ],
                        'additionalAssets' => $this->prepareAdditionalAssets($product->getMediaGalleryImages()),
                        'productIdentifiers' => $ProductIdentifier,
                        'productTaxCode' => isset($productArray[$attributes['productTaxCode']]) ?  $productArray[$attributes['productTaxCode']] :  Mage::getStoreConfig('walmart_configuration/productinfo_map/walmart_tax_code'),
                        $category[$indexOfcat] => Mage::getModel('walmart/product_' . $category[$indexOfcat])
                            ->setData( $product, $attributes, $category, $id ),
                    ],
                    'price' => [
                        'currency' => $currency,
                        'amount' => $price['splprice']
                    ],
                    'minAdvertisedPrice' => [
                        'currency' => $currency,
                        'amount' => $price['price']
                    ],
                    'shippingWeight' => [
                        'value' => $productArray[$attributes['shippingWeight/value']],
                        'unit' => 'LB',
                    ],
                ];
                $key += 1;
            }

            $path = $this->createDir('walmart', 'var');
            // $xml = Mage::getModel('walmart/generator');
            $path = $path['path']. '/' . 'MPProduct.xml';
            // $xml->arrayToXml($productToUpload)->save($path);
            // $handle = fopen($path, 'w') or die('Cannot open file: '. $path);
            // $data = $xml->__toString();
            // fwrite($handle, htmlspecialchars_decode($data));
            $response =$this->postRequest(self::GET_FEEDS_ITEMS_SUB_URL, [ 'file' => $path] );
            $this->updateInventoryOnWalmart($ids);
            $cpPath =  $this->createDir('walmart', 'media');
            $cpPath = $cpPath['path'].'/'.'MPProduct_'.$this->apiSignature->timestamp.'.xml';
            $this->fileIo->cp($path, $cpPath);
            $response = $this->responseParse($response, 'item', $cpPath);

            return $response;
        }
        return false;

    }

    /**
     * Prepare Additional Assets
     * @param Object $productImages
     * @return string|[]
     */
    public function prepareAdditionalAssets($productImages)
    {
        if ($productImages->getSize() > 0) {
            $additionalAssets = [
                '_attribute' => [],
            ];
            $count = 0;
            foreach ($productImages as $image) {
                $additionalAssets['_value'][$count] = [
                    'additionalAsset' => [
                        'assetUrl' =>  $image->getUrl(),
                    ],
                ];
                $count += 1;
            }
            return $additionalAssets;
        }
        return [];
    }

    /**
     * Update Price On Walmart
     * //$timeStamp = (string)$this->dateTime->gmtDate('Y-m-j\TH:m:s\Z');
     * @param string|[] $ids
     * @return bool
     */
    public function updatePriceOnWalmart($ids = null)
    {
        $timeStamp = (string)$this->apiSignature->timestamp;
        $priceArray = [
            'PriceFeed' => [
                '_attribute' => [
                    'xmlns:gmp' => "http://walmart.com/",
                ],
                '_value' => [
                    0 => [
                        'PriceHeader' => [
                            'version' => '1.5',
                        ],
                    ],
                ]
            ]
        ];
        $currency = Mage::app()->getStore()->getBaseCurrencyCode();
        if ($ids == null) {
            $categoryIds = $this->getMappedCategories();
            $categoryIds = $categoryIds->getData();
            if (count($categoryIds) > 0) {
                $key = 0;
                $categoryFactory = Mage::getModel('catalog/category');
                foreach ($categoryIds as $categoryId) {
                    $category = $categoryFactory->load($categoryId['entity_id']);
                    $productCollection = $category->getProductCollection()->addAttributeToSelect(
                        ['price', 'sku'])->load();
                    foreach ($productCollection as $product) {
                        if ($product->getTypeId() == 'configurable') {
                            $productType = $product->getTypeInstance();
                            $configurableProducts = $productType->getUsedProducts($product);
                            $price = $this->getWalmartPrice($configurableProducts);
                            foreach ($configurableProducts as $configurableProduct) {
                                $key += 1;

                                $priceArray['PriceFeed']['_value'][$key] = [
                                    'Price' => [
                                        'itemIdentifier' => [
                                            'sku' => $configurableProduct->getSku()
                                        ],
                                        'pricingList' => [
                                            'pricing' => [
                                                'currentPrice' => [
                                                    'value' => [
                                                        '_attribute' => [
                                                            'currency' => $currency,
                                                            'amount' => $price['splprice']
                                                        ],
                                                        '_value' => [

                                                        ]
                                                    ]
                                                ],
                                                'currentPriceType' => 'BASE',
                                                'comparisonPrice' => [
                                                    'value' => [
                                                        '_attribute' => [
                                                            'currency' => $currency,
                                                            'amount' => $price['price']
                                                        ],
                                                        '_value' => [

                                                        ]
                                                    ]
                                                ],
                                            ]
                                        ]
                                    ]
                                ];
                            }
                        } else {
                            $key += 1;
                            $price = $this->getWalmartPrice($product);
                            $priceArray['PriceFeed']['_value'][$key] = [
                                'Price' => [
                                    'itemIdentifier' => [
                                        'sku' => $product->getSku()
                                    ],
                                    'pricingList' => [
                                        'pricing' => [
                                            'currentPrice' => [
                                                'value' => [
                                                    '_attribute' => [
                                                        'currency' => $currency,
                                                        'amount' => $price['splprice']
                                                    ],
                                                    '_value' => [

                                                    ]
                                                ]
                                            ],
                                            'currentPriceType' => 'BASE',
                                            'comparisonPrice' => [
                                                'value' => [
                                                    '_attribute' => [
                                                        'currency' => $currency,
                                                        'amount' => $price['price']
                                                    ],
                                                    '_value' => [

                                                    ]
                                                ]
                                            ],
                                        ]
                                    ]
                                ]
                            ];
                        }
                    }
                }

            }
        } else {
            $key = 0;
            foreach ($ids as $id) {
                $product = Mage::getModel('catalog/product')
                    ->load($id);
                if ($product->getVisibility() == 4) {
                    if ($product->getTypeId() == 'configurable') {
                        $productType = $product->getTypeInstance();
                        $products = $productType->getUsedProducts($product);
                        foreach ($products as $product) {
                            $key += 1;
                            $price = $this->getWalmartPrice($product);
                            $priceArray['PriceFeed']['_value'][$key] = [
                                'Price' => [
                                    'itemIdentifier' => [
                                        'sku' => $product->getSku()
                                    ],
                                    'pricingList' => [
                                        'pricing' => [
                                            'currentPrice' => [
                                                'value' => [
                                                    '_attribute' => [
                                                        'currency' => $currency,
                                                        'amount' => $price['splprice']
                                                    ],
                                                    '_value' => [

                                                    ]
                                                ]
                                            ],
                                            'currentPriceType' => 'BASE',
                                            'comparisonPrice' => [
                                                'value' => [
                                                    '_attribute' => [
                                                        'currency' => $currency,
                                                        'amount' => $price['price']
                                                    ],
                                                    '_value' => [

                                                    ]
                                                ]
                                            ],
                                        ]
                                    ]
                                ]
                            ];
                        }
                    } elseif ($product->getTypeId() == 'simple') {
                        $key += 1;
                        $price = $this->getWalmartPrice($product);
                        $priceArray['PriceFeed']['_value'][$key] = [
                            'Price' => [
                                'itemIdentifier' => [
                                    'sku' => $product->getSku()
                                ],
                                'pricingList' => [
                                    'pricing' => [
                                        'currentPrice' => [
                                            'value' => [
                                                '_attribute' => [
                                                    'currency' => $currency,
                                                    'amount' => $price['splprice']
                                                ],
                                                '_value' => [

                                                ]
                                            ]
                                        ],
                                        'currentPriceType' => 'BASE',
                                        'comparisonPrice' => [
                                            'value' => [
                                                '_attribute' => [
                                                    'currency' => $currency,
                                                    'amount' => $price['price']
                                                ],
                                                '_value' => [

                                                ]
                                            ]
                                        ],
                                    ]
                                ]
                            ]
                        ];
                    }
                }

            }

        }

        $path = Mage::getBaseDir("var").DS."walmart".DS;
        $path = $path . 'PriceFeed.xml';
        $xml = Mage::getModel('walmart/generator');
        $xml->arrayToXml($priceArray)->save($path);
        $response = $this->postRequest(self::GET_FEEDS_PRICE_SUB_URL, ['file' => $path]);
        $cpPath =  $this->createDir('walmart', 'media');
        $cpPath = $cpPath['path'].'/'.'PriceFeed_'.$timeStamp.'.xml';
        $this->fileIo->cp($path, $cpPath);
        $response = $this->responseParse($response, 'price', $cpPath);
        return $response;
    }


    /**
     * Update Inventory On Walmart
     * @param string|[] $ids
     * @return bool
     */
    public function updateInventoryOnWalmart($ids = null)
    {

        $inventoryArray = [
            'InventoryFeed' => [
                '_attribute' => [
                    'xmlns' => "http://walmart.com/",
                ],
                '_value' => [
                    0 => [
                        'InventoryHeader' => [
                            'version' => '1.4',
                        ],
                    ],
                ]
            ]
        ];
        $timeStamp = (string)$this->apiSignature->timestamp;
        $stockState = $stock = Mage::getModel('cataloginventory/stock_item');
        $fulfillmentLagTime = Mage::getStoreConfig('walmart_configuration/productinfo_map/walmart_fullfilment_lag_time');
        if(is_null($fulfillmentLagTime)){
            $fulfillmentLagTime = '1';
        }
        if ($ids == null) {
            $categoryIds = $this->getMappedCategories();

            $categoryIds = $categoryIds->getData();
            if (count($categoryIds) > 0) {
                $key = 0;
                $categoryFactory = Mage::getModel('catalog/category');
                foreach ($categoryIds as $categoryId) {
                    $category = $categoryFactory->load($categoryId['entity_id']);
                    $productCollection = $category->getProductCollection()->addAttributeToSelect(
                        ['entity_id', 'sku', 'quantity_and_stock_status'])->load();
                    foreach ($productCollection as $product) {
                        if ($product->getTypeId() == 'configurable') {
                            $productType = $product->getTypeInstance();
                            $configurableProducts = $productType->getUsedProducts($product);
                            foreach ($configurableProducts as $configurableProduct) {
                                $key += 1;
                                $inventoryArray['InventoryFeed']['_value'][$key] = [
                                    'inventory' => [
                                        'sku' => $configurableProduct->getSku(),
                                        'quantity' => [
                                            'unit' => 'EACH',
                                            'amount' =>  (string)$stockState->getStockQty($configurableProduct->getId(),
                                                $configurableProduct->getStore()->getWebsiteId()),
                                        ],
                                        'fulfillmentLagTime' => $fulfillmentLagTime,
                                    ]
                                ];
                            }
                        } else {

                            $stock =$stockState->loadByProduct($product);
                            $key += 1;
                            $inventoryArray['InventoryFeed']['_value'][$key] = [
                                'inventory' => [
                                    'sku' =>  $product->getSku(),
                                    'quantity' => [
                                        'unit' => 'EACH',
                                        'amount' => (string)$stock->getQty()

                                    ],
                                    'fulfillmentLagTime' => $fulfillmentLagTime,
                                ]
                            ];
                        }
                    }
                }

            }
        } else {
            $key = 0;

            foreach ($ids as $id) {

                $product = Mage::getModel('catalog/product')->load($id['id']);

                if ($product->getVisibility() == 4) {
                    if ($product->getTypeId() == 'configurable') {
                        $productType = $product->getTypeInstance();
                        $products = $productType->getUsedProducts($product);
                        foreach ($products as $product) {
                            $key += 1;
                            $inventoryArray['InventoryFeed']['_value'][$key] = [
                                'inventory' => [
                                    'sku' =>  $product->getSku(),
                                    'quantity' => [
                                        'unit' => 'EACH',
                                        'amount' =>  (string)$stockState->getStockQty($product->getId(),
                                            $product->getStore()->getWebsiteId()),
                                    ],
                                    'fulfillmentLagTime' => '1',
                                ]
                            ];
                        }
                    } elseif ($product->getTypeId() == 'simple') {
                        $key += 1;
                        $stock =$stockState->loadByProduct($product);
                        $inventoryArray['InventoryFeed']['_value'][$key] = [
                            'inventory' => [
                                'sku' =>  $product->getSku(),
                                'quantity' => [
                                    'unit' => 'EACH',
                                    'amount' =>  (string)$stock->getQty()

                                ],
                                'fulfillmentLagTime' => '1',
                            ]
                        ];
                    }
                }

            }
        }
        $path = Mage::getBaseDir("var").DS."walmart".DS;
        $path = $path . 'InventoryFeed.xml';
        $xml = Mage::getModel('walmart/generator');
        $xml->arrayToXml($inventoryArray)->save($path);
        $response = $this->postRequest(self::GET_FEEDS_INVENTORY_SUB_URL, ['file' => $path]);
        $cpPath =  $this->createDir('walmart', 'media');
        $cpPath = $cpPath['path'].'/'.'InventoryFeed_'.$this->apiSignature->timestamp.'.xml';
        $this->fileIo->cp($path, $cpPath);
        $response = $this->responseParse($response, 'inventory', $cpPath);
        return $response;
    }

    /**
     * Acknowledge Order
     * @param string $purchaseOrderId
     * @param string $subUrl
     * @return string
     * @link  https://developer.walmartapis.com/#acknowledging-orders
     */
    public function acknowledgeOrder($purchaseOrderId , $subUrl = self::GET_ORDERS_SUB_URL)
    {
        $response = $this->postRequest($subUrl.'/'.$purchaseOrderId.'/acknowledge',
            [
                'headers' => 'WM_CONSUMER.CHANNEL.TYPE: ' . $this->apiConsumerChannelId,
            ]
        );
        try {
            $response = Mage::helper('core')->jsonDecode($response);
            if (isset($response['error'])) {
                throw new \Exception();
            }
            return $response;
        }
        catch(\Exception $e){
            return false;
        }

    }

    /**
     * Get a Order
     * @param string $purchaseOrderId
     * @param string $subUrl
     * @return array|string
     */
    public function getOrder($purchaseOrderId, $subUrl = self::GET_ORDERS_SUB_URL )
    {
        $response = $this->getRequest($subUrl . '?purchaseOrderId=' . $purchaseOrderId,
            ['headers' => 'WM_CONSUMER.CHANNEL.TYPE: ' . $this->apiConsumerChannelId]);
        try {
            $response = Mage::helper('core')->jsonDecode($response);
            if (isset($response['error'])) {
                throw new \Exception();
            }
            return $response;
        }
        catch(\Exception $e) {
            if ($this->debugMode) {
                Mage::log("Walmart: getOrder() " . $purchaseOrderId . ": Response : \n " .
                    var_export($response, true), null, "walmart.log", true
                );
            }
            return false;
        }
    }

    /**
     * Get Orders
     * @param string|[] $params - date in yy-mm-dd
     * @param string $subUrl
     * @return string
     * @link  https://developer.walmartapis.com/#get-all-orders
     */
    public function getOrders($params = ['createdStartDate' => '2016-01-01'], $subUrl = self::GET_ORDERS_SUB_URL)
    {
        $queryString = empty($params) ? '' : '?' . http_build_query($params);
        $response = $this->getRequest($subUrl . $queryString,
            ['headers' => 'WM_CONSUMER.CHANNEL.TYPE: ' . $this->apiConsumerChannelId]);
        try {
            $response = Mage::helper('core')->jsonDecode($response);
            if (isset($response['error'])) {
                throw new \Exception();
            }
            return $response;
        }
        catch(\Exception $e) {
            if ($this->debugMode) {
                Mage::log("Walmart: getOrders() : Response : \n " .
                    var_export($response, true), null, "walmart.log", true);
            }
            return false;
        }

    }

    public function validateProducts($ids = [])
    {
        $callToGetItem = '1';
        if(isset($ids['callToGetItem'])){
          $callToGetItem = $ids['callToGetItem'];
        }
        $validatedProducts = [];
        foreach ($ids as $id) {
            $product = Mage::getModel('catalog/product')
                ->load($id);
            if ($product->getVisibility() == 4) {
                if ($product->getTypeId() == 'configurable') {
                    $sku = $product->getSku();
                    $parentId = $product->getId();
                    $productType = $product->getTypeInstance();
                    $products = $productType->getUsedProducts($product);
                    $attributes = $productType->getConfigurableAttributesAsArray($product);
                    
                    foreach ($attributes as $attribute) {
                        $variantattr[] = $attribute['attribute_code'];
                    } 
                    $walmartVariantAttr = Mage::getModel('walmart/walmartattribute')->getCollection()
                                ->addFieldToSelect('walmart_attribute_name')
                                ->addFieldToFilter('magento_attribute_code', ['in' => $variantattr])
                                ->getData();
                    if (count($walmartVariantAttr) !== count($variantattr)) {
                        $product->setData('walmart_product_validation', 'Variant Attributes:
                                 Unmapped/Not Present in this category');
                        $product->save();
                        continue;
                    }
                    $walmartVariantAttrArray = [];
                    foreach ($walmartVariantAttr as $value) {
                                $attribute = explode('/', $value['walmart_attribute_name']);
                                $walmartVariantAttrArray[] =  $attribute[0];
                            }
                    $variantFlag = $this->checkForVariantAttributes($walmartVariantAttrArray , $this->getWalmartCategory($parentId));
                    if($variantFlag['flag']){
                        $product->setWalmartProductValidation('This product contains variant attribute '.$variantFlag['attribute_code'].' which is not accepted by its current mapped Walmart Category');
                        $product->save();
                        continue;
                    }else{
                        $product->setWalmartProductValidation('');
                        $product->save();
                    }
                                       
                    $key = 0;
                    foreach ($products as $product) {
                        $productid = $this->validateProduct($product->getId(), NULL, $callToGetItem , $parentId);
                        if (isset($productid['id'])) {                       
                            $validatedProducts[$product->getId()]['id'] = $productid['id'];
                            $validatedProducts[$product->getId()]['status'] = $productid['status'];
                            $validatedProducts[$product->getId()]['type'] = 'configurable';
                            $validatedProducts[$product->getId()]['variantid'] = substr($sku, 0,19);
                            $validatedProducts[$product->getId()]['parentid'] = $parentId;
                            
                            $validatedProducts[$product->getId()]['variantattr'] = $walmartVariantAttrArray;
                            $validatedProducts[$product->getId()]['isprimary'] = 'false';
                            if ($key == 0) {
                                $validatedProducts[$product->getId()]['isprimary'] = 'true';
                                $key = 1;
                            }                         
                        }
                    }
                } elseif ($product->getTypeId() == 'simple') {
                    $productid = $this->validateProduct($product->getId(), $product, $callToGetItem);
                    if (isset($productid['id'])) {
                        $validatedProducts[$product->getId()] = [
                            'id' => $productid['id'],
                            'status' => $productid['status'],
                            'type' => 'simple',
                            'variantid' => null,
                            'variantattr' => null
                        ];
                    }
                }
            }
        }
        return $validatedProducts;
    }

    /**
     * Validate product for availability of required walmart product attribute data
     * @param string $id
     * @param null $product
     * @return bool|string
     */
    public function validateProduct($id, $product = null, $callToGetItem = null , $parentId = null)
    {
        $validatedProduct = false;
        $magentoAttributes = $this->getWalmartAttributes($id, ['required' => true, 'mapped' => true,
            'validation' => true]);
        if (!is_null($parentId)) {
                    $magentoAttributes = $this->getWalmartAttributes($parentId, ['required' => true, 'mapped' => true,'validation' => true]);
        } else {
            $magentoAttributes = $this->getWalmartAttributes($id, ['required' => true, 'mapped' => true,'validation' => true]);
        }
        if ($product == null) {
            $product = Mage::getModel('catalog/product')
                ->load($id);
        }
        $productArray =  $product->toArray();    
        $attributesEmpty = [];
        $flag = -1;        
        foreach ($magentoAttributes as $walmartAttribute => $magentoAttribute) {
            if($magentoAttribute['magento_attribute_code'] == 'walmart_productid_type'){
                continue;
            }
            if($magentoAttribute['magento_attribute_code'] == 'walmart_product_taxcode' && !is_null(Mage::getStoreConfig('walmart_configuration/productinfo_map/walmart_tax_code'))){
                continue;
            }
            if (!isset($productArray[$magentoAttribute['magento_attribute_code']])
                || empty($productArray[$magentoAttribute['magento_attribute_code']])) {
                $flag = 1;
                $attributesEmpty[] = $walmartAttribute .':Required-Attribute-Empty';                
            } elseif (isset($magentoAttribute['walmart_attribute_type'])) {
                $flag = 0;
                $maxlength = explode(',', $magentoAttribute['walmart_attribute_type']);
                if (isset($maxlength[1]) &&
                    strlen($productArray[$magentoAttribute['magento_attribute_code']]) > $maxlength[1]) {
                    $flag = 2;
                    $length = strlen($productArray[$magentoAttribute['magento_attribute_code']]);
                    $attributesEmpty[] = $walmartAttribute . ':'. $length .'MaxLength-'. $maxlength[1] .'-Exceded';
                }
            }
        }  
        if (count($attributesEmpty) > 0) {
            $attributesEmpty = implode(',', $attributesEmpty);
            $product->setData('walmart_product_validation', $attributesEmpty);
        } else {
            $product->setData('walmart_product_validation', 'valid');
            $validatedProduct['id'] = $id;
        }
        $product->setData('walmart_product_status', 'UNPUBLISHED');
        if($callToGetItem == '1'){
          $validatedProduct['status'] = $this->getItem($product->getSku(), 'publishedStatus');
        }
        if ( isset($validatedProduct['status'])) {
            $product->setData('walmart_product_status', $validatedProduct['status']);
        }
        $product->save();
        return $validatedProduct;
    }

    /**
     * Get Walmart Category for product id
     * @param integer $productId
     * @return array|boolean
     */
    public function getWalmartCategory($productId)
    {
        if ($productId != null) {
            $catId = Mage::getModel('catalog/product')->load($productId)->getCategoryIds();
            foreach ($catId as $k => $id) {
                if ($id == 2 || $id == 0) {
                    unset($catId[$k]);
                }
            }
            $mappedIds = Mage::getModel('walmart/catlist')->getCollection();



            foreach($mappedIds as $value)
            {
                $ids=explode(',',$value->getMagentoCatId());

                foreach($ids as $mag_cat_id)
                {
                    foreach ($catId as $assignedCatId)
                    {
                        if($assignedCatId == $mag_cat_id)
                        {

                            $array = $value->getData();
                            return $array;
                        }
                    }
                }

            }
        }
        /*
                    $walmartCatId = Mage::getModel('walmart/catlist')->getCollection()
                        ->addFieldToFilter('magento_cat_id', ['in' => $catId]);
                    $walmartCatId = $walmartCatId->getData();
                    $ar=isset($walmartCatId[0])?$walmartCatId[0]:NULL;
                   // print_r($ar); die;
                    return $ar;
                }
                return false;*/
    }

    /**
     * Get Walmart Attributes for product id
     * @param $productId
     * @param array $params
     * @return array|bool
     */
    public function getWalmartAttributes($productId, $params =
    ['required' => true, 'mapped' => false, 'validation' => false]
    ) {
        if ($productId) {
            $catData = $this->getWalmartCategory($productId);
            $walmartAttributes = explode(',', $catData['walmart_required_attributes']);
            if (isset($params['required']) && $params['required'] == false) {
                $walmartAttributes = array_merge($walmartAttributes, explode(',', $catData['walmart_attributes']));
            }
            $attributes = [];
            if ($params['mapped'] == true) {
                $magentoAttributes = Mage::getModel('walmart/walmartattribute')->getCollection()
                    ->addFieldToSelect(['walmart_attribute_name', 'magento_attribute_code', 'walmart_attribute_type'])
                    ->addFieldToFilter('walmart_attribute_name', ['in' => $walmartAttributes])
                    ->addFieldToFilter('magento_attribute_code', ['notnull' => true]);
                if ($params['validation'] == true) {
                    foreach ($magentoAttributes as $value) {
                        $attributes[$value['walmart_attribute_name']] = [
                            'magento_attribute_code' => $value['magento_attribute_code'],
                            'walmart_attribute_type' => isset($value['walmart_attribute_type']) ?
                                $value['walmart_attribute_type'] : 'string',
                        ];
                    }
                } else {

                    foreach ($magentoAttributes as $value) {
                        $attributes[$value['walmart_attribute_name']] = $value['magento_attribute_code'];
                    }

                    foreach ($walmartAttributes as $attribute) {
                        if (!isset($attributes[$attribute])) {
                            $attributes[$attribute] = 'blank';
                        }
                    }
                }


            }

            return $attributes;
        }
        return false;
    }

    /**
     * Get Items
     * @param string|[] $params
     * @param string $subUrl
     * @return string
     * @link https://developer.walmartapis.com/#get-all-items
     */
    public function getItems($params = [], $subUrl = self::GET_ITEMS_SUB_URL)
    {
        if (!isset($params['limit']) || empty($params['limit'])) {
            $params['limit'] = '20';
        }
        $queryString = empty($params) ? '' : '?' . http_build_query($params);
        $response = $this->getRequest($subUrl . $queryString);

        return $response;
    }

    /**
     * Get Item
     * @param string $sku
     * @param string $returnField
     * @param string $subUrl
     * @return string|[]
     * @link https://developer.walmartapis.com/#get-an-item
     */
    public function getItem($sku, $returnField = null, $subUrl = self::GET_ITEMS_SUB_URL )
    {
        $response = $this->getRequest($subUrl . '?sku=' . $sku);
        try {
            $response = Mage::helper('core')->jsonDecode($response);
            if ($returnField) {
                return $response['MPItemView'][0]['publishedStatus'];
            }
            return $response;
        }
        catch(\Exception $e){
            Mage::log('Data Helper: getItem: ' . $sku .' Item Not Found. Response : '.
                var_export($response, true),null,'walmart.log');
            return false;
        }
    }


    /**
     * Get Magento Category for Mapped to Walmart Category
     * @return Object|boolean
     */
    public function getMappedCategories()
    {

        $walmartCat = $this->objectManager->create('\Ced\Walmart\Model\ResourceModel\Categories\Collection')
            ->addFieldToFilter('magento_cat_id', ['neq' => 0]);
        $magentoCatIds = [];
        foreach ($walmartCat as $value) {
            $magentoCatIds[] =  $value['magento_cat_id'];
        }
        if (count($magentoCatIds) > 0) {
            $catId = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Category\Collection')
                ->addAttributeToSelect('entity_id', 'name')
                ->addAttributeToFilter('entity_id', ['in' => $magentoCatIds])->load();
            return $catId;
        }
        return false;

    }

    /**
     * Get Walmart Price
     * @param Object $productObject
     * @return array
     */
    public function getWalmartPrice( $productObject )
    {
        $splprice =(float)$productObject->getFinalPrice();
        $price = (float)$productObject->getPrice();
        $configPrice = trim(Mage::getStoreConfig(
            'walmart_configuration/productinfo_map/walmart_product_price'));
        switch($configPrice) {
            case 'plus_fixed':

                $fixedPrice = trim(Mage::getStoreConfig(
                    'walmart_configuration/productinfo_map/walmart_fix_price'));
                $price = $this->forFixPrice($price, $fixedPrice, 'plus_fixed');
                $splprice = $this->forFixPrice($splprice, $fixedPrice, 'plus_fixed');
                break;

            case 'plus_per':
                $percentPrice = trim(Mage::getStoreConfig(
                    'walmart_configuration/productinfo_map/walmart_percentage_price'));
                $price = $this->forPerPrice($price, $percentPrice, 'plus_per');
                $splprice = $this->forPerPrice($splprice, $percentPrice, 'plus_per');
                break;

            case 'min_fixed':
                $fixedPrice = trim(Mage::getStoreConfig(
                    'walmart_configuration/productinfo_map/walmart_fix_price_min'));
                $price = $this->forFixPrice($price, $fixedPrice, 'min_fixed');
                $splprice = $this->forFixPrice($splprice, $fixedPrice, 'min_fixed');
                break;

            case 'min_per':
                $percentPrice = trim(Mage::getStoreConfig(
                    'walmart_configuration/productinfo_map/walmart_percentage_price_min'));
                $price = $this->forPerPrice($price, $percentPrice, 'min_per');
                $splprice = $this->forPerPrice($splprice, $percentPrice, 'min_per');
                break;

            case 'differ':
                $customPriceAttr = trim(Mage::getStoreConfig(
                    'walmart_configuration/productinfo_map/walmart_different_price'));
                try {
                    $cprice =(float)$productObject -> getData($customPriceAttr);
                } catch(\Exception $e) {
                    Mage::log(" Walmart: Walmart Helper: getWalmartPrice() : " . $e->getMessage(),null,'walmart');
                }
                $price =(isset($cprice) && $cprice != 0) ? $cprice : $price ;
                $splprice = $price;
                break;

            default:
                return [
                    'price' => (string)$price,
                    'splprice' => (string)$splprice,
                ];
        }
        return [
            'price' => (string)$price,
            'splprice' => (string)$splprice,
        ];
    }

    /**
     * ForFixPrice
     * @param null $price
     * @param null $fixedPrice
     * @param string $configPrice
     * @return float|null
     */
    public function forFixPrice($price = null, $fixedPrice = null, $configPrice)
    {
        if (is_numeric($fixedPrice) && ($fixedPrice != '')) {
            $fixedPrice =(float)$fixedPrice;
            if ($fixedPrice > 0) {
                $price= $configPrice == 'plus_fixed' ?(float)($price + $fixedPrice)
                    :(float)($price - $fixedPrice);
            }
        }
        return $price;
    }

    /**
     * ForPerPrice
     * @param null $price
     * @param null $percentPrice
     * @param string $configPrice
     * @return float|null
     */
    public function forPerPrice($price = null, $percentPrice = null, $configPrice)
    {
        if (is_numeric($percentPrice)) {
            $percentPrice =(float)$percentPrice;
            if ($percentPrice > 0) {
                $price = $configPrice == 'plus_per' ?
                    (float)($price + (($price/100)*$percentPrice))
                    :(float)($price - (($price/100)*$percentPrice));
            }
        }
        return $price;
    }

    /**
     * Response Parse and Save to db
     * $response =
     *'<ns2:FeedAcknowledgement xmlns:ns2="http://walmart.com/">
     * <ns2:feedId>F34AC08BE61843E59739C665C5761D0C@AQMB_wA</ns2:feedId></ns2:FeedAcknowledgement>';
     * @param string $response
     * @param string $type
     * @param string $filePath
     * @return string|[]
     */
    public function responseParse($response = '', $type = "item", $filePath = '' )
    {
        if ($type) {
            $feedModel = Mage::getModel('walmart/walmartfeed');
            $data = str_replace('ns2:', "", $response);
            $parser =  Mage::Helper("walmart/parser");
            $data = $parser->loadXML($data)->xmlToArray();
            try {
                $feedModel->setData('feed_id', $data['FeedAcknowledgement']['feedId']);
                $feed = $this->getFeeds($data['FeedAcknowledgement']['feedId']);
                $feedModel->setData('feed_date', date('Y-m-d H:i:s'));
                $feedModel->setData('feed_source', $type);
                $feedModel->setData('feed_status', "UPLOADED");
                if (isset($feed['results']) && $feed['totalResults'] != 0) {
                    $feed = $feed['results']['0'];
                    $feedData = $feedModel->load($feed['feedId'], 'feed_id')->getData();
                    if (!empty($feedData)) {
                        $feedModel->load($feed['feedId'], 'feed_id');
                    } else {
                        $feedModel->setData('feed_id', $feed['feedId']);
                    }
                    $arr = [
                        'feedStatus' => 'feed_status',
                        'itemsReceived' => 'items_received',
                        'itemsSucceeded' => 'items_succeeded',
                        'itemsFailed' => 'items_failed',
                        'itemsProcessing' => 'items_processing',
                        'feedDate' => 'feed_date',
                        'feedType' => 'feed_type',
                        'feedSource' => 'feed_source',
                    ];

                    foreach ($arr as $key => $value) {
                        if (isset($feed[$key])) {
                            if ($key == 'feedDate') {
                                $feedModel->setData('feed_date',
                                    date( 'Y-m-d H:i:s', substr($feed['feedDate'], 0, 10)));
                            } else {
                                $feedModel->setData($value, $feed[$key]);
                            }
                        }
                    }
                }
                $feedModel->setData('feed_file', $filePath);
                $feedModel->save();
            }
            catch (\Exception $e) {
                if ($this->debugMode) {
                    Mage::log("walmart->Helper->Data.php : responseParse() : \n Response : \n" .
                        var_export($response, true) .
                        "\n Exception : \n" . var_export($e->getMessage(), true), null, "walmart.log", true
                    );
                }
            }

        }
        return true;
    }

    /**
     * Get Feeds, Get Single Feed, Get Single Feed with Error Details
     * @param null $feedId
     * @param string $subUrl
     * @param boolean $includeDetails
     * @return string|boolean
     * @link https://developer.walmartapis.com/#feeds
     */
    public function getFeeds($feedId = null, $includeDetails = false, $subUrl = self::GET_FEEDS_SUB_URL )
    {
        $response = NULL;
        try {
            if ($feedId != null) {
                $subUrl = $subUrl . '?feedId=' . $feedId;
                if ($includeDetails) {
                    $subUrl = self::GET_FEED_SUB_URL . $feedId . '?includeDetails=true';
                }
            }
            $response = $this->getRequest($subUrl);
            return Mage::helper('core')->jsonDecode($response);

        } catch (\Exception $e) {
            if ($this->debugMode) {
                Mage::log("walmart->Helper->Data.php : getFeeds() : \n Response : \n" .
                    var_export($response, true) .
                    "\n Exception : \n" . var_export($e->getMessage(), true), null, "walmart.log", true
                );
            }
            return false;
        }

    }


    /**
     * Create walmart directory in the specified root directory.
     * used for storing json/xml files to be synced.
     * @param string $name
     * @param string $code
     * @return array|string
     */
    public function createDir($name = 'walmart', $code='var')
    {
        $path = Mage::getBaseDir($code) . "/" . $name;
        if (file_exists($path)) {
            return ['status' => true,'path' => $path, 'action' => 'dir_exists'];
        } else {
            try
            {
                $this->fileIo->mkdir($path, 0775, true);
                return  ['status' => true,'path' => $path,  'action' => 'dir_created'];
            }
            catch (\Exception $e){
                return $code . '/' . $name . "Directory Creation Failed.";
            }
        }

    }

    /**
     * Create Json/Xml File
     * @param string|[] $data associative array to be converted into json or xml file
     * @param string|[] $params
     * 'type': file type json or xml, default json,
     * 'name': file name,
     * 'path': path to save file, default 'var/walmart'
     * 'code': drectory code, default 'var'
     * @return boolean
     */
    public function createFile($data, $params = [])
    {
        $type = 'json';
        $timestamp = Mage::getModel('core/date');
        $name = 'walmart_' . $timestamp->timestamp(time());
        $path = 'walmart';
        $code = 'var';

        if (isset($params['type'])) {
            $type = $params['type'];
        }
        if (isset($params['name'])) {
            $name = $params['name'];
        }
        if (isset($params['path'])) {
            $path = $params['path'];
        }
        if (isset($params['code'])) {
            $code = $params['code'];
        }
        if ($type == 'xml') {
            $data = Mage::helper('core')->assocToXml($data);
        } elseif ($type == 'json') {
            $data = Mage::helper('core')->jsonEncode($data);
        } elseif ($type == 'string') {
            $data = ($data);
        }

        $dir = $this->createDir($path, $code);
        $filePath = $dir['path'];
        $fileName = $name ."." . $type;
        try {
            $this->fileIo->write($filePath . "/" . $fileName, $data);
        }
        catch (\Exception $e){
            return false;
        }

        return true;
    }

    /**
     * Ship Orders
     * @param string $postData
     * @param string $subUrl
     * @return string
     * @link  https://developer.walmartapis.com/#cancelling-order-lines
     */
    public function shipOrder($postData = null , $subUrl = self::GET_ORDERS_SUB_URL)
    {
        $purchaseOrderId = $postData['shipments'][0]['purchaseOrderId'];
        $shipArray = [
            'ns2:orderShipment' => [
                '_attribute' => [
                    'xmlns:ns2' => "http://walmart.com/mp/v3/orders",
                    'xmlns:ns3' => "http://walmart.com/",
                ],
                '_value' => [
                    'ns2:orderLines' => [
                        '_attribute' => [
                        ],
                        '_value' => [

                        ]
                    ],
                ]
            ]
        ];
        $url = '';
        if (isset($postData['shipments'][0]['shipment_tracking_url'])) {
            $url = $postData['shipments'][0]['shipment_tracking_url'];
        }
        $count = -1;
        foreach ($postData['shipments'] as $key => $values) {
            if (!isset($values['shipment_items'])) {
                continue;
            }
            foreach ($values['shipment_items'] as $value) {
                $lineNumbers =  explode(',', $value['lineNumber']);
                foreach ($lineNumbers as $lineNumber) {
                    $shipArray['ns2:orderShipment'][ '_value']['ns2:orderLines']['_value'][++$count] =
                        ['ns2:orderLine' => [
                            'ns2:lineNumber' => $lineNumber,
                            'ns2:orderLineStatuses' => [
                                'ns2:orderLineStatus' => [
                                    'ns2:status' => 'Shipped',
                                    'ns2:statusQuantity' => [
                                        'ns2:unitOfMeasurement' => 'Each',
                                        'ns2:amount' => '1'//(string)$value['response_shipment_sku_quantity']
                                    ],
                                    'ns2:trackingInfo' => [
                                        'ns2:shipDateTime' => $postData['shipments'][0]['carrier_pick_up_date'],
                                        'ns2:carrierName' => [
                                            'ns2:carrier' => $postData['shipments'][0]['carrier']
                                        ],
                                        'ns2:methodCode' => $postData['shipments'][0]['methodCode'],
                                        'ns2:trackingNumber' => $postData['shipments'][0]['shipment_tracking_number'],
                                        'ns2:trackingURL' => $url
                                    ]
                                ]
                            ]
                        ]
                        ];
                }
            }

        }
        $customGenerator = Mage::helper('walmart/customgenerator');
        $customGenerator->arrayToXml($shipArray);
        $str = preg_replace
        ('/(\<\?xml\ version\=\"1\.0\"\?\>)/', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>',
            $customGenerator->__toString());
        $params['data'] = $str;
        $this->createFile($str, ['type' => 'string', 'name' => 'OrderShip']);
        $params['headers'] = 'WM_CONSUMER.CHANNEL.TYPE: ' . $this->apiConsumerChannelId;
        $response = $this->postRequest($subUrl.'/'.$purchaseOrderId.'/shipping',
            $params);
        try{
            $parser = Mage::getModel('walmart/parser');
            $response = str_replace('ns:2','', $response);
            $data = $parser->loadXML($response)->xmlToArray();
            return  Mage::helper('core')->jsonEncode($data);
        }
        catch(\Exception $e){
            return false;
        }
    }

    /**
     * Reject Order
     * @param string $purchaseOrderId
     * @param string $lineNumber
     * @param string $subUrl
     * @return string
     * @link  https://developer.walmartapis.com/#cancelling-order-lines
     */
    public function rejectOrders($purchaseOrderId , $dataship , $subUrl = self::GET_ORDERS_SUB_URL)
    {
        $cancelArray = [
            'ns2:orderCancellation' => [
                '_attribute' => [
                    'xmlns:ns2' => "http://walmart.com/mp/v3/orders",
                    'xmlns:ns3' => "http://walmart.com/",
                ],
                '_value' => [
                    'ns2:orderLines' => []
                ]
            ]
        ];

        $counter = 0;
        foreach ($dataship['shipments'] as $values){
            if (!isset($values['cancel_items'])) {
                continue;
            }
            foreach ($values['cancel_items'] as $value) {
                $lineNumbers =  explode(',', $value['lineNumber']);
                $cancelArray['ns2:orderCancellation']['_value']['ns2:orderLines']['_attribute'] = [];
                foreach ($lineNumbers as $lineNumber) {
                    $cancelArray['ns2:orderCancellation']['_value']['ns2:orderLines']
                    ['_value'][$counter]['ns2:orderLine']['ns2:lineNumber'] = (string)$lineNumber;
                    $cancelArray['ns2:orderCancellation']['_value']['ns2:orderLines']
                    ['_value'][$counter]['ns2:orderLine']['ns2:orderLineStatuses'] = [
                        'ns2:orderLineStatus' => [
                            'ns2:status' => 'Cancelled',
                            'ns2:cancellationReason' => 'CANCEL_BY_SELLER',
                            'ns2:statusQuantity' => [
                                'ns2:unitOfMeasurement' => 'EACH',
                                'ns2:amount' => '1'
                            ]
                        ]
                    ];
                    $counter++;
                }
            }

        }
        $customGenerator = Mage::helper('walmart/customGenerator');
        $customGenerator->arrayToXml($cancelArray);
        $str = preg_replace('/(\<\?xml\ version\=\"1\.0\"\?\>)/', '<?xml version="1.0" encoding="UTF-8" ?>',
            $customGenerator->__toString());
        $params['data'] = $str;
        $params['headers'] = 'WM_CONSUMER.CHANNEL.TYPE: ' . $this->apiConsumerChannelId;
        $this->createFile($str, ['type' => 'string', 'name' => 'CancelOrder']);
        $response = $this->postRequest($subUrl.'/'.$purchaseOrderId.'/cancel',
            $params);

        try{
            $parser = Mage::getModel('walmart/parser');
            $response = str_replace('ns:2','', $response);
            $data = $parser->loadXML($response)->xmlToArray();
            return  Mage::helper('core')->jsonEncode($data);

            if (isset($response['error'])) {
                throw new \Exception();
            }
            return $response;
        }
        catch(\Exception $e){
            return false;
        }
    }

    /**
     * Refund Order
     * @param string $purchaseOrderId
     * @param string $orderData
     * @param string $subUrl
     * @return string
     * @link  https://developer.walmartapis.com/#cancelling-order-lines
     */
    public function refundOrder($purchaseOrderId , $orderData , $subUrl = self::GET_ORDERS_REFUND_SUB_URL)
    {
        $refundData = [
            'ns2:orderRefund' => [
                '_attribute' => [
                    'xmlns:ns2' => "http://walmart.com/mp/orders",
                    'xmlns:ns3' => "http://walmart.com/",
                ],
                '_value' => [
                    'ns2:purchaseOrderId' => $purchaseOrderId,
                ]
            ]
        ];
        $refundData['ns2:orderRefund']['_value']['ns2:orderLines']['ns2:orderLine'][ '_attribute'] = [];
        foreach ($orderData['lineNumber'] as  $key=>$value) { 

            $refundData['ns2:orderRefund']['_value']['ns2:orderLines']['ns2:orderLine'][ '_value'][$key] = [
                                'ns2:lineNumber' => $value,
                                'ns2:refunds' => [
                                    'ns2:refund' => [
                                        'ns2:refundId' => $orderData['refundComments'],
                                        'ns2:refundReason' => $orderData['refundReason'],
                                        'ns2:refundComments' => $orderData['refundComments'],
                                        'ns2:charges' => [
                                            '_attribute' => [],
                                            '_value' =>[
                                                0 => [
                                                        'ns2:charge' => [
                                                            'ns2:chargeType' => 'Product',
                                                            'ns2:chargeName' => 'Item Price',
                                                            'ns2:chargeAmount' => [
                                                                'ns2:currency' => 'USD',
                                                                'ns2:amount' => $orderData['amount']
                                                            ],
                                                            'ns2:tax' => [
                                                                'ns2:taxName' => 'Item Price Tax',
                                                                'ns2:taxAmount' => [
                                                                    'ns2:currency' => 'USD',
                                                                    'ns2:amount' => $orderData['taxAmount']
                                                                ]
                                                            ]
                                                        ]
                                                    
                                                ],
                                                1 =>[
                                                    'ns2:refundCharge' => [
                                                        'ns2:refundReason' => $orderData['refunReasonShipping'],
                                                        'ns2:charge' => [
                                                            'ns2:chargeType' => 'SHIPPING',
                                                            'ns2:chargeName' => 'Shipping Price',
                                                            'ns2:chargeAmount' => [
                                                                'ns2:currency' => 'USD',
                                                                'ns2:amount' => $orderData['shipping']
                                                            ],
                                                            'ns2:tax' => [
                                                                'ns2:taxName' => 'Shipping Tax',
                                                                'ns2:taxAmount' => [
                                                                    'ns2:currency' => 'USD',
                                                                    'ns2:amount' => $orderData['shippingTax']
                                                                ]
                                                            ] 
                                                        ]
                                                    ]
                                                ]
                                            ]

                                        ]
                                    ]
                                ]
                            ];
        
        }

        $customGenerator =  Mage::helper('walmart/customGenerator'); 
        $customGenerator->arrayToXml($refundData);
        $str = preg_replace('/(\<\?xml\ version\=\"1\.0\"\?\>)/', '<?xml version="1.0" encoding="UTF-8" ?>',
        $customGenerator->__toString());
        $params['data'] = $str;
        $params['headers'] = 'WM_CONSUMER.CHANNEL.TYPE: ' . $this->apiConsumerChannelId;
        $this->createFile($str, ['type' => 'string', 'name' => 'RefundOrder']);
        $response = $this->postRequest($subUrl.'/'.$purchaseOrderId.'/refund',
            $params);
        try{
            $parser = Mage::getModel('walmart/parser');
            $response = $parser->loadXML($response)->xmlToArray();
            $response = json_encode($response); 
            $data = str_replace('ns2:', "", $response);
            $response = Mage::helper('core')->jsonDecode($data);

            if (isset($response['error'])) {
                throw new \Exception();
            }
            if ($this->debugMode) {
            Mage::log("Walmart->Helper->Data.php->refundOrder() : Response : \n". var_export($response, true), null, "walmart.log", true, true
            );}
            return $response;
        }
        catch(\Exception $e){
           if ($this->debugMode) {
            Mage::log("Walmart->Helper->Data.php->refundOrder() : Response : \n". var_export($response, true), null, "walmart.log", true, true
            );
            Mage::log("Walmart->Helper->Data.php->refundOrder() : Response : \n". var_export($response, true), null, "walmart.log", true, true
            );
        }
        
            return false;
        }
    }

    public function logger(
        $url = "",
        $params = array(),
        $headers = array(),
        $serverOutput = "",
        $curlError = "",
        $desc = "walmart->Helper->Data.php : "
    ) {
        if ($this->debugMode) {
            Mage::log("$desc" . " : \n Request : \n URL: ". $url."\n Headers : \n" . var_export($headers, true) . "\n Body : \n".
                var_export($params, true) ."\n Response : \n " . var_export($serverOutput, true) .
                "\n Error : \n " . var_export($curlError, true), null, "walmart.log", true, true
            );
        }
        return true;
    }

    public function checkForVariantAttributes($variantattr,$walmartCategory)
    {
        $variantArray = array (
          'Animal' => 
          array (
            0 => 'color',
            1 => 'size',
            2 => 'count',
            3 => 'flavor',
            4 => 'scent',
            5 => 'assembledProductLength',
            6 => 'assembledProductWidth',
            7 => 'assembledProductHeight',
          ),
          'ArtAndCraft' => 
          array (
            0 => 'color',
            1 => 'shape',
            2 => 'material',
            3 => 'finish',
            4 => 'scent',
            5 => 'size',
            6 => 'assembledProductLength',
            7 => 'assembledProductWidth',
            8 => 'assembledProductHeight',
          ),
          'Baby' => 
          array (
            0 => 'color',
            1 => 'finish',
            2 => 'babyClothingSize',
            3 => 'shoeSize',
            4 => 'size',
            5 => 'pattern',
            6 => 'count',
            7 => 'scent',
            8 => 'flavor',
          ),
          'CarriersAndAccessories' => 
          array (
            0 => 'color',
            1 => 'material',
            2 => 'pattern',
            3 => 'size',
            4 => 'bagStyle',
            5 => 'assembledProductLength',
            6 => 'assembledProductWidth',
            7 => 'assembledProductHeight',
          ),
          'Clothing' => 
          array (
            0 => 'color',
            1 => 'clothingSize',
            2 => 'pattern',
            3 => 'material',
            4 => 'inseam',
            5 => 'waistSize',
            6 => 'neckSize',
            7 => 'hatSize',
            8 => 'pantySize',
            9 => 'sockSize',
            10 => 'count',
            11 => 'braSize',
            12 => 'braBandSize',
            13 => 'braCupSize',
          ),
          'Electronics' => 
          array (
            0 => 'color',
            1 => 'screenSize',
            2 => 'resolution',
            3 => 'ramMemory',
            4 => 'hardDriveCapacity',
            5 => 'connections',
            6 => 'cableLength',
            7 => 'digitalFileFormat',
            8 => 'physicalMediaFormat',
            9 => 'physicalMediaFormat',
            10 => 'platform',
            11 => 'edition',
          ),
          'FoodAndBeverage' => 
          array (
            0 => 'flavor',
            1 => 'size',
          ),
          'Footwear' => 
          array (
            0 => 'color',
            1 => 'pattern',
            2 => 'material',
            3 => 'shoeWidth',
            4 => 'shoeSize',
            5 => 'heelHeight',
          ),
          'Furniture' => 
          array (
            0 => 'color',
            1 => 'material',
            2 => 'bedSize',
            3 => 'finish',
            4 => 'pattern',
          ),
          'GardenAndPatio' => 
          array (
            0 => 'color',
            1 => 'material',
            2 => 'finish',
            3 => 'pattern',
            4 => 'assembledProductLength',
            5 => 'assembledProductWidth',
            6 => 'assembledProductHeight',
          ),
          'HealthAndBeauty' => 
          array (
            0 => 'color',
            1 => 'size',
            2 => 'count',
            3 => 'scent',
            4 => 'flavor',
            5 => 'shape',
          ),
          'Home' => 
          array (
            0 => 'color',
            1 => 'size',
            2 => 'capacity',
            3 => 'pattern',
            4 => 'homeDecorStyle',
            5 => 'assembledProductLength',
            6 => 'assembledProductWidth',
            7 => 'assembledProductHeight',
            8 => 'bedSize',
            9 => 'material',
            10 => 'scent',
            11 => 'count',
          ),
          'Jewelry' => 
          array (
            0 => 'metal',
            1 => 'size',
            2 => 'ringSize',
            3 => 'color',
            4 => 'karats',
            5 => 'carats',
            6 => 'gemstone',
            7 => 'birthstone',
            8 => 'chainLength',
          ),
          'Media' => 
          array (
            0 => 'physicalMediaFormat',
            1 => 'edition',
          ),
          'MusicalInstrument' => 
          array (
            0 => 'color',
            1 => 'pattern',
            2 => 'material',
            3 => 'finish',
            4 => 'audioPowerOutput',
          ),
          'OccasionAndSeasonal' => 
          array (
            0 => 'color',
            1 => 'clothingSize',
            2 => 'pattern',
            3 => 'material',
            4 => 'count',
            5 => 'theme',
            6 => 'occasion',
          ),
          'Office' => 
          array (
            0 => 'color',
            1 => 'paperSize',
            2 => 'material',
            3 => 'count',
            4 => 'numberOfSheets',
            5 => 'envelopeSize',
          ),
          'Other' => 
          array (
            0 => 'count',
            1 => 'size',
            2 => 'scent',
            3 => 'color',
            4 => 'finish',
            5 => 'capacity',
          ),
          'Photography' => 
          array (
            0 => 'color',
            1 => 'material',
            2 => 'focalLength',
            3 => 'displayResolution',
          ),
          'SportAndRecreation' => 
          array (
            0 => 'color',
            1 => 'size',
            2 => 'assembledProductWeight',
            3 => 'material',
            4 => 'shoeSize',
            5 => 'clothingSize',
            6 => 'sportsTeam',
            7 => 'sportsLeague',
          ),
          'ToolsAndHardware' => 
          array (
            0 => 'color',
            1 => 'finish',
            2 => 'grade',
            3 => 'count',
            4 => 'volts',
            5 => 'amps',
            6 => 'watts',
            7 => 'workingLoadLimit',
            8 => 'gallonsPerMinute',
            9 => 'size',
            10 => 'assembledProductLength',
            11 => 'assembledProductWidth',
            12 => 'assembledProductHeight',
          ),
          'Toys' => 
          array (
            0 => 'color',
            1 => 'size',
            2 => 'count',
            3 => 'flavor',
          ),
          'Vehicle' => 
          array (
            0 => 'color',
            1 => 'finish',
            2 => 'vehicleYear',
            3 => 'engineModel',
            4 => 'vehicleMake',
            5 => 'vehicleModel',
          ),
          'Watches' => 
          array (
            0 => 'color',
            1 => 'material',
            2 => 'watchBandMaterial',
            3 => 'plating',
            4 => 'watchStyle',
          ),
        );
    if($walmartCategory['csv_parent_id'] !== '0') {
        $indexOfcat = 'csv_parent_id';
    } else {
        $indexOfcat = 'csv_cat_id';
    }
    if(!isset($variantArray[$walmartCategory[$indexOfcat]])){
        return true;
    }
    foreach ($variantattr as $key => $value) {
        if(!in_array($value, $variantArray[$walmartCategory[$indexOfcat]])) {
            return array('flag' => true , 'attribute_code' => $value);
        } 
    }    
    return false;
    }
}
