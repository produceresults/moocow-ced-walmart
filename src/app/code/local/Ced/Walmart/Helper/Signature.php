<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author 		CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

include __DIR__ . '/vendor/autoload.php';
use phpseclib\Crypt\RSA;

class Ced_Walmart_Helper_Signature extends Mage_Core_Helper_Abstract
{

    const BASE_REQUEST_URL_PATH = 'https://marketplace.walmartapis.com/';

    /**
     * Consumer ID provided by Developer Portal
     * @var string $consumerId
     */
    public $consumerId;

    /**
     * Base64 Encoded Private Key provided by Developer Portal
     * @var string $privateKey
     */
    public $privateKey;

    /**
     * Request URL  of API request being made
     * @var string $requestUrl
     */
    public $requestUrl;

    /**
     * HTTP Request Method for API call (GET/POST/PUT/DELETE/OPTIONS/PATCH)
     * @var string $requestMethod
     */
    public $requestMethod;

    /**
     * Timestamp of certificate generation
     * @var integer $timestamp
     */
    public $timestamp = 0;
    /**
     * Debug Mode Check
     * @var boolean $debugMode
     */
    public $debugMode;

    public function __construct()
    {
        $this->debugMode = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_debug_mode');
    }
    /**
     * Get signature with optional timestamp. If using Signature class as object, you can repeatedly call this
     * method to get a new signature without having to provide $consumerId, $privateKey, $requestUrl, $requestMethod
     * every time.
     * @param string $requestUrl
     * @param string $requestMethod
     * @param null $timestamp
     * @param null $consumerId
     * @param null $privateKey
     * @return \Exception|string
     */
    public function getSignature(
        $requestUrl,
        $requestMethod='GET',
        $timestamp=null,
        $consumerId=null,
        $privateKey=null
    ) {
        $this->requestUrl = self::BASE_REQUEST_URL_PATH . $requestUrl;
        if (preg_match('/^https/', $requestUrl)) {
            $this->requestUrl = $requestUrl;
        }
        $this->requestMethod =  $requestMethod;
        $this->consumerId = is_null($consumerId) ?  Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_customerId') : $consumerId;
        $this->privateKey = is_null($privateKey) ? Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_privatekey') : $privateKey;
        $this->timestamp = $timestamp;

        if (is_null($this->timestamp) || !is_numeric($this->timestamp)) {
            $this->timestamp = $this->getMilliseconds();
        }
        return $this->calculateSignature(
            $this->requestUrl,
            $this->requestMethod,
            $this->consumerId,
            $this->privateKey,
            $this->timestamp
        );

    }

    /**
     * Static method for quick calls to calculate a signature.
     * @param string $requestUrl
     * @param string $requestMethod
     * @param string $consumerId
     * @param string $privateKey
     * @param null $timestamp
     * @return \Exception|string
     * @link https://developer.walmartapis.com/#authentication
     */
    function calculateSignature($requestUrl, $requestMethod, $consumerId, $privateKey, $timestamp=null)
    {
        /**
         * Append values into string for signing
         */
        $message = $consumerId."\n".$requestUrl."\n".strtoupper($requestMethod)."\n".$timestamp."\n";

        /*
           * Get RSA object for signing
        */
        $rsa = new RSA();
        $decodedPrivateKey = base64_decode($privateKey);
        $rsa->setPrivateKeyFormat(RSA::PRIVATE_FORMAT_PKCS8);
        $rsa->setPublicKeyFormat(RSA::PRIVATE_FORMAT_PKCS8);

        /**
         * Load private key
         */
        try {
            if ($rsa->loadKey($decodedPrivateKey, RSA::PRIVATE_FORMAT_PKCS8)) {
                /**
                 * Make sure we use SHA256 for signing
                 */
                $rsa->setHash('sha256');


                $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1);
                $signed = $rsa->sign($message);

                if ($this->debugMode) {
                    Mage::log( "\n Message : \n" . var_export($message, true) .
                        "\n Signature : \n" . var_export(base64_encode($signed), true), null, "walmart.log" );

                }

                /**
                 * Return Base64 Encode generated signature
                 */
                return base64_encode($signed);

            } else {
                throw new \Exception("Unable to load private key");
            }
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Get current timestamp in milliseconds
     * @return float
     */
    public function getMilliseconds()
    {
        return date_timestamp_get(date_create())*1000;
    }
        

}