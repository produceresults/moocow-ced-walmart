<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author 		CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
class Ced_Walmart_Helper_Walmart extends Mage_Core_Helper_Abstract{


	 /**
     * Calculate walmart shipment qty and cancel qty
     * @param Object $orderModelData
     * @return  array|boolean
     */
    public function getShippedCancelledQty($orderModelData)
    {
        foreach ($orderModelData as $order) {
            $shipserializedata = $order->getShipment_data();
        }
        if (isset($shipserializedata )) {
            $shipData = unserialize($shipserializedata);
            if (isset($shipData)? $shipData : false) {
                $shipItemsInfo = $this->shipItemInfo($shipData);
                $orderData = $this->getOrderedCancelledQty($orderModelData);
                return $shipItemsInfo;
            } else {
                $tempData = $orderModelData->getData();
                if (count($tempData) > 0) {
                    $shipData = unserialize(isset($tempData ['shipment_data']) ?
                        $tempData ['shipment_data']:false);
                    if (isset($shipData)?$shipData :false) {
                        $shipItemsInfo = $this->shipItemInfo($shipData);
                        return $shipItemsInfo;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Ship item info
     * @param array $shipData
     * @return : array
     */
    public function shipItemInfo($shipData)
    {
        $shipItemsInfo = [];
        $check = 0;
        $shipData = json_decode(str_replace('ns3:', '', $shipData['shippedData']),true)['order'];
        foreach ($shipData['orderLines'] as $items) {

            $shipItemsInfo [$items ['item']['sku']] [
            'response_shipment_cancel_qty'] = 0;
            $shipItemsInfo [$items ['item']['sku']] [
            'response_shipment_sku_quantity'] = 0;

        }
        return $shipItemsInfo;
    }

    /**
     * Get Order cancel qty
     * @param array $orderModelData
     * @return  array|boolean
     */
    public function getOrderedCancelledQty($orderModelData)
    {
        foreach ($orderModelData as $order) {
            $orderSerializeData = $order->getOrder_data();
        }
        if (isset($orderSerializeData)) {
            $orderData=unserialize($orderSerializeData);

            if (isset($orderData)) {
                $orderItemsInfo=[];
                foreach ($orderData['orderLines']['orderLine'] as $sdata) {
                    $orderItemsInfo[$sdata['item']['sku']]['request_sku_quantity'] = 0;
                    $orderItemsInfo[$sdata['item']['sku']]['request_cancel_qty'] = 0;
                    $orderItemsInfo[$sdata['item']['sku']]['request_sku_quantity'] +=
                        $sdata['orderLineQuantity']['amount'];
                }
                return $orderItemsInfo;
            } elseif (!$orderData) {
                $tempData = $orderModelData->getData();
                $orderData = unserialize($tempData[0]["order_data"]);
                if (sizeof($orderData) > 0) {
                    $orderItemsInfo = [];
                    foreach ($orderData->order_items as $sdata) {
                        $orderItemsInfo[$sdata->merchant_sku]['request_sku_quantity'] = 0;
                        $orderItemsInfo[$sdata->merchant_sku]['request_cancel_qty'] = 0;
                        $orderItemsInfo[$sdata->merchant_sku]['request_sku_quantity'] +=
                            $sdata->request_order_quantity;
                    }
                    return $orderItemsInfo;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get Refunded Qty Info
     * @param string $order
     * @param string $itemSku
     * @return array
     */
    public function getRefundedQtyInfo($order ="",$itemSku ="")
    {
        $itemSku = trim($itemSku);
        $check=[];
        $check['error']=1;
        if ($order == "") {
            $check['error_msg']="Order not found for current item.";
            return $check;
        }
        if ($itemSku=="") {
            $check['error_msg']="Item Sku not found for current item.";
            return $check;
        }
 
        if ($order instanceof Mage_Sales_Model_Order) {

            $qtyOrdered=0;
            $qtyRefunded=0;

            foreach ($order->getAllItems() as $items) { 
                if ($itemSku == $items->getSku()) {
                    $qtyOrdered = intval($items->getQtyOrdered());
                    $qtyRefunded = intval($items->getQtyRefunded());
                    $shippingamount = number_format($items->getShippingAmount(),2);
                }
            }
            $availableToRefundQty = intval($qtyOrdered - $qtyRefunded);
            $check['error']=0;
            $check['qty_already_refunded'] = $qtyRefunded;
            $check['available_to_refund_qty'] = $availableToRefundQty;
            $check['qty_ordered'] = $qtyOrdered;
            $check['shipping_amount'] = isset($shippingamount) ? $shippingamount : 0;
            return $check;
        }
        return $check;
    }

    /**
     * @return array
     */

    public function feedbackOptArray()
    {
        return [
            [
                'value' => '',
                'label' => __('Please Select an Option')
            ],
            [
                'value' => 'item damaged', 'label' =>__('item damaged')
            ],
            [
                'value' => 'not shipped in original packaging',
                'label' => __('not shipped in original packaging')
            ],
            [
                'value' => 'customer opened item',
                'label' => __('customer opened item')
            ]
        ];
    }


    /**
     * @return array
     */
    public function refundreasonOptionArr()
    {
        return [
            [
                'value' => '', 'label' => __('Please Select an Option')
            ],
            [
                'value' => 'BillingError', 'label' =>  __('BillingError')
            ],
            [
                'value' => 'TaxExemptCustomer', 'label' =>  __('TaxExemptCustomer')
            ],
            [
                'value' => 'ItemNotAsAdvertised', 'label' =>  __('ItemNotAsAdvertised')
            ],
            [
                'value' =>'IncorrectItemReceived', 'label' =>  __('IncorrectItemReceived')
            ],
            [
                'value' => 'CancelledYetShipped', 'label' =>  __('CancelledYetShipped')
            ],
            [
                'value' => 'ItemNotReceivedByCustomer', 'label' =>  __('ItemNotReceivedByCustomer')
            ],
            [
                'value' => 'IncorrectShippingPrice', 'label' =>  __('IncorrectShippingPrice')
            ],
            [
                'value' => 'DamagedItem', 'label' =>  __('DamagedItem')
            ],
            [
                'value' => 'DefectiveItem', 'label' =>  __('DefectiveItem')
            ],
            [
                'value' => 'CustomerChangedMind', 'label' =>  __('CustomerChangedMind')
            ],
            [
                'value' => 'CustomerReceivedItemLate', 'label' =>  __('CustomerReceivedItemLate')
            ],
            [
                'value' => 'Missing Parts / Instructions', 'label' =>  __('Missing Parts / Instructions')
            ],
            [
                'value' => 'Finance -> Goodwill', 'label' =>  __('Finance -> Goodwill')
            ],
            [
                'value' => 'Finance -> Rollback', 'label' =>  __('Finance -> Rollback')
            ]
        ];
    }
    
    /**
     * Get Walmart Price
     * @param Object $productObject
     * @return array
     */
    public function getWalmartPrice( $productObject )
    {
        $splprice =(float)$productObject->getFinalPrice();
        $price = (float)$productObject->getPrice();
        $configPrice = trim($this->scopeConfigManager->getvalue(
            'walmartconfiguration/productinfo_map/walmart_product_price'));
        switch($configPrice) {
            case 'plus_fixed':
                $fixedPrice = trim($this->scopeConfigManager->getvalue(
                    'walmartconfiguration/productinfo_map/walmart_fix_price'));
                $price = $this->forFixPrice($price, $fixedPrice, 'plus_fixed');
                $splprice = $this->forFixPrice($splprice, $fixedPrice, 'plus_fixed');
                break;

            case 'plus_per':
                $percentPrice = trim($this->scopeConfigManager->getvalue(
                    'walmartconfiguration/productinfo_map/walmart_percentage_price'));
                $price = $this->forPerPrice($price, $percentPrice, 'plus_per');
                $splprice = $this->forPerPrice($splprice, $percentPrice, 'plus_per');
                break;

            case 'min_fixed':
                $fixedPrice = trim($this->scopeConfigManager->getvalue(
                    'walmartconfiguration/productinfo_map/walmart_fix_price'));
                $price = $this->forFixPrice($price, $fixedPrice, 'min_fixed');
                $splprice = $this->forFixPrice($splprice, $fixedPrice, 'min_fixed');
                break;

            case 'min_per':
                $percentPrice = trim($this->scopeConfigManager->getvalue(
                    'walmartconfiguration/productinfo_map/walmart_percentage_price'));
                $price = $this->forPerPrice($price, $percentPrice, 'min_per');
                $splprice = $this->forPerPrice($splprice, $percentPrice, 'min_per');
                break;

            case 'differ':
                $customPriceAttr = trim($this->scopeConfigManager->getvalue(
                    'walmartconfiguration/productinfo_map/walmart_different_price'));
                try {
                    $cprice =(float)$productObject -> getData($customPriceAttr);
                } catch(\Exception $e) {
                    $this->_logger->debug(" Walmart: Walmart Helper: getWalmartPrice() : " . $e->getMessage());
                }
                $price =(isset($cprice) && $cprice != 0) ? $cprice : $price ;
                $splprice = $price;
                break;

            default:
                return [
                    'price' => (string)$price,
                    'splprice' => (string)$splprice,
                ];
        }
        return [
            'price' => (string)$price,
            'splprice' => (string)$splprice,
        ];
    }

    /**
     * ForFixPrice
     * @param null $price
     * @param null $fixedPrice
     * @param string $configPrice
     * @return float|null
     */
    public function forFixPrice($price = null, $fixedPrice = null, $configPrice)
    {
        if (is_numeric($fixedPrice) && ($fixedPrice != '')) {
            $fixedPrice =(float)$fixedPrice;
            if ($fixedPrice > 0) {
                $price= $configPrice == 'plus_fixed' ?(float)($price + $fixedPrice)
                    :(float)($price - $fixedPrice);
            }
        }
        return $price;
    }

    /**
     * ForPerPrice
     * @param null $price
     * @param null $percentPrice
     * @param string $configPrice
     * @return float|null
     */
    public function forPerPrice($price = null, $percentPrice = null, $configPrice)
    {
        if (is_numeric($percentPrice)) {
            $percentPrice =(float)$percentPrice;
            if ($percentPrice > 0) {
                $price = $configPrice == 'plus_per' ?
                    (float)($price + (($price/100)*$percentPrice))
                    :(float)($price - (($price/100)*$percentPrice));
            }
        }
        return $price;
    }


    /**
     * Get Updated Refund Quantity
     * @param string $merchantOrderId
     * @return array
     */
    public function getUpdatedRefundQty($merchantOrderId)
    {
        $refundcollection = Mage::getModel('walmart/walmartrefund')
            ->getCollection()
            ->addFieldToFilter('refund_id', $merchantOrderId);
        $refundQty = [];
        if ($refundcollection->getSize()>0) {

            foreach ($refundcollection as $coll) {
                $refundData = unserialize($coll->getData('saved_data'));
                foreach ($refundData['sku_details'] as $data) {
                    $refundQty[$data['merchant_sku']]+=$data['refund_quantity'];
                }
            }
        }
        return $refundQty;
    }

}