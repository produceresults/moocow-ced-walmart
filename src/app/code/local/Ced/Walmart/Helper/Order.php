<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/


class Ced_Walmart_Helper_Order extends Mage_Core_Helper_Abstract{


    /**
     * @Walmart Orders Synchronisation
     * Create Available Walmart Orders in Magento
     */
    public function fetchLatestWalmartOrders()
    { 
        $date = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_fetchstartdate' );  
        $storeId = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_storeid');
        $website = Mage::getModel('core/store')->load($storeId);
        $websiteId = $website->getWebsiteId();
        $store = Mage::app()->getStore($website->getCode());

        $response = Mage::Helper('walmart')->getOrders (['createdStartDate' => $date , 'status' =>'Created']);
        /*$response = 'a:6:{s:15:"purchaseOrderId";s:13:"3143778987143";s:15:"customerOrderId";s:13:"5651664670775";s:15:"customerEmailId";s:19:"mbwylie@comcast.net";s:9:"orderDate";i:1477007444000;s:12:"shippingInfo";a:5:{s:5:"phone";s:10:"4042724934";s:21:"estimatedDeliveryDate";i:1477720800000;s:17:"estimatedShipDate";i:1477375200000;s:10:"methodCode";s:8:"Standard";s:13:"postalAddress";a:8:{s:4:"name";s:15:"Michelle  Wylie";s:8:"address1";s:16:"304 birch laurel";s:8:"address2";N;s:4:"city";s:9:"Woodstock";s:5:"state";s:2:"GA";s:10:"postalCode";s:5:"30188";s:7:"country";s:3:"USA";s:11:"addressType";s:11:"RESIDENTIAL";}}s:10:"orderLines";a:1:{s:9:"orderLine";a:1:{i:0;a:7:{s:10:"lineNumber";s:1:"3";s:4:"item";a:2:{s:11:"productName";s:67:"Make America Great Again - Donald Trump 2016 Campaign Cap Hat (003)";s:3:"sku";s:12:"TRUMP003-RED";}s:7:"charges";a:1:{s:6:"charge";a:2:{i:0;a:4:{s:10:"chargeType";s:7:"PRODUCT";s:10:"chargeName";s:9:"ItemPrice";s:12:"chargeAmount";a:2:{s:8:"currency";s:3:"USD";s:6:"amount";d:8.9900000000000002131628207280300557613372802734375;}s:3:"tax";N;}i:1;a:4:{s:10:"chargeType";s:8:"SHIPPING";s:10:"chargeName";s:8:"Shipping";s:12:"chargeAmount";a:2:{s:8:"currency";s:3:"USD";s:6:"amount";d:1.9899999999999999911182158029987476766109466552734375;}s:3:"tax";N;}}}s:17:"orderLineQuantity";a:2:{s:17:"unitOfMeasurement";s:4:"EACH";s:6:"amount";s:1:"1";}s:10:"statusDate";i:1477007477000;s:17:"orderLineStatuses";a:1:{s:15:"orderLineStatus";a:1:{i:0;a:4:{s:6:"status";s:7:"Created";s:14:"statusQuantity";a:2:{s:17:"unitOfMeasurement";s:4:"EACH";s:6:"amount";s:1:"1";}s:18:"cancellationReason";N;s:12:"trackingInfo";N;}}}s:6:"refund";N;}}}}';*/

        if (isset($response['elements']['order'])) { 
            foreach ( $response['elements']['order'] as $order ) {
             
                $orderObject =  $order ;
                $email = $this->validateString
                ( isset ( $order ['customerEmailId'] ) ) ? $order ['customerEmailId'] : 'customer@walmart.com';
               $customer = Mage::getModel('customer/customer')
                            ->setWebsiteId($websiteId)
                            ->loadByEmail($email);

                if (count ( $order ) > 0 && $this->validateString ( $order ['purchaseOrderId'] )) { 

                    $purchaseOrderid = $order ['purchaseOrderId'];
                    $resultdata = Mage::getModel('walmart/walmartorder')->getCollection()
                        ->addFieldToFilter ( 'purchase_order_id', $purchaseOrderid );

                    if (! $this->validateString ( $resultdata->getData () )==true) { 
                        $ncustomer = $this->_assignCustomer( $order, $customer,$websiteId,$store, $email );
                        if (! $ncustomer->getId()) { 
                           
                            return false;
                        } else { 
                           
                            $this->generateQuote ( $store, $ncustomer, $order, $orderObject );
                        }
                    }
                }
            }

        }
    }


      public function validateString($string)
    {
        $stringValidation = (isset ( $string ) && ! empty ( $string )) ? true : false;
        return $stringValidation;
    }

//delete files once in a day code start
    public function walmartfilesDelete()
    {
        $url = Mage::getBaseDir();
        $path = $url.'/var/walmartupload/';
        $handle = opendir($path);
         if ($handle = opendir($path))
         { 
            while (false !== ($file = readdir($handle)))
              { 
                 $filelastmodified = filemtime($path . $file);
                 //24 hours in a day * 3600 seconds per hour
                if((time() - $filelastmodified) > 24*3600 && is_file($file))
                 {
                    unlink($path . $file);
                }

             }
            closedir($handle); 
        }
    }


  /**
     * Create Walmart customer on Magento
     * @param array $order
     * @param array $customer
     * @param null $store
     * @param string $email
     * @return bool|\Magento\Customer\Api\Data\CustomerInterface
     */
    public function _assignCustomer($order, $customer,$websiteId, $store=null, $email) {
        if (! $this->validateString ( $customer->getId () )) { 
            try {
                $cname = $order ['shippingInfo']['postalAddress'] ['name'];

                // if (trim ( $Cname ) == '' || $Cname == null) {
                //     $Cname = $result ['shipping_to'] ['recipient'] ['name'];
                // }
                // $Cname = preg_replace ( '/\s+/', ' ', $Cname );
                $customerName = explode ( ' ', $cname );
                $firstname = $customerName [0];
                unset($customerName[0]);
                $customerName = array_values($customerName) ;
                $lastname = implode(' ', $customerName);
                if (! isset ( $customerName [1] ) || $customerName [1] == '') {
                    $customerName [1] = $customerName [0];
                }
                   
                 

                //$websiteId = $this->storeManager->getStore ()->getWebsiteId ();




                $customer = Mage::getModel('customer/customer');
                $customer->setWebsiteId($websiteId)
                    ->setStore($store)
                    ->setFirstname($firstname)
                    ->setLastname($lastname)
                    ->setEmail($email)
                    ->setPassword("password");    
                $customer->save();

            /*    $customer = $this->customerFactory->create ();
                $customer->setWebsiteId ( $websiteId );
                $customer->setEmail ( $email );
                $customer->setFirstname ( $firstname );
                $customer->setLastname ( $lastname );
                $customer->setPassword ( "password" );
                $customer->save();*/
                // $customer->sendNewAccountEmail();
                /*
                 * $customerData = $this->objectManager->create ( 'Magento\Customer\Api\Data\CustomerInterface' );
                 * $customerData ->setStore($store) ->setStoreId ( $store->getId() )->setFirstname ( $customer_name [0] )->setLastname ( $customer_name [1] )->setEmail ( $email )/* ->setPassword ( "password" )
                 */
                
                // $customerData->save ();*/
                return $customer;
            } catch ( \Exception $e ) {
                $orderObject = json_decode ( $order );
                $message = $e->getMessage ();
                $walmartOrderError = mage::getModel('walmart/orderimport');
                $walmartOrderError->setPurchaseOrderId ( $order ['purchase_order_Id'] );
                $walmartOrderError->setReferenceNumber ( $order ['customer_order_id'] );
                $walmartOrderError->setReason ( $message );
                $walmartOrderError->setOrderData($orderObject);
                $walmartOrderError->save ();
                return false;
            }
        } else {
            return $customer;
        }
    }
     
    public function parserArray($array){
        $arr = [];
        foreach ($array as $key => $value){
            if(in_array($key,$arr))
                continue;
            $count = count($array);
            $sku = $value['item']['sku'];
            $quantity = 1;
            $lineNumber = $value['lineNumber'];
            for ( $i = $key+1 ; $i < $count;$i++){
                if($array[$i]['item']['sku'] == $sku){
                    $quantity++;
                    $lineNumber = $lineNumber.','.$array[$i]['lineNumber'];
                    unset($array[$i]);
                    array_push($arr,$i);
                    array_values($array);
                }
            }
            $array[$key]['lineNumber'] = $lineNumber;
            $array[$key]['orderLineQuantity']['amount'] = $quantity;
        }
        return $array;
    }

    /*
    * @Auto Order Rejection Request
    */
    public function rejectOrder($item , $result , $lineNumber , $quantity) 
    {
        $orderData = json_encode($result);
        $reject_items_arr [] = ['order_item_acknowledgement_status' => 'nonfulfillable - invalid merchant SKU',
            'order_item_line_number' => $item['lineNumber']];
        $message = "Product " . $item['item']['sku'] . " is Not Enabled or not in stock or inventry<=0 or Product  visiblity is set to not visible individually";
        $collection = Mage::getModel('walmart/orderimport')->getCollection()->addFieldToFilter('purchase_order_id',$result ['purchaseOrderId'])->getData();

        if (count($collection) != 0) {
            $walmartOrderError = Mage::getModel('walmart/orderimport' )->load($result ['purchaseOrderId'],'purchase_order_id');
        } else {
            $walmartOrderError = Mage::getModel('walmart/orderimport' ); // for error
        }
        $walmartOrderError->setPurchaseOrderId ( $result ['purchaseOrderId'] );
        $walmartOrderError->setReferenceNumber ( $result ['customerOrderId'] );
        $walmartOrderError->setReason ( $message );
        $walmartOrderError->setOrderData($orderData);
        $walmartOrderError->save ();

        $data_var = [];
        $data_var ['acknowledgement_status'] = "rejected - item level error";
        $data_var ['order_items'] = $reject_items_arr;
        // $data = Mage::helper('walmart/data')->rejectOrders (  $result ['purchaseOrderId'] , $lineNumber , $quantity);
    }

    public function generateQuote($store, $customer, $order, $orderObject)
    {
        $shippingMethod = 'shipwalmartcom';

        $paymentMethod = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_default_payment');
        $paymentMethod = !empty($paymentMethod) ? $paymentMethod : "checkmo";
        $productArray = array();
        $baseCurrency = $store->getBaseCurrencyCode();
        $itemsArray = $this->parserArray($order['orderLines']['orderLine']);
        $baseprice = 0;
        $shippingcost = 0;
        $tax = 0;
        $storeId = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_storeid');   
       // $storeId = $store->getId();
        $autoReject = false;
        $reject_items_arr = array();
        $order_items_qty_arr = array();
        $walmartOrder = $order;
        $order = Mage::getModel('sales/order')
            ->setStoreId($storeId)
            ->setQuoteId(0)
            ->setGlobal_currency_code($baseCurrency)
            ->setBase_currency_code($baseCurrency)
            ->setStore_currency_code($baseCurrency)
            ->setOrder_currency_code($baseCurrency);
      

        $shippingcost = 0;
        $tax = 0;
        $baseprice = 0;
        foreach ($itemsArray as $item) { 
            $message = '';
            $sku = $item['item']['sku'];
            $lineNumber = $item['lineNumber'];
            $quantity = $item['orderLineQuantity']['amount'];
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item['item']['sku']);
            
            if ($product) { 
               
                if ($product->getStatus () == '1') {
                    $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                    /* Get stock item */
                    
                    $cancelItemQuantity = '';
                    if (!empty($item['orderLineStatuses']['orderLineStatus'][0]['statusQuantity']['amount'])) {
                        $cancelItemQuantity = 0;
                    }

                    $stockstatus = ($stock->getQty () > 0) ? ($stock->getIsInStock () == '1' ?
                        ($stock->getQty () >= $item ['orderLineQuantity']['amount'] ?
                            ($item ['orderLineQuantity']['amount'] != $cancelItemQuantity ?
                                true : false) : false) : false) : false;

                    if ($stockstatus) { 
                        $productArray [] = [
                            'id' => $product->getEntityId (),
                            'qty' => $item ['orderLineQuantity']['amount']
                        ];
                        $price = $item ['charges'] ['charge'][0]['chargeAmount']['amount'];
                        $qty = $item ['orderLineQuantity']['amount'];
                        $cancelqty = $cancelItemQuantity;
                        $baseprice += $qty * $price;
                        
                        if(isset($item ['charges']['charge'][1])){
                            $shippingcost += ($item ['charges']['charge'][1]['chargeAmount']['amount'] * $qty) ;
                            $tax = $tax + ($item ['charges'] ['charge'][0]['tax']['taxAmount']['amount'] * $qty) +
                                ($item ['charges']['charge'][1]['tax']['taxAmount']['amount'] * $qty);
                        } else{
                            $tax = $tax + ($item ['charges'] ['charge'][0]['tax']['taxAmount']['amount'] * $qty);
                        }                        
                        $rowTotal = $price * $qty;                   
                            $orderItem = Mage::getModel('sales/order_item')
                            ->setStoreId($storeId)
                            ->setQuoteItemId(0)
                            ->setQuoteParentItemId(NULL)
                            ->setProductId($product->getEntityId())
                            ->setProductType($product->getTypeId())
                            ->setQtyBackordered(NULL)
                            ->setTotalQtyOrdered($qty)
                            ->setQtyOrdered($qty)
                            ->setName($product->getName())
                            ->setSku($product->getSku())
                            ->setPrice($price)
                            ->setBasePrice($price)
                            ->setOriginalPrice($price)
                            ->setRowTotal($rowTotal)
                            ->setBaseRowTotal($rowTotal);

                        //$subTotal += $rowTotal;
                        $order->addItem($orderItem);
                        Mage::getSingleton('cataloginventory/stock')->registerItemSale($orderItem);
                        $Quote_execute = True;

                    } else {
                        // needs to check again
                        $autoReject = true;
                        $this->rejectOrder($item, $walmartOrder , $lineNumber , $quantity);
                    }
                }
            } else {
                $autoReject = true;
                $this->rejectOrder($item , $walmartOrder , $lineNumber , $quantity);
            }

            /// old code
         

        if ($autoReject) { 

            Mage::getSingleton('core/session')->addError('Failed to Fetch Order.. Please Check Failed Walmart Orders Import Log.');
            return false;
           /* $data_var = array();
            $data_var['acknowledgement_status'] = "rejected - item level error";
            $data_var['order_items'] = $reject_items_arr;
            $data = Mage::helper('walmart')->CPutRequest('/orders/' . $result['merchant_order_id'] . '/acknowledge', json_encode($data_var));
            $response = json_decode($data);

            $reject=Mage::getModel('walmart/walmartorder')->getCollection()->addFieldToFilter('merchant_order_id', $result['merchant_order_id'])->getFirstItem();
            if($response == NULL){
                //$this->saveWalmartOrder($reject->getId());
            }*/
        }
        if (count($productArray) > 0 && count($itemsArray) == count($productArray) && !$autoReject) {
           
            $transaction = Mage::getModel('core/resource_transaction');
            $order->setCustomer_email($customer->getEmail())
                ->setCustomerFirstname($customer->getFirstname())
                ->setCustomerLastname($customer->getLastname())
                ->setCustomerGroupId($customer->getGroupId())
                ->setCustomer_is_guest(0)
                ->setCustomer($customer);

            // set Billing Address
            $countryId = $this->getCountryId($walmartOrder ['shippingInfo'] ['postalAddress'] ['country']);    
            try {

                $billingAddress = Mage::getModel('sales/order_address')
                    ->setStoreId($storeId)
                    ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
                    ->setCustomerId($customer->getId())
                    ->setCustomerAddressId($customer->getDefaultBilling())
                    ->setCustomer_address_id('')
                    ->setPrefix('')
                    ->setFirstname($customer->getFirstname ())
                    ->setMiddlename('')
                    ->setLastname($customer->getLastname ())
                    ->setSuffix('')
                    ->setCompany('')
                    ->setStreet($walmartOrder ['shippingInfo'] ['postalAddress'] ['address1'].' '.$walmartOrder ['shippingInfo'] ['postalAddress'] ['address2'])
                    ->setCity($walmartOrder['shippingInfo']['postalAddress']['city'])
                    ->setCountry_id($countryId)
                    ->setRegion($walmartOrder ['shippingInfo'] ['postalAddress'] ['state'])
                    ->setRegion_id('')
                    ->setPostcode($walmartOrder ['shippingInfo'] ['postalAddress'] ['postalCode'])
                    ->setTelephone($walmartOrder ['shippingInfo'] ['phone'])
                    ->setFax('');
                $order->setBillingAddress($billingAddress);

                $shipping = $customer->getDefaultShippingAddress();
                $shippingAddress = Mage::getModel('sales/order_address')
                    ->setStoreId($storeId)
                    ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
                    ->setCustomerId($customer->getId())
                    ->setCustomerAddressId($customer->getDefaultBilling())
                    ->setCustomer_address_id('')
                    ->setPrefix('')
                    ->setFirstname($customer->getFirstname ())
                    ->setMiddlename('')
                    ->setLastname($customer->getLastname ())
                    ->setSuffix('')
                    ->setCompany('')
                    ->setStreet($walmartOrder ['shippingInfo'] ['postalAddress'] ['address1'].' '.$walmartOrder ['shippingInfo'] ['postalAddress'] ['address2'])
                    ->setCity($walmartOrder['shippingInfo']['postalAddress']['city'])
                    ->setCountry_id($countryId)
                    ->setRegion($walmartOrder ['shippingInfo'] ['postalAddress'] ['state'])
                    ->setRegion_id('')
                    ->setPostcode($walmartOrder ['shippingInfo'] ['postalAddress'] ['postalCode'])
                    ->setTelephone($walmartOrder ['shippingInfo'] ['phone'])
                    ->setFax('');
                $order->setShippingAddress($shippingAddress)
                    ->setShippingMethod('flatrate_flatrate')
                    ->setShippingDescription('Shipping Desc - ');

                $orderPayment = Mage::getModel('sales/order_payment')
                    ->setStoreId($storeId)
                    ->setCustomerPaymentId(0)
                    ->setMethod($paymentMethod);
                //->setPo_number(' - ');
                $order->setPayment($orderPayment);


                $order->setSubtotal($baseprice)
                    ->setBaseSubtotal($baseprice)
                    ->setShippingAmount($shippingcost)
                    ->setBaseShippingAmount($shippingcost)
                    ->setTaxAmount($tax)
                    ->setBaseTaxAmount($tax)
                    ->setGrandTotal($baseprice + $shippingcost + $tax)
                    ->setBaseGrandTotal($baseprice + $shippingcost + $tax);
                        
                $transaction->addObject($order);
                $transaction->addCommitCallback(array($order, 'place'));
                $transaction->addCommitCallback(array($order, 'save'));

                if ($transaction->save() && $order->getId() > 0) {

                    /*$order1 = Mage::getModel('sales/order')->load($order->getId(), 'entity_id');
                    $order1->setStatus('Processing')->save(); */
                    // after order placed
                $deliver_by = date ( 'Y-m-d H:i:s', substr( $walmartOrder ['shippingInfo'] ['estimatedDeliveryDate'],0,10 ) );
                $order_place = date ( 'Y-m-d H:i:s', substr ( $walmartOrder ['orderDate'],0,10 ) );
                $OrderData = [
                    'purchase_order_id' => $walmartOrder ['purchaseOrderId'],
                    'deliver_by' => $deliver_by,
                    'order_place_date' => $order_place,
                    'magento_order_id' => $order->getIncrementId (),
                    'status' => $walmartOrder['orderLines']['orderLine'][0]['orderLineStatuses']['orderLineStatus'][0]['status'],
                    'order_data' => serialize ( $walmartOrder ),
                    'merchant_order_id' => $walmartOrder ['customerOrderId']
                    ];
                    $model = Mage::getModel('walmart/walmartorder')->addData($OrderData);
                    $model->save(); 

                   // if (Mage::getStoreConfig('walmart_options/walmart_order/active') == 1) {
                         $this->generateInvoice($order);
                         $this->autoOrderacknowledge ( $walmartOrder ['purchaseOrderId'], $model );

                        
                  //  }
                   // $this->sendmailtoadmin($order->getIncrementId(),$result['order_detail']['request_ship_by'],$order_items_qty_arr);

                    
                }

                //$incid = $this->get

            } catch (Exception $e) {
                $message = "Fail To Create Order Due to Error " . $e->getMessage();
                $walmartOrderError = Mage::getModel('walmart/orderimport');
                $walmartOrderError->setMerchantOrderId($result['merchant_order_id']);
                        $walmartOrderError->setReferenceNumber($result['reference_order_id']);
                $walmartOrderError->setOrderItemId($item['order_item_id']);
                $walmartOrderError->setReason($message);
                $walmartOrderError->save();

            }
        }
     
     }   //old code end
    }


public function generateInvoice($order)
{
    
    if($order->canInvoice()) {
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
            $invoice->register();
            $invoice->getOrder()->setCustomerNoteNotify(false);
            $invoice->getOrder()->setIsInProcess(true);

            $transactionSave = Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder());

            $transactionSave->save();
        }
}
     /*
     * @Auto Order Acknowledgement Process
     */
    public function autoOrderacknowledge($purchaseOrderId, $ordermodel)
    {
        $dataHelper = Mage::Helper('walmart');
       /* $serialize_data = unserialize ( $ordermodel->getOrderData () );
        if (empty ( $serialize_data ) || count ( $serialize_data ) == 0) {

            $result =  $dataHelper->getOrder($purchaseOrderId);
            $ord_result = $result ;
            if (empty ( $result ) || count ( $result ) == 0) {
                return 0;
            } else if( isset($result['elements']['order'][0]['orderLines']['orderLine'][0]['orderLineStatuses']['orderLineStatus'][0]['status']) && ($result['elements']['order'][0]['orderLines']['orderLine'][0]['orderLineStatuses']['orderLineStatus'][0]['status'] == 'Acknowledged')) {
                return 0;
            } 
        }*/

       /* $fullfill_array = [];
        foreach ( $serialize_data['orderLines']['orderLine'] as $k => $valdata ) {
            $fullfill_array [] = ['order_item_acknowledgement_status' => 'fulfillable',
                'order_item_id' => $valdata['item']['sku']];
        }*/

        // $order_id = $ordermodel->getPurchaseOrderId ();
       /* $data_var = [];
        $data_var ['acknowledgement_status'] = "accepted";
        $data_var ['order_items'] = $fullfill_array;*/
        // Api call to Acknowledge Order
        //$url = 'v3/orders/'.$purchaseOrderId.'/acknowledge';
        //$response = $dataHelper->postRequest($url);

        $response = $dataHelper->acknowledgeOrder ($purchaseOrderId);

        if (empty ( $response ) && $response == null ) {
           return 0;
        } else { 

            try { 
                //$response = json_decode($response,true);
                $modeldata = Mage::getModel('walmart/walmartorder')
                            ->getCollection ()->addFieldToFilter ( 'purchase_order_id', $purchaseOrderId )->getData ();

                if (count ( $modeldata ) > 0) {

                    $id = $modeldata [0] ['id'];
                    $model = Mage::getModel('walmart/walmartorder')->load ( $id );
                    $model->setOrderData(serialize($response));
                    $model->setStatus ( 'Acknowledged' );
                    $model->save ();
                   
                }
            }
            catch(\Exception $e){ 
                /*$this->_logger->debug('Walmart :acknowledgeOrder : NO JSON Response . Response : '.$response);*/
                return false;
            }
            // Setting acknowleged status here
           
        }
        return 0;
    }

    public function getOrderFlag($order_to_complete,$order_cancel,$mixed)
    {
        $order_to_complete = isset($order_to_complete)?$order_to_complete:[];
        $order_cancel = isset($order_cancel)?$order_cancel:[];
        $mixed = isset($mixed)?$mixed:[];


        $Order_flag_array=array_merge($order_to_complete,$order_cancel,$mixed);
        $itemcount = sizeof($Order_flag_array);
        $complete = 0;
        $cancel = 0;
        $mix = 0;
        foreach ($Order_flag_array as $key => $value) {
            if($value == 'complete')
            {
                $complete++;
            }elseif($value == 'cancel')
            {
                $cancel++;
            }else
            {
                $mix++;
            }
        }

        return [
            'item_count' => $itemcount,
            'complete'=>$complete,
            'cancel'=> $cancel,
            'mix' => $mix
        ];
    }

    /*
     * @Shipment generation Process
     */
    public function generateShipment($order, $shipQty)
    {

        $shipment = $this->prepareShipment($order, $shipQty);
        if ($shipment) {
            $shipment->register();
            $shipment->getOrder()->setIsInProcess(true);
            try {
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($shipment)
                    ->addObject($shipment->getOrder())
                    ->save();
            } catch (Mage_Core_Exception $e) {
                Mage::log("Errror While Creating Shipment..." . $e->getMessage());
            }
        }
    }

    public function prepareShipment($order, $shipQty)
    {/*
        foreach($order->getAllItems() as $orderItems)
        {

            $qty_ordered = $orderItems->getQtyOrdered();
            $cancelleditems[$orderItems->getId()] = (int) ($qty_ordered - $cancelleditems[$orderItems->getId()]);
        }
*/
    
       /* $shipment = Mage::getModel('sales/order')->create ( $order, isset ( $cancelleditems ) ? $cancelleditems : [ ], [ ] );*/
        $shipment = Mage::getModel('sales/service_order', $order)
                            ->prepareShipment($shipQty);
        $totalQuantity = $shipment->getTotalQty();                            
        if (!isset($totalQuantity) && !$totalQuantity) {
            return false;
        }
        return $shipment;
    }

     /*
     * @Ship by walmart save process
     */
    public function putShipOrder($data_ship = NULL, $postData, $order_to_complete = [], $order_cancel = [], $mixed = []) {
        $flag=$this->getOrderFlag($order_to_complete,$order_cancel,$mixed);
        if($flag['item_count'] == $flag['complete'])
        {
            $order_to_complete = true;
            $order_cancel = false;
            $mixed = false;
        }elseif($flag['item_count'] == $flag['cancel'])
        {
            $order_to_complete = false;
            $order_cancel = true;
            $mixed = false;
        }else{
            $order_to_complete = false;
            $order_cancel = false;
            $mixed = true;
        }

        $id = $postData ['key1'];
        $order_id = $postData ['orderid'];
        $magento_order_id = $postData ['order'];
        $items_data = $postData ['items'];
        $items_data = json_decode ( $items_data );
      
        /* Do not touch*/
        $quantity_to_cancel = $items_data [0] [2];
       
        /* Do not touch end */
        $order = Mage::getModel( 'sales/order' )->loadByIncrementId ( $id );
        
        // for order items ids and cancel qty relation
        foreach($order->getAllItems() as $orderItem)
        {
            foreach($items_data as $val)
            {
                if($orderItem->getSku() == $val[0])
                    $cancelleditems[$orderItem->getId()] = (int)$val[2];
                    $shipQty[$orderItem->getId()] = (int)$val[3];
            }

        }
        $cancelData = Mage::helper('walmart/data')->rejectOrders($order_id,$data_ship);
        $data = Mage::helper('walmart/data')->shipOrder(  $data_ship  );

        // Api call to complete shipment on walmart
        
        $responsedata['shippedData'] = $data;
        $responsedata['cancelData'] = $cancelData;
        $walmartmodel = Mage::getModel ( 'walmart/walmartorder' )->load ( $magento_order_id,'magento_order_id' );
       
        if ($responsedata && (count($walmartmodel)!=0)) {
            try {
                // as cancel quantity is not available in $dataship array for complete case
                if ($quantity_to_cancel != 0) {
                    $data_ship ['shipments'] [0] ['shipment_items'] [0] ['response_shipment_cancel_qty'] = $quantity_to_cancel;
                }

                $this->saveWalmartShipData ( $walmartmodel, $data_ship, $order_to_complete, $order_cancel,$mixed , $order, $cancelleditems,$responsedata,$shipQty);
                return  "Success" ;
            } catch ( \Exception $e ) {
                return $e->getMessage ();
            }
        } else {
            $err =  'Error while generating shipment on walmart.com';
            return $err;
        }
    }

    public function generateCreditMemo($order ,$itemQtytoCancel){
        $qtys = array('qtys' => $itemQtytoCancel);
        $service = Mage::getModel('sales/service_order', $order);
        $service->prepareCreditmemo($qtys)->register()->save();
        foreach($itemQtytoCancel as $itemId=>$qtyCancel)
        {
            Mage::getModel('sales/order_item')->load($itemId)->setQtyRefunded($qtyCancel);
        }

    }

     

      /**
     * @Ship by walmart save process
     */
    public function saveWalmartShipData($walmartmodel, $data_ship, $order_to_complete=null, $order_cancel=null, $mixed=null,$order, $cancelleditems , $responsedata,$shipQty) {

        // change 1 sept optimize it merging similar case removed whole complete case due to repetition of code

        // mixed_complete_case // And need one more case for multiple shipment (data of mixed will used)

        // all case in one

        $serData = json_decode($responsedata['shippedData'],true);
        $canceldata = json_decode($responsedata['cancelData'],true);
        $flag = '';

        if(((!$order_cancel || $mixed) && (isset($serData['ns4:errors'])))) { 
            $flag = 'complete';
            $walmartmodel->setStatus ( 'Complete' );
        } elseif (strpos($serData['ns4:errors']['ns4:error']['ns4:description'],'SHIPPED status')!=false ) {
            $flag = 'already_shipped';
            $walmartmodel->setStatus ( 'Already Shipped' );
        } else{ 
            $flag = 'cancelled';
            $walmartmodel->setStatus ( 'Cancelled' );
        }

        $walmartmodel->setShipmentData ( serialize ( $responsedata ) );
        $walmartmodel->save ();


        if(!$order_cancel)
            if (! $order->canShip()) { 
                 Mage::getSingleton('core/session')->addError("You can\'t create an shipment.");
            }else{

                $this->generateShipment ( $order , $shipQty);
                $this->markOrderComplete($order);
            }

        if(!$order_to_complete || $order_cancel) 
            if (!$order->canCreditmemo()) {
                Mage::getSingleton('core/session')->addError("We can\'t create credit memo for the order.");
                return false;
            }else{  
                $this->generateCreditMemo($order,$cancelleditems);
                $this->cancelOrderinMagento($order);
            }
            
        if ($flag == 'complete') {
            Mage::getSingleton('core/session')->addSuccess('Your Walmart Order ' . $order->getId() . ' has been Completed.' );
        }
        if ($flag == 'already_shipped') {
            Mage::getSingleton('core/session')->addError('Your Walmart Order ' . $order->getId() . 'already in Shipped Status.' );
        }
        if ($flag == 'cancelled') {
            Mage::getSingleton('core/session')->addSuccess('Your Walmart Order ' . $order->getId() . 'has been successfully cancelled.' );
        }
       
    }

    /**
     * @param string $details_after_saved
     * @return bool
     */

    public function generateCreditMemoForRefund($details_after_saved ='',$refund_order_id)
    {
        if (!empty($details_after_saved)) {
            $sku_details="";
            $sku_details=$details_after_saved;
            $item_details= [];
            $merchant_order_id="";
            $merchant_order_id=$refund_order_id;
            $shipping_amount=0;
            $adjustment_positive=0;
            foreach ($sku_details as $detail) {
                if ($this->checkifTrue($detail)) {
                    $item_details[]=['sku'=>$detail['merchant_sku'],'refund_qty'=>$detail['refund_quantity']];
                    $return_shipping_cost=0;
                    $return_shipping_tax=0;
                    $return_tax=0;
                    $return_shipping_cost=(float)trim(isset($detail['return_shipping_cost'])?$detail['return_shipping_cost']:0);
                    $return_tax=(float)trim(isset($detail['return_tax'])?$detail['return_tax']:0);
                    $return_shipping_tax=(float)trim(isset($detail['return_shipping_tax'])?$detail['return_shipping_tax']:0);
                    $shipping_amount = $shipping_amount + $return_shipping_cost +
                        $return_shipping_tax;
                    $adjustment_positive=$adjustment_positive+$return_tax;
                }
            }
            $collection="";
            $collection=Mage::getModel('walmart/walmartorder')->getCollection()->addFieldToSelect('magento_order_id')->addFieldToFilter(
                'purchase_order_id', $merchant_order_id);

            if ($collection->getSize()>0) {
                foreach ($collection as $coll) {
                    $magento_order_id=$coll->getData('magento_order_id');
                    break;
                }
            }

            if ($magento_order_id !='') {
                try {
                    $order ="";
                    $order = Mage::getModel('sales/order')->loadByIncrementId($magento_order_id);
                    $data=[];
                    $shipping_amount=1; // enable credit memo
                    $data['shipping_amount']=0;
                    $data['adjustment_positive']=0;

                    ($shipping_amount>0) ? $data['shipping_amount']=$shipping_amount : false;
                    ($adjustment_positive>0) ? $data['adjustment_positive']=$adjustment_positive : false ;

                    foreach ($item_details as $key => $value) {
                        $orderItem="";
                        $orderItem = $order->getItemsCollection()->getItemByColumnValue('sku', $value['sku']);
                        $data['qtys'][$orderItem->getId()]=$value['refund_qty'] ;
                    }

                    if (!array_key_exists("qtys",$data)) {
                        Mage::getSingleton('adminhtml/session')->addError("Problem in Credit Memo Data Preparation.Can't generate Credit Memo.");
                        return false;
                    }

                    ($data['shipping_amount']==0) ? Mage::getSingleton('adminhtml/session')
                        ->addError("Amount is 0 .So Credit Memo Cannot be generated.") :
                        $this->generateCreditMemo($merchant_order_id, $data) ;

                } catch(\Exception $e) {
                     Mage::getSingleton('adminhtml/session')->addError($e->getMessage().".Can't generate Credit Memo.");
                    return false;
                }
            } else {
                 Mage::getSingleton('adminhtml/session')->addError("Order not found.Can't generate Credit Memo.");
                return false;
            }
        } else {
             Mage::getSingleton('adminhtml/session')->addError("Can't generate Credit Memo.");
            return false;
        }
    }

    /**
     * @param $detail
     * @return bool
     */
    public function checkifTrue($detail)
    {
        if ($detail['refund_quantity']>0
            && $detail['return_quantity']>=$detail['refund_quantity']
            && $detail['refund_quantity']<=$detail['available_to_refund_qty']) {
            return true;
        } else {
            return false;
        }
    }

    public function cancelOrderinMagento($orderModel){
        //if ($orderModel->canCancel()) {
            $orderModel->cancel();
            $orderModel->setStatus('canceled');
            $history = $orderModel->addStatusHistoryComment(' Order was set to Cancelled by walmart.com ', false);
            $history->setIsCustomerNotified(false);
            $orderModel->save();
       // }
    }

    public function markOrderComplete($order)
    {
        $order->setData('state', "complete");
        $order->setStatus("complete");
        $history = $order->addStatusHistoryComment(' Order was set to Complete by walmart.com ', false);
        $history->setIsCustomerNotified(false);
        $order->save();
        /*$id = $order->getIncrementId();
        $model_data = Mage::getModel('jet/autoship')->getCollection()
            ->addFieldToFilter('order_id', $id)
            ->getData();
            if($model_data)
            {
           foreach ($model_data as $key => $value) {
            $model = Mage::getModel('jet/autoship')->load($value['id'])->setData('jet_shipment_status', 'shipped')->save();
            }  
           }*/
            
    }

    function getCountryId($key){
       $country = array(
        'Argentina'     => 'AR',
        'Armenia'       => 'AM',
        'Australia'     => 'AU',
        'Austria'       => 'AT',
        'Belgium'       => 'BE',
        'Botswana'      => 'BW',
        'Brazil'        => 'BR',
        'Bulgaria'      => 'BG',
        'Canada'        => 'CA',
        'Chile'         => 'CL',
        'China'         => 'CN',
        'Colombia'      => 'CO',
        'Costa Rica'    => 'CR',
        'Croatia'       => 'HR',
        'Czech Republic' => 'CZ',
        'Denmark'       => 'DK',
        'Dominican Republic' => 'DO',
        'Ecuador'       => 'EC',
        'Egypt'         => 'EG',
        'El Salvador'   => 'SV',
        'Estonia'       => 'EE',
        'Finland'       => 'FI',
        'France'        => 'FR',
        'Germany'       => 'DE',
        'Greece'        => 'GR',
        'Guatemala'     => 'GT',
        'Honduras'      => 'HN',
        'Hong Kong SAR China' => 'HK',
        'Hungary'       => 'HU',
        'India'         => 'IN',
        'Indonesia'     => 'ID',
        'Ireland'       => 'IE',
        'Israel'        => 'IL',
        'Italy'         => 'IT',
        'Jamaica'       => 'JM',
        'Japan'         => 'JP',
        'Jordan'        => 'JO',
        'Kazakstan'     => 'KZ',
        'Kenya'         => 'KE',
        'South Korea'   => 'KR',
        'Kuwait'        => 'KW',
        'Latvia'        => 'LV',
        'Lebanon'       => 'LB',
        'Lithuania'     => 'LT',
        'Luxembourg'    => 'LU',
        'Macau SAR China' => 'MO',
        'Macedonia'     => 'MK',
        'Madagascar'    => 'MG',
        'Malaysia'      => 'MY',
        'Mali'          => 'ML',
        'Malta'         => 'MT',
        'Mauritius'     => 'MU',
        'Mexico'        => 'MX',
        'Moldova'       => 'MD',
        'Netherlands'   => 'NL',
        'New Zealand'   => 'NZ',
        'Nicaragua'     => 'NI',
        'Niger'         => 'NE',
        'Norway'        => 'NO',
        'Pakistan'      => 'PK',
        'Panama'        => 'PA',
        'Paraguay'      => 'PY',
        'Peru'          => 'PE',
        'Philippines'   => 'PH',
        'Poland'        => 'PL',
        'Portugal'      => 'PT',
        'Qatar'         => 'QA',
        'Romania'       => 'RO',
        'Russia'        => 'RU',
        'Saudi Arabia'  => 'SA',
        'Senegal'       => 'SN',
        'Singapore'     => 'SG',
        'Slovakia'      => 'SK',
        'Slovenia'      => 'SI',
        'South Africa'  => 'ZA',
        'Spain'         => 'ES',
        'Sri Lanka'     => 'LK',
        'Sweden'        => 'SE',
        'Switzerland'   => 'CH',
        'Taiwan'        => 'TW',
        'Thailand'      => 'TH',
        'Tunisia'       => 'TN',
        'Turkey'        => 'TR',
        'Uganda'        => 'UG',
        'United Arab Emirates' => 'AE',
        'United Kingdom' => 'GB',
        'United States' => 'US',
        'Uruguay'       => 'UY',
        'Venezuela'     => 'VE',
        'Vietnam'       => 'VN',
    );
       if(isset($country[$key])){
        return $country[$key];
       }
       else{
            return 'US';
       }
    }


}