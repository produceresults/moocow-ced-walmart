<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class Ced_Walmart_Model_Observer
{

    /**
     * Predispath admin action controller
     *
     * @param Varien_Event_Observer $observer
     */
    public function preDispatch(Varien_Event_Observer $observer)
    {
      /*  if (Mage::getSingleton('admin/session')->isLoggedIn()) {
            $feedModel = Mage::getModel('walmart/feed');
            $feedModel->checkUpdate();
        }*/
    }
  

     public function controllerFrontInitBefore(Varien_Event_Observer $event)
    {
        //self::init();
    }
 

        public function adminhtmlWidgetContainerHtmlBefore($event) {
            $block = $event->getBlock();

            if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
				$id=Mage::app()->getRequest()->getParam('order_id');
				$increment_id = Mage::getModel('sales/order')->load($id)->getIncrementId();

				$ifexist=count(Mage::getModel('walmart/walmartorder')->getCollection()->addFieldToFilter('magento_order_id',$increment_id));

				if($ifexist){
					$block->removeButton('order_ship');   // Remove tab by id
					$block->removeButton('order_invoice');
					$block->removeButton('order_creditmemo');
				}
            }
        }

        public function shipbywalmart($observer)
        {

        $conName = Mage::app()->getRequest()->getControllerName();       

         if(( $conName != 'auctane' ) && ($conName != '') ) {
             $shipment = $observer->getEvent()->getShipment();
             $flag = false;
             $magento_order_id = $shipment->getOrder()->getIncrementId();
             $magento_order_data = Mage::getModel('walmart/walmartorder')->getCollection()->getData();
             $ses_var = Mage::getSingleton('core/session')->getShip_by_walmart();

             foreach ($magento_order_data as $key => $value) {
                    if($value['magento_order_id'] == $magento_order_id)$flag = true;
                }
                if($flag == true && $ses_var == true)Mage::getSingleton('core/session')->unsShip_by_walmart();
                elseif($flag == true && !$ses_var)
                {
                     Mage::getSingleton('core/session')->unsShip_by_walmart();
                     Mage::getSingleton('core/session')->addError('This Order is Walmart Order create shipment by walmart');
                    Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
                    Mage::app()->getResponse()->sendResponse();
                     exit;
                }
            }            
        }
	public function checkEnabled()
    {
        $helper = Mage::helper('walmart');
        if (!$helper->isEnabled()) {
            Mage::app()->getFrontController()->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
            Mage::app()->getFrontController()->getResponse()->setHeader('Status', '404 File not found');
            $request = Mage::app()->getRequest();
            $request->initForward()
                ->setControllerName('indexController')
                ->setModuleName('Mage_Cms')
                ->setActionName('defaultNoRoute')
                ->setDispatched(false);
            return;
        }
    }
	/*
	*   this observer created for getting listing direct cancel orders
	*/
	public function directCancel(){
        $orderdata = Mage::helper('walmart')->CGetRequest('/orders/directedCancel');
        //$response = json_decode($orderdata, true);

		$this->createOrder();


		/*
        //Mage::log($response, null, 'mylogfile.log');

        $autoReject=false;

        if (isset($response['order_urls']) && count($response['order_urls']) > 0) {
            foreach ($response['order_urls'] as $walmartorderurl) {
                $result = Mage::helper('walmart')->CGetRequest($walmartorderurl);
                $resultObject = json_decode($result);
                $resultarray = json_decode($result, true);
                $orderitems_count = count($resultarray['order_items']);
                $recursive_itemcount = 0;
                $reject_items_arr = array();
                if (sizeof($resultarray) > 0 && isset($resultarray['merchant_order_id']) && ($resultarray['status']=='acknowledged')) {
                    foreach($resultarray['order_items'] as $arr){
                        $updatedQty=$arr['request_order_quantity'] - $arr['request_order_cancel_qty'];
                        $uniqui_id = mt_rand(10, 10000124);
                        $shipment_arr[] = array('shipment_item_id' => "$uniqui_id",
                            'merchant_sku' => $arr['merchant_sku'],
                            'response_shipment_sku_quantity' => (int)$updatedQty,
                            'response_shipment_cancel_qty' => (int)$arr['request_order_cancel_qty']
                        );

                        if($arr['request_order_quantity']==$arr['request_order_cancel_qty']){
                            $recursive_itemcount++;
                            $autoReject=true;
                            $reject_items_arr[] = array('order_item_acknowledgement_status' => 'nonfulfillable - invalid merchant SKU',
                                'order_item_id' => $arr['order_item_id']);
                        }
                    }

                    $merchantOrderid = $resultarray['merchant_order_id'];
                    $walmartOrder = Mage::getModel('walmart/walmartorder')->getCollection()->addFieldToFilter('merchant_order_id', $merchantOrderid)->getFirstItem();
                    $id=$walmartOrder->getId();
                    $mage_id=$walmartOrder->getData('magento_order_id');
                    if ($autoReject && ($recursive_itemcount == $orderitems_count)) {
                        $order_id =$merchantOrderid;
                        $offset_end = Mage::helper('walmart/walmart')->getStandardOffsetUTC();
                        if (empty($offset_end) || trim($offset_end) == '') {
                            $offset = '.0000000-00:00';
                        } else {
                            $offset = '.0000000' . trim($offset_end);
                        }
                        $shipTodate = date("Y-m-d");
                        $shipTotime = date("h:i:s");
                        $storeId = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_storeid');
                        $website = Mage::getModel('core/store')->load($storeId);
                        $websiteId = $website->getWebsiteId();
                        $zip = "01001";
                        $tommorow = mktime(date("H"), date("i"), date("s"), date("m"), date("d") + 1, date("Y"));
                        $exptime = date("Y-m-d", $tommorow);
                        $Ship_todate = $shipTodate . 'T' . $shipTotime . $offset;
                        $Exp_delivery = date("Y-m-d", $exptime) . 'T' . $shipTotime . $offset;
                        $Carr_pickdate = date("Y-m-d", $exptime) . 'T' . $shipTotime . $offset;
                        $carrier = "FedEx";
                        $inc_id = mt_rand(10, 100001244);
                        $data_ship = array();
                        $data_ship['shipments'][] = array(
                            'alt_shipment_id' => $inc_id,
                            'shipment_tracking_number' => "$inc_id",
                            'response_shipment_date' => $Ship_todate,
                            'response_shipment_method' => '',
                            'expected_delivery_date' => $Exp_delivery,
                            'ship_from_zip_code' => "$zip",
                            'carrier_pick_up_date' => $Carr_pickdate,
                            'carrier' => "$carrier",
                            'shipment_items' => $shipment_arr
                        );

                        if ($data_ship) {
                            $data = Mage::helper('walmart')->CPutRequest('/orders/' . $order_id . '/shipped', json_encode($data_ship));
                            $responsedata = json_decode($data);

                            if($responsedata == NULL){
                                $saveWalmartorder=Mage::getModel('walmart/walmartorder')->load($id);
                                $saveWalmartorder->setData('status','cancelled');
                                $saveWalmartorder->save();

                                $orderModel = Mage::getModel('sales/order')->loadByIncrementId($mage_id);
                                if($orderModel->canCancel()){
                                    $orderModel->cancel();
                                    $orderModel->setStatus('canceled');
                                    $orderModel->save();
                                }
                            }
                        }
                    }

                    echo $id.'<br/>';
                    if(strlen($id)){
                    $updateWalmartOrderData=Mage::getModel('walmart/walmartorder')->load($id);
                    $updateWalmartOrderData->setData('order_data',serialize($resultObject));
                    $updateWalmartOrderData->setData('customer_cancelled',1);
                    $updateWalmartOrderData->save();
                    }
                }
            }
        } */
	}

    public function cancelWalmartorder($observer)
    {
        $order = $observer->getOrder();
        $url=Mage::helper('core/url')->getCurrentUrl();
        $inc = $order->getData('increment_id');
        $reject=Mage::getModel('walmart/walmartorder')->getCollection()->addFieldToFilter('magento_order_id', $inc)->getFirstItem();

        if (($order->getState() == Mage_Sales_Model_Order::STATE_CANCELED) && (preg_match("/\bcancel\b/i", $url)) && ($reject->getId()!='')) {
            $order_id =$reject->getData('merchant_order_id');
            $offset_end = Mage::helper('walmart/walmart')->getStandardOffsetUTC();
            if (empty($offset_end) || trim($offset_end) == '') {
                $offset = '.0000000-00:00';
            } else {
                $offset = '.0000000' . trim($offset_end);
            }
            $shipTodate = date("Y-m-d");
            $shipTotime = date("h:i:s");
            $storeId = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_storeid');
            $website = Mage::getModel('core/store')->load($storeId);
            $websiteId = $website->getWebsiteId();
            $zip = "01001";
            $tommorow = mktime(date("H"), date("i"), date("s"), date("m"), date("d") + 1, date("Y"));
            $exptime = date("Y-m-d", $tommorow);
            $Ship_todate = $shipTodate . 'T' . $shipTotime . $offset;
            $Exp_delivery = date("Y-m-d", $exptime) . 'T' . $shipTotime . $offset;
            $Carr_pickdate = date("Y-m-d", $exptime) . 'T' . $shipTotime . $offset;

            $inc_id = $order->getData("increment_id");

            $temp_carrier = unserialize($reject->getData('order_data'));
            $carrier = $temp_carrier->order_detail->request_shipping_carrier;
            if(($carrier == '') || ($carrier == null)){$carrier = 'Fedex';}

            $shipment_arr = array();
            $items = $order->getAllItems();
            foreach ($items as $i) {

                $uniqui_id = mt_rand(10, 10000124);
                $shipment_arr[] = array('shipment_item_id' => "$uniqui_id",
                    'merchant_sku' => $i->getSku(),
                    'response_shipment_sku_quantity' => 0,
                    'response_shipment_cancel_qty' => (int)$i->getData('qty_ordered')
                );
            }

            $data_ship = array();
            $data_ship['shipments'][] = array(
                'alt_shipment_id' => $inc_id,
                'shipment_tracking_number' => "$inc_id",
                'response_shipment_date' => $Ship_todate,
                'response_shipment_method' => '',
                'expected_delivery_date' => $Exp_delivery,
                'ship_from_zip_code' => "$zip",
                'carrier_pick_up_date' => $Carr_pickdate,
                'carrier' => "$carrier",
                'shipment_items' => $shipment_arr
            );

            if ($data_ship) {
                $data = Mage::helper('walmart')->CPutRequest('/orders/' . $order_id . '/shipped', json_encode($data_ship));
                $responsedata = json_decode($data);

                if($responsedata == NULL){
                    $this->saveWalmartOrder($reject->getId());
                    /*$walmartId=$reject->getId();
                    $saveWalmartorder=Mage::getModel('walmart/walmartorder')->load($walmartId);
                    $saveWalmartorder->setData('status','rejected');
                    $saveWalmartorder->save();*/
                }

             }
        }
    }

    public function updateProduct()
    {
        $success_count = 0;
        $error_count = 0;
        $collection = Mage::getModel('walmart/fileinfo')->getCollection()->addFieldToFilter('Status', 'Acknowledged')->addFieldToSelect('walmart_file_id')->addFieldToSelect('id')->getData();

        $error_file_count = 0;
        foreach ($collection as $jdata) {

            if (isset($jdata['walmart_file_id']) && $jdata['walmart_file_id'] != null) {
                $response = Mage::helper('walmart')->CGetRequest('/files/' . $jdata['walmart_file_id']);
                $resvalue = json_decode($response);
                if (trim($resvalue->status) == 'Processed with errors') {
                    $error_count++;
                    $walmartfileid = trim($resvalue->walmart_file_id);
                    $filename = $resvalue->file_name;
                    $filetype = $resvalue->file_type;
                    $status = trim($resvalue->status);
                    $error = $resvalue->error_excerpt;
                    $comma_separatederrors = implode(",", $error);

                    $update = array('status' => trim($resvalue->status));
                    $model = Mage::getModel('walmart/fileinfo')->load($jdata['id'])->addData($update);
                    $model->save();

                    $collectiond = Mage::getModel('walmart/errorfile')->getCollection()->addFieldToFilter('walmartinfofile_id', $jdata['id']);

                    if (count($collectiond) == (int)0) {

                        $data = array(
                            'walmart_file_id' => $walmartfileid,
                            'file_name' => $filename,
                            'file_type' => $filetype,
                            'status' => $status,
                            'error' => $comma_separatederrors,
                            'date' => date('Y-m-d H:i:s'),
                            'walmartinfofile_id' => $jdata['id'],
                        );

                        $model1 = Mage::getModel('walmart/errorfile')->addData($data);
                        $model1->save();

                        $error_file_count++;
                    }

                } else {

                    $update = array('status' => trim($resvalue->status));
                    $model = Mage::getModel('walmart/fileinfo')->load($jdata['id'])->addData($update);
                    $model->save();
                    $success_count++;

                }
            }
        }

    }

    /**
     * @Walmart Orders Synchronisation
     * Create Available Walmart Orders in Magento
     */
    public function createOrder()
    {
             
        $data =$this->fetchLatestWalmartOrders();

    }

//delete files once in a day code start
    public function walmartfilesDelete()
    {
        $url = Mage::getBaseDir();
        $path = $url.'/var/walmartupload/';
        $handle = opendir($path);
         if ($handle = opendir($path))
         { 
            while (false !== ($file = readdir($handle)))
              { 
                 $filelastmodified = filemtime($path . $file);
                 //24 hours in a day * 3600 seconds per hour
                if((time() - $filelastmodified) > 24*3600 && is_file($file))
                 {
                    unlink($path . $file);
                }

             }
            closedir($handle); 
        }
    }
//delete files once in a day code end


    public function _assignCustomer($result, $customer, $websiteId, $store, $email)
    {
        if (is_object($customer) && $customer->getId() == NULL && $customer->getId() == '') {
            try {
                $Cname = $result['buyer']['name'];
                if (trim($Cname) == '' || $Cname == null) {
                    $Cname = $result['shipping_to']['recipient']['name'];
                }
                $Cname = preg_replace('/\s+/', ' ', $Cname);
                $customer_name = explode(' ', $Cname);

                if (!isset($customer_name[1]) || $customer_name[1] == '') {
                    $customer_name[1] = $customer_name[0];
                }

                $customer = Mage::getModel('customer/customer');
                $customer->setWebsiteId($websiteId)
                    ->setStore($store)
                    ->setFirstname($customer_name[0])
                    ->setLastname($customer_name[1])
                    ->setEmail($email)
                    ->setPassword("password");
                $customer->save();


                return $customer;

            } catch (Exception $e) {
                //$message = "please check the customer Email Id either email format or enter email properly into walmart setting!";
                $message = $e->getMessage();
                $walmartOrderError = Mage::getModel('walmart/orderimport');
                $walmartOrderError->setMerchantOrderId($result['merchant_order_id']);
				$walmartOrderError->setReferenceNumber($result['reference_order_id']);
                $walmartOrderError->setReason($message);
                $walmartOrderError->save();
                return false;
            }
        } else {
            return $customer;
        }

     }

    public function prepareQuote($result, $customer, $websiteId, $store, $email, $resultObject)
    {
        /*$productexist_config = Mage::getStoreConfig('walmart_options/acknowledge_options_options/exist');
        $productoutofstock_config = Mage::getStoreConfig('walmart_options/acknowledge_options/outofstock');
        $productdisabled_config = Mage::getStoreConfig('walmart_options/acknowledge_options/pdisabled');
        $Quote_execute = false;
        */


        $shippingMethod = 'shipwalmartcom';
        $paymentMethod= Mage::getStoreConfig('walmart_options/ced_walmart/walmart_default_payment');
        if($paymentMethod== NULL || $paymentMethod==""){
            $paymentMethod = 'paywalmartcom';
        }
        $productArray = array();
        $baseCurrency = $store->getBaseCurrencyCode();
        $items_array = $result['order_items'];
        $baseprice = 0;
        $shippingcost = 0;
        $tax = 0;
        $storeId = $store->getId();
        $autoReject = false;
        $reject_items_arr = array();
        $order_items_qty_arr = array();
        $order = Mage::getModel('sales/order')
            ->setStoreId($storeId)
            ->setQuoteId(0)
            ->setGlobal_currency_code($baseCurrency)
            ->setBase_currency_code($baseCurrency)
            ->setStore_currency_code($baseCurrency)
            ->setOrder_currency_code($baseCurrency);

        foreach ($items_array as $item) {
            $message = '';
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item['merchant_sku']);
            if ($product) {
                $product = Mage::getModel('catalog/product')->load($product->getEntityId());
                if ($product->getStatus() == '1') {
                    $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                    if (($stock->getQty() > 0) && ($stock->getIsInStock() == '1') && ($stock->getQty() >= $item['request_order_quantity']) && ($item['request_order_quantity'] != $item['request_order_cancel_qty'])) {

                        /*
                        for inventory mail code start
                        */
                        if($stock->getQty() < 3)
                        {

                            $order_items_qty_arr[$item['merchant_sku']] = $stock->getQty();
                        }

                        /*
                        for inventory mail code end
                        */
                        $productArray[] = array('id' => $product->getEntityId(), 'qty' => $item['request_order_quantity']);
                        $price = $item['item_price']['base_price'];
                        $qty = $item['request_order_quantity'];
                        $cancelqty= $item['request_order_cancel_qty'];
                        //if($cancelqty!=0)$qty=$qty - $cancelqty;
                        $baseprice += $qty * $price;
                        $shippingcost += ($item['item_price']['item_shipping_cost'] * $qty) + ($item['item_price']['item_shipping_tax'] * $qty);
                        $tax += $item['item_price']['item_tax'];

                        $rowTotal = $price * $qty;
                        $orderItem = Mage::getModel('sales/order_item')
                            ->setStoreId($storeId)
                            ->setQuoteItemId(0)
                            ->setQuoteParentItemId(NULL)
                            ->setProductId($product->getEntityId())
                            ->setProductType($product->getTypeId())
                            ->setQtyBackordered(NULL)
                            ->setTotalQtyOrdered($qty)
                            ->setQtyOrdered($qty)
                            ->setName($product->getName())
                            ->setSku($product->getSku())
                            ->setPrice($price)
                            ->setBasePrice($price)
                            ->setOriginalPrice($price)
                            ->setRowTotal($rowTotal)
                            ->setBaseRowTotal($rowTotal);

                        //$subTotal += $rowTotal;
                        $order->addItem($orderItem);
                        $Quote_execute = True;

                    } else {

                        /*if ($productoutofstock_config) {*/
                            $autoReject = true;
                            $reject_items_arr[] = array('order_item_acknowledgement_status' => 'nonfulfillable - invalid merchant SKU',
                                'order_item_id' => $item['order_item_id']);
                        /*}*/
                        $message = "Product " . $item['merchant_sku'] . " is Out Of Stock";
                        $walmartOrderError = Mage::getModel('walmart/orderimport');
                        $walmartOrderError->setMerchantOrderId($result['merchant_order_id']);
						$walmartOrderError->setReferenceNumber($result['reference_order_id']);
                        $walmartOrderError->setReason($message);
                        $walmartOrderError->setOrderItemId($item['order_item_id']);
                        $walmartOrderError->save();
                        $Quote_execute = False;
                    }
                } else {


                    /*if ($productdisabled_config) {*/
                        $autoReject = true;
                        $reject_items_arr[] = array('order_item_acknowledgement_status' => 'nonfulfillable - invalid merchant SKU',
                            'order_item_id' => $item['order_item_id']);
                    /*}*/
                    $message = "Product " . $item['merchant_sku'] . " is Not Enabled!";
                    $walmartOrderError = Mage::getModel('walmart/orderimport');
                    $walmartOrderError->setMerchantOrderId($result['merchant_order_id']);
					$walmartOrderError->setReferenceNumber($result['reference_order_id']);
                    $walmartOrderError->setReason($message);
                    $walmartOrderError->setOrderItemId($item['order_item_id']);
                    $walmartOrderError->save();
                    $Quote_execute = False;
                }
            }else{


                /*if ($productexist_config) {*/
                    $autoReject = true;
                    $reject_items_arr[] = array('order_item_acknowledgement_status' => 'nonfulfillable - invalid merchant SKU',
                        'order_item_id' => $item['order_item_id']);
                /*}*/
                $message = "Product SKU " . $item['merchant_sku'] . " Not Found on the site!";
                $walmartOrderError = Mage::getModel('walmart/orderimport');
                $walmartOrderError->setMerchantOrderId($result['merchant_order_id']);
				$walmartOrderError->setReferenceNumber($result['reference_order_id']);
                $walmartOrderError->setReason($message);
                $walmartOrderError->setOrderItemId($item['order_item_id']);
                $walmartOrderError->save();
                $Quote_execute = False;
            }
        }

        if ($autoReject) {
            $data_var = array();
            $data_var['acknowledgement_status'] = "rejected - item level error";
            $data_var['order_items'] = $reject_items_arr;
            $data = Mage::helper('walmart')->CPutRequest('/orders/' . $result['merchant_order_id'] . '/acknowledge', json_encode($data_var));
            $response = json_decode($data);

            $reject=Mage::getModel('walmart/walmartorder')->getCollection()->addFieldToFilter('merchant_order_id', $result['merchant_order_id'])->getFirstItem();
            if($response == NULL){
                //$this->saveWalmartOrder($reject->getId());
            }
        }

        if (count($productArray) > 0 && count($items_array) == count($productArray) && !$autoReject) {
            $transaction = Mage::getModel('core/resource_transaction');
            $order->setCustomer_email($customer->getEmail())
                ->setCustomerFirstname($customer->getFirstname())
                ->setCustomerLastname($customer->getLastname())
                ->setCustomerGroupId($customer->getGroupId())
                ->setCustomer_is_guest(0)
                ->setCustomer($customer);

            $CshippingInfo = $result['shipping_to']['recipient']['name'];
            $CshippingInfo = preg_replace('/\s+/', ' ', $CshippingInfo);
            $customer_shippingInfo = explode(' ', $CshippingInfo);

            if (!isset($customer_shippingInfo[1]) || $customer_shippingInfo[1] == '') {
                $customer_shippingInfo[1] = $customer_shippingInfo[0];
            }
            // set Billing Address
            try {
                $billing = $customer->getDefaultBillingAddress();
                $complete_address1 = $result['shipping_to']['address']['address1'];
                $complete_address2 = $result['shipping_to']['address']['address2'];

                if ($complete_address2 != null && trim($complete_address2) != '') {
                    $complete_address1 = $complete_address1 . ' ' . $complete_address2;
                }
                $billingAddress = Mage::getModel('sales/order_address')
                    ->setStoreId($storeId)
                    ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
                    ->setCustomerId($customer->getId())
                    ->setCustomerAddressId($customer->getDefaultBilling())
                    ->setCustomer_address_id('')
                    ->setPrefix('')
                    ->setFirstname($customer_shippingInfo[0])
                    ->setMiddlename('')
                    ->setLastname($customer_shippingInfo[1])
                    ->setSuffix('')
                    ->setCompany('')
                    //->setStreet($result['shipping_to']['address']['address1'])
                    ->setStreet($complete_address1)
                    ->setCity($result['shipping_to']['address']['city'])
                    ->setCountry_id('US')
                    ->setRegion($result['shipping_to']['address']['state'])
                    ->setRegion_id('')
                    ->setPostcode($result['shipping_to']['address']['zip_code'])
                    ->setTelephone($result['shipping_to']['recipient']['phone_number'])
                    ->setFax('');
                $order->setBillingAddress($billingAddress);

                $shipping = $customer->getDefaultShippingAddress();
                $shippingAddress = Mage::getModel('sales/order_address')
                    ->setStoreId($storeId)
                    ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
                    ->setCustomerId($customer->getId())
                    ->setCustomerAddressId($customer->getDefaultShipping())
                    ->setCustomer_address_id('')
                    ->setPrefix('')
                    ->setFirstname($customer_shippingInfo[0])
                    ->setMiddlename('')
                    ->setLastname($customer_shippingInfo[1])
                    ->setSuffix('')
                    ->setCompany('')
                    //->setStreet($result['shipping_to']['address']['address1'])
                    ->setStreet($complete_address1)
                    ->setCity($result['shipping_to']['address']['city'])
                    ->setCountry_id('US')
                    ->setRegion($result['shipping_to']['address']['state'])
                    ->setRegion_id('')
                    ->setPostcode($result['shipping_to']['address']['zip_code'])
                    ->setTelephone($result['shipping_to']['recipient']['phone_number'])
                    ->setFax('');
                $order->setShippingAddress($shippingAddress)
                    ->setShippingMethod($shippingMethod)
                    ->setShippingDescription(Mage::getStoreConfig("carriers/shipwalmartcom/title") . "-" . $shippingMethod);
                $orderPayment = Mage::getModel('sales/order_payment')
                    ->setStoreId($storeId)
                    ->setCustomerPaymentId(0)
                    ->setMethod($paymentMethod);
                //->setPo_number(' - ');
                $order->setPayment($orderPayment);


                $order->setSubtotal($baseprice)
                    ->setBaseSubtotal($baseprice)
                    ->setShippingAmount($shippingcost)
                    ->setBaseShippingAmount($shippingcost)
                    ->setTaxAmount($tax)
                    ->setBaseTaxAmount($tax)
                    ->setGrandTotal($baseprice + $shippingcost + $tax)
                    ->setBaseGrandTotal($baseprice + $shippingcost + $tax);

                $transaction->addObject($order);
                $transaction->addCommitCallback(array($order, 'place'));
                $transaction->addCommitCallback(array($order, 'save'));
                if ($transaction->save() && $order->getId() > 0) {
                    $OrderData = array('order_item_id' => $result['order_items'][0]['order_item_id'],
                        'merchant_order_id' => $result['merchant_order_id'],
                        'merchant_sku' => $result['order_items'][0]['merchant_sku'],
                        'deliver_by' => $result['order_detail']['request_delivery_by'],
                        'magento_order_id' => $order->getIncrementId(),
                        'status' => $result['status'],
                        'order_data' => serialize($resultObject),
                        'reference_order_id' => $result['reference_order_id']
                    );

                    $model = Mage::getModel('walmart/walmartorder')->addData($OrderData);
                    $model->save();

                    //if (Mage::getStoreConfig('walmart_options/walmart_order/active') == 1) {
                     	 $this->autoOrderacknowledge($order->getIncrementId());
                    //}
                    $this->sendmailtoadmin($order->getIncrementId(),$result['order_detail']['request_ship_by'],$order_items_qty_arr);

                    if($order->canInvoice()) {
                        /*$invoiceId = Mage::getModel('sales/order_invoice_api')
                            ->create($order->getIncrementId(), array());
                        $invoice = Mage::getModel('sales/order_invoice')
                            ->loadByIncrementId($invoiceId);
                        $invoice->capture()->save();*/

                        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                        $invoice->register();
                        $invoice->getOrder()->setCustomerNoteNotify(false);
                        $invoice->getOrder()->setIsInProcess(true);

                        $transactionSave = Mage::getModel('core/resource_transaction')
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder());

                        $transactionSave->save();
                    }
                }

            } catch (Exception $e) {
                $message = "Fail To Create Order Due to Error " . $e->getMessage();
                $walmartOrderError = Mage::getModel('walmart/orderimport');
                $walmartOrderError->setMerchantOrderId($result['merchant_order_id']);
				        $walmartOrderError->setReferenceNumber($result['reference_order_id']);
                $walmartOrderError->setOrderItemId($item['order_item_id']);
                $walmartOrderError->setReason($message);
                $walmartOrderError->save();

            }
        }
    }
     /*
     * @Auto Order Notification Mail To Store Admin
     */
     public function sendmailtoadmin($order_id,$ship_date,$lesinvetory)
        {
           $from_email = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_admin_email_id');
            if($from_email)
            {
                    if($ship_date)
                    {
                        $current_date =  new DateTime(date('Y-m-d'));
                        $date_array = array();
                        $date_array = explode('T',$ship_date);
                        $time = new DateTime($date_array[0]);
                        $interval = $current_date->diff($time)->days;
                      if($interval<=2)
                      {
                        

                          if(count($lesinvetory)>0)
                         {
                             $msg = "Hello! \n  Congratulations. You have a New Walmart Order ".$order_id." imported to your Magento admin panel. The Shipment date of this order is near and the inventory of the products existing in this order is also getting low. Please review your admin panel instantly.\n Thanks";
                          $msg = wordwrap($msg,70);
                         mail($from_email,"New Walmart Order Imported",$msg);
                         }
                         else
                         {
                             $msg = "Hello ! \n  Congratulations. You have a New Walmart Order ".$order_id." imported to your Magento admin panel. The Shipmentent Date of this order is near. Please review your admin panel instantly. \n Thanks";
                          $msg = wordwrap($msg,70);
                         mail($from_email,"New Walmart Order Imported",$msg);
                         }
                      }
                      else
                      {
                        
                         if(count($lesinvetory)>0)
                         {
                             $msg = "Hello !\n  Congratulations. You have a New Walmart Order ".$order_id." imported to your Magento admin panel. The inventory of the products existing in this order is getting low. Therefore, please review your admin panel and update it.\n Thanks";
                          $msg = wordwrap($msg,70);
                         mail($from_email,"New Walmart Order Imported",$msg);
                         }
                         else
                         {
                            $msg = "Hello !\n  Congratulations. You have a New Walmart Order ".$order_id." imported to your Magento admin panel. Please review your admin panel.\n Thanks";
                          $msg = wordwrap($msg,70);
                         mail($from_email,"New Walmart Order Imported",$msg);
                         }
                      }
                       
                    }
                        else
                        {
                             $msg = "Hello !\n  Congratulations. You have a New Walmart Order ".$order_id." imported to your Magento admin panel. Please review your admin panel.\n Thanks";
                          $msg = wordwrap($msg,70);
                         mail($from_email,"New Walmart Order Imported",$msg);
                        }
                 
            }

           return;
        }

    /*
     * @Auto Order Acknowledgement Process
     */
    public function autoOrderacknowledge($Incrementid)
    {
        $resultdata = Mage::getModel('walmart/walmartorder')->getCollection()
            ->addFieldToFilter('magento_order_id', $Incrementid)
            ->addFieldToSelect('order_data')
            ->addFieldToSelect('id')
            ->addFieldToSelect('merchant_order_id')
            ->getData();


        if (empty($resultdata) || count($resultdata) == 0) {
            return 0;
        }

        $serialize_data = unserialize($resultdata[0]['order_data']);

        if (empty($serialize_data) || count($serialize_data) == 0) {

            $result = Mage::helper('walmart')->CGetRequest('/orders/withoutShipmentDetail/' . $resultdata[0]['merchant_order_id']);
            $Ord_result = json_decode($result);

            if (empty($result) || count($result) == 0) {
                return 0;
            } else {

                $jobj = Mage::getModel('walmart/walmartorder')->load($resultdata[0]['id']);
                $jobj->setOrderData(serialize($Ord_result));
                $jobj->save();

                $serialize_data = $Ord_result;
            }
        }

        if (empty($serialize_data)) {
            return 0;
        }

        $fullfill_array = array();
        foreach ($serialize_data->order_items as $k => $valdata) {
            $fullfill_array[] = array('order_item_acknowledgement_status' => 'fulfillable',
                'order_item_id' => $valdata->order_item_id);
        }


        $order_id = $resultdata[0]['merchant_order_id'];
        $data_var = array();
        $data_var['acknowledgement_status'] = "accepted";


        $data_var['order_items'] = $fullfill_array;


        $data = Mage::helper('walmart')->CPutRequest('/orders/' . $order_id . '/acknowledge', json_encode($data_var));

        $response = json_decode($data);

        if (!empty($response) && $response != null && count($response->errors) > 0 && $response->errors[0] != "") {
            return 0;
        } else {
            $modeldata = Mage::getModel('walmart/walmartorder')->getCollection()
                ->addFieldToFilter('magento_order_id', $Incrementid)->getData();

            if (count($modeldata) > 0) {
                $id = $modeldata[0]['id'];
                $model = Mage::getModel('walmart/walmartorder')->load($id);
                $model->setStatus('acknowledged');
                $model->save();
            }
        }
        return 0;
    }

    public function addButton(Varien_Event_Observer $event)
    {

        $block = $event->getBlock();
        if ($block->getId() != 'sales_order_view') {
            return $this;
        }

        $order = $block->getOrder();
        if (!$order->getId()) {
            return $this;
        }

        $orderid = $order->getId();
        $Incrementid = $order->getIncrementId();
        $resultdata = Mage::registry('current_walmart_order');

        if (count($resultdata) == 0) {
            $resultdata = Mage::getModel('walmart/walmartorder')->getCollection()
                ->addFieldToFilter('magento_order_id', $Incrementid)
                //->addFieldToSelect('status')
                ->getData();
        }
      //  $status = $resultdata[0]['status'];
        $status = 'ready';
        if ($status == 'ready') {
            $block->addButton('delete', array(
                'label' => 'Acknowledge',
                'class' => 'Acknowledge',
                'onclick' => 'deleteConfirm(\'' . Mage::helper('adminhtml')->__('Are you sure you want to send acknowledge for this order?')
                    . '\', \'' . $block->getUrl('adminhtml/adminhtml_walmartorder/acknowledge', array('increment_id' => $Incrementid)) . '\')',
            ));
            $block->addButton('delete1', array(
                'label' => 'Reject',
                'class' => 'Reject',
                'onclick' => 'deleteConfirm(\'' . Mage::helper('adminhtml')->__('Are you sure you want to send rejection for this order?')
                    . '\', \'' . $block->getUrl('adminhtml/adminhtml_walmartorder/rejectreason', array('increment_id' => $Incrementid)) . '\')',
            ));
        } else if ($status == 'acknowledged') {

            $block->addButton('delete', array(
                'label' => 'Acknowledge',
                'class' => 'Acknowledge',
                'disabled' => true,
                'onclick' => 'deleteConfirm(\'' . Mage::helper('adminhtml')->__('Are you sure you want to send acknowledge for this order?')
                    . '\', \'' . $block->getUrl('adminhtml/adminhtml_walmartorder/acknowledge', array('increment_id' => $Incrementid)) . '\')',


            ));

            $block->addButton('delete1', array(
                'label' => 'Reject',
                'class' => 'Reject',
                'disabled' => true,
                'onclick' => 'deleteConfirm(\'' . Mage::helper('adminhtml')->__('Are you sure you want to send rejection for this order?')
                    . '\', \'' . $block->getUrl('adminhtml/adminhtml_walmartorder/reject', array('increment_id' => $Incrementid)) . '\')',


            ));
        }
    }

    public function saveAttribute(Varien_Event_Observer $event)
    {
        $attribute = $event->getDataObject();
        $mcode = $attribute->getAttributeCode();
        $walmartId = Mage::app()->getRequest()->getParam('walmart_attribute_id');
        $mwalmartattr_id = trim($walmartId);
        $map_attribute = false;
		$exist=false;

        if (isset($mwalmartattr_id) && $mwalmartattr_id != null && $mwalmartattr_id != '') {
           $exist =true;

        } else {
           $exist =false;
        }

        $resource = Mage::getSingleton('core/resource');
        $table = $resource->getTableName('walmart/walmartattribute');

		$readconnection = $resource->getConnection('core_read');
		$query = 'SELECT * FROM ' . $table . ' WHERE `magento_attr_id`=' . $attribute->getId().'';
		$row = $readconnection->fetchRow($query);

		if($exist){
            if (count($row) > 0 && isset($row['id']) && $row['id'] != '') {
				if($mwalmartattr_id == $row['walmart_attr_id']){
					$map_attribute = false;
				}else{
					$query = 'DELETE FROM ' . $table . ' WHERE `id` =' . $row['id'] . '';
	                $writeConnection = $resource->getConnection('core_write');
    	            $writeConnection->query($query);

					$map_attribute = true;
				}
			}else{
				$map_attribute = true;
			}
        }else{
			if (count($row) > 0 && isset($row['id']) && $row['id'] != '') {
				$query = 'DELETE FROM ' . $table . ' WHERE `id` =' . $row['id'].'';
                $writeConnection = $resource->getConnection('core_write');
                $writeConnection->query($query);
            }
			$map_attribute = false;
		}

        if ($map_attribute) {

            $units_or_options = array();
            $csv = new Varien_File_Csv();
            $file = Mage::getBaseDir("var") . DS . "walmartcsv" . DS . "Walmart_Taxonomy_attribute.csv";

            if (!file_exists($file)) {
                Mage::getSingleton('adminhtml/session')->addError('Walmart Extension Csv missing please check "Walmart_Taxonomy_attribute.csv" exist at "var/walmartcsv" location.');
                return;
            }

            $taxonomy = $csv->getData($file);
            unset($taxonomy[0]);

            $save_attr_id = false;
            $save_attr_unitType = false;

            foreach ($taxonomy as $txt) {
                if (number_format($txt[0], 0, '', '') == $mwalmartattr_id) {

                    $save_attr_id = number_format($txt[0], 0, '', '');
                    $save_attr_unitType = $txt[3];
                    break;
                }
            }

            if ($save_attr_id == false) {
                Mage::getSingleton('adminhtml/session')->addError('Requested Walmart Attribute id:  ' . $walmartId . ' does not exist in CSV.');
                return;
            }

            $csv = new Varien_File_Csv();
            $file = Mage::getBaseDir("var") . DS . "walmartcsv" . DS . "Walmart_Taxonomy_attribute_value.csv";


            if (!file_exists($file)) {
                Mage::getSingleton('adminhtml/session')->addError('Walmart Extension Csv missing please check "Walmart_Taxonomy_attribute_value.csv" exist at "var/walmartcsv" location.');
                return;
            }
            $taxonomy = $csv->getData($file);


            unset($taxonomy[0]);
            try {
                if ($save_attr_unitType == 2) {
                    foreach ($taxonomy as $txt) {
                        $numberfomat_id = number_format($txt[0], 0, '', '');

                        if ($save_attr_id == $numberfomat_id) {

                            $units_or_options[] = $txt[2];
                        }
                    }
                } else if ($save_attr_unitType == 0) {

                    foreach ($taxonomy as $txt) {
                        if ($save_attr_id == number_format($txt[0], 0, '', '')) {
                            $units_or_options[] = $txt[1];
                        }
                    }

                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }

            unset($query);

            $resource = Mage::getSingleton('core/resource');
            $table = $resource->getTableName('walmart/walmartattribute');
            $readconnection = $resource->getConnection('core_read');
            $query = 'SELECT `id` FROM ' . $table . ' WHERE `magento_attr_id`=' . $attribute->getId() . '';
            $row = $readconnection->fetchCol($query);

            $query = '';
            $excute_qry = false;
            $unit_or_option_exists = false;
            $allunits = '';
            $alloptions = '';

            if ($save_attr_unitType == 2) {

                if (count($units_or_options) > 0) {
                    $unit_or_option_exists = true;
                    $allunits = implode(',', $units_or_options);

                    if (count($row) > 0 && isset($row[0]) && $row[0] != '') {
                        $query = 'UPDATE ' . $table . ' SET `walmart_attr_id` = ' . $save_attr_id . ', `magento_attr_code` = "' . $mcode . '" , `unit` = "' . addslashes($allunits) . '" ,`list_option` = Null , `freetext` = 2 WHERE `magento_attr_id` = ' . $attribute->getId() . '';
                        $excute_qry = true;
                    } else {

                        $query = 'INSERT INTO ' . $table . ' (walmart_attr_id, magento_attr_id,magento_attr_code, freetext,unit,list_option) VALUES (' . $save_attr_id . ', ' . $attribute->getId() . ',"' . $mcode . '", 2 ,"' . addslashes($allunits) . '",Null)';
                        $excute_qry = true;
                    }
                } else {
                    if (count($row) > 0 && isset($row[0]) && $row[0] != '') {
                        $query = 'UPDATE ' . $table . ' SET `walmart_attr_id` = ' . $save_attr_id . ' ,`magento_attr_code` = "' . $mcode . '", `unit`= NULL , `freetext` = 2  WHERE `magento_attr_id` = ' . $attribute->getId() . '';
                        $excute_qry = true;
                    } else {
                        $query = 'INSERT INTO ' . $table . ' (walmart_attr_id, magento_attr_id, magento_attr_code,freetext)VALUES (' . $save_attr_id . ', ' . $attribute->getId() . ',"' . $mcode . '",2)';
                        $excute_qry = true;
                    }
                }
            } else if ($save_attr_unitType == 0) {

                if (count($units_or_options) > 0) {
                    $unit_or_option_exists = true;
                    $alloptions = implode(',', $units_or_options);

                    if (count($row) > 0 && isset($row[0]) && $row[0] != '') {

                        $query = 'UPDATE ' . $table . ' SET `walmart_attr_id` = ' . $save_attr_id . ', `magento_attr_code` = "' . $mcode . '" , `unit` = Null, `list_option` = "' . $alloptions . '" , `freetext` = 0  WHERE `magento_attr_id` = ' . $attribute->getId() . '';
                        $excute_qry = true;
                    } else {
                        $query = 'INSERT INTO ' . $table . ' (walmart_attr_id, magento_attr_id,magento_attr_code, freetext,unit,list_option) VALUES (' . $save_attr_id . ', ' . $attribute->getId() . ',"' . $mcode . '",0 ,Null,"' . $alloptions . '")';
                        $excute_qry = true;
                    }

                } else {
                    if (count($row) > 0 && isset($row[0]) && $row[0] != '') {

                        $query = 'UPDATE ' . $table . ' SET `walmart_attr_id` = ' . $save_attr_id . ',`magento_attr_code` = "' . $mcode . '", `freetext` = 0 WHERE `magento_attr_id` = ' . $attribute->getId() . '';
                        $excute_qry = true;
                    } else {

                        $query = 'INSERT INTO ' . $table . ' (walmart_attr_id, magento_attr_id,magento_attr_code, freetext) VALUES (' . $save_attr_id . ', ' . $attribute->getId() . ',"' . $mcode . '",0)';
                        $excute_qry = true;
                    }
                }
            } else {

                if (count($row) > 0 && isset($row[0]) && $row[0] != '') {
                    $query = 'UPDATE ' . $table . ' SET `walmart_attr_id` = ' . $save_attr_id . ' ,`magento_attr_code` = "' . $mcode . '" , `freetext` = 1 WHERE `magento_attr_id` = ' . $attribute->getId() . '';
                    $excute_qry = true;
                } else {
                    $query = 'INSERT INTO ' . $table . ' (walmart_attr_id, magento_attr_id,magento_attr_code, freetext) VALUES (' . $save_attr_id . ', ' . $attribute->getId() . ',"' . $mcode . '",1)';
                    $excute_qry = true;
                }

            }

            if ($excute_qry) {
					$resource = Mage::getSingleton('core/resource');
                try {

                    $writeConnection = $resource->getConnection('core_write');
                    $writeConnection->query($query);

                    if ($unit_or_option_exists == true) {
                        if ($save_attr_unitType == 2) {

                            $message = $save_attr_id . ' is a "UNIT" type attribute in Walmart.com so you need to add options values for these units: ' . $allunits . ' Please view details in "Walmart Attribute" tab inside of "Attribute Information"';
                            Mage::getSingleton('adminhtml/session')->addNotice($message);

                        } else {
                            if ($save_attr_unitType == 0) {
                                $message = $save_attr_id . ' is a "List" type attribute in Walmart.com so you need to add these options values : ' . $alloptions . ' Please view details in "Walmart Attribute" tab inside of "Attribute Information"';
                                Mage::getSingleton('adminhtml/session')->addNotice($message);

                            }
                        }
                    }
			    } catch (Exception $e) {
					$readconnection = $resource->getConnection('core_read');
					$query = 'SELECT * FROM ' . $resource->getTableName('walmart/walmartattribute') . ' WHERE `walmart_attr_id`=' . $mwalmartattr_id.'';
					$row = $readconnection->fetchRow($query);
					if(count($mwalmartattr_id)>0){
						Mage::getSingleton('adminhtml/session')->addError("$mwalmartattr_id Walmart Attribute id already mapped with Magento '".$row['magento_attr_code']."' Attribute please remove this mapping if you want to map $mwalmartattr_id Walmart Attribute with $mcode.");
					}else{
						Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					}

                }
            }

        }


    }

    public function walmartreturn()
    {
        $false_return = "";
        $success_return = "";
        $success_count = 0;
        $false_count = 0;
        $data = Mage::helper('walmart')->CGetRequest('/returns/created');

        $response = json_decode($data);
        $response = $response->return_urls;


        if (!empty($response) && count($response) > 0) {

            foreach ($response as $res) {
                $arr = explode("/", $res);
                $returnid = "";
                $returnid = $arr[3];
                $resultdata = Mage::getModel('walmart/walmartreturn')->getCollection()->addFieldToFilter('returnid', $returnid)->getData();
               
                if (empty($resultdata)) {

                    $returndetails = Mage::helper('walmart')->CGetRequest($res);
                    //print_r($returndetails);die();
                    if ($returndetails) {
                        $return = json_decode($returndetails);
                        $serialized_details = serialize($return);

						try{
							$text = array(
								 'merchant_order_id' => $return->merchant_order_id,
								 'status' => 'created',
								 'returnid' => "$returnid",
								 'return_details' => $serialized_details);

							$model = Mage::getModel('walmart/walmartreturn')->addData($text);
							$model->save();
						}catch(Exception $e){
							//echo $e->getMessage(); die;
						}

                        if ($success_return == "") {
                            $success_return = $returnid;
                            $success_count++;
                        } else {
                            $success_return = $success_return . " , " . $returnid;
                            $success_count++;
                        }
                    } else {
                        if ($false_return == "") {
                            $false_return = $returnid;
                            $false_count++;
                        } else {
                            $false_return = $false_return . " , " . $returnid;
                            $false_count++;
                        }
                    }


                }

            }
        }
    }

    public function updaterefund()
    {
        $res_arr=array('created','processing');
        $result = Mage::getModel('walmart/walmartrefund')->getCollection()->addFieldToFilter('refund_status', array(array('in' => $res_arr)))->addFieldToSelect('refund_id')->getData();
        $count = count($result);
        $success_count = 0;
        $success_ids = "";

        if ($count > 0) {

            foreach ($result as $res) {

                $refundid = "";
                $refundid = $res['refund_id'];
                $data = Mage::helper('walmart')->CGetRequest('/refunds/state/' . $refundid . '');
                $responsedata = json_decode($data);
                $success_count++;
                if ($responsedata->refund_status != 'created') {
                    $modeldata = Mage::getModel('walmart/walmartrefund')->getCollection()->addFieldToFilter('refund_id', $refundid);
                    foreach ($modeldata as $models) {
                        $id = $models['id'];
                        $update = array('refund_status' => $responsedata->refund_status);
                        $model = "";
                        $model = Mage::getModel('walmart/walmartrefund')->load($id);
                        $model->addData($update);
                        $model->save();
                        $status = "";
                        $status = $responsedata->refund_status;
                        if (trim($status) == 'accepted') {
                            $saved_data = "";
                            $saved_data = $model->getData('saved_data');
                            if ($saved_data != "") {
                                $saved_data = unserialize($saved_data);
                                $flag = false;
                                $flag = Mage::helper('walmart')->generateCreditMemoForRefund($saved_data);
                            }

                        }
                    }
                }
            }
        }
    }

    public function deleteAttribute(Varien_Event_Observer $event)
    {
        $attribute = $event->getDataObject();
        $walmartattribute = Mage::getModel('walmart/walmartattribute');
        $collection = $walmartattribute->getCollection()->addFieldToFilter('magento_attr_id', $attribute->getAttributeId());
        if (count($collection) > 0) {
            $walmartattribute->load($collection->getFirstItem()->getId());
            $walmartattribute->delete();
        }

    }

    public function deleteCategory(Varien_Event_Observer $event)
   	{
		$category = $event->getDataObject();
        $walmart_cat_name = "Walmart.com Category";
        $cat_name = "";
        $cat_name = $category->getName();
        $cat = "";
        $subcats = "";

            $cat = Mage::getModel('catalog/category')->load($category->getId());
            $subcats = $cat->getChildren();

            foreach (explode(',', $subcats) as $subCatid) {
                $walmartCategory = "";
                $collection = "";
                $walmartCategory = Mage::getModel('walmart/walmartcategory');
                $collection = $walmartCategory->getCollection()->addFieldToFilter('magento_cat_id', $subCatid);
                if (count($collection) > 0) {
                    $walmartCategory->load($collection->getFirstItem()->getId());
                    $walmartCategory->delete();
                }
            }
            $walmartCategory = "";
            $collection = "";
            $walmartCategory = Mage::getModel('walmart/walmartcategory');
            $collection = $walmartCategory->getCollection()->addFieldToFilter('magento_cat_id', $category->getId());
            if (count($collection) > 0) {
                $walmartCategory->load($collection->getFirstItem()->getId());
                $walmartCategory->delete();
            }
            return;
    }

    public function updatePassive_status()
    {
		$this->getProductByStatus('Archived', 'archived');
		$this->getProductByStatus('Excluded', 'excluded');
        $this->getProductByStatus('Unauthorized', 'unauthorized');
    }

    public function updateReview_status()
    {
        $this->getProductByStatus('Under Walmart Review', 'under_walmart_review');
		$this->getProductByStatus('Missing Listing Data', 'missing_listing_data');
    }

    public function updateActive_status()
    {
        $this->getProductByStatus('Available for Purchase', 'available_for_purchase');
    }

    public function getProductByStatus($status, $status_code)
    {


        //testing code start for report
        /*$raw_encode = rawurlencode('ProductStatus');
        $response = Mage::helper('walmart')->CPostRequest('/reports/'. $raw_encode);
        
        $result = json_decode($response,true);
        $report_id = $result['report_id'];
        $rr = Mage::helper('walmart')->CGetRequest('/reports/state/'. $report_id);
        print_r(json_decode($rr,true));die();*/
        //testing code end for report
		$collection = Mage::getResourceModel('catalog/product_collection');
		$count = $collection->getSize() + 100;

		$raw_encode = rawurlencode($status);
        $response = Mage::helper('walmart')->CGetRequest('/portal/merchantskus?from=0&size=5000&statuses='.$raw_encode);
        $result = json_decode($response,true);

		$SKU = array();

		if(is_array($result) && isset($result['merchant_skus']) && count($result['merchant_skus'])>0){
			foreach ($result['merchant_skus'] as $sku) {
				 $SKU[] = $sku['merchant_sku'];
			}
		}

        if (count($SKU) == 0)
            return;

        $collection->addAttributeToFilter('sku', array('in', $SKU));
        $allIds = $collection->getAllIds();

        if (sizeof($allIds) > 0) {

            $chunk_data = array_chunk($allIds, 500);
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');

            foreach ($chunk_data as $k => $chunk) {
                $query = "Update " . $resource->getTableName('catalog_product_entity_varchar') . " cped
			   join " . $resource->getTableName('eav_attribute') . "  ev ON ev.attribute_id = cped.attribute_id
			   Set cped.value = '" . $status_code . "' where ev.attribute_code = 'walmart_product_status' and  cped.entity_id in(" . implode(",", $chunk) . ")";
                $writeConnection->query($query);
            }
            /*foreach ($chunk_data as $k => $chunk) {
                for($i=0;count($chunk)>$i;$i++){
                    if(trim($chunk[$i])!=""){
                        $model="";
                        $model=Mage::getModel('catalog/product')->load(trim($chunk[$i]));
                        $model->setData('walmart_product_status',$status_code);
                        $model->save();
                    }
                }
                
            }*/

        }

    }

    public function getupdatedStatus()
    {
		$this->getProductByStatus('Under Walmart Review', 'under_walmart_review');
        $this->getProductByStatus('Missing Listing Data', 'missing_listing_data');
        $this->getProductByStatus('Unauthorized', 'unauthorized');
        $this->getProductByStatus('Excluded', 'excluded');
        $this->getProductByStatus('Available for Purchase', 'available_for_purchase');
        $this->getProductByStatus('Archived', 'archived');

	}


  public function updateInventory($observer)
  {
      $order = $observer->getEvent()->getOrder();
      $_items = $order->getAllItems();
      $inventory = array();
      $fullfillmentnodeid = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_fullfillmentnode');
      $commaseperatedids = '';

      foreach ($_items as $_item) {
          $walmart_product_status = "";
          $product_id = "";
          $product = "";
          $product1 = "";

          $product = $_item->getProduct();
          $product_id = $product->getId();

          $product1 = Mage::getModel('catalog/product')->loadByAttribute('sku', $product->getSku());
          if($product1){$walmart_product_status = $product1->getData('walmart_product_status');}


          if ($walmart_product_status) {
              if ($walmart_product_status == "missing_listing_data" ||
                  $walmart_product_status == "available_for_purchase" ||
                  $walmart_product_status == "under_walmart_review") {
                  if ($product1->isConfigurable()) {
                              $simple_collection = Mage::getModel('catalog/product_type_configurable')->setProduct($product1)
                                  ->getUsedProductCollection()
                                  ->addAttributeToSelect('sku')
                                  ->addFilterByRequiredOptions();
                      foreach ($simple_collection  as $_item) {
                                      if ($_item->getData('type_id') == 'simple'){
                                          $sku = $_item->getSku();
                                          if ($commaseperatedids == "") {
                                              $commaseperatedids = $_item->getId();
                                          } else {
                                              $commaseperatedids = $commaseperatedids . "," .$_item->getId();
                                          }
                                          $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_item);
                                          $qty = (int)$stock->getQty();
                                          if ($qty < 0) {
                                              $qty = 0;
                                          }
                                          $node1 = array();
                                          $node1['fulfillment_node_id'] = "$fullfillmentnodeid";
                                          $node1['quantity'] = $qty;
                                          $inventory[$sku]['fulfillment_nodes'][] = $node1;
                                  }
                              }
                  } elseif ($product1->getTypeId() == 'simple') {
                              $skus = $product1->getSku();
                              if ($commaseperatedids == "") {
                                  $commaseperatedids = $product1->getId();
                              } else {
                                  $commaseperatedids = $commaseperatedids . "," . $product1->getId();
                              }
                              $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product1);
                              $qty = (int)$stock->getQty();
                              if ($qty < 0) {
                                  $qty = 0;
                              }
                              $node1 = array();
                              $node1['fulfillment_node_id'] = "$fullfillmentnodeid";
                              $node1['quantity'] = $qty;
                              $inventory[$skus]['fulfillment_nodes'][] = $node1;
                         }

              }
          }
      }

      if (count($inventory) > 0) {

          try {

              $finalinventoryjson = "";
              $t = "";

              $finalinventoryjson = json_encode($inventory);
              $t = time();
              $file_type = "Inventory";
              $file_name = "inventrys" . $t . ".json";

              $file_path = Mage::getBaseDir("var") . DS . "walmartupload" . DS . "inventrys" . $t . ".json";
              $myfile = fopen($file_path, "w");
              fwrite($myfile, $finalinventoryjson);
              fclose($myfile);
              if (fopen($file_path . ".gz", "r") == false) {
                  Mage::helper('walmart')->gzCompressFile($file_path, 9);
              }

              $compressed_file_path = $file_path . ".gz";

              if (fopen($compressed_file_path, "r") != false) {

                  $response = Mage::helper('walmart')->CGetRequest('/files/uploadToken');
                  $data = json_decode($response);
                  $fileinfo = $data->walmart_file_id;
                  $tokenurl = $data->url;
                  $model = "";
                  $text = "";
                  $currentid = "";

                  $text = array('magento_batch_info' => $commaseperatedids,
                      'walmart_file_id' => $fileinfo,
                      'token_url' => $tokenurl,
                      'file_name' => $file_name,
                      'file_type' => $file_type,
                      'status' => 'unprocessed');

                  $model = Mage::getModel('walmart/fileinfo')->addData($text);
                  $model->save();

                  $currentid = $model->getId();

                  $reponse = Mage::helper('walmart')->uploadFile($compressed_file_path, $data->url);
                  $postFields = '{"url":"' . $data->url . '","file_type":"' . $file_type . '","file_name":"' . $file_name . '"}';

                  $response = Mage::helper('walmart')->CPostRequest('/files/uploaded', $postFields);
                  $data2 = json_decode($response);
                  if ($data2->status == 'Acknowledged') {
                      $update = array('status' => 'Acknowledged');
                      $model = Mage::getModel('walmart/fileinfo')->load($currentid)->addData($update);
                      $model->save();
        }
              }
          } catch (Exception $e) {}

      }
  }

    /**
     * @save Walmart Category information validation
     */
	/*
    public function walmartCatInfocheck($observer)
    {
       $observer = $observer->getEvent()->getCategory();
        if (count($observer) > 0) {
            $is_walmart_category = $observer->getIsWalmartCategory();
            $walmart_category_id = $observer->getWalmartCategoryId();

            if ($observer->getId()) {

                if ($observer->getData('name') != NULL && $is_walmart_category == '1' && (!is_numeric($walmart_category_id) || $walmart_category_id == null || $walmart_category_id <= 0)) {

                    throw new Exception("Fail to Save Walmart Category.If you chosen Yes Is Walmart Category Information then  please enter the Walmart Category Id! ");
                }
            }
        }
    }
	*/

    /**
     * @save Walmart Category information save
     */
    public function walmartCatInfosave($observer)
    {
     
        $observer = $observer->getEvent()->getCategory();
        $walmart_category_id = Mage::app()->getRequest()->getPost('custom_tab_text_walmart');
        $walmart_parent_case = Mage::app()->getRequest()->getPost('custom_tab_text_walmart_parent');
       
        $current_cat_id = $observer->getEntityId();
    
       // Mage::log('walmart_category_id: '.$walmart_category_id);
        if($current_cat_id == '' || $current_cat_id == 0 || $current_cat_id == 1  || $current_cat_id == 2){
           Mage::getSingleton('adminhtml/session')->addError(__('Select Magento Sub Category , Walmart does not allow to map with root category.'));
        }
        else
        {
            $model = Mage::getModel('walmart/catlist');
            if($walmart_parent_case !="")
            {
                $collection = $model->getCollection()->addFieldToFilter('level','0');
                $walmart_category_id = $walmart_parent_case;
               // Mage::getSingleton('adminhtml/session')->addError($walmart_category_id);
            }else{
                $collection = $model->getCollection()->addFieldToFilter('level','1');
            }

            $check = 0;
            
            if(isset($walmart_category_id))
            {
                foreach($collection as $val)
                {
                    if($val->getCsvCatId() == $walmart_category_id)
                    {
                       /* if($val->getMagentoCatId() == $current_cat_id)
                        {
                            Mage::getSingleton('adminhtml/session')->addError(__($val->getCsvCatId().'this Walmart Catgeory Id already mapped or imported into another Magento Category id:'.$val->getMagentoCatId().' .Please choose another Walmart Categroy id or Delete mapped Category '));
                            break;
                        }*/
                        
                            $model->load($val->getId());
                            
                            if($model->getMagentoCatId())
                            {
                                 $model->setdata( 'magento_cat_id',$model->getMagentoCatId().','. $current_cat_id)->save();
                            }else{
                               
                                 $model->setdata( 'magento_cat_id', $current_cat_id)->save();
                            }
                            $check++;

                        
                    }
                }
            }
            else
            {
                Mage::getSingleton('adminhtml/session')->addError(__('Error while saving walmart category mapping, Please select sub category'));
            }
            if($check>0)
            {
                Mage::getSingleton('adminhtml/session')->addSuccess(__('Walmart Mapping details saved'));
            }

        }



 /* $observer = $observer->getEvent()->getCategory();

        if (count($observer) > 0 && $observer->getEntityId() > 0) {
            $walmart_category_id = Mage::app()->getRequest()->getPost('custom_tab_text');
            $MagentoCategoryId = $observer->getEntityId();

            if ((strlen($walmart_category_id) > 0) && ($walmart_category_id > 0)) {
                $resource = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $table_catlist = $resource->getTableName('walmart/catlist');
                $table_catattr = $resource->getTableName('walmart/walmartcategory');

                $update = true;
                $query = 'SELECT * FROM  ' . $table_catattr . ' where walmart_cate_id = "' . $walmart_category_id . '"';

                $jcategorydata = $readConnection->fetchRow($query);

                $query2 = 'SELECT * FROM  ' . $table_catlist . ' where csv_cat_id = "' . $walmart_category_id . '"';
                $walmartattributemap = $readConnection->fetchRow($query2);


                if (!empty($jcategorydata) && count($jcategorydata) > 0) {
                    if ($jcategorydata['walmart_cate_id'] != '' &&
                        $jcategorydata['magento_cat_id'] != $MagentoCategoryId
                    ) {
                        Mage::getSingleton('adminhtml/session')->addError($jcategorydata['walmart_cate_id'] . " this Walmart Catgeory Id already mapped or imported into another Magento Category id:" . $jcategorydata['magento_cat_id'] . ".Please choose another Walmart Categroy id or 'Delete' mapped Category ");
                        $update = false;
                        try {
                            $categorySingleton = Mage::getSingleton('catalog/category');
                            $categorySingleton->setId($observer->getEntityId());
                            $categorySingleton->setWalmartCategoryId('');
                        } catch (Exception $e) {}
                    }
                }
                elseif(empty($walmartattributemap) && (!empty($walmart_category_id))){
                    $update=false;
                    $walmartUrl = Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_walmartattrlist/categorylist');
                    Mage::getSingleton('adminhtml/session')->addError("Walmart Catgeory Id : " . $walmart_category_id . " doesn't exist . <a href='$walmartUrl' target='_blank'>click here</a> for existing walmart categories ");
                }

                if ($update) {
                    $walmartCategoryCollection = Mage::getModel('walmart/walmartcategory')->getCollection()
                        ->addFieldToFilter('magento_cat_id', $MagentoCategoryId);
                    $table_catlist = $resource->getTableName('walmart/catlist');
                    $query = 'SELECT `attribute_ids` FROM  ' . $table_catlist . ' where csv_cat_id = "' . $walmart_category_id . '"';
                    $attr_data = $readConnection->fetchRow($query);

                    try { 
                        if ((count($walmartCategoryCollection) > 0)) {
                            foreach ($walmartCategoryCollection as $collection) {
                                $Walmartcategory = Mage::getModel('walmart/walmartcategory')->load($collection->getId());
                                if(empty($walmart_category_id)){$Walmartcategory->setWalmartCateId('');}
                                else{$Walmartcategory->setWalmartCateId($walmart_category_id);}
								if ($attr_data['attribute_ids'] != '' && $attr_data['attribute_ids'] != null) {
                                    $Walmartcategory->setWalmartAttributes($attr_data['attribute_ids']);
                                }
                                $Walmartcategory->save();
                            }
                        } else{

                            $Walmartcategory = Mage::getModel('walmart/walmartcategory');
                            $Walmartcategory->setWalmartCateId($walmart_category_id);
                            $Walmartcategory->setMagentoCatId($MagentoCategoryId);
                            $Walmartcategory->setIsCsvCategory(0);
                            if ($attr_data['attribute_ids'] != '' && $attr_data['attribute_ids'] != null) {
                                $Walmartcategory->setWalmartAttributes($attr_data['attribute_ids']);
                            }
                            $Walmartcategory->save();
                        }
                    } catch (Exception $e) {
                    }
                }
            }

            elseif(strlen($walmart_category_id) == 0){
                $id = Mage::getModel('walmart/walmartcategory')->getCollection()->addFieldToFilter('magento_cat_id', $MagentoCategoryId)->getFirstItem()->getData('id');

                if(($id != '') && ($id != null)){
                    $row = Mage::getModel('walmart/walmartcategory')->load($id);
                    $row->delete();
                    Mage::getSingleton('adminhtml/session')->addSuccess("Current Magento category is successfully unlinked from walmart .");
                }


            }
            elseif($walmart_category_id<0){
                Mage::getSingleton('adminhtml/session')->addError("Entered Walmart Id is invalid . <a href='../adminhtml_walmartattrlist/categorylist' target='_blank'>click here</a> for existing walmart categories ");
            }
        }*/

    }

    /*
     * observer for clearing walmart token after saving new Walmart Config details
     */

    public function clearToken(Varien_Event_Observer $observer)
    {
        /*$data = $observer->getEvent()->getData();
        $setup = new Mage_Core_Model_Resource_Setup();
        $setup->deleteConfigData('walmartcom/token');*/

    }

    public function walmartProductDelete($observer)
    {
        $product = "";
        $product_available = true;
        $checkStatus= Mage::getStoreConfig('walmart_options/ced_walmartproductedit/walmart_config_product_option');
        $product = $observer->getEvent()->getProduct();
        if ($product->getTypeId() == 'simple') {
            $sku = "";
            $sku = $product->getSku();
            $data = "";
            $response = '';
            $data = Mage::helper('walmart')->CGetRequest('/merchant-skus/'.rawurlencode($sku));
            $response = json_decode($data);
            if (!$data) {
                $product_available = false;
            }
            if ($product_available) {
                $data = "";
                $response = '';
                $arr = array();
                $arr['is_archived'] = true;
                $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/status/archive', json_encode($arr));
                $response = json_decode($data);
            }
        }
        if ($product->isConfigurable()) {
                $simple_collection = Mage::getModel('catalog/product_type_configurable')->setProduct($product)
                    ->getUsedProductCollection()
                    ->addAttributeToSelect('sku')
                    ->addFilterByRequiredOptions();
                foreach ($simple_collection  as $_item) {
                    if ($_item->getData('type_id') == 'simple') {
                        $arr = array();
                        $arr['is_archived'] = true;
                        if (!$checkStatus) {
                            $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $_item->getSku() . '/status/archive', json_encode($arr));
                            $response = json_decode($data);
                            break;
                        } else {
                            $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $_item->getSku() . '/status/archive', json_encode($arr));
                            $response = json_decode($data);
                        }
                    }
                }
        }
       
    }

    public function walmartProductEdit($observer)
    {
        $datahelper= Mage::helper('walmart');
        $ids = $datahelper->validateProduct($observer->getProduct()->getId());
        print_r($ids);
        die;
        $data = $datahelper->updateInventoryOnWalmart($observer->getProduct()->getId());
        print_r($data);
        die('test2');
        $auto_sync_enable = Mage::getStoreConfig('walmart_options/ced_walmartproductedit/walmart_product_auto_sync');
        if($auto_sync_enable == 1)
        {
            $product = $observer->getProduct();

        if ($product->getTypeId() == 'simple') {
            $is_update_price = 1;
            $is_update_qty = 1;
            $is_update_all = 1;
            $is_update_image = 1;
            $is_update_price = Mage::getStoreConfig('walmart_options/ced_walmartproductedit/walmart_product_price');
            $is_update_qty = Mage::getStoreConfig('walmart_options/ced_walmartproductedit/walmart_product_inventory');
            $is_update_image = Mage::getStoreConfig('walmart_options/ced_walmartproductedit/walmart_product_images');
            $is_update_all = Mage::getStoreConfig('walmart_options/ced_walmartproductedit/walmart_product_details');
            $product_available = true;

            $fullfillmentnodeid = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_fullfillmentnode');

            $id = $product->getId();
            $sku = $product->getSku();
            $response = '';
            $data = Mage::helper('walmart')->CGetRequest('/merchant-skus/' . $sku);
            $response = json_decode($data);
            if (!$data) {
                $product_available = false;
            }
            $is_archived = false;
            if ($response) {
                if ($response->is_archived) {
                    $is_archived = true;
                }
            }
            if ($product_available) {
                $product_qty = 0;
                $is_in_stock = true;
                $stock = "";
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                if ($stock && $stock->getIsInStock() != '1') {
                    $is_in_stock = false;
                }
                if ($stock && $stock->getQty() > 0) {
                    $product_qty = $stock->getQty();
                }else{
                    $product_qty = 0;
                }

                $status = "";
                $price = Mage::helper('walmart/walmart')->getWalmartPrice($product);

                $status = $product->getStatus();
                if (!$status) {
                    $data = "";
                    $response = '';
                    $arr = array();
                    $arr['is_archived'] = true;
                    $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/status/archive', json_encode($arr));
                    $response = json_decode($data);
                } else {
                    $data = "";
                    $response = '';
                    $arr = array();
                    $arr['is_archived'] = false;
                    $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/status/archive', json_encode($arr));
                    $response = json_decode($data);
                }
                if ($status) {
                    /*-----all data update code starts----*/
                    $update_image = true;
                    if ($is_update_all == "1") {
                        $alldataupdate = 0;
                        $alldataupdate = Mage::helper('walmart')->createProductOnWalmart($product);
                        if ($alldataupdate['merchantsku'][$product->getSku()]) {
                            $data = "";
                            $response = '';
                            $updatedata = '';
                            $updatedata = $alldataupdate['merchantsku'][$product->getSku()];
                            $finalskujson = "";
                            $finalskujson = json_encode($updatedata);
                            $newJsondata = Mage::helper('walmart')->ConvertNodeInt($finalskujson);
                            $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku, $newJsondata);
                            $response = json_decode($data);
                            if ($data == "") {}
                            $update_image = false;
                        }
                    }
                    /*-----all data update code ends----*/
                    /*-----price update code starts----*/
                    if ($is_update_price == "1") {
                        $data_var = array();
                        $fulfillment_arr = array();
                        $data = ""; $response = '';

                        $fulfillment_arr[0]['fulfillment_node_id'] = $fullfillmentnodeid;
                        $fulfillment_arr[0]['fulfillment_node_price'] = $price;
                        $data_var['price'] = (float)$price;
                        $data_var['fulfillment_nodes'] = $fulfillment_arr;
                        $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/price', json_encode($data_var));
                        $response = json_decode($data);
                        if ($data == "") {}
                    }

                    /*-----price update code ends----*/
                    /*-----inventory update code starts----*/
                    if ($is_update_qty == "1") {
                        $data_var = array();
                        $fulfillment_arr = array();
                        $data = "";
                        $response = '';
                        $fulfillment_arr[0]['fulfillment_node_id'] = $fullfillmentnodeid;
                        $fulfillment_arr[0]['quantity'] = (int)$product_qty;
                        if (!$is_in_stock) {
                            $fulfillment_arr[0]['quantity'] = 0;
                        }
                        $data_var['fulfillment_nodes'] = $fulfillment_arr;
                        $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/inventory', json_encode($data_var));
                        $response = json_decode($data);
                        if ($data == "") {}
                    }

                    /*-----inventory update code ends----*/
                    /*-----images update code starts----*/

                    if ($update_image && $is_update_image == "1") {
                        $no_image = false;
                        if ($product->getImage() == "no_selection") {
                            $no_image = true;
                        }
                        $main_image_url = "";
                        $alt_images = array();
                        if (!$no_image) {
                            $main_image_url = $product->getImageUrl();
                        }
                        if ($main_image_url != "") {
                            $alt_images["main_image_url"] = $main_image_url;
                        }
                        $all_images = '';
                        $all_images = $product->getMediaGalleryImages();
                        $walmart_image_slot = 1;
                        $slot = 1;
                        foreach ($all_images as $key => $alternat_image) {
                            if ($alternat_image->getUrl() != '') {
                                if (count($alt_images) == 0) {
                                    $alt_images["main_image_url"] = $alternat_image->getUrl();
                                }
                                $alt_images['alternate_images'][] = array('image_slot_id' => $slot,
                                    'image_url' => $alternat_image->getUrl()
                                );
                                $slot++;
                                if ($walmart_image_slot > 7) {
                                    break;
                                }
                                $walmart_image_slot++;
                            }
                        }
                        $data = "";
                        $response = "";
                        $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/image', json_encode($alt_images));
                        $response = json_decode($data);
                        if ($data == "") {}
                    }

                    /*-----images update code ends----*/
                }
            }
        }/*
        else if($product->getTypeId()=='configurable'){

            $is_update_all = Mage::getStoreConfig('walmart_options/ced_walmartproductedit/walmart_product_details');
            $fullfillmentnodeid = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_fullfillmentnode');
            $is_update_qty = Mage::getStoreConfig('walmart_options/ced_walmartproductedit/walmart_product_inventory');

            $id = $product->getId();
            $sku = $product->getSku();

            $data = Mage::helper('walmart')->CGetRequest('/merchant-skus/' . rawurlencode($sku));

            $response = json_decode($data);
            if (!$data) {
                $product_available = false;
            }
            $is_archived = false;
            if ($response) {
                if ($response->is_archived) {
                    $is_archived = true;
                }
            }
            if ($product_available) {
                $product_qty = 0;
                $is_in_stock = true;
                $stock = "";
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                if ($stock && $stock->getIsInStock() != '1') {
                    $is_in_stock = false;
                }
                if ($stock && $stock->getQty() > 0) {
                    $product_qty = $stock->getQty();
                }

                $price = "";
                $status = "";
                $price = $product->getPrice();

                $status = $product->getStatus();
                if (!$status) {
                    $data = "";
                    $response = '';
                    $arr = array();
                    $arr['is_archived'] = true;
                    $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/status/archive', json_encode($arr));
                    $response = json_decode($data);
                }else {
                    $data = "";
                    $response = '';
                    $arr = array();
                    $arr['is_archived'] = false;
                    $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/status/archive', json_encode($arr));
                    $response = json_decode($data);
                }
                if ($status && $response) {
                    if ($is_update_qty == "1") {
                        $data_var = array();
                        $fulfillment_arr = array();
                        $data = "";
                        $response = '';
                        $fulfillment_arr[0]['fulfillment_node_id'] = $fullfillmentnodeid;
                        $fulfillment_arr[0]['quantity'] = (int)$product_qty;
                        if (!$is_in_stock) {
                            $fulfillment_arr[0]['quantity'] = 0;
                        }
                        $data_var['fulfillment_nodes'] = $fulfillment_arr;
                        $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $sku . '/inventory', json_encode($data_var));
                        $response = json_decode($data);
                        if ($data == "") {}
                    }
                }
            }

        }*/
        }
        //return , shipping exception save code start

        $dataRequest=Mage::app()->getRequest()->getParams();

        $go_redirect =false;
        if($dataRequest)
        {
            try
            {

                $fullfillmentnodeid=Mage::getStoreConfig('walmart_options/ced_walmart/walmart_fullfillmentnode');
                 $product = $observer->getProduct();

                $sku=$product->getSku();
               
                if(Mage::app()->getRequest()->getPost('shipping_override')){
                        $chargeamount=Mage::app()->getRequest()->getPost('shipping_charge');
                        $exceptiontype=Mage::app()->getRequest()->getPost('shipping_excep');
                        $shippinglevel=Mage::app()->getRequest()->getPost('shipping_carrier');
                        $shippingmethod=Mage::app()->getRequest()->getPost('shipping_method');
                        $overridetype=Mage::app()->getRequest()->getPost('shipping_override');
                        if($shippinglevel){
                            $shipping=array();
                            $shipping['fulfillment_nodes'][]=array('fulfillment_node_id'=>"$fullfillmentnodeid",
                                                        'shipping_exceptions'=>array(
                                                            array('service_level'=>$shippinglevel,
                                                                  'override_type'=>$overridetype,
                                                                  'shipping_charge_amount'=>(float)$chargeamount,           
                                                                  'shipping_exception_type'=>$exceptiontype)));
                        }
                        else{
                            $shipping=array();
                            $shipping['fulfillment_nodes'][]=array('fulfillment_node_id'=>"$fullfillmentnodeid",
                                                            'shipping_exceptions'=>array(
                                                                array('shipping_method'=>trim($shippingmethod),
                                                                    'override_type'=>$overridetype,
                                                                    'shipping_charge_amount'=>(float)$chargeamount,
                                                                    'shipping_exception_type'=>$exceptiontype)));
                            
                            }
                        
                        $data=Mage::helper('walmart')->CPutRequest('/merchant-skus/'.rawurlencode($sku).'/shippingexception',json_encode($shipping));
                       
                        
                        if($data==''){
                            $shippingObj=Mage::getModel('walmart/walmartshippingexcep');
                            $collectionload=$shippingObj->getCollection()->addFieldToFilter('sku',$sku);
                            foreach ($collectionload as $value) {
                                $id=$value['id'];
                                break;
                            }

                            if($collectionload->count()>0){
                                $shippingObj->load($id)
                                            ->setData('sku',$sku)
                                            ->setData('shipping_charge',$chargeamount)
                                            ->setData('shipping_excep',$exceptiontype)
                                            ->setData('shipping_carrier',$shippinglevel)
                                            ->setData('shipping_method',$shippingmethod)
                                            ->setData('shipping_override',$overridetype);
                            }
                            else{       
                                $shippingObj->setData('sku',$sku)
                                            ->setData('shipping_charge',$chargeamount)
                                            ->setData('shipping_excep',$exceptiontype)
                                            ->setData('shipping_carrier',$shippinglevel)
                                            ->setData('shipping_method',$shippingmethod)
                                            ->setData('shipping_override',$overridetype);
                            } 

                            $shippingObj->save();
                            Mage::getSingleton('adminhtml/session')
                                          ->addSuccess('Shipping Exception has been saved successfully');
                            $go_redirect =true;           
                        }else{
                            $error = json_decode($data, true);
                            $msg = '';
                            if(isset($error['errors']) && !empty($error['errors'])){
                                $err_count = count($error['errors']);
                                if($err_count>0){
                                    for($i=0; $i<=$err_count; $i++){
                                        $msg = $msg.$error['errors'][$i].'</br>';
                                    }
                                    Mage::getSingleton('adminhtml/session')
                                          ->addError($msg);
                                }else{
                                    Mage::getSingleton('adminhtml/session')
                                          ->addError("There is an error in shipping exception processing"); 
                                }
                            }else{
                                Mage::getSingleton('adminhtml/session')
                                          ->addError("There is an error in shipping exception processing"); 
                            }
                            $go_redirect =false;    
                        }
                }

                if(Mage::app()->getRequest()->getPost('time_to_return')){
                            $return_arr=array();
                            $time_to_return='';
                            $time_to_return=Mage::app()->getRequest()->getPost('time_to_return');
                            if($time_to_return!=""  && trim($time_to_return)!=""){
                                $return_arr['time_to_return']=(int)$time_to_return;
                            }else{
                                        Mage::getSingleton('adminhtml/session')
                                            ->addError('Please enter correct Time to return.');
                                         //$this->_redirect('*/*/productDetails',array('id' => $this->getRequest()->getParam('id')));
                                        return;
                            }
                            $location_ids=array();
                            $locations=array();
                            if(Mage::app()->getRequest()->getPost('locations')){
                                        $locations=Mage::app()->getRequest()->getPost('locations');
                                        if(count($locations['value'])>0){
                                                    for($i=0;$i < count($locations['value']);$i++){
                                                            if($locations['delete'][$i]==""){
                                                                    if($locations['value'][$i]!=""  && trim($locations['value'][$i])!=""){
                                                                            $location_ids[]=$locations['value'][$i];
                                                                    }
                                                                    
                                                            }
                                                    }
                                        }
                            }
                            if(count($location_ids)>0){
                                    $return_arr['return_location_ids']=$location_ids;
                            }

                            $ship_methods=array();
                            $ship=array();
                            if(Mage::app()->getRequest()->getPost('ship_methods')){
                                        $ship=Mage::app()->getRequest()->getPost('ship_methods');
                                        if(count($ship['value'])>0){
                                                    for($i=0;$i < count($ship['value']);$i++){
                                                            if($ship['delete'][$i]==""){
                                                                    if($ship['value'][$i]!="" && trim($ship['value'][$i])!=""){
                                                                            $ship_methods[]=trim($ship['value'][$i]);
                                                                    }
                                                            }
                                                    }
                                        }
                            }
                            if(count($ship_methods)>0){
                                    $return_arr['return_shipping_methods']=$ship_methods;
                            }
                            if(count($location_ids)<=0){
                                         Mage::getSingleton('adminhtml/session')->addError('Please enter Return Location Ids.');
                                         //$this->_redirect('*/*/productDetails',array('id' => $this->getRequest()->getParam('id')));
                                        return;
                            }
                            if(count($ship_methods)<=0){
                                         Mage::getSingleton('adminhtml/session')->addError('Please enter Return Shipping Methods.');
                                        //$this->_redirect('*/*/productDetails',array('id' => $this->getRequest()->getParam('id')));
                                        return;
                            }
                            if(count($return_arr)>0){
                                    $url ='/merchant-skus/'.rawurlencode($sku).'/returnsexception';
                                    $data = Mage::helper('walmart')->CPutRequest($url,json_encode($return_arr));
                                    
                                    if(!empty($data) || $data!=''){
                                                $data1="";
                                                $data1=json_decode($data);
                                                $error_str1='Return Exception Failed.<br/>';
                                                $error_str='';
                                                $j=0;

                                                foreach($data1->errors as $error){
                                                            $string="";
                                                            $title="";
                                                            $time=false;
                                                            if(strpos($error, 'return_shipping_methods')){
                                                                    $title="Error in Return Shipping Methods :";
                                                            }
                                                            if(strpos($error, 'time_to_return')){
                                                                    $title="Error in Time to Return :";
                                                                    $time=true;
                                                            }
                                                            if(preg_match('/location/',$error)){
                                                                    $title="Error in Return Location Ids :";

                                                            }
                                                            if(strpos($error, 'Path:')){
                                                                    $string=substr($error,0,strpos($error, 'Path:'));
                                                            }
                                                            if($time && strpos($error, '30L')){
                                                                    $string="Value should be 30 or fewer days.";
                                                            }
                                                            if($j>0){
                                                                if($string!=""){
                                                                        $error_str=$error_str.'<br/>'.$title.$string;
                                                                }else{
                                                                        $error_str=$error_str.'<br/>'.$title.$error;
                                                                }
                                                                        
                                                            }else{
                                                                if($string!=""){
                                                                    $error_str=$title.$string;
                                                                }else{
                                                                    $error_str=$title.$error;
                                                                }
                                                                
                                                            }
                                                    $j++;
                                                }
                                                if($error_str){
                                                    Mage::getSingleton('adminhtml/session')->addError($error_str1.$error_str);
                                                }
                                                
                                                $go_redirect =false;    
                                    }else{
                                        Mage::getSingleton('adminhtml/session')->addSuccess('Return Exception has been saved successfully');
                                        
                                        $go_redirect =true; 
                                    }
                                    
                            }

                }
            }catch (Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->settestData($this->getRequest()->getPost());
                $go_redirect =false;
            }
        }
    
        if($go_redirect){
            //$this->_redirect('*/*/*');
        }else{
            //$this->_redirect('*/*/*',array('id' => $this->getRequest()->getParam('id')));
        }
        //return , shipping exception save code end
        
    }
    public function walmartProductSaveBefore($observer)
    {
       $auto_sync_enable = Mage::getStoreConfig('walmart_options/ced_walmartproductedit/walmart_product_auto_sync');
        if($auto_sync_enable == 1)
        {
            $product = $observer->getProduct();
            if ($product->getTypeId() == 'simple')
             {
                $product_available = true;
                $id = $product->getId();
                $current_sku = $product->getSku();
                $previous_sku = Mage::getModel('catalog/product')->load($id)->getSku();
                $response = '';
                $data = Mage::helper('walmart')->CGetRequest('/merchant-skus/' . $previous_sku);
                $response = json_decode($data);
                if (!$data) {
                    $product_available = false;
                }
                $is_archived = false;
                if ($response) {
                    if ($response->is_archived) {
                        $is_archived = true;
                    }
                }
                if ($product_available) 
                {
                     if($current_sku!= $previous_sku)
                        {
                        $data = "";
                        $response = '';
                        $arr = array();
                        $arr['is_archived'] = true;
                        $data = Mage::helper('walmart')->CPutRequest('/merchant-skus/' . $previous_sku . '/status/archive', json_encode($arr));
                        $response = json_decode($data);
                        }
                }
            }
        }
    } 

    public function addwalmartAttributeTab($observer)
    {
        $tabs = $observer->getEvent()->getTabs();
       /* $tabs->addTab('walmart_attributes', array(
            'label' => Mage::helper('catalog')->__('Walmart Attributes'),
            'content' => $tabs->getLayout()->createBlock(
                'walmart/adminhtml_category_tab_attribute',
                'category.attribute.grid'
            )->toHtml(),
        ));*/
        $tabs->addTab('walmart_categorymapping', array(
            'label' => Mage::helper('catalog')->__('Walmart Category Mapping '),
            'content' => $tabs->getLayout()->createBlock(
                'walmart/adminhtml_category_mapping'
            )->toHtml(),
        ));
    }

    public function saveWalmartOrder($walmartId){
        $saveWalmartorder=Mage::getModel('walmart/walmartorder')->load($walmartId);
        $saveWalmartorder->setData('status','cancelled');
        $saveWalmartorder->save();
    }


	public function updateInvcron(){
		Mage::Helper('walmart/walmart')->createuploadDir();

		$fullfillmentnodeid = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_fullfillmentnode');

		$resource = Mage::getSingleton('core/resource');
        $table = $resource->getTableName('walmart/walmartcron');


		$result=Mage::getModel('walmart/walmartcategory') ->getCollection()->addFieldToSelect('magento_cat_id');
		$resultdata=array();
		foreach($result as $val){
			$resultdata[]=$val['magento_cat_id'];
		}

		$collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku');

        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')){
            $collection->joinField('qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');
		}

		$collectionData = $collection;
		$collectionProd = Mage::getModel('catalog/product')->getCollection();
		$collectionProd->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id =entity_id', null, 'left');
		$collectionProd->addAttributeToSelect('*')
        ->addAttributeToFilter('category_id', array('in' => $resultdata));

		$ids = $collectionProd->getAllIds();
		$ids = array_unique($ids);

		$collection->addFieldToFilter('entity_id', array('in'=>$ids))
					->addAttributeToFilter('type_id', array('in' => array('simple','configurable')));
		$total_size = $collection->getSize();

		if($total_size!=0){
			$goupload = true;
		}


		$data = array();
		if($goupload){

			$data = $collection->getData();

			$batch_data = (array_chunk($data, 5000));

			foreach($batch_data as $data){

				$Inventory  =array();
				foreach($data as $product){

					if($product['type_id']=='configurable'){
						$ids=Mage::getResourceSingleton('catalog/product_type_configurable')->getChildrenIds($product['entity_id']);
						if(isset($ids[0]) && count ($ids[0])>0){
							$child_array = $ids[0];
							$_subproducts = Mage::getModel('catalog/product')->getCollection()
											->addAttributeToSelect('*');

							 if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')){
								$_subproducts->joinField('qty',
									'cataloginventory/stock_item',
									'qty',
									'product_id=entity_id',
									'{{table}}.stock_id=1',
									'left');
							}

							$child_data =$_subproducts->addFieldToFilter('entity_id', array('in'=>$child_array))
										 ->getData();

							if(isset($child_data[0]) && count($child_data[0])>0){
								foreach($child_data as $child){
									$temp_inv = array();
									$node =array();
									$node['fulfillment_node_id']="$fullfillmentnodeid";
									$node['quantity']=(int)$child['qty'];
									$temp_inv[$child['sku']]['fulfillment_nodes'][]=$node;

									$Inventory = Mage::Helper('walmart/walmart')->Jarray_merge($temp_inv,$Inventory);
								}
							}

						}

					}else{
						$temp_inv = array();
						$node =array();
						$node['fulfillment_node_id']="$fullfillmentnodeid";
						$node['quantity']=(int)$product['qty'];
						$temp_inv[$product['sku']]['fulfillment_nodes'][]=$node;

						$Inventory = Mage::Helper('walmart/walmart')->Jarray_merge($temp_inv,$Inventory);
					}
				}

				if(count($Inventory)>0){
					$tokenresponse = Mage::helper('walmart')->CGetRequest('/files/uploadToken');
					$tokendata = json_decode($tokenresponse);

					$inventoryPath = Mage::helper('walmart')->createJsonFile( "Inventory", $Inventory);
					$sku_file_name =  end(explode(DS, $inventoryPath));
					$inventoryPath=$inventoryPath.'.gz';

					$reponse = Mage::helper('walmart')->uploadFile($inventoryPath,$tokendata->url);
					$postFields='{"url":"'.$tokendata->url.'","file_type":"Inventory","file_name":"'.$sku_file_name.'"}';

					$responseinventry = Mage::helper('walmart')->CPostRequest('/files/uploaded',$postFields);
					//$invetrydata = json_decode($responseinventry);
					//if($invetrydata->status == 'Acknowledged'){echo $invetrydata->status;}
					$Inventory = array();
				}

			}

		}

	}

	/*
	public function updateInvcronBatch(){
		Mage::Helper('walmart/walmart')->createuploadDir();
		$fullfillmentnodeid = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_fullfillmentnode');

		$resource = Mage::getSingleton('core/resource');
        $table = $resource->getTableName('walmart/walmartcron');


		$result=Mage::getModel('walmart/walmartcategory') ->getCollection()->addFieldToSelect('magento_cat_id');
		$resultdata=array();
		foreach($result as $val){
			$resultdata[]=$val['magento_cat_id'];
		}

		$collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku');

        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')){
            $collection->joinField('qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');
		}

		$collectionData = $collection;
		$collectionProd = Mage::getModel('catalog/product')->getCollection();
		$collectionProd->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id =entity_id', null, 'left');
		$collectionProd->addAttributeToSelect('*')
        ->addAttributeToFilter('category_id', array('in' => $resultdata));

		$ids = $collectionProd->getAllIds();
		$ids = array_unique($ids);

		$collection->addFieldToFilter('entity_id', array('in'=>$ids))
					->addAttributeToFilter('type_id', array('in' => array('simple','configurable')));
		$total_size = $collection->getSize();


		$readconnection = $resource->getConnection('core_read');
		$writeConnection = $resource->getConnection('core_write');

		$query = 'SELECT * FROM '.$table .' WHERE `event`= "inventory"';
		$row = $readconnection->fetchRow($query);

		$batch_start = 0;
		$batch_end = 1000;
		$goupload= false;

		if($row==false && $row['event']==''){
			$query = 'INSERT INTO ' . $table . '(event,batch_start,timestamp) VALUES ("inventory",0,'.time().')';
			$writeConnection->query($query);
			$batch_start = 0;
		}else{
			$batch_start = $row['batch_start']+$batch_end;
			$query = 'UPDATE '.$table.' SET batch_start='.$batch_start.', timestamp= '.time().' WHERE event="inventory"';
			$writeConnection->query($query);

			$diff =  round((time() - $row['timestamp'])/3600,1);

			if(($total_size <= $batch_start) && $diff > 2){
				$batch_start = 0;
				$query = 'UPDATE '.$table.' SET batch_start='.$batch_start.', timestamp= '.time().' WHERE event="inventory"';
				$writeConnection->query($query);
			}

		}

		if($total_size!=0 && $total_size > $batch_start){
			$goupload = true;
		}


		$data = array();
		if($goupload){
			$collection->setPage($batch_start ,$batch_end);
			$data = $collection->getData();
			$Inventory  =array();
			foreach($data as $product){

				if($product['type_id']=='configurable'){
					$ids=Mage::getResourceSingleton('catalog/product_type_configurable')->getChildrenIds($product['entity_id']);
					if(isset($ids[0]) && count ($ids[0])>0){
						$child_array = $ids[0];
						$_subproducts = Mage::getModel('catalog/product')->getCollection()
										->addAttributeToSelect('*');

						 if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')){
							$_subproducts->joinField('qty',
								'cataloginventory/stock_item',
								'qty',
								'product_id=entity_id',
								'{{table}}.stock_id=1',
								'left');
						}

						$child_data =$_subproducts->addFieldToFilter('entity_id', array('in'=>$child_array))
									 ->getData();

						if(isset($child_data[0]) && count($child_data[0])>0){
							foreach($child_data as $child){
								$temp_inv = array();
								$node =array();
								$node['fulfillment_node_id']="$fullfillmentnodeid";
								$node['quantity']=(int)$child['qty'];
								$temp_inv[$child['sku']]['fulfillment_nodes'][]=$node;

								$Inventory = Mage::Helper('walmart/walmart')->Jarray_merge($temp_inv,$Inventory);
							}
						}

					}

				}else{
					$temp_inv = array();
					$node =array();
					$node['fulfillment_node_id']="$fullfillmentnodeid";
					$node['quantity']=(int)$product['qty'];
					$temp_inv[$product['sku']]['fulfillment_nodes'][]=$node;

					$Inventory = Mage::Helper('walmart/walmart')->Jarray_merge($temp_inv,$Inventory);
				}
			}

			if(count($Inventory)>0){
				$tokenresponse = Mage::helper('walmart')->CGetRequest('/files/uploadToken');
                $tokendata = json_decode($tokenresponse);

                $inventoryPath = Mage::helper('walmart')->createJsonFile( "Inventory", $Inventory);
                $sku_file_name =  end(explode(DS, $inventoryPath));
                $inventoryPath=$inventoryPath.'.gz';

                $reponse = Mage::helper('walmart')->uploadFile($inventoryPath,$tokendata->url);
				$postFields='{"url":"'.$tokendata->url.'","file_type":"Inventory","file_name":"'.$sku_file_name.'"}';

                $responseinventry = Mage::helper('walmart')->CPostRequest('/files/uploaded',$postFields);
                //$invetrydata = json_decode($responseinventry);
				//if($invetrydata->status == 'Acknowledged'){echo $invetrydata->status;}
				$Inventory = array();
			}

		}
	}

	*/

     /**
     * @Walmart Orders Synchronisation
     * Create Available Walmart Orders in Magento
     */
    public function fetchLatestWalmartOrders()
    { 
        $date = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_fetchstartdate' );  
        $storeId = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_storeid');
        $website = Mage::getModel('core/store')->load($storeId);
        $websiteId = $website->getWebsiteId();
        $store = Mage::app()->getStore($website->getCode());

        $response = Mage::Helper('walmart')->getOrders (['createdStartDate' => $date , 'status' =>'Created']);

        if (isset($response['elements']['order'])) { 
            foreach ( $response['elements']['order'] as $order ) {
             
                $orderObject =  $order ;
                $email = $this->validateString
                ( isset ( $order ['customerEmailId'] ) ) ? $order ['customerEmailId'] : 'customer@walmart.com';
               $customer = Mage::getModel('customer/customer')
                            ->setWebsiteId($websiteId)
                            ->loadByEmail($email);

                if (count ( $order ) > 0 && $this->validateString ( $order ['purchaseOrderId'] )) { 

                    $purchaseOrderid = $order ['purchaseOrderId'];
                    $resultdata = Mage::getModel('walmart/walmartorder')->getCollection()
                        ->addFieldToFilter ( 'purchase_order_id', $purchaseOrderid );

                    if (! $this->validateString ( $resultdata->getData () )==true) { 
                        $ncustomer = $this->_assignCustomer( $order, $customer,$websiteId,$store, $email );
                        if (! $ncustomer->getId()) { 
                           
                            return false;
                        } else {  
                            $this->generateQuote ( $store, $ncustomer, $order, $orderObject );
                        }
                    }
                }
            }

        }
    }


      public function validateString($string)
    {
        $stringValidation = (isset ( $string ) && ! empty ( $string )) ? true : false;
        return $stringValidation;
    }

//delete files once in a day code start
    public function walmartfilesDelete()
    {
        $url = Mage::getBaseDir();
        $path = $url.'/var/walmartupload/';
        $handle = opendir($path);
         if ($handle = opendir($path))
         { 
            while (false !== ($file = readdir($handle)))
              { 
                 $filelastmodified = filemtime($path . $file);
                 //24 hours in a day * 3600 seconds per hour
                if((time() - $filelastmodified) > 24*3600 && is_file($file))
                 {
                    unlink($path . $file);
                }

             }
            closedir($handle); 
        }
    }


  /**
     * Create Walmart customer on Magento
     * @param array $order
     * @param array $customer
     * @param null $store
     * @param string $email
     * @return bool|\Magento\Customer\Api\Data\CustomerInterface
     */
    public function _assignCustomer($order, $customer,$websiteId, $store=null, $email) {
        if (! $this->validateString ( $customer->getId () )) { 
            try {
                $cname = $order ['shippingInfo']['postalAddress'] ['name'];

                // if (trim ( $Cname ) == '' || $Cname == null) {
                //     $Cname = $result ['shipping_to'] ['recipient'] ['name'];
                // }
                // $Cname = preg_replace ( '/\s+/', ' ', $Cname );
                $customerName = explode ( ' ', $cname );
                $firstname = $customerName [0];
                unset($customerName[0]);
                $customerName = array_values($customerName) ;
                $lastname = implode(' ', $customerName);
                if (! isset ( $customerName [1] ) || $customerName [1] == '') {
                    $customerName [1] = $customerName [0];
                }
                   
                 

                //$websiteId = $this->storeManager->getStore ()->getWebsiteId ();




                $customer = Mage::getModel('customer/customer');
                $customer->setWebsiteId($websiteId)
                    ->setStore($store)
                    ->setFirstname($firstname)
                    ->setLastname($lastname)
                    ->setEmail($email)
                    ->setPassword("password");    
                $customer->save();

            /*    $customer = $this->customerFactory->create ();
                $customer->setWebsiteId ( $websiteId );
                $customer->setEmail ( $email );
                $customer->setFirstname ( $firstname );
                $customer->setLastname ( $lastname );
                $customer->setPassword ( "password" );
                $customer->save();*/
                // $customer->sendNewAccountEmail();
                /*
                 * $customerData = $this->objectManager->create ( 'Magento\Customer\Api\Data\CustomerInterface' );
                 * $customerData ->setStore($store) ->setStoreId ( $store->getId() )->setFirstname ( $customer_name [0] )->setLastname ( $customer_name [1] )->setEmail ( $email )/* ->setPassword ( "password" )
                 */
                
                // $customerData->save ();*/
                return $customer;
            } catch ( \Exception $e ) {
                $orderObject = json_decode ( $order );
                $message = $e->getMessage ();
                $walmartOrderError = mage::getModel('walmart/orderimport');
                $walmartOrderError->setPurchaseOrderId ( $order ['purchase_order_Id'] );
                $walmartOrderError->setReferenceNumber ( $order ['customer_order_id'] );
                $walmartOrderError->setReason ( $message );
                $walmartOrderError->setOrderData($orderObject);
                $walmartOrderError->save ();
                return false;
            }
        } else {
            return $customer;
        }
    }
     
    public function parserArray($array){
        $arr = [];
        foreach ($array as $key => $value){
            if(in_array($key,$arr))
                continue;
            $count = count($array);
            $sku = $value['item']['sku'];
            $quantity = 1;
            $lineNumber = $value['lineNumber'];
            for ( $i = $key+1 ; $i < $count;$i++){
                if($array[$i]['item']['sku'] == $sku){
                    $quantity++;
                    $lineNumber = $lineNumber.','.$array[$i]['lineNumber'];
                    unset($array[$i]);
                    array_push($arr,$i);
                    array_values($array);
                }
            }
            $array[$key]['lineNumber'] = $lineNumber;
            $array[$key]['orderLineQuantity']['amount'] = $quantity;
        }
        return $array;
    }

    /*
    * @Auto Order Rejection Request
    */
    public function rejectOrder($item , $result , $lineNumber , $quantity) 
    {
        $orderData = json_encode($result);
        $reject_items_arr [] = ['order_item_acknowledgement_status' => 'nonfulfillable - invalid merchant SKU',
            'order_item_line_number' => $item['lineNumber']];
        $message = "Product " . $item['item']['sku'] . " is Not Enabled or not in stock or inventry<=0 or Product  visiblity is set to not visible individually";
        $collection = Mage::getModel('walmart/orderimport')->getCollection()->addFieldToFilter('purchase_order_id',$result ['purchaseOrderId'])->getData();

        if (count($collection) != 0) {
            $walmartOrderError = Mage::getModel('walmart/orderimport' )->load($result ['purchaseOrderId'],'purchase_order_id');
        } else {
            $walmartOrderError = Mage::getModel('walmart/orderimport' ); // for error
        }
        $walmartOrderError->setPurchaseOrderId ( $result ['purchaseOrderId'] );
        $walmartOrderError->setReferenceNumber ( $result ['customerOrderId'] );
        $walmartOrderError->setReason ( $message );
        $walmartOrderError->setOrderData($orderData);
        $walmartOrderError->save ();

        $data_var = [];
        $data_var ['acknowledgement_status'] = "rejected - item level error";
        $data_var ['order_items'] = $reject_items_arr;
        // $data = Mage::helper('walmart/data')->rejectOrders (  $result ['purchaseOrderId'] , $lineNumber , $quantity);
    }

    public function generateQuote($store, $customer, $order, $orderObject)
    {
        $shippingMethod = 'shipwalmartcom';

        $paymentMethod = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_default_payment');
        $paymentMethod = !empty($paymentMethod) ? $paymentMethod : "checkmo";
        $productArray = array();
        $baseCurrency = $store->getBaseCurrencyCode();
        $itemsArray = $this->parserArray($order['orderLines']['orderLine']);
        
        $baseprice = 0;
        $shippingcost = 0;
        $tax = 0;
        $storeId = Mage::getStoreConfig('walmart_configuration/walmartsetting/walmart_storeid');   
       // $storeId = $store->getId();
        $autoReject = false;
        $reject_items_arr = array();
        $order_items_qty_arr = array();
        $walmartOrder = $order;
        $order = Mage::getModel('sales/order')
            ->setStoreId($storeId)
            ->setQuoteId(0)
            ->setGlobal_currency_code($baseCurrency)
            ->setBase_currency_code($baseCurrency)
            ->setStore_currency_code($baseCurrency)
            ->setOrder_currency_code($baseCurrency);
      

        $shippingcost = 0;
        $tax = 0;
        $baseprice = 0;
        foreach ($itemsArray as $item) { 
            $message = '';
            $sku = $item['item']['sku'];
            $lineNumber = $item['lineNumber'];
            $quantity = $item['orderLineQuantity']['amount'];
            //$item['item']['sku']='SKJT';
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $item['item']['sku']);

            if ($product) { 
               
                if ($product->getStatus () == '1') {
                    $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                    /* Get stock item */
                    
                    $cancelItemQuantity = '';
                    if (!empty($item['orderLineStatuses']['orderLineStatus'][0]['statusQuantity']['amount'])) {
                        $cancelItemQuantity = 0;
                    }

                    $stockstatus = ($stock->getQty () > 0) ? ($stock->getIsInStock () == '1' ?
                        ($stock->getQty () >= $item ['orderLineQuantity']['amount'] ?
                            ($item ['orderLineQuantity']['amount'] != $cancelItemQuantity ?
                                true : false) : false) : false) : false;

                    if ($stockstatus) { 
                        $productArray [] = [
                            'id' => $product->getEntityId (),
                            'qty' => $item ['orderLineQuantity']['amount']
                        ];
                        $price = $item ['charges'] ['charge'][0]['chargeAmount']['amount'];
                        $qty = $item ['orderLineQuantity']['amount'];
                        $cancelqty = $cancelItemQuantity;
                        $baseprice += $qty * $price;
                        
                        if(isset($item ['charges']['charge'][1])){
                            $shippingcost += ($item ['charges']['charge'][1]['chargeAmount']['amount'] * $qty) ;
                            $tax = $tax + ($item ['charges'] ['charge'][0]['tax']['taxAmount']['amount'] * $qty) +
                                ($item ['charges']['charge'][1]['tax']['taxAmount']['amount'] * $qty);
                        } else{
                            $tax = $tax + ($item ['charges'] ['charge'][0]['tax']['taxAmount']['amount'] * $qty);
                        }                        
                        $rowTotal = $price * $qty;                   
                            $orderItem = Mage::getModel('sales/order_item')
                            ->setStoreId($storeId)
                            ->setQuoteItemId(0)
                            ->setQuoteParentItemId(NULL)
                            ->setProductId($product->getEntityId())
                            ->setProductType($product->getTypeId())
                            ->setQtyBackordered(NULL)
                            ->setTotalQtyOrdered($qty)
                            ->setQtyOrdered($qty)
                            ->setName($product->getName())
                            ->setSku($product->getSku())
                            ->setPrice($price)
                            ->setBasePrice($price)
                            ->setOriginalPrice($price)
                            ->setRowTotal($rowTotal)
                            ->setBaseRowTotal($rowTotal);

                        //$subTotal += $rowTotal;
                        $order->addItem($orderItem);
                        $Quote_execute = True;

                    } else { 
                        // needs to check again
                        $autoReject = true;
                        $this->rejectOrder($item, $walmartOrder , $lineNumber , $quantity);
                    }
                }
            } else {
                $autoReject = true;
                $this->rejectOrder($item , $walmartOrder , $lineNumber , $quantity);
            }

            /// old code
         

        if ($autoReject) { 

            Mage::getSingleton('core/session')->addError('Failed to Fetch Order.. Please Check Failed Walmart Orders Import Log.');

           /* $data_var = array();
            $data_var['acknowledgement_status'] = "rejected - item level error";
            $data_var['order_items'] = $reject_items_arr;
            $data = Mage::helper('walmart')->CPutRequest('/orders/' . $result['merchant_order_id'] . '/acknowledge', json_encode($data_var));
            $response = json_decode($data);

            $reject=Mage::getModel('walmart/walmartorder')->getCollection()->addFieldToFilter('merchant_order_id', $result['merchant_order_id'])->getFirstItem();
            if($response == NULL){
                //$this->saveWalmartOrder($reject->getId());
            }*/
        }

        if (count($productArray) > 0 && count($itemsArray) == count($productArray) && !$autoReject) {
           
            $transaction = Mage::getModel('core/resource_transaction');
            $order->setCustomer_email($customer->getEmail())
                ->setCustomerFirstname($customer->getFirstname())
                ->setCustomerLastname($customer->getLastname())
                ->setCustomerGroupId($customer->getGroupId())
                ->setCustomer_is_guest(0)
                ->setCustomer($customer);

            // set Billing Address
            $countryId = $this->getCountryId($walmartOrder ['shippingInfo'] ['postalAddress'] ['country']);    
            try {

                $billingAddress = Mage::getModel('sales/order_address')
                    ->setStoreId($storeId)
                    ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
                    ->setCustomerId($customer->getId())
                    ->setCustomerAddressId($customer->getDefaultBilling())
                    ->setCustomer_address_id('')
                    ->setPrefix('')
                    ->setFirstname($customer->getFirstname ())
                    ->setMiddlename('')
                    ->setLastname($customer->getLastname ())
                    ->setSuffix('')
                    ->setCompany('')
                    ->setStreet($walmartOrder ['shippingInfo'] ['postalAddress'] ['address1'].' '.$walmartOrder ['shippingInfo'] ['postalAddress'] ['address2'])
                    ->setCity($walmartOrder['shippingInfo']['postalAddress']['city'])
                    ->setCountry_id($countryId)
                    ->setRegion($walmartOrder ['shippingInfo'] ['postalAddress'] ['state'])
                    ->setRegion_id('')
                    ->setPostcode($walmartOrder ['shippingInfo'] ['postalAddress'] ['postalCode'])
                    ->setTelephone($walmartOrder ['shippingInfo'] ['phone'])
                    ->setFax('');
                $order->setBillingAddress($billingAddress);

                $shipping = $customer->getDefaultShippingAddress();
                $shippingAddress = Mage::getModel('sales/order_address')
                    ->setStoreId($storeId)
                    ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
                    ->setCustomerId($customer->getId())
                    ->setCustomerAddressId($customer->getDefaultBilling())
                    ->setCustomer_address_id('')
                    ->setPrefix('')
                    ->setFirstname($customer->getFirstname ())
                    ->setMiddlename('')
                    ->setLastname($customer->getLastname ())
                    ->setSuffix('')
                    ->setCompany('')
                    ->setStreet($walmartOrder ['shippingInfo'] ['postalAddress'] ['address1'].' '.$walmartOrder ['shippingInfo'] ['postalAddress'] ['address2'])
                    ->setCity($walmartOrder['shippingInfo']['postalAddress']['city'])
                    ->setCountry_id($countryId)
                    ->setRegion($walmartOrder ['shippingInfo'] ['postalAddress'] ['state'])
                    ->setRegion_id('')
                    ->setPostcode($walmartOrder ['shippingInfo'] ['postalAddress'] ['postalCode'])
                    ->setTelephone($walmartOrder ['shippingInfo'] ['phone'])
                    ->setFax('');
                $order->setShippingAddress($shippingAddress)
                    ->setShippingMethod('flatrate_flatrate')
                    ->setShippingDescription('Shipping Desc - ');

                $orderPayment = Mage::getModel('sales/order_payment')
                    ->setStoreId($storeId)
                    ->setCustomerPaymentId(0)
                    ->setMethod($paymentMethod);
                //->setPo_number(' - ');
                $order->setPayment($orderPayment);


                $order->setSubtotal($baseprice)
                    ->setBaseSubtotal($baseprice)
                    ->setShippingAmount($shippingcost)
                    ->setBaseShippingAmount($shippingcost)
                    ->setTaxAmount($tax)
                    ->setBaseTaxAmount($tax)
                    ->setGrandTotal($baseprice + $shippingcost + $tax)
                    ->setBaseGrandTotal($baseprice + $shippingcost + $tax);
                        
                $transaction->addObject($order);
                $transaction->addCommitCallback(array($order, 'place'));
                $transaction->addCommitCallback(array($order, 'save'));

                if ($transaction->save() && $order->getId() > 0) {

                    /*$order1 = Mage::getModel('sales/order')->load($order->getId(), 'entity_id');
                    $order1->setStatus('Processing')->save(); */
                    // after order placed
                $deliver_by = date ( 'Y-m-d H:i:s', substr( $walmartOrder ['shippingInfo'] ['estimatedDeliveryDate'],0,10 ) );
                $order_place = date ( 'Y-m-d H:i:s', substr ( $walmartOrder ['orderDate'],0,10 ) );
                $OrderData = [
                    'purchase_order_id' => $walmartOrder ['purchaseOrderId'],
                    'deliver_by' => $deliver_by,
                    'order_place_date' => $order_place,
                    'magento_order_id' => $order->getIncrementId (),
                    'status' => $walmartOrder['orderLines']['orderLine'][0]['orderLineStatuses']['orderLineStatus'][0]['status'],
                    'order_data' => serialize ( $walmartOrder ),
                    'merchant_order_id' => $walmartOrder ['customerOrderId']
                    ];
                    $model = Mage::getModel('walmart/walmartorder')->addData($OrderData);
                    $model->save(); 

                   // if (Mage::getStoreConfig('walmart_options/walmart_order/active') == 1) {
                         $this->generateInvoice($order);
                         $this->autoOrderacknowledge ( $walmartOrder ['purchaseOrderId'], $model );

                        
                  //  }
                   // $this->sendmailtoadmin($order->getIncrementId(),$result['order_detail']['request_ship_by'],$order_items_qty_arr);

                    
                }

                //$incid = $this->get

            } catch (Exception $e) {
                $message = "Fail To Create Order Due to Error " . $e->getMessage();
                $walmartOrderError = Mage::getModel('walmart/orderimport');
                $walmartOrderError->setMerchantOrderId($result['merchant_order_id']);
                        $walmartOrderError->setReferenceNumber($result['reference_order_id']);
                $walmartOrderError->setOrderItemId($item['order_item_id']);
                $walmartOrderError->setReason($message);
                $walmartOrderError->save();

            }
        }
     
     }   //old code end
    }


public function generateInvoice($order)
{
    
    if($order->canInvoice()) {
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
            $invoice->register();
            $invoice->getOrder()->setCustomerNoteNotify(false);
            $invoice->getOrder()->setIsInProcess(true);

            $transactionSave = Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder());

            $transactionSave->save();
        }
}
     /*
     * @Auto Order Acknowledgement Process
     */
    public function autoOrderacknowledge($purchaseOrderId, $ordermodel)
    {
        $dataHelper = Mage::Helper('walmart');
       /* $serialize_data = unserialize ( $ordermodel->getOrderData () );
        if (empty ( $serialize_data ) || count ( $serialize_data ) == 0) {

            $result =  $dataHelper->getOrder($purchaseOrderId);
            $ord_result = $result ;
            if (empty ( $result ) || count ( $result ) == 0) {
                return 0;
            } else if( isset($result['elements']['order'][0]['orderLines']['orderLine'][0]['orderLineStatuses']['orderLineStatus'][0]['status']) && ($result['elements']['order'][0]['orderLines']['orderLine'][0]['orderLineStatuses']['orderLineStatus'][0]['status'] == 'Acknowledged')) {
                return 0;
            } 
        }*/

       /* $fullfill_array = [];
        foreach ( $serialize_data['orderLines']['orderLine'] as $k => $valdata ) {
            $fullfill_array [] = ['order_item_acknowledgement_status' => 'fulfillable',
                'order_item_id' => $valdata['item']['sku']];
        }*/

        // $order_id = $ordermodel->getPurchaseOrderId ();
       /* $data_var = [];
        $data_var ['acknowledgement_status'] = "accepted";
        $data_var ['order_items'] = $fullfill_array;*/
        // Api call to Acknowledge Order
        //$url = 'v3/orders/'.$purchaseOrderId.'/acknowledge';
        //$response = $dataHelper->postRequest($url);

        $response = $dataHelper->acknowledgeOrder ($purchaseOrderId);

        if (empty ( $response ) && $response == null ) {
           return 0;
        } else { 

            try { 
                $response = json_decode($response,true);
                $modeldata = Mage::getModel('walmart/walmartorder')
                            ->getCollection ()->addFieldToFilter ( 'purchase_order_id', $purchaseOrderId )->getData ();

                if (count ( $modeldata ) > 0) {

                    $id = $modeldata [0] ['id'];
                    $model = Mage::getModel('walmart/walmartorder')->load ( $id );
                    $model->setOrderData($response);
                    $model->setStatus ( 'Acknowledged' );
                    $model->save ();
                   
                }
            }
            catch(\Exception $e){ 
                /*$this->_logger->debug('Walmart :acknowledgeOrder : NO JSON Response . Response : '.$response);*/
                return false;
            }
            // Setting acknowleged status here
           
        }
        return 0;
    }

    public function getOrderFlag($order_to_complete,$order_cancel,$mixed)
    {
        $order_to_complete = isset($order_to_complete)?$order_to_complete:[];
        $order_cancel = isset($order_cancel)?$order_cancel:[];
        $mixed = isset($mixed)?$mixed:[];


        $Order_flag_array=array_merge($order_to_complete,$order_cancel,$mixed);
        $itemcount = sizeof($Order_flag_array);
        $complete = 0;
        $cancel = 0;
        $mix = 0;
        foreach ($Order_flag_array as $key => $value) {
            if($value == 'complete')
            {
                $complete++;
            }elseif($value == 'cancel')
            {
                $cancel++;
            }else
            {
                $mix++;
            }
        }

        return [
            'item_count' => $itemcount,
            'complete'=>$complete,
            'cancel'=> $cancel,
            'mix' => $mix
        ];
    }

    /*
     * @Shipment generation Process
     */
    public function generateShipment($order, $cancelleditems)
    {
        $shipment = $order->prepareShipment($order, $cancelleditems);
        if ($shipment) {
            $shipment->register();
            $shipment->getOrder()->setIsInProcess(true);
            try {
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($shipment)
                    ->addObject($shipment->getOrder())
                    ->save();
            } catch (Mage_Core_Exception $e) {
                Mage::log("Errror While Creating Shipment..." . $e->getMessage());
            }
        }
    }

    public function prepareShipment($order, $cancelleditems)
    {
        foreach($order->getAllItems() as $orderItems)
        {

            $qty_ordered = $orderItems->getQtyOrdered();
            $cancelleditems[$orderItems->getId()] = (int) ($qty_ordered - $cancelleditems[$orderItems->getId()]);
        }

        $shipment = Mage::getModel('sales/order')->create ( $order, isset ( $cancelleditems ) ? $cancelleditems : [ ], [ ] );

        if (! $shipment->getTotalQty ()) {

            return false;
        }

        return $shipment;
    }

     /*
     * @Ship by walmart save process
     */
    public function putShipOrder($data_ship = NULL, $postData, $order_to_complete = [], $order_cancel = [], $mixed = []) {
        $flag=$this->getOrderFlag($order_to_complete,$order_cancel,$mixed);
        if($flag['item_count'] == $flag['complete'])
        {
            $order_to_complete = true;
            $order_cancel = false;
            $mixed = false;
        }elseif($flag['item_count'] == $flag['cancel'])
        {
            $order_to_complete = false;
            $order_cancel = true;
            $mixed = false;
        }else{
            $order_to_complete = false;
            $order_cancel = false;
            $mixed = true;
        }

        $id = $postData ['key1'];
        $order_id = $postData ['orderid'];
        $magento_order_id = $postData ['order'];
        $items_data = $postData ['items'];
        $items_data = json_decode ( $items_data );
        /* Do not touch*/
        $quantity_to_cancel = $items_data [0] [2];
        /* Do not touch end */
        $order = Mage::getModel( 'sales/order' )->loadByIncrementId ( $id );
        
        // for order items ids and cancel qty relation
        foreach($order->getAllItems() as $orderItem)
        {
            foreach($items_data as $val)
            {
                if($orderItem->getSku() == $val[0])
                    $cancelleditems[$magento_order_id] = $val[2];
            }

        }
        $cancelData = Mage::helper('walmart/data')->rejectOrders($order_id,$data_ship);
        $data = Mage::helper('walmart/data')->shipOrder(  $data_ship  );

        // Api call to complete shipment on walmart
        
        $responsedata['shippedData'] = $data;
        $responsedata['cancelData'] = $cancelData;
        $walmartmodel = Mage::getModel ( 'walmart/walmartorder' )->load ( $magento_order_id,'magento_order_id' );
       
        if ($responsedata && (count($walmartmodel)!=0)) {
            try {
                // as cancel quantity is not available in $dataship array for complete case
                if ($quantity_to_cancel != 0) {
                    $data_ship ['shipments'] [0] ['shipment_items'] [0] ['response_shipment_cancel_qty'] = $quantity_to_cancel;
                }

                $this->saveWalmartShipData ( $walmartmodel, $data_ship, $order_to_complete, $order_cancel,$mixed , $order, $cancelleditems,$responsedata);
                return  "Success" ;
            } catch ( \Exception $e ) {
                return $e->getMessage ();
            }
        } else {
            $err =  'Error while generating shipment on walmart.com';
            return $err;
        }
    }

    public function generateCreditMemo($order ,$itemQtytoCancel){
        $qtys = array('qtys' => $itemQtytoCancel);
        $service = Mage::getModel('sales/service_order', $order);
        $service->prepareCreditmemo($qtys)->register()->save();
    }

     

      /**
     * @Ship by walmart save process
     */
    public function saveWalmartShipData($walmartmodel, $data_ship, $order_to_complete=null, $order_cancel=null, $mixed=null,$order, $cancelleditems , $responsedata) {

        // change 1 sept optimize it merging similar case removed whole complete case due to repetition of code

        // mixed_complete_case // And need one more case for multiple shipment (data of mixed will used)

        // all case in one

        $serData = json_decode($responsedata['shippedData'],true);
        $canceldata = json_decode($responsedata['cancelData'],true);

         if(((!$order_cancel || $mixed) && (!isset($serData['ns4:errors']['ns4:error']))) || (strpos($serData['ns4:errors']['ns4:error']['ns4:description'],'SHIPPED status')==true )) { 
            $walmartmodel->setStatus ( 'Complete' );
        }else{ 
            $walmartmodel->setStatus ( 'Cancelled' );
        }


        $walmartmodel->setShipmentData ( serialize ( $responsedata ) );
        $walmartmodel->save ();


        if(!$order_cancel)
            if (! $order->canShip()) {
                 Mage::getSingleton('core/session')->addError("You can\'t create an shipment.");
            }else{
                $this->generateShipment ( $order , $cancelleditems);
            }

        if(!$order_to_complete || $order_cancel)
            if (!$order->canCreditmemo()) {
                Mage::getSingleton('core/session')->addError("We can\'t create credit memo for the order.");
                return false;
            }else{
                $this->generateCreditMemo($order,$cancelleditems);
            }
        
        if(!isset($serData['ns4:errors']['ns4:error'])) {
             Mage::getSingleton('core/session')->addSuccess('Your Walmart Order ' . $order->getId() . ' has been Completed.' );
        } elseif (isset($serData['ns4:errors']['ns4:error']) && (strpos($serData['ns4:errors']['ns4:error']['ns4:description'],'SHIPPED status')==true)) {
            Mage::getSingleton('core/session')->addSuccess('Your Walmart Order ' . $order->getId() . 'already in Shipped Status.' );
        } else {
            if (isset($serData['ns4:errors']['ns4:error']['ns4:description']))
            Mage::getSingleton('core/session')->addError($serData['ns4:errors']['ns4:error']['ns4:description']);
        }
       
    }

    function getCountryId($key){
       $country = array(
        'Argentina'     => 'AR',
        'Armenia'       => 'AM',
        'Australia'     => 'AU',
        'Austria'       => 'AT',
        'Belgium'       => 'BE',
        'Botswana'      => 'BW',
        'Brazil'        => 'BR',
        'Bulgaria'      => 'BG',
        'Canada'        => 'CA',
        'Chile'         => 'CL',
        'China'         => 'CN',
        'Colombia'      => 'CO',
        'Costa Rica'    => 'CR',
        'Croatia'       => 'HR',
        'Czech Republic' => 'CZ',
        'Denmark'       => 'DK',
        'Dominican Republic' => 'DO',
        'Ecuador'       => 'EC',
        'Egypt'         => 'EG',
        'El Salvador'   => 'SV',
        'Estonia'       => 'EE',
        'Finland'       => 'FI',
        'France'        => 'FR',
        'Germany'       => 'DE',
        'Greece'        => 'GR',
        'Guatemala'     => 'GT',
        'Honduras'      => 'HN',
        'Hong Kong SAR China' => 'HK',
        'Hungary'       => 'HU',
        'India'         => 'IN',
        'Indonesia'     => 'ID',
        'Ireland'       => 'IE',
        'Israel'        => 'IL',
        'Italy'         => 'IT',
        'Jamaica'       => 'JM',
        'Japan'         => 'JP',
        'Jordan'        => 'JO',
        'Kazakstan'     => 'KZ',
        'Kenya'         => 'KE',
        'South Korea'   => 'KR',
        'Kuwait'        => 'KW',
        'Latvia'        => 'LV',
        'Lebanon'       => 'LB',
        'Lithuania'     => 'LT',
        'Luxembourg'    => 'LU',
        'Macau SAR China' => 'MO',
        'Macedonia'     => 'MK',
        'Madagascar'    => 'MG',
        'Malaysia'      => 'MY',
        'Mali'          => 'ML',
        'Malta'         => 'MT',
        'Mauritius'     => 'MU',
        'Mexico'        => 'MX',
        'Moldova'       => 'MD',
        'Netherlands'   => 'NL',
        'New Zealand'   => 'NZ',
        'Nicaragua'     => 'NI',
        'Niger'         => 'NE',
        'Norway'        => 'NO',
        'Pakistan'      => 'PK',
        'Panama'        => 'PA',
        'Paraguay'      => 'PY',
        'Peru'          => 'PE',
        'Philippines'   => 'PH',
        'Poland'        => 'PL',
        'Portugal'      => 'PT',
        'Qatar'         => 'QA',
        'Romania'       => 'RO',
        'Russia'        => 'RU',
        'Saudi Arabia'  => 'SA',
        'Senegal'       => 'SN',
        'Singapore'     => 'SG',
        'Slovakia'      => 'SK',
        'Slovenia'      => 'SI',
        'South Africa'  => 'ZA',
        'Spain'         => 'ES',
        'Sri Lanka'     => 'LK',
        'Sweden'        => 'SE',
        'Switzerland'   => 'CH',
        'Taiwan'        => 'TW',
        'Thailand'      => 'TH',
        'Tunisia'       => 'TN',
        'Turkey'        => 'TR',
        'Uganda'        => 'UG',
        'United Arab Emirates' => 'AE',
        'United Kingdom' => 'GB',
        'United States' => 'US',
        'Uruguay'       => 'UY',
        'Venezuela'     => 'VE',
        'Vietnam'       => 'VN',
    );
       if(isset($country[$key])){
        return $country[$key];
       }
       else{
            return 'US';
       }
    }

}
