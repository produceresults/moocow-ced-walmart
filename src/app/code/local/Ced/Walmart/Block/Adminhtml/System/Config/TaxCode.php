<?php 
class Ced_Walmart_Block_Adminhtml_System_Config_TaxCode extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        //$url = $this->getUrl('catalog/product'); //
        $html = $this->getLayout()->createBlock('core/template')->setTemplate('ced/walmart/taxcode.phtml')->toHtml();
        return $html;
    }
}
?>