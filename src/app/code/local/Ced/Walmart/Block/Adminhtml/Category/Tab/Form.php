<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class Ced_Walmart_Block_Adminhtml_Category_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm(){
                $form = new Varien_Data_Form();
                $this->setForm($form);
                $walmartUrl = Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_walmartattrlist/categorylist');
                $category_id=Mage::app()->getRequest()->getParam('id');
                $value= Mage::getModel('walmart/walmartcategory')->getCollection()->addFieldToFilter('magento_cat_id',$category_id)->getFirstItem();
                $walmart_mapped_id=$value->getData('walmart_cate_id');
                $walmart_mapped_id=($walmart_mapped_id==0?'':$walmart_mapped_id);
                $fieldset = $form->addFieldset('custom_category_tab_form', array('legend'=>Mage::helper('catalog')->__('Walmart Category Mapping')));
                $fieldset->addField('custom_tab_text', 'text', array(
                'label'=> Mage::helper('catalog')->__('Walmart Category Id'),
                'class' => '',
                'required' => false,
                'name' => 'custom_tab_text',
                'value' => $walmart_mapped_id,
                'note' => 'Please Enter correct Walmart Category Id , to get Walmart Category Id , <a href='.$walmartUrl.' target="_blank">click here</a> . Leave blank if don\'t want to accociate it with Walmart Category .',
            ));
            return parent::_prepareForm();
    }
}