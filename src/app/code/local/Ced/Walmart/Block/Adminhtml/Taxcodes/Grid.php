<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ced_Walmart_Block_Adminhtml_Taxcodes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('_taxcodes');
        $this->setDefaultSort('id');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);


    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    /**
     * prepare the collection of gift and set for grid
     */
    protected function _prepareCollection()
    {

        $collection = Mage::getModel('walmart/Walmarttaxcodes')->getCollection();

        $this->setCollection($collection);

        return parent::_prepareCollection();

    }

    /**
     * prepare the column in the grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('walmart')->__('Id'),
            'align'     =>'right',
            'width'     => '3%',
            'index'     => 'id',
        ));

        $this->addColumn('tax_code', array(
            'header'    => Mage::helper('walmart')->__('Walmart Tax Code'),
            'width' 	=> '5%',
            'align'     =>'left',
            'index'     => 'tax_code',
        ));

        $this->addColumn('cat_desc',
            array(
                'header'=> Mage::helper('walmart')->__('Category Description'),
                'align'     =>'left',
                'width' 	=> '45%',
                'index' => 'cat_desc',
            ));

        $this->addColumn('sub_cat_desc', array(
            'header'    => Mage::helper('walmart')->__('Sub Category Description'),
            'width' 	=> '45%',
            'align'     => 'left',
            'index'     => 'sub_cat_desc',
        ));
        return parent::_prepareColumns();

    }

    // Used for AJAX loading
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

}
