<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/
 
class Ced_Walmart_Block_Adminhtml_Sales_Order_View_Shipbywalmart extends Mage_Adminhtml_Block_Sales_Order_View_Tabs{

	public function getOrder()
		{
		
			$order_obj = Mage::registry('current_order');
			$Incrementid = $order_obj->getIncrementId(); 
			$resultdata = Mage::getModel('walmart/walmartorder')->getCollection()
				->addFieldToFilter('magento_order_id',$Incrementid)->getData();
			if(isset($resultdata[0]['order_data'])){
					return unserialize($resultdata[0]['order_data'])?
								unserialize($resultdata[0]['order_data']): false;	
					}else{
						return false;
					}
		}


		 /**
     * parserArray
     * {@inheritdoc}
     */
    public function parserArray($shipmentData = [])
    {
    	
        $shipmentData = count($shipmentData)>0?$shipmentData:$this->getOrder();

        if (!empty($shipmentData)) {
            $arr = [];
            foreach ($shipmentData["orderLines"]['orderLine'] as $key => $value) {
                if ( in_array($key, $arr)) {
                    continue;
                }
                $count = count($shipmentData["orderLines"]['orderLine']);
                $sku = $value['item']['sku'];
                $shipQuantity = 0;
                $cancelQuantity = 0;
                $quantity = 1;
                if ($value['orderLineStatuses']['orderLineStatus'][0]['status'] == 'Shipped') {
                    $shipQuantity = 1;
                } else {
                    $cancelQuantity = 1;
                }
                $lineNumber = $value['lineNumber'];
                for ( $i = $key+1 ; $i < $count;$i++) {
                    if ($shipmentData["orderLines"]['orderLine'][$i]['item']['sku'] == $sku ) {
                        $quantity++;
                        if (
                            $shipmentData["orderLines"]['orderLine'][$i]['orderLineStatuses']
                            ['orderLineStatus'][0]['status'] == 'Shipped') {
                            $shipQuantity++;
                        } else {
                            $cancelQuantity++;
                        }
                        $lineNumber = $lineNumber.','.$shipmentData["orderLines"]['orderLine'][$i]['lineNumber'];
                        unset($shipmentData["orderLines"]['orderLine'][$i]);
                        array_push($arr, $i);
                        array_values($shipmentData["orderLines"]['orderLine']);
                    }
                }
                $shipmentData["orderLines"]['orderLine'][$key]['lineNumber'] = $lineNumber;
                $shipmentData["orderLines"]['orderLine'][$key]['orderLineQuantity']['shipQuantity'] = $shipQuantity;
                $shipmentData["orderLines"]['orderLine'][$key]['orderLineQuantity']['amount'] = $quantity;
                $shipmentData["orderLines"]['orderLine'][$key]['orderLineQuantity']['cancelQuantity'] = $cancelQuantity;
            }
            return $shipmentData;
        }
        return false;
    }
    
    
    public function getShipmentData()
    {
        $order_obj = Mage::registry('current_order');
        $Incrementid = $order_obj->getIncrementId(); 
        $resultdata = Mage::getModel('walmart/walmartorder')->getCollection()
                            ->addFieldToFilter('magento_order_id',$Incrementid)->getData();
        if (isset($resultdata[0]['shipment_data'])) {
            return unserialize($resultdata[0]['shipment_data']);
        }
      
    }
}
