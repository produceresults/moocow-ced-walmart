<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class Ced_Walmart_Block_Adminhtml_Walmartattribute_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $id = '';
        $params = $this->getRequest()->getParams();
        if (isset($params['attribute_id'])) {
            $id = $params['attribute_id'];
        }
        $model = Mage::getModel('walmart/walmartattribute')->load($id);

        $walmartattr = Mage::getModel('walmart/walmartattribute')->getCollection()->addFieldToSelect('walmart_attribute_name')->getData();

        foreach ($walmartattr as $att) {
          $walmartattributesArrays[] = array (

                    'label' => $att['walmart_attribute_name'],
                    'value' => $att['walmart_attribute_name']
                    ); 
        }

        $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
                          ->getItems();

        foreach ($attributes as $attribute){
            $magentoattributeCodeArray[$attribute->getAttributecode()] = $attribute->getAttributecode();
        }
        $mattributecode = $model->getMagentoAttributeCode()!='' ? $model->getMagentoAttributeCode() : '--please select--';
        $magentoattributeCodeArray[$mattributecode] = $mattributecode;

        $form = new Varien_Data_Form();
        
        $fieldset = $form->addFieldset('walmart_return',array('legend'=>Mage::helper('walmart')->__('Mapping Attribute')));
            
            $fieldset->addField('attribute_id', 'hidden', array(
                                'label'     => Mage::helper('walmart')->__('Attribute Id'),
                                'name'=>"attribute_id",
                                'value'=> $id,
                              ));

            $fieldset->addField('walmart_attribute', 'select', array(
                                'label'     => Mage::helper('walmart')->__('Walmart Attribute'),
                                'required'  => false,
                                'disabled' => true,
                                'name'=>"walmart_attribute",
                                'value'=> $model->getWalmartAttributeName(),
                                'values' =>  $walmartattributesArrays,
                              ));

            $fieldset->addField('magento_attribute_code', 'select', array(
                                'label'     => Mage::helper('walmart')->__('Magento Attribute Code'),
                                'required'  => false,
                                'name'=>"magento_attribute_code",
                                'value'=>  $mattributecode,
                                'values'=> $magentoattributeCodeArray,
                              ));
         $this->setForm($form);
       
        return parent::_prepareForm();
    }
}
