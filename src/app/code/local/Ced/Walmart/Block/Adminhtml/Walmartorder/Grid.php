<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/


class Ced_Walmart_Block_Adminhtml_Walmartorder_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
    {
        parent::__construct();
        $this->setId('walmart_order_grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('magento_order_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Retrieve collection class
     * @return string
     */
    protected function _getCollectionClass()
    {
        return 'walmart/walmartorder';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel($this->_getCollectionClass())->getCollection();
		$salesFlatOrder = (string)Mage::getConfig()->getTablePrefix() .'sales_flat_order_grid';
		$walmartOrderDetail = (string)Mage::getConfig()->getTablePrefix().'walmart_order_detail';
		$collection->getSelect()->joinLeft(array('sales'=>$salesFlatOrder),'main_table.magento_order_id = sales.increment_id',array('sales.billing_name','sales.shipping_name','sales.billing_name','sales.grand_total'));
       	$this->setCollection($collection);
        //var_dump($collection->getData());die;

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
		$this->addColumn('magento_order_id', array(
            'header'=> Mage::helper('walmart')->__('Magento Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'magento_order_id',
			'renderer'  => 'Ced_Walmart_Block_Adminhtml_Walmartorder_Renderer_Vieworder'
        ));
		/*$this->addColumn('reference_order_id', array(
				'header'    => Mage::helper('walmart')->__('Walmart Referece Order #'),
				'align'     =>'left',
				'index'     => 'reference_order_id',
		));*/
		$this->addColumn('purchase_order_id', array(
				'header'    => Mage::helper('walmart')->__('Walmart Purchase Order Id #'),
				'align'     =>'left',
				'index'     => 'purchase_order_id',
		));
		$this->addColumn('billing_name', array(
				'header'    => Mage::helper('walmart')->__('Bill to Name'),
				'align'     =>'left',
				'index'     => 'billing_name',
				'filter_index'=>'sales.billing_name'
		));
		$this->addColumn('shipping_name', array(
				'header'    => Mage::helper('walmart')->__('Ship to Name'),
				'align'     =>'left',
				'index'     => 'shipping_name',
				'filter_index'=>'sales.shipping_name'
		));
		$this->addColumn('delivery_by', array(
            'header' => Mage::helper('walmart')->__('Delivery By'),
            'index' => 'deliver_by',
            'type' => 'datetime',
            'width' => '100px',
			'filter_index'=>'deliver_by'
        ));
		
		$this->addColumn('status', array(
			'header'    => Mage::helper('walmart')->__('Status'),
			'width' 	=> '200px',
			'align'     => 'left',
			'index'     => 'status',
			'type' => 'options',
			 'options' => array('Created'=>'Created','Acknowledged'=>'Acknowledged',
			 					'Cancelled'=>'Cancelled','Complete'=>'Completed'), 
			'filter_index'=>'main_table.status',
			'filter_condition_callback' => array($this, '_statusFilter'),					
			
		));
		$this->addColumn('grand_total', array(
            'header' => Mage::helper('walmart')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
			'filter_index'=>'sales.grand_total'
        ));
		/*
        $this->addColumn('customer_cancelled', array(
            'header'    => Mage::helper('walmart')->__('Order quantity cancelled by customer'),
            'align'     =>'left',
            'index'     => 'customer_cancelled',
            'filter_index'=>'customer_cancelled',
            'renderer'  => 'Ced_Walmart_Block_Adminhtml_Walmartorder_Renderer_Customercancel'
        ));
       */
	   
		$this->addExportType('*/*/exportCsv', Mage::helper('walmart')->__('CSV'));
        
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        
            $this->getMassactionBlock()->addItem('delete', array(
                 'label'=> Mage::helper('walmart')->__('Delete Order'),
                 'url'  => $this->getUrl('adminhtml/adminhtml_walmartorder/massdeleteorder'),
                  'confirm'  => Mage::helper('walmart')->__('Are you sure? Deleted order can not be undone.Order will delete from this panel only.Sales->Order will remain same.You can not process shipment/return/refund for these orders in future')
            ));
        

        return $this;
    }

    

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid2', array('_current'=>true));
    }
	
	protected function _statusFilter($collection, $column) {
		$filterroleid = $column->getFilter()->getValue();        
		if (!$value = $column->getFilter()->getValue()) {
			return $this;
		}        
		$this->getCollection()->addFieldToFilter('main_table.status', array('eq' => $filterroleid));
		return ;
	}
}
