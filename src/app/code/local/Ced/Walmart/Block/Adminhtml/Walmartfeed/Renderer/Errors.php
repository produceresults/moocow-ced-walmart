<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ced_Walmart_Block_Adminhtml_Walmartfeed_Renderer_Errors extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action {
    public function render(Varien_Object $row) {
        $id = $row->getId();
        $feeds = Mage::getModel('walmart/walmartfeed')->load($id);
        $errors = $feeds->getData('feed_errors');

        if ($errors == null) {
            $html = '<span class="grid-severity-notice"><span>Success</span></span>';
        } else {
            $errors = json_decode($errors, true);
            $html = "<div id='errors".$id."' style='padding:20px;'>";
            $html .= "<table style='width: 100%; border: 1px solid grey;' ><tr><th  style='border-right: 1px solid grey;border-bottom: 1px solid grey;'>Sl. No.</th><th style='border-bottom: 1px solid grey;'>Products</th></tr>";
            $count = 0;
            foreach ($errors["itemIngestionStatus"] as $key => $error) {
                $html .= "<tr><td style='border-right: 1px solid grey;'>" . ($key + 1) . "</td>";
                $html .= "<td><b>SKU : </b>" . $error['sku'] . "<br/>";
                $html .= "<b>Status : </b>" . $error['ingestionStatus'] . "<br />";
                $html .= "<b>Descripton : </b>";
                foreach ($error['ingestionErrors']['ingestionError'] as $desc) {
                    $html .= htmlentities($desc['description']) . "<br/>";
                }
                $html .=  "</td></tr>";
                $count += $key +1 ;
            }
            $html .= "</table></div>";
            $errorhtml='<a title="" onclick="javascript: showerror'.$id.'(\''.$id.'\')" href="#"><span class="grid-severity-critical"><span> ' .$count. ' Error(s)</span></span></a>';
            $newhtml='<script type="text/javascript">function showerror'.$id.'(idd) {
                        $$("body")[0].insert("'.$html.'");
                        oPopup = new Window({
                        id:"browser_window",
                        className: "magento",
                        windowClassName: "popup-window",
                        title: "Feed Errors",
                        width: 750,
                        height: 200,
                        minimizable: false,
                        maximizable: false,
                        showEffectOptions: {
                        duration: 0.4
                        },
                        hideEffectOptions:{
                        duration: 0.4
                        },
                        destroyOnClose: true
                        });
                        oPopup.setZIndex(100);
                        oPopup.showCenter(true);
                        oPopup.onbeforeunload = function () {
                                return "Are you sure?";
                            }
                        oPopup.setContent("errors"+idd,false,false);
                            $(\'browser_window_close\').observe(\'click\', function () {
                             document.getElementById("errors"+idd).remove();
                            });
                        }
                      
                        </script>';
            $errorhtml = $errorhtml.$newhtml;
            $html = $errorhtml;
        }
        return $html;

    }
}