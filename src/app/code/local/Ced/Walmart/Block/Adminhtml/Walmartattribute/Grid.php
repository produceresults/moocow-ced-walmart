<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/


class Ced_Walmart_Block_Adminhtml_Walmartattribute_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	protected function _prepareCollection()
    {
			/*$data=Mage::getModel('eav/entity_attribute_group')->getCollection()->addFieldToFilter('attribute_group_name','walmart')->getdata();*/
			
			//$groupid=$data[0]['attribute_group_id'];
			/*
			$collection = Mage::getResourceModel('catalog/product_attribute_collection')
				->addVisibleFilter()
		   		->setAttributeGroupFilter($groupid);*/
			  
			/*$walmartAttrTable =	Mage::getSingleton('core/resource')->getTableName('walmart/walmartattribute');
		
			$walmartGroupIds = $collection->getAllIds();*/
         
			$walmartAttr = Mage::getModel('walmart/walmartattribute')->getCollection();
			//$walmartAttrIds = $walmartAttr->getColumnValues('magento_attr_id');

			//$walmartAttrIds = array_merge($walmartGroupIds, $walmartAttrIds);
		//print_r($walmartAttrIds);
			/*$newCollection = Mage::getResourceModel('catalog/product_attribute_collection')->addFieldToFilter('main_table.attribute_id',array('in'=>$walmartAttrIds));
			*/
			/*$newCollection->getSelect()->joinLeft(
				array('walmart_attr'=>$walmartAttrTable),
			   'main_table.attribute_id = walmart_attr.magento_attr_id',
				array('walmart_attr_id')
			);*/
                //print_r($newCollection->getData());
               // die;
				$this->setCollection($walmartAttr);
        return parent::_prepareCollection();
    }

    /**
     * Prepare product attributes grid columns
     *
     * @return Mage_Adminhtml_Block_Catalog_Product_Attribute_Grid
     */
    protected function _prepareColumns()
    {
       // Mage_Eav_Block_Adminhtml_Attribute_Grid_Abstract::_prepareColumns();
		//Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
       
		$this->addColumnAfter('id', array(
            'header'=>Mage::helper('walmart')->__('Id'),
            'sortable'=>true,
            'index'=>'id',
            'align' => 'left',
        ), 'id');


        $this->addColumnAfter('walmart_attribute_name', array(
            'header'=>Mage::helper('walmart')->__('Walmart Attribute Name'),
            'sortable'=>true,
            'index'=>'walmart_attribute_name',
            'align' => 'left',
        ), 'walmart_attribute_name');

        $this->addColumnAfter('magento_attribute_code', array(
            'header'=>Mage::helper('walmart')->__('Magento Attribute Code'),
            'sortable'=>true,
            'index'=>'magento_attribute_code',
            'align' => 'left',
        ), 'magento_attribute_code');

		 $this->addColumnAfter('magento_attribute_code', array(
            'header'=>Mage::helper('walmart')->__('Magento Attribute Code'),
            'sortable'=>true,
            'index'=>'magento_attribute_code',
            'align' => 'left',
        ), 'magento_attribute_code');

        $this->addColumnAfter('walmart_attribute_enum', array(
            'header'=>Mage::helper('walmart')->__('Walmart Attribute Enum/Values'),
            'sortable'=>true,
            'index'=>'walmart_attribute_enum',
            'align' => 'left',
        ), 'level');
         $this->addColumnAfter('is_mapped', array(
            'header'=>Mage::helper('walmart')->__('is_mapped'),
            'sortable'=>true,
            'index'=>'is_mapped',
            'align' => 'left',
        ), 'is_mapped');
       /* $this->addColumnAfter('is_global', array(
            'header'=>Mage::helper('catalog')->__('Scope'),
            'sortable'=>true,
            'index'=>'is_global',
            'type' => 'options',
            'options' => array(
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE =>Mage::helper('catalog')->__('Store View'),
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE =>Mage::helper('catalog')->__('Website'),
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL =>Mage::helper('catalog')->__('Global'),
            ),
            'align' => 'center',
        ), 'is_visible');*/

       /* $this->addColumn('is_searchable', array(
            'header'=>Mage::helper('catalog')->__('Searchable'),
            'sortable'=>true,
            'index'=>'is_searchable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('catalog')->__('Yes'),
                '0' => Mage::helper('catalog')->__('No'),
            ),
            'align' => 'center',
        ), 'is_user_defined');
*/
       /* $this->addColumnAfter('is_filterable', array(
            'header'=>Mage::helper('catalog')->__('Use in Layered Navigation'),
            'sortable'=>true,
            'index'=>'is_filterable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('catalog')->__('Filterable (with results)'),
                '2' => Mage::helper('catalog')->__('Filterable (no results)'),
                '0' => Mage::helper('catalog')->__('No'),
            ),
            'align' => 'center',
        ), 'is_searchable');
*/
       /* $this->addColumnAfter('is_comparable', array(
            'header'=>Mage::helper('catalog')->__('Comparable'),
            'sortable'=>true,
            'index'=>'is_comparable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('catalog')->__('Yes'),
                '0' => Mage::helper('catalog')->__('No'),
            ),
            'align' => 'center',
        ), 'is_filterable');*/

        return $this;
    }
	
	/**
     * Return url of given row
     *
     * @return string
     */
   /* public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/catalog_product_attribute/edit', array('attribute_id' => $row->getAttributeId()));
    }*/
     
     public function getRowUrl($row)
    {
        return $this->getUrl('*/*/editattr', array('attribute_id' => $row->getId()));
    }
	
}
