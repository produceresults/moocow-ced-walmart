<?php
  class Ced_Walmart_Block_Adminhtml_Category_Mapping extends Mage_Adminhtml_Block_Template
  {
    public function __construct()
    {

        parent::__construct();
        $this->setTemplate('ced/walmart/mapping.phtml');
    }

    public function getWalmartCategoryId($field='csv_parent_id'){
      $category_id=Mage::app()->getRequest()->getParam('id');
      $value = Mage::getModel('walmart/catlist')->getCollection()->addFieldToFilter('magento_cat_id',$category_id)->getFirstItem();
      $walmart_mapped_id=$value->getData($field);
      $walmart_mapped_id=($walmart_mapped_id === 0?'':$walmart_mapped_id);

      return $walmart_mapped_id;
    }

    public function getFilteredWalmartCollection($level){
      return Mage::getModel('walmart/catlist')->getCollection()->addFieldToFilter('level' , $level);
    }

    public function getWalmartMappedIds()
    {
      $category_id=Mage::app()->getRequest()->getParam('id');
      $mappedIds = Mage::getModel('walmart/catlist')->getCollection();

      foreach($mappedIds as $value)
      {
          $ids=explode(',',$value->getMagentoCatId());

            foreach($ids as $mag_cat_id)
            {
              if($mag_cat_id == $category_id)
              { 
                  $mappedData = 
                  [
                    'csv_parent_id'=>$value->getCsvParentId(),
                    'csv_cat_id'=>$value->getCsvCatId()
                  ];
               
              } 
            }
        
      }
      return $mappedData;
    }

  }
 ?>
