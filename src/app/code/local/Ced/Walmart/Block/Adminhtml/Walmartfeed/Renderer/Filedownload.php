<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ced_Walmart_Block_Adminhtml_Walmartfeed_Renderer_Filedownload extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action {
    public function render(Varien_Object $row) {
        $id = $row->getId();
        $feeds = Mage::getModel('walmart/walmartfeed')->load($id);
        $filePath = $feeds->getData('feed_file');
        $fileUrl = "walmart/" . basename($filePath);

        if ($filePath == null) {
            $html = 'File Not Available';
        } else {
            $html = "<a href='".Mage::getBaseUrl('media'). $fileUrl . "' download>Download</a>";
        }
        return $html;

    }
}