<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/


class Ced_Walmart_Block_Adminhtml_Failedorders_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		//$this->_removeButton('add');
		$this->setId('_failedorders');
		$this->setDefaultSort('id');
	    $this->setUseAjax(true);
		$this->setSaveParametersInSession(true);
		
		
	}
	
	protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
	
	/**
	 * prepare the collection of gift and set for grid
	 */
	
	protected function _prepareCollection()
	{
		$collection = Mage::getModel('walmart/orderimport')->getCollection();
        $this->setCollection($collection);
		return parent::_prepareCollection();
	}
	
	/**
	 * prepare the column in the grid
	 */
	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
				'header'    => Mage::helper('walmart')->__('ID'),
				'align'     =>'right',
				'width'     => '80px',
				'index'     => 'id',
		));
		$this->addColumn('purchase_order_id', array(
				'header'    => Mage::helper('walmart')->__('Purchse order ID'),
				'align'     =>'left',
				'index'     => 'purchase_order_id',
		));
		$this->addColumn('reference_number', array(
				'header'    => Mage::helper('walmart')->__('Reference Number'),
				'align'     =>'left',
				'index'     => 'reference_number',
		));
		$this->addColumn('reason',
            array(
                'header'=> Mage::helper('walmart')->__('Reason to failed'),
                'align'     =>'left',
				'index' => 'reason',
        ));
        $this->addColumn('order_data',
            array(
                'header'=> Mage::helper('walmart')->__('Order Data'),
                'align'     =>'left',
				'index' => 'order_data',
        ));
		
		return parent::_prepareColumns();
		
	}
	
	/**
	 * Delete failed Walmart Order's Log
	 */
	protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        if (Mage::getSingleton('admin/session')->isAllowed('walmart/walmartorder/actions/delete')) {
            $this->getMassactionBlock()->addItem('delete', array(
                 'label'=> Mage::helper('walmart')->__('Delete'),
                 'url'  => $this->getUrl('adminhtml/adminhtml_walmartorder/deletewalmartorderlog'),
            ));
        }

        

        return $this;
    }
	
	// Used for AJAX loading
	public function getGridUrl()
	{
		return $this->getUrl('*/*/grid', array('_current'=>true));
	}
}
