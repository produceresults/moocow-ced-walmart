<?php
/**
* CedCommerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
* @category    Ced
* @package     Ced_Walmart
* @author      CedCommerce Core Team <connect@cedcommerce.com>
* @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
* @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

class Ced_Walmart_Block_Adminhtml_Prod_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('_prod');
        $this->setDefaultSort('id');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(false);

    }
    
    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
    
    protected function _prepareCollection()
    {           
        $result=Mage::getModel('walmart/catlist')->getCollection()->addFieldToSelect('magento_cat_id');
        
        $resultdata=array();
        $arr = array();
       foreach($result as $val){

            $arr = explode(',',$val['magento_cat_id']);

            if($arr[0]!=Null)
            {
                $zz = array();
                $zz = explode(',',$val['magento_cat_id']);
                foreach ($zz as $key => $value) {
                   $resultdata[] = $value;
                }
                
                //$resultdata[]=explode(',',$val['magento_cat_id']);
            }
        }
        
        
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('attribute_set_id')
            ->addAttributeToSelect('type_id')
            ->addAttributeToSelect('walmart_product_status')
            ->addAttributeToSelect('price');
                        
        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')){ 
            $collection->joinField('qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');
        } 
        
        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
        $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
                
        $collectionData = $collection;
    
        $collectionProd = Mage::getModel('catalog/product')->getCollection();
        $collectionProd->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id =entity_id', null, 'left');
        $collectionProd->addAttributeToSelect('*')
        ->addAttributeToFilter('category_id', array('in' => $resultdata));

        $ids = $collectionProd->getAllIds();
        $ids = array_unique($ids);
        
        $collection->addFieldToFilter('entity_id', array('in'=>$ids))
                    ->addAttributeToFilter('type_id', array('in' => array('simple','configurable')))
                    ->addAttributeToFilter('visibility', 4);
                    //->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                    //->addAttributeToFilter('type_id', array('in' => array('simple','configurable')));
        $this->setCollection($collection);
        
        return parent::_prepareCollection();

    }
    
    /**
     * prepare the column in the grid
     */
    protected function _prepareColumns()
    {

        $this->addColumn('entity_id', array(
                'header'    => Mage::helper('catalog')->__('ID'),
                'align'     =>'right',
                'width'     => '80px',
                'index'     => 'entity_id',
        ));

        $this->addColumn('sku', array(
                'header'    => Mage::helper('catalog')->__('Sku'),
                'align'     =>'left',
                'index'     => 'sku',
        ));
        
        $store = $this->_getStore();
        $this->addColumn('price',
            array(
                'header'=> Mage::helper('catalog')->__('Price'),
                'type'  => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'price',
        ));


        $this->addColumn('name', array(
                'header'    => Mage::helper('catalog')->__('Name'),
                'width'     => '200px',
                'align'     => 'left',
                'index'     => 'name',
        ));
        
        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $this->addColumn('qty',
                array(
                    'header'=> Mage::helper('catalog')->__('Qty'),
                    'width' => '100px',
                    'type'  => 'number',
                    'index' => 'qty',
            ));
        }
        
        $this->addColumn('visibility',
            array(
                'header'=> Mage::helper('catalog')->__('Visibility'),
                'width' => '70px',
                'index' => 'visibility',
                'type'  => 'options',
                'options' => Mage::getModel('catalog/product_visibility')->getOptionArray(),
        ));

       $this->addColumn('status',
            array(
                'header'=> Mage::helper('catalog')->__('Status'),
                'width' => '70px',
                'index' => 'status',
                'type'  => 'options',
                'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));
        
        $this->addColumn('type',
            array(
                'header'=> Mage::helper('catalog')->__('Type'),
                'width' => '60px',
                'index' => 'type_id',
                'type'  => 'options',
                'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));
      /*  $walmart_state = Mage::getModel('eav/config')->getAttribute('catalog_product', 'walmart_product_status');
        $options = $walmart_state->getSource()->getAllOptions(false);
        $walmartcomstatus = array();
        foreach ($options as $option){
            $walmartcomstatus[$option['value']] = $option['label'];
        }*/
        
        /*$this->addColumn('walmart_product_status',
            array(
                'header'=> Mage::helper('catalog')->__('Walmart Product Status'),
                'width' => '60px',
                'index' => 'walmart_product_status',
                'type'  => 'options',
                'options' => $walmartcomstatus,
        ));*/
        
        
        
        /*$this->addColumn('action',
            array(
                    'header'    =>  Mage::helper('walmart')->__('Action'),
                    'width'     => '100px',
                    'type'      => 'action',
                    'getter'    => 'getId',
                    'actions'   => array(
                            array(
                                    'caption'   => Mage::helper('walmart')->__(' Walmart Details'),
                                    'url'       => array('base'=> 'adminhtml/adminhtml_walmartrequest/productDetails'),
                                    'field'     => 'id'
                            )
                    ), 
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'action',
                    'is_system' => true,
                    'renderer' => 'Ced_Walmart_Block_Adminhtml_Prod_Renderer_Showerror',
            ));*/
        $this->addColumn('action145',
                array(
                        'header'    =>  Mage::helper('walmart')->__('Validation'),
                        'width'     => '100px',
                        'type'      => 'options',
                        'getter'    => 'getId',
                        'sortable'  => false,
                        'index'     => 'walmart_product_validation',
                         'options' => array('valid'=>'Valid','Required-Attribute'=>'Invalid','notvalidated'=>'Not Validated'), 
                        
                        'is_system' => true,
                        'renderer' => 'Ced_Walmart_Block_Adminhtml_Prod_Renderer_ValidationStatus',
                        'filter_index'=>'main_table.walmart_product_validation',
            'filter_condition_callback' => array($this, '_validateFilter'),   
                ));

        $this->addColumn('save', array(
            'header'    => Mage::helper('walmart')->__('Upload'),
            'align'     =>'right',
            'width'     => '50px',
            'renderer'  =>'Ced_Walmart_Block_Adminhtml_Prod_Renderer_Upload',
            'index'     => 'save',
            'filter'    => false,
            'sortable'  => false,
        ));
        
        return parent::_prepareColumns();
        
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('product');
       
        $this->getMassactionBlock()->addItem('import', array(
                'label'=> Mage::helper('walmart')->__('Selected Product Upload'),
                'url'  => $this->getUrl('adminhtml/adminhtml_walmartproduct/massimport'),
        ));
        /*$this->getMassactionBlock()->addItem('ajaximport', array(
                'label'=> Mage::helper('walmart')->__('Bulk Product Upload'),
                'url'  => $this->getUrl('adminhtml/adminhtml_walmartajax/massimport'),
        ));*/
      /*  $this->getMassactionBlock()->addItem('archived', array(
                'label'=> Mage::helper('walmart')->__('Selected Product Retire '),
                'url'  => $this->getUrl('adminhtml/adminhtml_walmartproduct/massarchived'),
        ));*/
       /* $this->getMassactionBlock()->addItem('ajaxarchive', array(
            'label'=> Mage::helper('walmart')->__('Bulk Product Archive'),
            'url'  => $this->getUrl('adminhtml/adminhtml_walmartajax/massarchived'),
        ));*/
       /* $this->getMassactionBlock()->addItem('unarchive', array(
                'label'=> Mage::helper('walmart')->__('Selected Product Unarchive'),
                'url'  => $this->getUrl('adminhtml/adminhtml_walmartproduct/unarchived'),
        ));*/
       
        $this->getMassactionBlock()->addItem('validate', array(
            'label'=> Mage::helper('walmart')->__('Validate Selected Product(s)'),
            'url'  => $this->getUrl('adminhtml/adminhtml_walmartajax/massvalidate'),
        ));
        $this->getMassactionBlock()->addItem('priceUpdate', array(
            'label'=> Mage::helper('walmart')->__('Product Price Update'),
            'url'  => $this->getUrl('adminhtml/adminhtml_walmartajax/masspriceupdate'),
        ));
        $this->getMassactionBlock()->addItem('inventoryUpdate', array(
            'label'=> Mage::helper('walmart')->__('Product Inventory Update'),
            'url'  => $this->getUrl('adminhtml/adminhtml_walmartajax/massinventoryupdate'),
        ));     
         $this->getMassactionBlock()->addItem('massretire', array(
            'label'=> Mage::helper('walmart')->__('Retire Seclected Product(s)'),
            'url'  => $this->getUrl('adminhtml/adminhtml_walmartajax/massretire'),
        ));
        return $this;
    }       
    
    public function getGridUrl()
    {
        return $this->getUrl('adminhtml/adminhtml_walmartrequest/uploadproductgrid2', array('_current'=>true));
    }
       public function getRowUrl($row)
   {

      return $this->getUrl('adminhtml/catalog_product/edit', array('id' => $row->getId()));

   }
   protected function _validateFilter($collection, $column) {

        $filterroleid = $column->getFilter()->getValue();        
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }        
        /*$this->getCollection()->addFieldToFilter('walmart_product_validation', array('eq' => $filterroleid));*/
        if($filterroleid == 'Required-Attribute')
        {

           $this->getCollection()->addFieldToFilter('walmart_product_validation', array('like' => '%'.$filterroleid.'%')); 
       }elseif($filterroleid == 'valid'){
        $this->getCollection()->addFieldToFilter('walmart_product_validation', array('eq' => $filterroleid));
        }else{

           $this->getCollection()->addFieldToFilter('walmart_product_validation', array('like' => ''));
        }
      
        return $this;
    }
}