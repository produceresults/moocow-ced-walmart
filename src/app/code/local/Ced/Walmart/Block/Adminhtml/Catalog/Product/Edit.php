<?php
class Ced_Walmart_Block_Adminhtml_Catalog_Product_Edit extends Mage_Adminhtml_Block_Catalog_Product_Edit
{
    /**
     * @var Mage_Catalog_Model_Product Product instance
     */
    private $_product;
 
    /**
     * Preparing global layout
     * 
     * @return Ced_Walmart_Block_Adminhtml_Catalog_Product_Edit|Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {

        parent::_prepareLayout();
$id = Mage::registry('current_product')->getId();
       /* $sku= Mage::registry('current_product')->getSku();

        $id = Mage::registry('current_product')->getId();
       $type =  Mage::registry('current_product')->getTypeId();
       $pro = Mage::registry('current_product');
        $response_simple = array();
        $response_configurable = array();
                    $api_url = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_apiurl');
                   $user = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_user');
                   $pass = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_userpwd');
                   $fullfil = Mage::getStoreConfig('walmart_options/ced_walmart/walmart_fullfillmentnode');
                   
                   if($api_url ='' || $user ='' || $pass ='' ||  $fullfil ='')
                   {

                        return $this;
                   }
       if($type == 'configurable')
        {
             $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$pro);
                foreach($childProducts as $chp){

                   $response_configurable[] = Mage::helper('walmart')->CGetRequest('/merchant-skus/' . $chp->getSku());
                   
                } 
        }
        if($type == 'simple')
        {
            $response_simple = Mage::helper('walmart')->CGetRequest('/merchant-skus/' . $sku);
            
        }
       
      
       
        */     
        $this->_product = $this->getProduct();
        $this->setChild('view_on_front',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                'label'     => Mage::helper('catalog')->__('Sync With Walmart'),
                'onclick'   =>  "setLocation('{$this->getUrl('adminhtml/adminhtml_walmartproduct/massimport/id/'.$id.'')}')",
                
                'title' => Mage::helper('catalog')->__('Upload Walmart')
            ))
        );
 
        
      
            return $this;
    }
 
    /**
     * Returns duplicate & view on front buttons html
     * 
     * @return string
     */
    public function getDuplicateButtonHtml()
    {
        return $this->getChildHtml('duplicate_button') . $this->getChildHtml('view_on_front');
    }
 
    /**
     * Checking product visibility
     * 
     * @return bool
     */
    private function _isVisible()
    {
        return $this->_product->isVisibleInCatalog() && $this->_product->isVisibleInSiteVisibility();
    }
 
}
?>