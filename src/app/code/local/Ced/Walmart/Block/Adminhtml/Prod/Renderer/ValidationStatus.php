<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ced_Walmart_Block_Adminhtml_Prod_Renderer_ValidationStatus extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action {
    public function render(Varien_Object $row) {
        $id=$row->getId();

        $view_url = $this->getUrl('adminhtml/adminhtml_walmartrequest/productDetails',array('id'=>$id));
        $editurl = $this->getUrl('adminhtml/catalog_product/edit',array('id'=>$id));
        $html='<a href="'.$view_url.'">View</a>';
        $html= $html.' |&nbsp;&nbsp;<a href="'.$editurl.'">Edit</a>';
        $error="";
        $date="";
        $date1="";
        $batchmod=Mage::getModel('catalog/product')->load($id);
        $errors = $batchmod->getData('walmart_product_validation');

        if ($batchmod->getTypeId() == 'configurable' && $errors == '') {
            $sku = $batchmod->getSku();
            $productType = $batchmod->getTypeInstance();
            $products = $productType->getUsedProducts($batchmod);
            $config =false;
            $count=0;
            $validcounts = 0;
            $errors = '';
            foreach($products as $product)
            {
                $childProduct=Mage::getModel('catalog/product')->load($product->getId());               
                if(($childProduct->getWalmartProductValidation() !== '') && ($childProduct->getWalmartProductValidation() !== 'valid')){
                    if($errors == ''){
                        $errors = "SKU -- <span style='color:blue'>".$childProduct->getSku().'</span> : '.$childProduct->getWalmartProductValidation();
                    } else {
                        $errors .= ','."SKU -- <span style='color:blue'>".$childProduct->getSku().'</span> : '.$childProduct->getWalmartProductValidation();                             
                    }
                }
            }  
            if($errors == ''){
                $errors = 'valid';
            }
        }     

        if($errors == 'valid'){
            $html = '<span class="grid-severity-notice"><span>Valid</span></span>';
        }
        else if($errors == ''){
            $html = '<span class="grid-severity-minor"><span>'.$this->__('Not Validated').'</span></span>';
        }
        else{
            $errors = explode(',',$errors);
            $html = "<div id='errors".$id."' style='padding:20px;'><ul style='list-style:disc;color:red;'>";
            foreach ($errors as $key => $error) {

                $html .= '<li>'.$error.'</li>';

            }
            $html .= '</ul></div>';
            $errorhtml='<a title="" onclick="showerror'.$id.'(\''.$id.'\')" href="#"><span class="grid-severity-critical"><span>Invalid</span></span></a>';
            $newhtml='<script type="text/javascript">function showerror'.$id.'(idd) {
                        $$("body")[0].insert("'.$html.'");
                        oPopup = new Window({
                        id:"browser_window",
                        className: "magento",
                        windowClassName: "popup-window",
                        title: "Missing Attributes",
                        width: 750,
                        height: 200,
                        minimizable: false,
                        maximizable: false,
                        showEffectOptions: {
                        duration: 0.4
                        },
                        hideEffectOptions:{
                        duration: 0.4
                        },
                        destroyOnClose: true
                        });
                        oPopup.setZIndex(100);
                        oPopup.showCenter(true);
                        oPopup.onbeforeunload = function () {
                                return "Are you sure?";
                            }
                        oPopup.setContent("errors"+idd,false,false);
                        }
                        
                        </script>';
            $errorhtml=$errorhtml.$newhtml;
            $html=$errorhtml;
        }
       
        return $html;

    }
}