<?php
/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * You can check the licence at this URL: http://cedcommerce.com/license-agreement.txt
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 *
 * @category    Ced
 * @package     Ced_Walmart
 * @author      CedCommerce Core Team <connect@cedcommerce.com>
 * @copyright   Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Ced_Walmart_Block_Adminhtml_Walmartfeed_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('walmart_feed_grid');
        $this->setUseAjax(false);
        $this->setDefaultSort('feed_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Retrieve collection class
     * @return string
     */
    protected function _getCollectionClass()
    {
        return 'walmart/walmartfeed';
    }

    protected function _prepareCollection()
    {
        $collection=Mage::getModel('walmart/walmartfeed')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'=> Mage::helper('walmart')->__('Id #'),
            'width' => '3%',
            'type'  => 'text',
            'index' => 'id'
        ));
        $this->addColumn('feed_id', array(
            'header'    => Mage::helper('walmart')->__('Feed Id #'),
            'align'     =>'left',
            'width' => '40%',
            'index'     => 'feed_id',
        ));
        $this->addColumn('feed_status', array(
            'header'    => Mage::helper('walmart')->__('Feed Status'),
            'width' => '3%',
            'align'     =>'left',
            'index'     => 'feed_status',
        ));
        $this->addColumn('feed_source', array(
            'header'    => Mage::helper('walmart')->__('Feed Source'),
            'width' => '3%',
            'align'     =>'left',
            'index'     => 'feed_source'
        ));
        $this->addColumn('items_received', array(
            'header'    => Mage::helper('walmart')->__('Items Received'),
            'width' => '3%',
            'align'     =>'left',
            'index'     => 'items_received'
        ));
        $this->addColumn('items_failed', array(
            'header' => Mage::helper('walmart')->__('Items Failed'),
            'width' => '3%',
            'align'     =>'left',
            'index' => 'items_failed'
        ));

        $this->addColumn('items_processing', array(
            'header'    => Mage::helper('walmart')->__('Items Processing'),
            'width' => '3%',
            'align'     => 'left',
            'index'     => 'items_processing'
        ));
        $this->addColumn('feed_date', array(
            'header' => Mage::helper('walmart')->__('Feed Date'),
            'width' => '10%',
            'index' => 'feed_date',
            'align' => 'left',
        ));
        $this->addColumn('feed_file', array(
            'header' => Mage::helper('walmart')->__('Feed File'),
            'width' => '3%',
            'index' => 'feed_file',
            'align' => 'left',
            'type'      => 'action',
            'getter'    => 'getId',
            'filter'    => false,
            'sortable'  => false,
            'is_system' => true,
            'renderer' => 'Ced_Walmart_Block_Adminhtml_Walmartfeed_Renderer_Filedownload',

        ));

        $this->addColumn('feed_errors',
            array(
                'header' => Mage::helper('walmart')->__('Feed Errors'),
                'width' => '30%',
                'type'      => 'action',
                'getter'    => 'getId',
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'action',
                'is_system' => true,
                'renderer' => 'Ced_Walmart_Block_Adminhtml_Walmartfeed_Renderer_Errors',
            ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('feed_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);


        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('walmart')->__('Delete Order'),
            'url'  => $this->getUrl('adminhtml/adminhtml_walmartfeed/massdeletefeed'),
            'confirm'  => Mage::helper('walmart')->__('Are you sure? Deleted order can not be undone.Order will delete from this panel only.Sales->Order will remain same.You can not process shipment/return/refund for these orders in future')
        ));


        return $this;
    }



    public function getGridUrl()
    {
        //return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _statusFilter($collection, $column) {
        $filterroleid = $column->getFilter()->getValue();
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $this->getCollection()->addFieldToFilter('main_table.status', array('eq' => $filterroleid));
        return ;
    }
}
